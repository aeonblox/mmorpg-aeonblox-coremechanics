package com.aeonblox.rpg.events;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import ca.wacos.nametagedit.NametagAPI;

import com.aeonblox.rpg.Aeonblox;
import com.haribo98.nametag.NameTagCommand;
import com.mewin.WGRegionEvents.events.RegionEnterEvent;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.StateFlag;

public class Respawn
  implements Listener
{
	/*public boolean isSameEntity(Entity e, Class<? extends Entity> o){
		if (e instanceof o){
			return true;
		}
		return false;
	}*/
  Aeonblox plugin;
  
  public Respawn(Aeonblox plugin) {
  this.plugin = plugin;
  }
  
  public final HashMap<String, ItemStack[]> inv = new HashMap<String, ItemStack[]>();
  List<String> neutral = new ArrayList<String>();
  List<String> chaotic = new ArrayList<String>();
  
  @EventHandler
  public void onChaoticSpawn(PlayerRespawnEvent e)
  {
    final Player p = e.getPlayer();
    if (this.chaotic.contains(p.getName()))
    {
      p.sendMessage(ChatColor.RED + "You " + ChatColor.UNDERLINE + "cannot" + ChatColor.RED + " enter " + ChatColor.BOLD + "NON-PVP" + ChatColor.RED + " zones with a chaotic alignment.");
      new BukkitRunnable()
      {
        public void run()
        {
          Bukkit.getServer().dispatchCommand(
            Bukkit.getConsoleSender(), 
            "warp chaospawn " + p.getName());
        }
      }.runTaskLater(this.plugin, 1L);
    }
  }
  
  @EventHandler
  public void onEnter(RegionEnterEvent e)
  {
	if ((((StateFlag.State)e.getRegion().getFlag(DefaultFlag.PVP)) != null) && (((StateFlag.State)e.getRegion().getFlag(DefaultFlag.MOB_DAMAGE)) != null)) {
    if ((((StateFlag.State)e.getRegion().getFlag(DefaultFlag.PVP)).equals(StateFlag.State.DENY)) && 
    (this.chaotic.contains(e.getPlayer().getName()))) {
      e.setCancelled(true);
      e.getPlayer().sendMessage(ChatColor.RED + "You " + ChatColor.UNDERLINE + "cannot" + ChatColor.RED + " enter " + ChatColor.BOLD + "NON-PVP" + ChatColor.RED + " zones with a chaotic alignment.");	
    }
  }
  }
  
  
  
  @EventHandler
  public void onChaoticEnter(PlayerMoveEvent e)
  {
    Player p = e.getPlayer();
    if (this.chaotic.contains(p.getName()))
    {
     int fromX = (int)e.getFrom().getX();
     int fromY = (int)e.getFrom().getY();
     int fromZ = (int)e.getFrom().getZ();
     int toX = (int)e.getTo().getX();
     int toY = (int)e.getTo().getY();
     int toZ = (int)e.getTo().getZ();
     if ((fromX != toX) || (fromZ != toZ) || (fromY != toY)) {
     if ((plugin.wg != null) && (plugin.wg.getRegionManager(p.getWorld()) != null) && (plugin.wg.getRegionManager(p.getWorld()).getApplicableRegions(p.getLocation()) != null))
     {
     ApplicableRegionSet set = this.plugin.wg.getRegionManager(
     e.getTo().getWorld()).getApplicableRegions(e.getTo());
     if (!set.allows(DefaultFlag.PVP)) {
            
     Vector unitVector = p.getLocation().toVector().subtract(e.getTo().toVector()).normalize();
     p.setVelocity(unitVector.multiply(0.5D).setY(0));
          }
        }
      }
    }
  }
  
  @EventHandler
  public void onLawfulEnter(RegionEnterEvent e)
  {
    Player p = e.getPlayer();
    if ((e.getRegion().getFlag(DefaultFlag.PVP) == StateFlag.State.DENY) && (e.getRegion().getFlag(DefaultFlag.MOB_DAMAGE) == StateFlag.State.DENY)) {
    p.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + "          *** SAFE ZONE (DMG-OFF MOBS-OFF) ***");
    p.playSound(p.getLocation(), Sound.WITHER_SHOOT, 1.0F, -0.0F); 
    }
  }
  
  @EventHandler
  public void onNeutralEnter(RegionEnterEvent e)
  {
    Player p = e.getPlayer();
    if ((e.getRegion().getFlag(DefaultFlag.PVP) == StateFlag.State.DENY) && (e.getRegion().getFlag(DefaultFlag.MOB_DAMAGE) == StateFlag.State.ALLOW)) {
    p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + "          *** WILDERNESS (PVP-OFF MOBS-ON) ***");
    p.playSound(p.getLocation(), Sound.WITHER_SHOOT, 1.0F, -0.0F); 
    }
  }
  
  @EventHandler
  public void onChaoticEnter(RegionEnterEvent e)
  {
    Player p = e.getPlayer();
    if ((e.getRegion().getFlag(DefaultFlag.PVP) == StateFlag.State.ALLOW)) {
    p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "          *** CHAOTIC ZONE (PVP-ON MOBS-ON) ***");
    p.playSound(p.getLocation(), Sound.WITHER_SHOOT, 1.0F, -0.0F); 
    }
  }
  
  /**@EventHandler
  public void onLeaveMessage(RegionLeaveEvent e)
  {
  Player p = e.getPlayer();
  if (e.getRegion().getFlag(DefaultFlag.PVP) == StateFlag.State.DENY) {
  p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "          *** CHAOTIC ZONE (PVP-ON MOBS-ON) ***");
    }
  }*/
  
  @EventHandler
  public void onJoin(PlayerJoinEvent e)
  {
    Player p = e.getPlayer();
    if (this.chaotic.contains(p.getName())) {
      NametagAPI.updateNametagHard(p.getName(), ChatColor.RED.toString(), null);
    } else if ((this.neutral.contains(p.getName())) && (!this.chaotic.contains(p.getName()))) {
      NametagAPI.updateNametagHard(p.getName(), ChatColor.YELLOW.toString(), null);
    } else {
      NametagAPI.updateNametagHard(p.getName(), null, null);
    }
  }
  
  @EventHandler
  public void onNeutral(EntityDamageByEntityEvent e)
  {
    if (((e.getDamager() instanceof Player)) && ((e.getEntity() instanceof Player)))
    {
      final Player d = (Player)e.getDamager();
      if (e.getDamage() <= 0.0D) {
        return;
      }
      if ((this.neutral.contains(d.getName())) && (!this.chaotic.contains(d.getName())))
      {
        this.neutral.add(d.getName());
        new BukkitRunnable()
        {
          public void run()
          {
            Respawn.this.neutral.remove(d.getName());
            if ((!Respawn.this.neutral.contains(d.getName())) && 
              (!Respawn.this.chaotic.contains(d.getName())) && 
              (d.isOnline()))
            {
              d.sendMessage(ChatColor.GREEN + "          * YOU ARE NOW A " + ChatColor.BOLD + "LAWFUL" + ChatColor.GREEN + " CITIZEN *");
              d.sendMessage(ChatColor.GRAY + "While lawful, you will not lose any equipped armor on death.");
              d.sendMessage(ChatColor.GRAY + "Any players who kill you while you're lawfully aligned will");
              d.sendMessage(ChatColor.GRAY + "become chaotic.");
              d.sendMessage(ChatColor.GREEN + "          * YOU ARE NOW" + ChatColor.BOLD + " LAWFUL" + ChatColor.GREEN + " CITZEN *");
              NametagAPI.updateNametagHard(d.getName(), null, 
                null);
            }
          }
        }.runTaskLater(this.plugin, 1200L);
      }
      else if ((!this.neutral.contains(d.getName())) && 
        (!this.chaotic.contains(d.getName())))
      {
        d.sendMessage(ChatColor.YELLOW + "          * YOU ARE NOW A " + ChatColor.BOLD + "NEUTRAL" + ChatColor.YELLOW + " CITIZEN *");
        d.sendMessage(ChatColor.GRAY + "While neutral, players who kill you will not become chaotic. You");
        d.sendMessage(ChatColor.GRAY + "have a 50% chance of dropping your weapon, and a 25%");
        d.sendMessage(ChatColor.GRAY + "chance of dropping each piece of equiped armor on death.");
        d.sendMessage(ChatColor.GRAY + "Neutral alignment will expire 1 minute after last hit on player.");
        d.sendMessage(ChatColor.YELLOW + "          * YOU ARE NOW A" + ChatColor.BOLD + " NEUTRAL" + ChatColor.YELLOW + " CITIZEN *");
        this.neutral.add(d.getName());
        NametagAPI.updateNametagHard(d.getName(), 
        ChatColor.YELLOW.toString(), null);
        new BukkitRunnable()
        {
          public void run()
          {
            Respawn.this.neutral.remove(d.getName());
            if ((!Respawn.this.neutral.contains(d.getName())) && 
              (!Respawn.this.chaotic.contains(d.getName())) && 
              (d.isOnline()))
            {
              d.sendMessage(ChatColor.GREEN + "          * YOU ARE NOW A " + ChatColor.BOLD + "LAWFUL" + ChatColor.GREEN + " CITIZEN *");
              d.sendMessage(ChatColor.GRAY + "While lawful, you will not lose any equipped armor on death.");
              d.sendMessage(ChatColor.GRAY + "Any players who kill you while you're lawfully aligned will");
              d.sendMessage(ChatColor.GRAY + "become chaotic.");
              d.sendMessage(ChatColor.GREEN + "          * YOU ARE NOW A" + ChatColor.BOLD + " LAWFUL" + ChatColor.GREEN + " CITIZEN *");
              NametagAPI.updateNametagHard(d.getName(), null, null);
            }
          }
        }.runTaskLater(this.plugin, 1200L);
      }
    }
  }
  
  @EventHandler(priority=EventPriority.HIGH)
  public void onChaotic(EntityDamageByEntityEvent e)
  {
    if ((e.getEntity() instanceof Player))
    {
      Player p = (Player)e.getEntity();
      if ((e.getDamager() instanceof Player))
      {
        final Player d = (Player)e.getDamager();
        Damageable hp = (Player) p;
        if ((e.getDamage() >= hp.getHealth()) && 
          (p.getNoDamageTicks() <= p.getMaximumNoDamageTicks() / 2) && 
          (!this.neutral.contains(p.getName())) && 
          (!this.chaotic.contains(p.getName()))) {
          if (this.chaotic.contains(d.getName()))
          {
            this.chaotic.add(d.getName());
            d.sendMessage(ChatColor.GREEN + "LAWFUL(Innocent) player slain," + ChatColor.RED + "" + ChatColor.BOLD + " +300s" + ChatColor.RED + " added to Chaotic timer");
            new BukkitRunnable()
            {
              public void run()
              {
                Respawn.this.chaotic.remove(d.getName());
                Respawn.this.neutral.add(d.getName());
                if ((!Respawn.this.chaotic.contains(d.getName())) && 
                  (d.isOnline()))
                {
                  d.sendMessage(ChatColor.YELLOW + "          * YOU ARE NOW A " + ChatColor.BOLD + "NEUTRAL" + ChatColor.YELLOW + " CITIZEN *");
                  d.sendMessage(ChatColor.GRAY + "While neutral, players who kill you will not become chaotic. You");
                  d.sendMessage(ChatColor.GRAY + "have a 50% chance of dropping your weapon, and a 25%");
                  d.sendMessage(ChatColor.GRAY + "chance of dropping each piece of equiped armor on death.");
                  d.sendMessage(ChatColor.GRAY + "Neutral alignment will expire 1 minute after last hit on player.");
                  d.sendMessage(ChatColor.YELLOW + "          * YOU ARE NOW A" + ChatColor.BOLD + " NEUTRAL" + ChatColor.YELLOW + " CITIZEN *");
                  NametagAPI.updateNametagHard(d.getName(), 
                    ChatColor.YELLOW.toString(), null);
                }
              }
            }.runTaskLater(this.plugin, 6000L);
            new BukkitRunnable()
            {
              public void run()
              {
                Respawn.this.neutral.remove(d.getName());
                if ((!Respawn.this.chaotic.contains(d.getName())) && 
                  (!Respawn.this.neutral.contains(d.getName())) && 
                  (d.isOnline()))
                {
                    d.sendMessage(ChatColor.GREEN + "          * YOU ARE NOW A " + ChatColor.BOLD + "LAWFUL" + ChatColor.GREEN + " CITIZEN *");
                    d.sendMessage(ChatColor.GRAY + "While lawful, you will not lose any equipped armor on death.");
                    d.sendMessage(ChatColor.GRAY + "Any players who kill you while you're lawfully aligned will");
                    d.sendMessage(ChatColor.GRAY + "become chaotic.");
                    d.sendMessage(ChatColor.GREEN + "          * YOU ARE NOW A" + ChatColor.BOLD + " LAWFUL" + ChatColor.GREEN + " CITIZEN *");
                  NametagAPI.updateNametagHard(d.getName(), null, null);
                }
              }
            }.runTaskLater(this.plugin, 8000L);
          }
          else if (!this.chaotic.contains(d.getName()))
          {
            NametagAPI.updateNametagHard(d.getName(), 
              ChatColor.RED.toString(), null);
            d.sendMessage(ChatColor.RED + "          * YOU ARE NOW A" + ChatColor.BOLD + " BANDIT" + ChatColor.RED + " *");
            d.sendMessage(ChatColor.GRAY + "If you are killed whilst a bandit, you will lose enerything");
            d.sendMessage(ChatColor.GRAY + "in your inventory. Chaotic alignment will expire 5 minutes after");
            d.sendMessage(ChatColor.GRAY + "your last player kill.");
            d.sendMessage(ChatColor.RED + "          * YOU ARE NOW A" + ChatColor.BOLD + " BANDIT" + ChatColor.RED + " *");
            d.sendMessage(ChatColor.GREEN + "LAWFUL(Innocent) player slain," + ChatColor.RED + "" + ChatColor.BOLD + " +300s" + ChatColor.RED + " added to Chaotic timer");
            this.chaotic.add(d.getName());
            new BukkitRunnable()
            {
              public void run()
              {
                Respawn.this.chaotic.remove(d.getName());
                Respawn.this.neutral.add(d.getName());
                if ((!Respawn.this.chaotic.contains(d.getName())) && 
                  (d.isOnline()))
                {
                    d.sendMessage(ChatColor.YELLOW + "          * YOU ARE NOW A " + ChatColor.BOLD + "NEUTRAL" + ChatColor.YELLOW + " CITIZEN *");
                    d.sendMessage(ChatColor.GRAY + "While neutral, players who kill you will not become chaotic. You");
                    d.sendMessage(ChatColor.GRAY + "have a 50% chance of dropping your weapon, and a 25%");
                    d.sendMessage(ChatColor.GRAY + "chance of dropping each piece of equiped armor on death.");
                    d.sendMessage(ChatColor.GRAY + "Neutral alignment will expire 1 minute after last hit on player.");
                    d.sendMessage(ChatColor.YELLOW + "          * YOU ARE NOW A" + ChatColor.BOLD + " NEUTRAL" + ChatColor.YELLOW + " CITIZEN *");
                  NametagAPI.updateNametagHard(d.getName(), 
                    ChatColor.YELLOW.toString(), null);
                }
              }
            }.runTaskLater(this.plugin, 6000L);
            new BukkitRunnable()
            {
              public void run()
              {
                Respawn.this.neutral.remove(d.getName());
                if ((!Respawn.this.chaotic.contains(d.getName())) && 
                  (!Respawn.this.neutral.contains(d.getName())) && 
                  (d.isOnline()))
                {
                    d.sendMessage(ChatColor.GREEN + "          * YOU ARE NOW A " + ChatColor.BOLD + "LAWFUL" + ChatColor.GREEN + " CITIZEN *");
                    d.sendMessage(ChatColor.GRAY + "While lawful, you will not lose any equipped armor on death.");
                    d.sendMessage(ChatColor.GRAY + "Any players who kill you while you're lawfully aligned will");
                    d.sendMessage(ChatColor.GRAY + "become chaotic.");
                    d.sendMessage(ChatColor.GREEN + "          * YOU ARE NOW A" + ChatColor.BOLD + " LAWFUL" + ChatColor.GREEN + " CITIZEN *");
                  NametagAPI.updateNametagHard(d.getName(), null, null);
                }
              }
            }.runTaskLater(this.plugin, 8000L);
          }
        }
      }
    }
  }
  
  @EventHandler(priority=EventPriority.LOWEST)
  public void onDeath(PlayerDeathEvent e)
  {
    Player p = e.getEntity();
    Random random = new Random();
    
    int wepdrop = random.nextInt(2) + 1;
    int armor = random.nextInt(4) + 1;
    int piece = random.nextInt(4) + 1;
    
    int min = random.nextInt(2) + 2;
    int max = random.nextInt(2) + 4;
    List<ItemStack> newInventory = new ArrayList<ItemStack>();
    if ((!this.neutral.contains(p.getName())) && (!this.chaotic.contains(p.getName())))
    {
      if (p.getInventory().getBoots() != null) {
        newInventory.add(p.getInventory().getBoots());
      }
      if (p.getInventory().getLeggings() != null) {
        newInventory.add(p.getInventory().getLeggings());
      }
      if (p.getInventory().getChestplate() != null) {
        newInventory.add(p.getInventory().getChestplate());
      }
      if (p.getInventory().getHelmet() != null) {
        newInventory.add(p.getInventory().getHelmet());
      }
      if (p.getInventory().getItem(0) != null)
      {
        newInventory.add(p.getInventory().getItem(0));
      }
      else
      {
        int wep = random.nextInt(2) + 1;
        if (wep == 1)
        {
          ItemStack S = new ItemStack(Material.WOOD_SWORD);
          ItemMeta smeta = S.getItemMeta();
          smeta.setDisplayName(ChatColor.WHITE + "Training Sword");
          List<String> slore = new ArrayList<String>();
          slore.add(ChatColor.RED + "DMG: " + min + " - " + max);
          slore.add(ChatColor.GRAY + "Untradeable");
          smeta.setLore(slore);
          S.setItemMeta(smeta);
          newInventory.add(S);
        }
        if (wep == 2)
        {
          ItemStack S = new ItemStack(Material.WOOD_AXE);
          ItemMeta smeta = S.getItemMeta();
          smeta.setDisplayName(ChatColor.WHITE + "Training Hatchet");
          List<String> slore = new ArrayList<String>();
          slore.add(ChatColor.RED + "DMG: " + min + " - " + max);
          slore.add(ChatColor.GRAY + "Untradeable");
          smeta.setLore(slore);
          S.setItemMeta(smeta);
          newInventory.add(S);
        }
      }
      if (p.getInventory().contains(Material.GOLD_PICKAXE)) {
        for (int i = 1; i < p.getInventory().getSize(); i++) {
          if ((p.getInventory().getItem(i) != null) && 
            (p.getInventory().getItem(i).getType() == Material.GOLD_PICKAXE)) {
            newInventory.add(p.getInventory().getItem(i));
          }
        }
      }
    }
    else if ((this.neutral.contains(p.getName())) && 
      (!this.chaotic.contains(p.getName())))
    {
      if (armor == 1)
      {
        if ((piece != 1) && 
          (p.getInventory().getBoots() != null)) {
          newInventory.add(p.getInventory().getBoots());
        }
        if ((piece != 2) && 
          (p.getInventory().getLeggings() != null)) {
          newInventory.add(p.getInventory().getLeggings());
        }
        if ((piece != 3) && 
          (p.getInventory().getChestplate() != null)) {
          newInventory.add(p.getInventory().getChestplate());
        }
        if ((piece != 4) && 
          (p.getInventory().getHelmet() != null)) {
          newInventory.add(p.getInventory().getHelmet());
        }
      }
      else
      {
        if (p.getInventory().getBoots() != null) {
          newInventory.add(p.getInventory().getBoots());
        }
        if (p.getInventory().getLeggings() != null) {
          newInventory.add(p.getInventory().getLeggings());
        }
        if (p.getInventory().getChestplate() != null) {
          newInventory.add(p.getInventory().getChestplate());
        }
        if (p.getInventory().getHelmet() != null) {
          newInventory.add(p.getInventory().getHelmet());
        }
      }
      int wep = random.nextInt(2) + 1;
      if (wepdrop == 1)
      {
        if (p.getInventory().getItem(0) != null)
        {
          newInventory.add(p.getInventory().getItem(0));
        }
        else
        {
          if (wep == 1)
          {
            ItemStack S = new ItemStack(Material.WOOD_SWORD);
            ItemMeta smeta = S.getItemMeta();
            smeta.setDisplayName(ChatColor.WHITE + "Training Sword");
            List<String> slore = new ArrayList<String>();
            slore.add(ChatColor.RED + "DMG: " + min + " - " + max);
            slore.add(ChatColor.GRAY + "Untradeable");
            smeta.setLore(slore);
            S.setItemMeta(smeta);
            newInventory.add(S);
            
            ItemStack B = new ItemStack(Material.BREAD, 4);
            ItemMeta bmeta = B.getItemMeta();
            bmeta.setDisplayName(ChatColor.WHITE + "Baked Bread");
            List<String> blore = new ArrayList<String>();
            blore.add(ChatColor.GRAY + "Bread specially baked by Datdenkikniet");
            blore.add(ChatColor.GRAY + "Untradeable");
            bmeta.setLore(blore);
            B.setItemMeta(bmeta);
            newInventory.add(B);
          }
          if (wep == 2)
          {
            ItemStack S = new ItemStack(Material.WOOD_AXE);
            ItemMeta smeta = S.getItemMeta();
            smeta.setDisplayName(ChatColor.WHITE + 
              "Training Hatchet");
            List<String> slore = new ArrayList<String>();
            slore.add(ChatColor.RED + "DMG: " + min + " - " + max);
            slore.add(ChatColor.GRAY + "Untradeable");
            smeta.setLore(slore);
            S.setItemMeta(smeta);
            newInventory.add(S);
            
            ItemStack B = new ItemStack(Material.BREAD, 5);
            ItemMeta bmeta = B.getItemMeta();
            bmeta.setDisplayName(ChatColor.WHITE + "Baked Bread");
            List<String> blore = new ArrayList<String>();
            blore.add(ChatColor.GRAY + "Bread specially baked by KingPsychopath");
            blore.add(ChatColor.GRAY + "Untradeable");
            bmeta.setLore(blore);
            B.setItemMeta(bmeta);
            newInventory.add(B);
          }
        }
      }
      else
      {
        if (wep == 1)
        {
          ItemStack S = new ItemStack(Material.WOOD_SWORD);
          ItemMeta smeta = S.getItemMeta();
          smeta.setDisplayName(ChatColor.WHITE + "Training Sword");
          List<String> slore = new ArrayList<String>();
          slore.add(ChatColor.RED + "DMG: " + min + " - " + max);
          slore.add(ChatColor.GRAY + "Untradeable");
          smeta.setLore(slore);
          S.setItemMeta(smeta);
          newInventory.add(S);
          
          ItemStack B = new ItemStack(Material.BREAD, 5);
          ItemMeta bmeta = B.getItemMeta();
          bmeta.setDisplayName(ChatColor.WHITE + "Baked Bread");
          List<String> blore = new ArrayList<String>();
          blore.add(ChatColor.GRAY + "Bread specially baked by Jumla");
          blore.add(ChatColor.GRAY + "Untradeable");
          bmeta.setLore(blore);
          B.setItemMeta(bmeta);
          newInventory.add(B);
        }
        if (wep == 2)
        {
          ItemStack S = new ItemStack(Material.WOOD_AXE);
          ItemMeta smeta = S.getItemMeta();
          smeta.setDisplayName(ChatColor.WHITE + "Training Hatchet");
          List<String> slore = new ArrayList<String>();
          slore.add(ChatColor.RED + "DMG: " + min + " - " + max);
          slore.add(ChatColor.GRAY + "Untradeable");
          smeta.setLore(slore);
          S.setItemMeta(smeta);
          newInventory.add(S);
          
          ItemStack B = new ItemStack(Material.BREAD, 5);
          ItemMeta bmeta = B.getItemMeta();
          bmeta.setDisplayName(ChatColor.WHITE + "Baked Bread");
          List<String> blore = new ArrayList<String>();
          blore.add(ChatColor.GRAY + "Bread specially baked by Feralistic");
          blore.add(ChatColor.GRAY + "Untradeable");
          bmeta.setLore(blore);
          B.setItemMeta(bmeta);
          newInventory.add(B);
        }
      }
      if (p.getInventory().contains(Material.GOLD_PICKAXE)) {
        for (int i = 1; i < p.getInventory().getSize(); i++) {
          if ((p.getInventory().getItem(i) != null) && 
            (p.getInventory().getItem(i).getType() == Material.GOLD_PICKAXE)) {
            newInventory.add(p.getInventory().getItem(i));
          }
        }
      }
    }
    else if (this.chaotic.contains(p.getName()))
    {
      int wep = random.nextInt(2) + 1;
      if (wep == 1)
      {
        ItemStack S = new ItemStack(Material.WOOD_SWORD);
        ItemMeta smeta = S.getItemMeta();
        smeta.setDisplayName(ChatColor.WHITE + "Training Sword");
        List<String> slore = new ArrayList<String>();
        slore.add(ChatColor.RED + "DMG: " + min + " - " + max);
        slore.add(ChatColor.GRAY + "Untradeable");
        smeta.setLore(slore);
        S.setItemMeta(smeta);
        newInventory.add(S);
        
        ItemStack B = new ItemStack(Material.BREAD, 5);
        ItemMeta bmeta = B.getItemMeta();
        bmeta.setDisplayName(ChatColor.WHITE + "Baked Bread");
        List<String> blore = new ArrayList<String>();
        blore.add(ChatColor.GRAY + "Bread specially baked by KingPsychopath");
        blore.add(ChatColor.GRAY + "Untradeable");
        bmeta.setLore(blore);
        B.setItemMeta(bmeta);
        newInventory.add(B);
      }
      if (wep == 2)
      {
        ItemStack S = new ItemStack(Material.WOOD_AXE);
        ItemMeta smeta = S.getItemMeta();
        smeta.setDisplayName(ChatColor.WHITE + "Training Hatchet");
        List<String> slore = new ArrayList<String>();
        slore.add(ChatColor.RED + "DMG: " + min + " - " + max);
        slore.add(ChatColor.GRAY + "Untradeable");
        smeta.setLore(slore);
        S.setItemMeta(smeta);
        newInventory.add(S);
        
        ItemStack B = new ItemStack(Material.BREAD, 5);
        ItemMeta bmeta = B.getItemMeta();
        bmeta.setDisplayName(ChatColor.WHITE + "Baked Bread");
        List<String> blore = new ArrayList<String>();
        blore.add(ChatColor.GRAY + "Bread specially baked by KingPsychopath");
        blore.add(ChatColor.GRAY + "Untradeable");
        bmeta.setLore(blore);
        B.setItemMeta(bmeta);
        newInventory.add(B);
      }
    }
    ItemStack[] newStack = (ItemStack[])newInventory.toArray(
      new ItemStack[newInventory.size()]);
    this.inv.put(p.getName(), newStack);
    e.getDrops().removeAll(newInventory);
  }
  
  @EventHandler
  public void onRespawn(PlayerRespawnEvent e)
  {
    ItemStack[] newInventory = (ItemStack[])this.inv.get(e.getPlayer().getName());
    if (this.inv.containsKey(e.getPlayer().getName()))
    {
      e.getPlayer().getInventory().addItem(newInventory);
      e.getPlayer().setMaxHealth(50.0D);
      e.getPlayer().setHealth(50.0D);
    }
    this.inv.remove(e.getPlayer());
  }
}