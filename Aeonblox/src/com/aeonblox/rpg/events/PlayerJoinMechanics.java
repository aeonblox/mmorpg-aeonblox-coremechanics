package com.aeonblox.rpg.events;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

import com.aeonblox.rpg.Aeonblox;

public class PlayerJoinMechanics
  implements Listener
{
 /*
  * 
  * @ NO CONSTRUCTOR :))
	Main plugin;
  
  public PlayerJoinMechanics(Main main)
  {
    this.plugin = main;
  }
  */
	Aeonblox plugin;
	
	/**
	 * THis is the default constructor for the PlayerJoinMechanics class.
	 * @param plugin - An instance of the plugin ({@link Aeonblox}).
	 */
	public PlayerJoinMechanics(Aeonblox plugin){	
		this.plugin = plugin;
	}
	
	
  String staff = ChatColor.RED + "A staff member has arrived...";
  String king =  ChatColor.GOLD + "A King has arrived...";
  
  private String getHealth(Player player)
  {
	  Damageable d = (Damageable) player;
    return String.valueOf(d.getHealth());
  }
  
  private String getMaxHealth(Player player)
  {
	  Damageable d = (Damageable) player;
    return String.valueOf(d.getMaxHealth());
  }
  @SuppressWarnings("deprecation")
@EventHandler(priority=EventPriority.MONITOR)
  public void onAutoSync(InventoryClickEvent event)
  {
	  final Player p = (Player) event.getWhoClicked();

      new BukkitRunnable()
      {
        public void run()
        {
         p.updateInventory();
         p.saveData();
        }
      }.runTaskLater(plugin, 1L);
	  
	  
  }
  
  
  @EventHandler(priority=EventPriority.MONITOR)
  public void onPlayerLogin(PlayerJoinEvent event)
  {
	  
      Player player = (event.getPlayer());  
      final Player pz = event.getPlayer();
    if (!event.getPlayer().hasPlayedBefore()) {
        new BukkitRunnable()
        {
          public void run()
          {
            plugin.listeners.hpCheck(pz);
          }
        }.runTaskLater(plugin, 1L);
        player.setHealthScale(20.0D);
        player.setHealthScaled(true);
    event.setJoinMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + event.getPlayer().getName() + "" + ChatColor.GRAY + " has joined the server for the first time!  Welcome them, Embrace them & Support them on their up-coming adventure.");
    player.playSound(player.getLocation(), Sound.WITHER_SHOOT, 1.0F, -0.0F);    
    }else{
        new BukkitRunnable()
        {
          public void run()
          {
            plugin.listeners.hpCheck(pz);
          }
        }.runTaskLater(plugin, 1L);
        player.setHealthScale(20.0D);
        player.setHealthScaled(true);
    	plugin.listeners.Kit(event.getPlayer());
    }
    event.setJoinMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + event.getPlayer().getName() + ChatColor.GRAY + " joined AEONBLOX with " + ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD +  getHealth(player) + ChatColor.GRAY + " / " + ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + getMaxHealth(player) + ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "HP");
    if (event.getPlayer().hasPermission("aeonblox.login")) {
      Player p = (event.getPlayer()); 
        p.playSound(p.getLocation(), Sound.WITHER_SHOOT, 1.0F, -0.0F);      
    } else if (event.getPlayer().hasPermission("aeonblox.stafflogin")) {
        Bukkit.broadcastMessage(this.staff);
    	for (Player p : Bukkit.getOnlinePlayers())
      {
        p.playSound(p.getLocation(), Sound.AMBIENCE_THUNDER, 1.0F, 0.0F);
      }
    } else if (event.getPlayer().hasPermission("aeonblox.kinglogin")) {
        Bukkit.broadcastMessage(this.king);
      for (Player p : Bukkit.getOnlinePlayers())
      {
        p.playSound(p.getLocation(), Sound.ENDERDRAGON_GROWL, 1.0F, 0.0F);
      }
    }
  }
  
  @EventHandler(priority=EventPriority.MONITOR)
  public void onPlayerQuit(PlayerQuitEvent event)
  {
      Player player = (event.getPlayer());
	  event.setQuitMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + event.getPlayer().getName() + ChatColor.GRAY + " left AEONBLOX with " + ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD +  getHealth(player) + ChatColor.GRAY + " / " + ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + getMaxHealth(player) + ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "HP");
  }
}
