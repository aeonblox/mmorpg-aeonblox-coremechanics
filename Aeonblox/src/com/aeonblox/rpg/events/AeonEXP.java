package com.aeonblox.rpg.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import com.aeonblox.rpg.Aeonblox;

/**
 * This class deals with all EXP based activities within the Aeon plugin.
 * 
 * 
 */
public class AeonEXP implements Listener {
	Aeonblox plugin;

	/**
	 * 
	 * Only constructor on the AeonEXP class.
	 * 
	 * @param instance
	 *            - An instance of the plugin.
	 */
	public AeonEXP(Aeonblox instance) {
		this.plugin = instance;
	}

	private String DoubleString(Double d) {
		return String.valueOf(d);
	}

	/**
	 * This is an event listener that will be called when an entity is damaged
	 * by another entity.
	 * 
	 * @param e
	 *            - The event passed to it by Bukkit
	 */
	@EventHandler(priority = EventPriority.LOW)
	public void damageAndResist(EntityDamageByEntityEvent e) {
		if ((e.getDamager() instanceof Player)) {
			Player player = (Player) e.getDamager();
			if ((player.hasPermission("aeonexp.damage")) && (player.getLevel() > 0)) {
				e.setDamage(e.getDamage() * (1.0D + this.plugin.cfgHandler.getCustomConfig(plugin.expConfig).getDouble("extra damage") / 100.0D * player.getLevel()));
			}
		} else if ((e.getEntity() instanceof Player)) {
			Player player = (Player) e.getEntity();
			if (player.hasPermission("aeonexp.resist")) {
				Double resist = Double.valueOf(this.plugin.cfgHandler.getCustomConfig(plugin.expConfig).getDouble("extra resistance"));
				Double maxResist = Double.valueOf(this.plugin.cfgHandler.getCustomConfig(plugin.expConfig).getDouble("max resistance"));
				if (player.getLevel() > 0) {
					if (resist.doubleValue() * player.getLevel() <= maxResist.doubleValue()) {
						Double damage = Double.valueOf(e.getDamage() * (1.0D - resist.doubleValue() / 100.0D * player.getLevel()));
						e.setDamage(damage.doubleValue());
					} else {
						e.setDamage(e.getDamage() * (1.0D - maxResist.doubleValue() / 100.0D));
					}
				}
			}
		}
	}
}
