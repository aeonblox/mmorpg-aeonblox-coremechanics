package com.aeonblox.rpg.events;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.aeonblox.rpg.Aeonblox;

/**
 * This class deals with the disabling use of non tiered armour.
 * @docsince 1.9.0-SNAPSHOT
 *
 */
public class Blocker implements Listener {
	Aeonblox plugin;
	/**
	 * This is the only constructor for the Blocker class.
	 * @param instance - An instance of the plugin (Aeonblox)
	 */
	public Blocker(Aeonblox instance) {
		this.plugin = instance;
	}

	private ArrayList<String> hitter = new ArrayList<String>();
	
	/**
	 * This is an event listener that will be called when an item in the inventory is clicked.
	 * It stops Non-Tiered armour begin equiped
	 * 
	 * @param event
	 *            - The event passed to it by Bukkit
	 */
			@SuppressWarnings("deprecation")
			@EventHandler (priority = EventPriority.HIGHEST)
		    public void onInventoryArmor(InventoryClickEvent event) {
				String p = event.getWhoClicked().getName();
		        Player player = (Player) event.getWhoClicked();
				if(!(event.getInventory().getType() == InventoryType.CHEST || event.getInventory().getType() == InventoryType.CRAFTING)) { event.setCancelled(false); }
			    if(!Aeonblox.invarray.contains(p)) {
					if(player.hasPermission("aeonblox.journalblock")) {
				if(((event.getCurrentItem() != null && event.getCurrentItem().getType() != null) && (isArmor(event.getCurrentItem().getType())))) {
		        ItemStack item = event.getCurrentItem();
		        if(player.hasPermission("aeonblox.armorenchantblock")) {
						item.removeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL);      		

		        }
		    	if (player.hasPermission("aeonblox.armorblock")) {
		        if (item.hasItemMeta()) {
		        if (item.getItemMeta().getLore() != null)
		        {}}else{ 
		        player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "You cannot equip non-tiered armor!");
		        player.getInventory().remove(event.getCurrentItem());
		        player.updateInventory();
		        player.saveData();
		        
		    }
		    	}
		}else{
			
		}
			}
				}
			}
			
		
			/**
			 * This is an event listener that will be called when a player interacts.
			 * This is used to stop the right-clicking and equiping of non tiered armour.
			 * 
			 * @param event
			 *            - The event passed to it by Bukkit
			 */
			  @SuppressWarnings("deprecation")
			@EventHandler(priority=EventPriority.HIGHEST)
			 public void onPlayerInteract(PlayerInteractEvent event) {
			 String p = event.getPlayer().getName();
			 Player player = event.getPlayer();
			 Action action = event.getAction();
			if(!Aeonblox.invarray.contains(p)) {
			if(player.hasPermission("aeonblox.journalblock")) {
			 if (((action.equals(Action.RIGHT_CLICK_BLOCK)) || 
			    (action.equals(Action.RIGHT_CLICK_AIR))) && 
			    (isArmor(player.getItemInHand().getType()))) {
		/**OLD - Item list (event.getCurrentItem().getType() != Material.GOLD_HELMET || event.getCurrentItem().getType() != Material.DIAMOND_HELMET || event.getCurrentItem().getType() != Material.IRON_HELMET || event.getCurrentItem().getType() != Material.CHAINMAIL_HELMET || event.getCurrentItem().getType() != Material.LEATHER_HELMET || event.getCurrentItem().getType() != Material.GOLD_CHESTPLATE || event.getCurrentItem().getType() != Material.DIAMOND_CHESTPLATE || event.getCurrentItem().getType() != Material.IRON_CHESTPLATE || event.getCurrentItem().getType() != Material.CHAINMAIL_CHESTPLATE || event.getCurrentItem().getType() != Material.LEATHER_CHESTPLATE || event.getCurrentItem().getType() != Material.GOLD_LEGGINGS || event.getCurrentItem().getType() != Material.DIAMOND_LEGGINGS || event.getCurrentItem().getType() != Material.IRON_LEGGINGS || event.getCurrentItem().getType() != Material.CHAINMAIL_LEGGINGS || event.getCurrentItem().getType() != Material.LEATHER_LEGGINGS || event.getCurrentItem().getType() != Material.GOLD_BOOTS || event.getCurrentItem().getType() != Material.DIAMOND_BOOTS || event.getCurrentItem().getType() != Material.IRON_BOOTS || event.getCurrentItem().getType() != Material.CHAINMAIL_BOOTS || event.getCurrentItem().getType() != Material.LEATHER_BOOTS) {
		if (nuller.getItemInHand().getType().equals(null)) return; */
			    if (player.hasPermission("aeonblox.armorblock")) {
			    	ItemStack item = event.getItem();
			    	ItemMeta itemMeta = item.getItemMeta();
			    	if (itemMeta.getLore() == null) {
			         player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "You cannot use non-tiered equipment!");
			      event.setCancelled(true);
			      player.getInventory().remove(player.getItemInHand());
			      player.updateInventory();
			      player.saveData();
			    }
			  }
		   }
		}
			}else{
				
			}
			  }
		
			  
	/**
	 * This is an event listener that will be called when
	 * 
	 * @param event
	 *            - The event passed to it by Bukkit
	 */
				@SuppressWarnings("deprecation")
				@EventHandler (priority = EventPriority.HIGHEST)
			    public void onSwordHit(EntityDamageByEntityEvent event) {
					Entity damager = event.getDamager();
			        if (damager instanceof Player) {
			        Player p = (Player) damager;
			        if(isWeapon(p.getItemInHand().getType())) {;
			        Player player = (Player) event.getDamager();
					String name = player.getName();
					if(!Aeonblox.invarray.contains(name)) {
			        if(this.hitter.contains(name) && (player.getFoodLevel() < 3)) {
			    	if (player.hasPermission("aeonblox.swordblock")){
			        event.setDamage(0);
			        p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 120, 3));
			        p.sendMessage(ChatColor.GREEN + "You feel faint, your " + ChatColor.UNDERLINE + "ENERGY" + ChatColor.GREEN + " is dwindled." );
			        p.sendMessage(ChatColor.GRAY + "" + ChatColor.ITALIC + "You should probably slow down, and renegerate a bit.");
					this.hitter.remove(name);
			    	} 
			    	}else{    			
			//if (event.getSlotType().equals(SlotType.ARMOR) && !event.getCurrentItem().getType().equals(Material.AIR))
			//event.setCancelled(true);
			        
			    }
			   }else{
			}
				
			}
			        }else{
			        
			        }
				}
				/**
				 * This is an event listener that will be called when 
				 * 
				 * @param e
				 *            - The event passed to it by Bukkit
				 */
				  @EventHandler
					public void onSwordInteract(PlayerInteractEvent e){
					Player player = e.getPlayer();
					Action action = e.getAction();
					ItemStack item = e.getItem();
					String pl = player.getName();
					if(!Aeonblox.invarray.contains(pl)) {
					if (((action.equals(Action.RIGHT_CLICK_BLOCK)) || (action.equals(Action.RIGHT_CLICK_AIR)) || (action.equals(Action.LEFT_CLICK_AIR)) || (action.equals(Action.LEFT_CLICK_BLOCK))) && 
				    (isWeapon(player.getItemInHand().getType()))){
						 String name = ((player)).getName();
					this.hitter.add(name);
					if(e.getPlayer().getFoodLevel() > 2) {
						 this.hitter.remove(name);
				    int change = e.getPlayer().getFoodLevel() - 2;
				    e.getPlayer().setFoodLevel(change);
				    if(player.hasPermission("aeonblox.swordenchant")) {
				    item.removeEnchantment(Enchantment.DAMAGE_ALL);
				    item.removeEnchantment(Enchantment.DAMAGE_ARTHROPODS);
				    item.removeEnchantment(Enchantment.DAMAGE_UNDEAD);
				    item.removeEnchantment(Enchantment.FIRE_ASPECT);
				    item.removeEnchantment(Enchantment.KNOCKBACK);
				    item.removeEnchantment(Enchantment.LOOT_BONUS_MOBS);
				    }
				    }
					}
					else if(e.getPlayer().getFoodLevel() < 1){
					e.setCancelled(true);
					}
					}
				  }
		  
		  
		  
		  
		  
		  
		  
		
		
		  
		  /**
		   * -----------------------------------------------------------------------------------------------------------------
		   * Methods                                                                                                          |
		   * -----------------------------------------------------------------------------------------------------------------
		   */
		  /**
		  * Armor List
		  */ 
			/**
			 * This method checks whether an item is armour	  
			 * @param material - The material of the item to check
			 * @return whether the item is amour.
			 */
			public boolean isArmor(Material material) {
					    if(material != null){
					    switch(material) {
					    case GOLD_HELMET:
					    case DIAMOND_HELMET:
					    case IRON_HELMET:
					    case CHAINMAIL_HELMET:
					    case LEATHER_HELMET: 
					    case GOLD_CHESTPLATE:
					    case DIAMOND_CHESTPLATE:
					    case IRON_CHESTPLATE:
					    case CHAINMAIL_CHESTPLATE:
					    case LEATHER_CHESTPLATE:
					    case GOLD_LEGGINGS:
					    case DIAMOND_LEGGINGS:
					    case IRON_LEGGINGS:
					    case CHAINMAIL_LEGGINGS:
					    case LEATHER_LEGGINGS:
					    case GOLD_BOOTS:
					    case DIAMOND_BOOTS:
					    case IRON_BOOTS:
					    case CHAINMAIL_BOOTS:
					    case LEATHER_BOOTS:
					    return true;
					    default:
					    return false;
					    }
					    }else{
					      //log that it's null
					    	return false;
					    }
						//return false;
			}
		 
		 /**
		  * This returns whether the given item is a weapon.
		  * @param material - The material of the item to check
		  * @return whether the item is a weapon
		  */
		 public boolean isWeapon(Material material) {
			 switch(material) {
			 case GOLD_AXE:
			 case DIAMOND_AXE:
			 case IRON_AXE:
			 case STONE_AXE:
			 case WOOD_AXE:
			 case GOLD_SWORD:
			 case DIAMOND_SWORD:
			 case IRON_SWORD:
			 case STONE_SWORD:
			 case WOOD_SWORD:
			 case GOLD_SPADE:
			 case DIAMOND_SPADE:
			 case IRON_SPADE:
			 case STONE_SPADE:
			 case WOOD_SPADE:
			 return true;
			 default:
			 return false;
			 }
			 }
		 
		 /*
		 public ItemStack isWeapon2(Material material) {
			 switch(material) {
			 case GOLD_AXE:
			 case DIAMOND_AXE:
			 case IRON_AXE:
			 case STONE_AXE:
			 case WOOD_AXE:
			 case GOLD_SWORD:
			 case DIAMOND_SWORD:
			 case IRON_SWORD:
			 case STONE_SWORD:
			 case WOOD_SWORD:
			 case GOLD_SPADE:
			 case DIAMOND_SPADE:
			 case IRON_SPADE:
			 case STONE_SPADE:
			 case WOOD_SPADE:
			default:
				break;
			 }
			return null;
			 }
			 */
}
