package com.aeonblox.rpg.events;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Effect;
import org.bukkit.FireworkEffect;
import org.bukkit.Material;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;

import com.aeonblox.rpg.Aeonblox;
import com.aeonblox.rpg.utils.ParticleEffect;
/**
 * This class handles everything related to Orbs.
 * @docsince 1.9.0-SNAPSHOT
 */
public class Orbs
  implements Listener
{
  Aeonblox plugin;
  
	/**
	 * This is the default constructor for the Orbs class.
	 * 
	 * @param instance
	 *            - An instance of the plugin {@link com.aeonblox.rpg.Aeonblox}.
	 */
public Orbs(Aeonblox instance)
  {
    plugin = instance;
  }
  
	/**
	 * This is a listener called by Bukkit when a player clicks inside their
	 * inventory.
	 * 
	 * @param e
	 *            - The {@link org.bukkit.event.inventory.InventoryClickEvent}
	 *            passed to it by Bukkit.
	 */
@EventHandler
  public void onInvClick(InventoryClickEvent e)
  {
    Player p = (Player)e.getWhoClicked();
    if (e.getInventory().getHolder() != p) {
      return;
    }
    if ((e.getCursor() != null) && 
      (e.getCursor().getType() == Material.MAGMA_CREAM) && 
      (e.getCursor().getItemMeta().getDisplayName().equals(ChatColor.LIGHT_PURPLE + "Orb of Alteration")) && 
      (e.getCurrentItem() != null))
    {
      if ((e.getCurrentItem().getType() == Material.GOLD_AXE) && 
        (e.getCurrentItem().getItemMeta().getLore() != null))
      {
        e.setCancelled(true);
        List<String> curlore = e.getCurrentItem().getItemMeta()
          .getLore();
        if (e.getCursor().getAmount() > 1) {
          e.getCursor().setAmount(e.getCursor().getAmount() - 1);
        } else if (e.getCursor().getAmount() == 1) {
          e.setCursor(null);
        }
        String plus = "�e";
        String name = "Legendary Axe";
        if (e.getCurrentItem().getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+")) {
          plus = 
          
            e.getCurrentItem().getItemMeta().getDisplayName().substring(0, 7) + plus;
        }
        Random random = new Random();
        int elemamt = random.nextInt(31) + 25;
        int pureamt = random.nextInt(31) + 25;
        int lifeamt = random.nextInt(20) + 1;
        int critamt = random.nextInt(11) + 1;
        int elem = random.nextInt(12) + 1;
        int life = random.nextInt(4) + 1;
        int crit = random.nextInt(4) + 1;
        int pure = random.nextInt(4) + 1;
        List<String> lore = new ArrayList<String>();
        lore.add((String)curlore.get(0));
        if (pure == 1)
        {
          lore.add(ChatColor.RED + "PURE DMG: +" + pureamt);
          name = "Pure " + name;
        }
        if (life == 1)
        {
          lore.add(ChatColor.RED + "LIFE STEAL: " + lifeamt + "%");
          name = "Vampyric " + name;
        }
        if (crit == 1)
        {
          lore.add(ChatColor.RED + "CRITICAL HIT: " + critamt + 
            "%");
          name = "Deadly " + name;
        }
        if (elem == 3)
        {
          lore.add(ChatColor.RED + "ICE DMG: +" + elemamt);
          name = name + " of Ice";
        }
        if (elem == 2)
        {
          lore.add(ChatColor.RED + "POISON DMG: +" + elemamt);
          name = name + " of Poison";
        }
        if (elem == 1)
        {
          lore.add(ChatColor.RED + "FIRE DMG: +" + elemamt);
          name = name + " of Fire";
        }
        lore.add((String)curlore.get(curlore.size() - 1));
        
        ItemStack is = new ItemStack(Material.GOLD_AXE);
        ItemMeta meta = is.getItemMeta();
        meta.setDisplayName(plus + name);
        meta.setLore(lore);
        is.setItemMeta(meta);
        e.setCurrentItem(is);
        if (lore.size() <= curlore.size())
        {
          p.getWorld().playEffect(p.getLocation(), 
            Effect.EXTINGUISH, 0);
          ParticleEffect.LAVA.display(p.getEyeLocation(), 0.0F, 0.0F, 0.0F, 5.0F, 10);
        }
        else
        {
          Firework fw = (Firework)p.getWorld().spawn(p.getLocation(), 
            Firework.class);
          FireworkMeta data = fw.getFireworkMeta();
          data.addEffects(new FireworkEffect[] {FireworkEffect.builder()
            .withColor(Color.YELLOW).with(FireworkEffect.Type.BALL).build() });
          data.setPower(0);
          fw.setFireworkMeta(data);
        }
      }
      else if ((e.getCurrentItem().getType() == Material.GOLD_HELMET) && 
        (e.getCurrentItem().getItemMeta().getLore() != null))
      {
        e.setCancelled(true);
        Random random = new Random();
        int wepdrop = random.nextInt(2) + 1;
        int regen = random.nextInt(21) + 90;
        int vit = random.nextInt(161) + 150;
        String plus = "";
        String fulname = e.getCurrentItem().getItemMeta()
          .getDisplayName();
        if (fulname.startsWith(ChatColor.RED + "[+")) {
          plus = fulname.substring(0, 7);
        }
        List<String> curlore = e.getCurrentItem().getItemMeta()
          .getLore();
        if (e.getCursor().getAmount() > 1) {
          e.getCursor().setAmount(e.getCursor().getAmount() - 1);
        } else if (e.getCursor().getAmount() == 1) {
          e.setCursor(null);
        }
        if (wepdrop == 1)
        {
          ItemStack H = new ItemStack(Material.GOLD_HELMET);
          ItemMeta helmmeta = H.getItemMeta();
          helmmeta.setDisplayName(plus + ChatColor.YELLOW + 
            "Mending Legendary Full Helmet");
          List<String> hlore = new ArrayList<String>();
          hlore.add((String)curlore.get(0));
          hlore.add((String)curlore.get(1));
          hlore.add(ChatColor.RED + "HP REGEN: +" + regen + 
            " HP/s");
          hlore.add((String)curlore.get(curlore.size() - 1));
          helmmeta.setLore(hlore);
          H.setItemMeta(helmmeta);
          e.setCurrentItem(H);
        }
        if (wepdrop == 2)
        {
          ItemStack H = new ItemStack(Material.GOLD_HELMET);
          ItemMeta helmmeta = H.getItemMeta();
          helmmeta.setDisplayName(plus + ChatColor.YELLOW + 
            "Legendary Full Helmet of Fortitude");
          List<String> hlore = new ArrayList<String>();
          hlore.add((String)curlore.get(0));
          hlore.add((String)curlore.get(1));
          hlore.add(ChatColor.RED + "VIT: +" + vit);
          hlore.add((String)curlore.get(curlore.size() - 1));
          helmmeta.setLore(hlore);
          H.setItemMeta(helmmeta);
          e.setCurrentItem(H);
        }
        if (wepdrop == 1)
        {
          p.getWorld().playEffect(p.getLocation(), 
            Effect.EXTINGUISH, 0);
          ParticleEffect.LAVA.display(p.getEyeLocation(), 0.0F, 0.0F, 0.0F, 5.0F, 10);
        }
        else
        {
          Firework fw = (Firework)p.getWorld().spawn(p.getLocation(), 
            Firework.class);
          FireworkMeta data = fw.getFireworkMeta();
          data.addEffects(new FireworkEffect[] {FireworkEffect.builder()
            .withColor(Color.YELLOW).with(FireworkEffect.Type.BALL).build() });
          data.setPower(0);
          fw.setFireworkMeta(data);
        }
      }
      else if ((e.getCurrentItem().getType() == Material.GOLD_CHESTPLATE) && 
        (e.getCurrentItem().getItemMeta().getLore() != null))
      {
        e.setCancelled(true);
        Random random = new Random();
        int wepdrop = random.nextInt(2) + 1;
        int regen = random.nextInt(21) + 90;
        int vit = random.nextInt(161) + 150;
        String plus = "";
        String fulname = e.getCurrentItem().getItemMeta()
          .getDisplayName();
        if (fulname.startsWith(ChatColor.RED + "[+")) {
          plus = fulname.substring(0, 7);
        }
        List<String> curlore = e.getCurrentItem().getItemMeta()
          .getLore();
        if (e.getCursor().getAmount() > 1) {
          e.getCursor().setAmount(e.getCursor().getAmount() - 1);
        } else if (e.getCursor().getAmount() == 1) {
          e.setCursor(null);
        }
        if (wepdrop == 1)
        {
          ItemStack H = new ItemStack(Material.GOLD_CHESTPLATE);
          ItemMeta helmmeta = H.getItemMeta();
          helmmeta.setDisplayName(plus + ChatColor.YELLOW + 
            "Mending Legendary Platemail");
          List<String> hlore = new ArrayList<String>();
          hlore.add((String)curlore.get(0));
          hlore.add((String)curlore.get(1));
          hlore.add(ChatColor.RED + "HP REGEN: +" + regen + 
            " HP/s");
          hlore.add((String)curlore.get(curlore.size() - 1));
          helmmeta.setLore(hlore);
          H.setItemMeta(helmmeta);
          e.setCurrentItem(H);
        }
        if (wepdrop == 2)
        {
          ItemStack H = new ItemStack(Material.GOLD_CHESTPLATE);
          ItemMeta helmmeta = H.getItemMeta();
          helmmeta.setDisplayName(plus + ChatColor.YELLOW + 
            "Legendary Platemail of Fortitude");
          List<String> hlore = new ArrayList<String>();
          hlore.add((String)curlore.get(0));
          hlore.add((String)curlore.get(1));
          hlore.add(ChatColor.RED + "VIT: +" + vit);
          hlore.add((String)curlore.get(curlore.size() - 1));
          helmmeta.setLore(hlore);
          H.setItemMeta(helmmeta);
          e.setCurrentItem(H);
        }
        if (wepdrop == 1)
        {
          p.getWorld().playEffect(p.getLocation(), 
            Effect.EXTINGUISH, 0);
          ParticleEffect.LAVA.display(p.getEyeLocation(), 0.0F, 0.0F, 0.0F, 5.0F, 10);
        }
        else
        {
          Firework fw = (Firework)p.getWorld().spawn(p.getLocation(), 
            Firework.class);
          FireworkMeta data = fw.getFireworkMeta();
          data.addEffects(new FireworkEffect[] {FireworkEffect.builder()
            .withColor(Color.YELLOW).with(FireworkEffect.Type.BALL).build() });
          data.setPower(0);
          fw.setFireworkMeta(data);
        }
      }
      else if ((e.getCurrentItem().getType() == Material.GOLD_LEGGINGS) && 
        (e.getCurrentItem().getItemMeta().getLore() != null))
      {
        e.setCancelled(true);
        Random random = new Random();
        int wepdrop = random.nextInt(2) + 1;
        int regen = random.nextInt(21) + 90;
        int vit = random.nextInt(161) + 150;
        String plus = "";
        String fulname = e.getCurrentItem().getItemMeta()
          .getDisplayName();
        if (fulname.startsWith(ChatColor.RED + "[+")) {
          plus = fulname.substring(0, 7);
        }
        List<String> curlore = e.getCurrentItem().getItemMeta()
          .getLore();
        if (e.getCursor().getAmount() > 1) {
          e.getCursor().setAmount(e.getCursor().getAmount() - 1);
        } else if (e.getCursor().getAmount() == 1) {
          e.setCursor(null);
        }
        if (wepdrop == 1)
        {
          ItemStack H = new ItemStack(Material.GOLD_LEGGINGS);
          ItemMeta helmmeta = H.getItemMeta();
          helmmeta.setDisplayName(plus + ChatColor.YELLOW + 
            "Mending Legendary Platemail Leggings");
          List<String> hlore = new ArrayList<String>();
          hlore.add((String)curlore.get(0));
          hlore.add((String)curlore.get(1));
          hlore.add(ChatColor.RED + "HP REGEN: +" + regen + 
            " HP/s");
          hlore.add((String)curlore.get(curlore.size() - 1));
          helmmeta.setLore(hlore);
          H.setItemMeta(helmmeta);
          e.setCurrentItem(H);
        }
        if (wepdrop == 2)
        {
          ItemStack H = new ItemStack(Material.GOLD_LEGGINGS);
          ItemMeta helmmeta = H.getItemMeta();
          helmmeta.setDisplayName(plus + ChatColor.YELLOW + 
            "Legendary Platemail Leggings of Fortitude");
          List<String> hlore = new ArrayList<String>();
          hlore.add((String)curlore.get(0));
          hlore.add((String)curlore.get(1));
          hlore.add(ChatColor.RED + "VIT: +" + vit);
          hlore.add((String)curlore.get(curlore.size() - 1));
          helmmeta.setLore(hlore);
          H.setItemMeta(helmmeta);
          e.setCurrentItem(H);
        }
        if (wepdrop == 1)
        {
          p.getWorld().playEffect(p.getLocation(), 
            Effect.EXTINGUISH, 0);
          ParticleEffect.LAVA.display(p.getEyeLocation(), 0.0F, 0.0F, 0.0F, 5.0F, 10);
        }
        else
        {
          Firework fw = (Firework)p.getWorld().spawn(p.getLocation(), 
            Firework.class);
          FireworkMeta data = fw.getFireworkMeta();
          data.addEffects(new FireworkEffect[] {FireworkEffect.builder()
            .withColor(Color.YELLOW).with(FireworkEffect.Type.BALL).build() });
          data.setPower(0);
          fw.setFireworkMeta(data);
        }
      }
      else if ((e.getCurrentItem().getType() == Material.GOLD_BOOTS) && 
        (e.getCurrentItem().getItemMeta().getLore() != null))
      {
        e.setCancelled(true);
        Random random = new Random();
        int wepdrop = random.nextInt(2) + 1;
        int regen = random.nextInt(21) + 90;
        int vit = random.nextInt(161) + 150;
        String plus = "";
        String fulname = e.getCurrentItem().getItemMeta()
          .getDisplayName();
        if (fulname.startsWith(ChatColor.RED + "[+")) {
          plus = fulname.substring(0, 7);
        }
        List<String> curlore = e.getCurrentItem().getItemMeta()
          .getLore();
        if (e.getCursor().getAmount() > 1) {
          e.getCursor().setAmount(e.getCursor().getAmount() - 1);
        } else if (e.getCursor().getAmount() == 1) {
          e.setCursor(null);
        }
        if (wepdrop == 1)
        {
          ItemStack H = new ItemStack(Material.GOLD_BOOTS);
          ItemMeta helmmeta = H.getItemMeta();
          helmmeta.setDisplayName(plus + ChatColor.YELLOW + 
            "Mending Legendary Platemail Boots");
          List<String> hlore = new ArrayList<String>();
          hlore.add((String)curlore.get(0));
          hlore.add((String)curlore.get(1));
          hlore.add(ChatColor.RED + "HP REGEN: +" + regen + 
            " HP/s");
          hlore.add((String)curlore.get(curlore.size() - 1));
          helmmeta.setLore(hlore);
          H.setItemMeta(helmmeta);
          e.setCurrentItem(H);
        }
        if (wepdrop == 2)
        {
          ItemStack H = new ItemStack(Material.GOLD_BOOTS);
          ItemMeta helmmeta = H.getItemMeta();
          helmmeta.setDisplayName(plus + ChatColor.YELLOW + 
            "Legendary Platemail Boots of Fortitude");
          List<String> hlore = new ArrayList<String>();
          hlore.add((String)curlore.get(0));
          hlore.add((String)curlore.get(1));
          hlore.add(ChatColor.RED + "VIT: +" + vit);
          hlore.add((String)curlore.get(curlore.size() - 1));
          helmmeta.setLore(hlore);
          H.setItemMeta(helmmeta);
          e.setCurrentItem(H);
        }
        if (wepdrop == 1)
        {
          p.getWorld().playEffect(p.getLocation(), 
            Effect.EXTINGUISH, 0);
          ParticleEffect.LAVA.display(p.getEyeLocation(), 0.0F, 0.0F, 0.0F, 5.0F, 10);
        }
        else
        {
          Firework fw = (Firework)p.getWorld().spawn(p.getLocation(), 
            Firework.class);
          FireworkMeta data = fw.getFireworkMeta();
          data.addEffects(new FireworkEffect[] {FireworkEffect.builder()
            .withColor(Color.YELLOW).with(FireworkEffect.Type.BALL).build() });
          data.setPower(0);
          fw.setFireworkMeta(data);
        }
      }
      else if ((e.getCurrentItem().getType() == Material.GOLD_SWORD) && 
        (e.getCurrentItem().getItemMeta().getLore() != null))
      {
        e.setCancelled(true);
        List<String> curlore = e.getCurrentItem().getItemMeta()
          .getLore();
        if (e.getCursor().getAmount() > 1) {
          e.getCursor().setAmount(e.getCursor().getAmount() - 1);
        } else if (e.getCursor().getAmount() == 1) {
          e.setCursor(null);
        }
        String plus = "�e";
        String name = "Legendary Sword";
        if (e.getCurrentItem().getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+")) {
          plus = 
          
            e.getCurrentItem().getItemMeta().getDisplayName().substring(0, 7) + plus;
        }
        Random random = new Random();
        int elemamt = random.nextInt(31) + 25;
        int lifeamt = random.nextInt(20) + 1;
        int critamt = random.nextInt(9) + 1;
        int elem = random.nextInt(9) + 1;
        int life = random.nextInt(3) + 1;
        int crit = random.nextInt(3) + 1;
        List<String> lore = new ArrayList<String>();
        lore.add((String)curlore.get(0));
        if (life == 1)
        {
          lore.add(ChatColor.RED + "LIFE STEAL: " + lifeamt + "%");
          name = "Vampyric " + name;
        }
        if (crit == 1)
        {
          lore.add(ChatColor.RED + "CRITICAL HIT: " + critamt + 
            "%");
          name = "Deadly " + name;
        }
        if (elem == 3)
        {
          lore.add(ChatColor.RED + "ICE DMG: +" + elemamt);
          name = name + " of Ice";
        }
        if (elem == 2)
        {
          lore.add(ChatColor.RED + "POISON DMG: +" + elemamt);
          name = name + " of Poison";
        }
        if (elem == 1)
        {
          lore.add(ChatColor.RED + "FIRE DMG: +" + elemamt);
          name = name + " of Fire";
        }
        lore.add((String)curlore.get(curlore.size() - 1));
        ItemStack is = new ItemStack(Material.GOLD_SWORD);
        ItemMeta meta = is.getItemMeta();
        meta.setDisplayName(plus + name);
        meta.setLore(lore);
        is.setItemMeta(meta);
        e.setCurrentItem(is);
        if (lore.size() <= curlore.size())
        {
          p.getWorld().playEffect(p.getLocation(), 
            Effect.EXTINGUISH, 0);
          ParticleEffect.LAVA.display(p.getEyeLocation(), 0.0F, 0.0F, 0.0F, 5.0F, 10);
        }
        else
        {
          Firework fw = (Firework)p.getWorld().spawn(p.getLocation(), 
            Firework.class);
          FireworkMeta data = fw.getFireworkMeta();
          data.addEffects(new FireworkEffect[] {FireworkEffect.builder()
            .withColor(Color.YELLOW).with(FireworkEffect.Type.BALL).build() });
          data.setPower(0);
          fw.setFireworkMeta(data);
        }
      }
      else if ((e.getCurrentItem().getType() == Material.DIAMOND_AXE) && 
        (e.getCurrentItem().getItemMeta().getLore() != null))
      {
        e.setCancelled(true);
        List<String> curlore = e.getCurrentItem().getItemMeta()
          .getLore();
        if (e.getCursor().getAmount() > 1) {
          e.getCursor().setAmount(e.getCursor().getAmount() - 1);
        } else if (e.getCursor().getAmount() == 1) {
          e.setCursor(null);
        }
        String plus = "�d";
        String name = "Ancient Axe";
        if (e.getCurrentItem().getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+")) {
          plus = 
          
            e.getCurrentItem().getItemMeta().getDisplayName().substring(0, 7) + plus;
        }
        Random random = new Random();
        int elemamt = random.nextInt(11) + 10;
        int pureamt = random.nextInt(11) + 10;
        int lifeamt = random.nextInt(20) + 1;
        int critamt = random.nextInt(11) + 1;
        int elem = random.nextInt(12) + 1;
        int life = random.nextInt(4) + 1;
        int crit = random.nextInt(4) + 1;
        int pure = random.nextInt(4) + 1;
        List<String> lore = new ArrayList<String>();
        lore.add((String)curlore.get(0));
        if (pure == 1)
        {
          lore.add(ChatColor.RED + "PURE DMG: +" + pureamt);
          name = "Pure " + name;
        }
        if (life == 1)
        {
          lore.add(ChatColor.RED + "LIFE STEAL: " + lifeamt + "%");
          name = "Vampyric " + name;
        }
        if (crit == 1)
        {
          lore.add(ChatColor.RED + "CRITICAL HIT: " + critamt + 
            "%");
          name = "Deadly " + name;
        }
        if (elem == 3)
        {
          lore.add(ChatColor.RED + "ICE DMG: +" + elemamt);
          name = name + " of Ice";
        }
        if (elem == 2)
        {
          lore.add(ChatColor.RED + "POISON DMG: +" + elemamt);
          name = name + " of Poison";
        }
        if (elem == 1)
        {
          lore.add(ChatColor.RED + "FIRE DMG: +" + elemamt);
          name = name + " of Fire";
        }
        lore.add((String)curlore.get(curlore.size() - 1));
        
        ItemStack is = new ItemStack(Material.DIAMOND_AXE);
        ItemMeta meta = is.getItemMeta();
        meta.setDisplayName(plus + name);
        meta.setLore(lore);
        is.setItemMeta(meta);
        e.setCurrentItem(is);
        if (lore.size() <= curlore.size())
        {
          p.getWorld().playEffect(p.getLocation(), 
            Effect.EXTINGUISH, 0);
          ParticleEffect.LAVA.display(p.getEyeLocation(), 0.0F, 0.0F, 0.0F, 5.0F, 10);
        }
        else
        {
          Firework fw = (Firework)p.getWorld().spawn(p.getLocation(), 
            Firework.class);
          FireworkMeta data = fw.getFireworkMeta();
          data.addEffects(new FireworkEffect[] {FireworkEffect.builder()
            .withColor(Color.YELLOW).with(FireworkEffect.Type.BALL).build() });
          data.setPower(0);
          fw.setFireworkMeta(data);
        }
      }
      else if ((e.getCurrentItem().getType() == Material.DIAMOND_HELMET) && 
        (e.getCurrentItem().getItemMeta().getLore() != null))
      {
        e.setCancelled(true);
        Random random = new Random();
        int wepdrop = random.nextInt(2) + 1;
        int regen = random.nextInt(6) + 60;
        int vit = random.nextInt(91) + 60;
        String plus = "";
        String fulname = e.getCurrentItem().getItemMeta()
          .getDisplayName();
        if (fulname.startsWith(ChatColor.RED + "[+")) {
          plus = fulname.substring(0, 7);
        }
        List<String> curlore = e.getCurrentItem().getItemMeta()
          .getLore();
        if (e.getCursor().getAmount() > 1) {
          e.getCursor().setAmount(e.getCursor().getAmount() - 1);
        } else if (e.getCursor().getAmount() == 1) {
          e.setCursor(null);
        }
        if (wepdrop == 1)
        {
          ItemStack H = new ItemStack(Material.DIAMOND_HELMET);
          ItemMeta helmmeta = H.getItemMeta();
          helmmeta.setDisplayName(plus + ChatColor.LIGHT_PURPLE + 
            "Mending Ancient Full Helmet");
          List<String> hlore = new ArrayList<String>();
          hlore.add((String)curlore.get(0));
          hlore.add((String)curlore.get(1));
          hlore.add(ChatColor.RED + "HP REGEN: +" + regen + 
            " HP/s");
          hlore.add((String)curlore.get(curlore.size() - 1));
          helmmeta.setLore(hlore);
          H.setItemMeta(helmmeta);
          e.setCurrentItem(H);
        }
        if (wepdrop == 2)
        {
          ItemStack H = new ItemStack(Material.DIAMOND_HELMET);
          ItemMeta helmmeta = H.getItemMeta();
          helmmeta.setDisplayName(plus + ChatColor.LIGHT_PURPLE + 
            "Ancient Full Helmet of Fortitude");
          List<String> hlore = new ArrayList<String>();
          hlore.add((String)curlore.get(0));
          hlore.add((String)curlore.get(1));
          hlore.add(ChatColor.RED + "VIT: +" + vit);
          hlore.add((String)curlore.get(curlore.size() - 1));
          helmmeta.setLore(hlore);
          H.setItemMeta(helmmeta);
          e.setCurrentItem(H);
        }
        if (wepdrop == 1)
        {
          p.getWorld().playEffect(p.getLocation(), 
            Effect.EXTINGUISH, 0);
          ParticleEffect.LAVA.display(p.getEyeLocation(), 0.0F, 0.0F, 0.0F, 5.0F, 10);
        }
        else
        {
          Firework fw = (Firework)p.getWorld().spawn(p.getLocation(), 
            Firework.class);
          FireworkMeta data = fw.getFireworkMeta();
          data.addEffects(new FireworkEffect[] {FireworkEffect.builder()
            .withColor(Color.YELLOW).with(FireworkEffect.Type.BALL).build() });
          data.setPower(0);
          fw.setFireworkMeta(data);
        }
      }
      else if ((e.getCurrentItem().getType() == Material.DIAMOND_CHESTPLATE) && 
        (e.getCurrentItem().getItemMeta().getLore() != null))
      {
        e.setCancelled(true);
        Random random = new Random();
        int wepdrop = random.nextInt(2) + 1;
        int regen = random.nextInt(6) + 60;
        int vit = random.nextInt(91) + 60;
        String plus = "";
        String fulname = e.getCurrentItem().getItemMeta()
          .getDisplayName();
        if (fulname.startsWith(ChatColor.RED + "[+")) {
          plus = fulname.substring(0, 7);
        }
        List<String> curlore = e.getCurrentItem().getItemMeta()
          .getLore();
        if (e.getCursor().getAmount() > 1) {
          e.getCursor().setAmount(e.getCursor().getAmount() - 1);
        } else if (e.getCursor().getAmount() == 1) {
          e.setCursor(null);
        }
        if (wepdrop == 1)
        {
          ItemStack H = new ItemStack(Material.DIAMOND_CHESTPLATE);
          ItemMeta helmmeta = H.getItemMeta();
          helmmeta.setDisplayName(plus + ChatColor.LIGHT_PURPLE + 
            "Mending Magic Platemail");
          List<String> hlore = new ArrayList<String>();
          hlore.add((String)curlore.get(0));
          hlore.add((String)curlore.get(1));
          hlore.add(ChatColor.RED + "HP REGEN: +" + regen + 
            " HP/s");
          hlore.add((String)curlore.get(curlore.size() - 1));
          helmmeta.setLore(hlore);
          H.setItemMeta(helmmeta);
          e.setCurrentItem(H);
        }
        if (wepdrop == 2)
        {
          ItemStack H = new ItemStack(Material.DIAMOND_CHESTPLATE);
          ItemMeta helmmeta = H.getItemMeta();
          helmmeta.setDisplayName(plus + ChatColor.LIGHT_PURPLE + 
            "Magic Platemail of Fortitude");
          List<String> hlore = new ArrayList<String>();
          hlore.add((String)curlore.get(0));
          hlore.add((String)curlore.get(1));
          hlore.add(ChatColor.RED + "VIT: +" + vit);
          hlore.add((String)curlore.get(curlore.size() - 1));
          helmmeta.setLore(hlore);
          H.setItemMeta(helmmeta);
          e.setCurrentItem(H);
        }
        if (wepdrop == 1)
        {
          p.getWorld().playEffect(p.getLocation(), 
            Effect.EXTINGUISH, 0);
          ParticleEffect.LAVA.display(p.getEyeLocation(), 0.0F, 0.0F, 0.0F, 5.0F, 10);
        }
        else
        {
          Firework fw = (Firework)p.getWorld().spawn(p.getLocation(), 
            Firework.class);
          FireworkMeta data = fw.getFireworkMeta();
          data.addEffects(new FireworkEffect[] {FireworkEffect.builder()
            .withColor(Color.YELLOW).with(FireworkEffect.Type.BALL).build() });
          data.setPower(0);
          fw.setFireworkMeta(data);
        }
      }
      else if ((e.getCurrentItem().getType() == Material.DIAMOND_LEGGINGS) && 
        (e.getCurrentItem().getItemMeta().getLore() != null))
      {
        e.setCancelled(true);
        Random random = new Random();
        int wepdrop = random.nextInt(2) + 1;
        int regen = random.nextInt(6) + 60;
        int vit = random.nextInt(91) + 60;
        String plus = "";
        String fulname = e.getCurrentItem().getItemMeta()
          .getDisplayName();
        if (fulname.startsWith(ChatColor.RED + "[+")) {
          plus = fulname.substring(0, 7);
        }
        List<String> curlore = e.getCurrentItem().getItemMeta()
          .getLore();
        if (e.getCursor().getAmount() > 1) {
          e.getCursor().setAmount(e.getCursor().getAmount() - 1);
        } else if (e.getCursor().getAmount() == 1) {
          e.setCursor(null);
        }
        if (wepdrop == 1)
        {
          ItemStack H = new ItemStack(Material.DIAMOND_LEGGINGS);
          ItemMeta helmmeta = H.getItemMeta();
          helmmeta.setDisplayName(plus + ChatColor.LIGHT_PURPLE + 
            "Mending Magic Platemail Leggings");
          List<String> hlore = new ArrayList<String>();
          hlore.add((String)curlore.get(0));
          hlore.add((String)curlore.get(1));
          hlore.add(ChatColor.RED + "HP REGEN: +" + regen + 
            " HP/s");
          hlore.add((String)curlore.get(curlore.size() - 1));
          helmmeta.setLore(hlore);
          H.setItemMeta(helmmeta);
          e.setCurrentItem(H);
        }
        if (wepdrop == 2)
        {
          ItemStack H = new ItemStack(Material.DIAMOND_LEGGINGS);
          ItemMeta helmmeta = H.getItemMeta();
          helmmeta.setDisplayName(plus + ChatColor.LIGHT_PURPLE + 
            "Magic Platemail Leggings of Fortitude");
          List<String> hlore = new ArrayList<String>();
          hlore.add((String)curlore.get(0));
          hlore.add((String)curlore.get(1));
          hlore.add(ChatColor.RED + "VIT: +" + vit);
          hlore.add((String)curlore.get(curlore.size() - 1));
          helmmeta.setLore(hlore);
          H.setItemMeta(helmmeta);
          e.setCurrentItem(H);
        }
        if (wepdrop == 1)
        {
          p.getWorld().playEffect(p.getLocation(), 
            Effect.EXTINGUISH, 0);
          ParticleEffect.LAVA.display(p.getEyeLocation(), 0.0F, 0.0F, 0.0F, 5.0F, 10);
        }
        else
        {
          Firework fw = (Firework)p.getWorld().spawn(p.getLocation(), 
            Firework.class);
          FireworkMeta data = fw.getFireworkMeta();
          data.addEffects(new FireworkEffect[] {FireworkEffect.builder()
            .withColor(Color.YELLOW).with(FireworkEffect.Type.BALL).build() });
          data.setPower(0);
          fw.setFireworkMeta(data);
        }
      }
      else if ((e.getCurrentItem().getType() == Material.DIAMOND_BOOTS) && 
        (e.getCurrentItem().getItemMeta().getLore() != null))
      {
        e.setCancelled(true);
        Random random = new Random();
        int wepdrop = random.nextInt(2) + 1;
        int regen = random.nextInt(6) + 60;
        int vit = random.nextInt(91) + 60;
        String plus = "";
        String fulname = e.getCurrentItem().getItemMeta()
          .getDisplayName();
        if (fulname.startsWith(ChatColor.RED + "[+")) {
          plus = fulname.substring(0, 7);
        }
        List<String> curlore = e.getCurrentItem().getItemMeta()
          .getLore();
        if (e.getCursor().getAmount() > 1) {
          e.getCursor().setAmount(e.getCursor().getAmount() - 1);
        } else if (e.getCursor().getAmount() == 1) {
          e.setCursor(null);
        }
        if (wepdrop == 1)
        {
          ItemStack H = new ItemStack(Material.DIAMOND_BOOTS);
          ItemMeta helmmeta = H.getItemMeta();
          helmmeta.setDisplayName(plus + ChatColor.LIGHT_PURPLE + 
            "Mending Magic Platemail Boots");
          List<String> hlore = new ArrayList<String>();
          hlore.add((String)curlore.get(0));
          hlore.add((String)curlore.get(1));
          hlore.add(ChatColor.RED + "HP REGEN: +" + regen + 
            " HP/s");
          hlore.add((String)curlore.get(curlore.size() - 1));
          helmmeta.setLore(hlore);
          H.setItemMeta(helmmeta);
          e.setCurrentItem(H);
        }
        if (wepdrop == 2)
        {
          ItemStack H = new ItemStack(Material.DIAMOND_BOOTS);
          ItemMeta helmmeta = H.getItemMeta();
          helmmeta.setDisplayName(plus + ChatColor.LIGHT_PURPLE + 
            "Magic Platemail Boots of Fortitude");
          List<String> hlore = new ArrayList<String>();
          hlore.add((String)curlore.get(0));
          hlore.add((String)curlore.get(1));
          hlore.add(ChatColor.RED + "VIT: +" + vit);
          hlore.add((String)curlore.get(curlore.size() - 1));
          helmmeta.setLore(hlore);
          H.setItemMeta(helmmeta);
          e.setCurrentItem(H);
        }
        if (wepdrop == 1)
        {
          p.getWorld().playEffect(p.getLocation(), 
            Effect.EXTINGUISH, 0);
          ParticleEffect.LAVA.display(p.getEyeLocation(), 0.0F, 0.0F, 0.0F, 5.0F, 10);
        }
        else
        {
          Firework fw = (Firework)p.getWorld().spawn(p.getLocation(), 
            Firework.class);
          FireworkMeta data = fw.getFireworkMeta();
          data.addEffects(new FireworkEffect[] {FireworkEffect.builder()
            .withColor(Color.YELLOW).with(FireworkEffect.Type.BALL).build() });
          data.setPower(0);
          fw.setFireworkMeta(data);
        }
      }
      else if ((e.getCurrentItem().getType() == Material.DIAMOND_SWORD) && 
        (e.getCurrentItem().getItemMeta().getLore() != null))
      {
        e.setCancelled(true);
        List<String> curlore = e.getCurrentItem().getItemMeta()
          .getLore();
        if (e.getCursor().getAmount() > 1) {
          e.getCursor().setAmount(e.getCursor().getAmount() - 1);
        } else if (e.getCursor().getAmount() == 1) {
          e.setCursor(null);
        }
        String plus = "�d";
        String name = "Ancient Sword";
        if (e.getCurrentItem().getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+")) {
          plus = 
          
            e.getCurrentItem().getItemMeta().getDisplayName().substring(0, 7) + plus;
        }
        Random random = new Random();
        int elemamt = random.nextInt(11) + 10;
        int lifeamt = random.nextInt(20) + 1;
        int critamt = random.nextInt(9) + 1;
        int elem = random.nextInt(9) + 1;
        int life = random.nextInt(3) + 1;
        int crit = random.nextInt(3) + 1;
        List<String> lore = new ArrayList<String>();
        lore.add((String)curlore.get(0));
        if (life == 1)
        {
          lore.add(ChatColor.RED + "LIFE STEAL: " + lifeamt + "%");
          name = "Vampyric " + name;
        }
        if (crit == 1)
        {
          lore.add(ChatColor.RED + "CRITICAL HIT: " + critamt + 
            "%");
          name = "Deadly " + name;
        }
        if (elem == 3)
        {
          lore.add(ChatColor.RED + "ICE DMG: +" + elemamt);
          name = name + " of Ice";
        }
        if (elem == 2)
        {
          lore.add(ChatColor.RED + "POISON DMG: +" + elemamt);
          name = name + " of Poison";
        }
        if (elem == 1)
        {
          lore.add(ChatColor.RED + "FIRE DMG: +" + elemamt);
          name = name + " of Fire";
        }
        lore.add((String)curlore.get(curlore.size() - 1));
        ItemStack is = new ItemStack(Material.DIAMOND_SWORD);
        ItemMeta meta = is.getItemMeta();
        meta.setDisplayName(plus + name);
        meta.setLore(lore);
        is.setItemMeta(meta);
        e.setCurrentItem(is);
        if (lore.size() <= curlore.size())
        {
          p.getWorld().playEffect(p.getLocation(), 
            Effect.EXTINGUISH, 0);
          ParticleEffect.LAVA.display(p.getEyeLocation(), 0.0F, 0.0F, 0.0F, 5.0F, 10);
        }
        else
        {
          Firework fw = (Firework)p.getWorld().spawn(p.getLocation(), 
            Firework.class);
          FireworkMeta data = fw.getFireworkMeta();
          data.addEffects(new FireworkEffect[] {FireworkEffect.builder()
            .withColor(Color.YELLOW).with(FireworkEffect.Type.BALL).build() });
          data.setPower(0);
          fw.setFireworkMeta(data);
        }
      }
      if ((e.getCurrentItem().getType() == Material.IRON_AXE) && 
        (e.getCurrentItem().getItemMeta().getLore() != null))
      {
        e.setCancelled(true);
        List<String> curlore = e.getCurrentItem().getItemMeta()
          .getLore();
        if (e.getCursor().getAmount() > 1) {
          e.getCursor().setAmount(e.getCursor().getAmount() - 1);
        } else if (e.getCursor().getAmount() == 1) {
          e.setCursor(null);
        }
        String plus = "�b";
        String name = "War Axe";
        if (e.getCurrentItem().getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+")) {
          plus = 
          
            e.getCurrentItem().getItemMeta().getDisplayName().substring(0, 7) + plus;
        }
        Random random = new Random();
        int elemamt = random.nextInt(6) + 5;
        int pureamt = random.nextInt(6) + 5;
        int lifeamt = random.nextInt(20) + 1;
        int critamt = random.nextInt(11) + 1;
        int elem = random.nextInt(12) + 1;
        int life = random.nextInt(4) + 1;
        int crit = random.nextInt(4) + 1;
        int pure = random.nextInt(4) + 1;
        List<String> lore = new ArrayList<String>();
        lore.add((String)curlore.get(0));
        if (pure == 1)
        {
          lore.add(ChatColor.RED + "PURE DMG: +" + pureamt);
          name = "Pure " + name;
        }
        if (life == 1)
        {
          lore.add(ChatColor.RED + "LIFE STEAL: " + lifeamt + "%");
          name = "Vampyric " + name;
        }
        if (crit == 1)
        {
          lore.add(ChatColor.RED + "CRITICAL HIT: " + critamt + 
            "%");
          name = "Deadly " + name;
        }
        if (elem == 3)
        {
          lore.add(ChatColor.RED + "ICE DMG: +" + elemamt);
          name = name + " of Ice";
        }
        if (elem == 2)
        {
          lore.add(ChatColor.RED + "POISON DMG: +" + elemamt);
          name = name + " of Poison";
        }
        if (elem == 1)
        {
          lore.add(ChatColor.RED + "FIRE DMG: +" + elemamt);
          name = name + " of Fire";
        }
        lore.add((String)curlore.get(curlore.size() - 1));
        
        ItemStack is = new ItemStack(Material.IRON_AXE);
        ItemMeta meta = is.getItemMeta();
        meta.setDisplayName(plus + name);
        meta.setLore(lore);
        is.setItemMeta(meta);
        e.setCurrentItem(is);
        if (lore.size() <= curlore.size())
        {
          p.getWorld().playEffect(p.getLocation(), 
            Effect.EXTINGUISH, 0);
          ParticleEffect.LAVA.display(p.getEyeLocation(), 0.0F, 0.0F, 0.0F, 5.0F, 10);
        }
        else
        {
          Firework fw = (Firework)p.getWorld().spawn(p.getLocation(), 
            Firework.class);
          FireworkMeta data = fw.getFireworkMeta();
          data.addEffects(new FireworkEffect[] {FireworkEffect.builder()
            .withColor(Color.YELLOW).with(FireworkEffect.Type.BALL).build() });
          data.setPower(0);
          fw.setFireworkMeta(data);
        }
      }
      else if ((e.getCurrentItem().getType() == Material.IRON_HELMET) && 
        (e.getCurrentItem().getItemMeta().getLore() != null))
      {
        e.setCancelled(true);
        Random random = new Random();
        int wepdrop = random.nextInt(2) + 1;
        int regen = random.nextInt(11) + 40;
        int vit = random.nextInt(41) + 20;
        String plus = "";
        String fulname = e.getCurrentItem().getItemMeta()
          .getDisplayName();
        if (fulname.startsWith(ChatColor.RED + "[+")) {
          plus = fulname.substring(0, 7);
        }
        List<String> curlore = e.getCurrentItem().getItemMeta()
          .getLore();
        if (e.getCursor().getAmount() > 1) {
          e.getCursor().setAmount(e.getCursor().getAmount() - 1);
        } else if (e.getCursor().getAmount() == 1) {
          e.setCursor(null);
        }
        if (wepdrop == 1)
        {
          ItemStack H = new ItemStack(Material.IRON_HELMET);
          ItemMeta helmmeta = H.getItemMeta();
          helmmeta.setDisplayName(plus + ChatColor.AQUA + 
            "Mending Full Helmet");
          List<String> hlore = new ArrayList<String>();
          hlore.add((String)curlore.get(0));
          hlore.add((String)curlore.get(1));
          hlore.add(ChatColor.RED + "HP REGEN: +" + regen + 
            " HP/s");
          hlore.add((String)curlore.get(curlore.size() - 1));
          helmmeta.setLore(hlore);
          H.setItemMeta(helmmeta);
          e.setCurrentItem(H);
        }
        if (wepdrop == 2)
        {
          ItemStack H = new ItemStack(Material.IRON_HELMET);
          ItemMeta helmmeta = H.getItemMeta();
          helmmeta.setDisplayName(plus + ChatColor.AQUA + 
            "Full Helmet of Fortitude");
          List<String> hlore = new ArrayList<String>();
          hlore.add((String)curlore.get(0));
          hlore.add((String)curlore.get(1));
          hlore.add(ChatColor.RED + "VIT: +" + vit);
          hlore.add((String)curlore.get(curlore.size() - 1));
          helmmeta.setLore(hlore);
          H.setItemMeta(helmmeta);
          e.setCurrentItem(H);
        }
        if (wepdrop == 1)
        {
          p.getWorld().playEffect(p.getLocation(), 
            Effect.EXTINGUISH, 0);
          ParticleEffect.LAVA.display(p.getEyeLocation(), 0.0F, 0.0F, 0.0F, 5.0F, 10);
        }
        else
        {
          Firework fw = (Firework)p.getWorld().spawn(p.getLocation(), 
            Firework.class);
          FireworkMeta data = fw.getFireworkMeta();
          data.addEffects(new FireworkEffect[] {FireworkEffect.builder()
            .withColor(Color.YELLOW).with(FireworkEffect.Type.BALL).build() });
          data.setPower(0);
          fw.setFireworkMeta(data);
        }
      }
      else if ((e.getCurrentItem().getType() == Material.IRON_CHESTPLATE) && 
        (e.getCurrentItem().getItemMeta().getLore() != null))
      {
        e.setCancelled(true);
        Random random = new Random();
        int wepdrop = random.nextInt(2) + 1;
        int regen = random.nextInt(11) + 40;
        int vit = random.nextInt(41) + 20;
        String plus = "";
        String fulname = e.getCurrentItem().getItemMeta()
          .getDisplayName();
        if (fulname.startsWith(ChatColor.RED + "[+")) {
          plus = fulname.substring(0, 7);
        }
        List<String> curlore = e.getCurrentItem().getItemMeta()
          .getLore();
        if (e.getCursor().getAmount() > 1) {
          e.getCursor().setAmount(e.getCursor().getAmount() - 1);
        } else if (e.getCursor().getAmount() == 1) {
          e.setCursor(null);
        }
        if (wepdrop == 1)
        {
          ItemStack H = new ItemStack(Material.IRON_CHESTPLATE);
          ItemMeta helmmeta = H.getItemMeta();
          helmmeta.setDisplayName(plus + ChatColor.AQUA + 
            "Mending Platemail");
          List<String> hlore = new ArrayList<String>();
          hlore.add((String)curlore.get(0));
          hlore.add((String)curlore.get(1));
          hlore.add(ChatColor.RED + "HP REGEN: +" + regen + 
            " HP/s");
          hlore.add((String)curlore.get(curlore.size() - 1));
          helmmeta.setLore(hlore);
          H.setItemMeta(helmmeta);
          e.setCurrentItem(H);
        }
        if (wepdrop == 2)
        {
          ItemStack H = new ItemStack(Material.IRON_CHESTPLATE);
          ItemMeta helmmeta = H.getItemMeta();
          helmmeta.setDisplayName(plus + ChatColor.AQUA + 
            "Platemail of Fortitude");
          List<String> hlore = new ArrayList<String>();
          hlore.add((String)curlore.get(0));
          hlore.add((String)curlore.get(1));
          hlore.add(ChatColor.RED + "VIT: +" + vit);
          hlore.add((String)curlore.get(curlore.size() - 1));
          helmmeta.setLore(hlore);
          H.setItemMeta(helmmeta);
          e.setCurrentItem(H);
        }
        if (wepdrop == 1)
        {
          p.getWorld().playEffect(p.getLocation(), 
            Effect.EXTINGUISH, 0);
          ParticleEffect.LAVA.display(p.getEyeLocation(), 0.0F, 0.0F, 0.0F, 5.0F, 10);
        }
        else
        {
          Firework fw = (Firework)p.getWorld().spawn(p.getLocation(), 
            Firework.class);
          FireworkMeta data = fw.getFireworkMeta();
          data.addEffects(new FireworkEffect[] {FireworkEffect.builder()
            .withColor(Color.YELLOW).with(FireworkEffect.Type.BALL).build() });
          data.setPower(0);
          fw.setFireworkMeta(data);
        }
      }
      else if ((e.getCurrentItem().getType() == Material.IRON_LEGGINGS) && 
        (e.getCurrentItem().getItemMeta().getLore() != null))
      {
        e.setCancelled(true);
        Random random = new Random();
        int wepdrop = random.nextInt(2) + 1;
        int regen = random.nextInt(11) + 40;
        int vit = random.nextInt(41) + 20;
        String plus = "";
        String fulname = e.getCurrentItem().getItemMeta()
          .getDisplayName();
        if (fulname.startsWith(ChatColor.RED + "[+")) {
          plus = fulname.substring(0, 7);
        }
        List<String> curlore = e.getCurrentItem().getItemMeta()
          .getLore();
        if (e.getCursor().getAmount() > 1) {
          e.getCursor().setAmount(e.getCursor().getAmount() - 1);
        } else if (e.getCursor().getAmount() == 1) {
          e.setCursor(null);
        }
        if (wepdrop == 1)
        {
          ItemStack H = new ItemStack(Material.IRON_LEGGINGS);
          ItemMeta helmmeta = H.getItemMeta();
          helmmeta.setDisplayName(plus + ChatColor.AQUA + 
            "Mending Platemail Leggings");
          List<String> hlore = new ArrayList<String>();
          hlore.add((String)curlore.get(0));
          hlore.add((String)curlore.get(1));
          hlore.add(ChatColor.RED + "HP REGEN: +" + regen + 
            " HP/s");
          hlore.add((String)curlore.get(curlore.size() - 1));
          helmmeta.setLore(hlore);
          H.setItemMeta(helmmeta);
          e.setCurrentItem(H);
        }
        if (wepdrop == 2)
        {
          ItemStack H = new ItemStack(Material.IRON_LEGGINGS);
          ItemMeta helmmeta = H.getItemMeta();
          helmmeta.setDisplayName(plus + ChatColor.AQUA + 
            "Platemail Leggings of Fortitude");
          List<String> hlore = new ArrayList<String>();
          hlore.add((String)curlore.get(0));
          hlore.add((String)curlore.get(1));
          hlore.add(ChatColor.RED + "VIT: +" + vit);
          hlore.add((String)curlore.get(curlore.size() - 1));
          helmmeta.setLore(hlore);
          H.setItemMeta(helmmeta);
          e.setCurrentItem(H);
        }
        if (wepdrop == 1)
        {
          p.getWorld().playEffect(p.getLocation(), 
            Effect.EXTINGUISH, 0);
          ParticleEffect.LAVA.display(p.getEyeLocation(), 0.0F, 0.0F, 0.0F, 5.0F, 10);
        }
        else
        {
          Firework fw = (Firework)p.getWorld().spawn(p.getLocation(), 
            Firework.class);
          FireworkMeta data = fw.getFireworkMeta();
          data.addEffects(new FireworkEffect[] {FireworkEffect.builder()
            .withColor(Color.YELLOW).with(FireworkEffect.Type.BALL).build() });
          data.setPower(0);
          fw.setFireworkMeta(data);
        }
      }
      else if ((e.getCurrentItem().getType() == Material.IRON_BOOTS) && 
        (e.getCurrentItem().getItemMeta().getLore() != null))
      {
        e.setCancelled(true);
        Random random = new Random();
        int wepdrop = random.nextInt(2) + 1;
        int regen = random.nextInt(11) + 40;
        int vit = random.nextInt(41) + 20;
        String plus = "";
        String fulname = e.getCurrentItem().getItemMeta()
          .getDisplayName();
        if (fulname.startsWith(ChatColor.RED + "[+")) {
          plus = fulname.substring(0, 7);
        }
        List<String> curlore = e.getCurrentItem().getItemMeta()
          .getLore();
        if (e.getCursor().getAmount() > 1) {
          e.getCursor().setAmount(e.getCursor().getAmount() - 1);
        } else if (e.getCursor().getAmount() == 1) {
          e.setCursor(null);
        }
        if (wepdrop == 1)
        {
          ItemStack H = new ItemStack(Material.IRON_BOOTS);
          ItemMeta helmmeta = H.getItemMeta();
          helmmeta.setDisplayName(plus + ChatColor.AQUA + 
            "Mending Platemail Boots");
          List<String> hlore = new ArrayList<String>();
          hlore.add((String)curlore.get(0));
          hlore.add((String)curlore.get(1));
          hlore.add(ChatColor.RED + "HP REGEN: +" + regen + 
            " HP/s");
          hlore.add((String)curlore.get(curlore.size() - 1));
          helmmeta.setLore(hlore);
          H.setItemMeta(helmmeta);
          e.setCurrentItem(H);
        }
        if (wepdrop == 2)
        {
          ItemStack H = new ItemStack(Material.IRON_BOOTS);
          ItemMeta helmmeta = H.getItemMeta();
          helmmeta.setDisplayName(plus + ChatColor.AQUA + 
            "Platemail Boots of Fortitude");
          List<String> hlore = new ArrayList<String>();
          hlore.add((String)curlore.get(0));
          hlore.add((String)curlore.get(1));
          hlore.add(ChatColor.RED + "VIT: +" + vit);
          hlore.add((String)curlore.get(curlore.size() - 1));
          helmmeta.setLore(hlore);
          H.setItemMeta(helmmeta);
          e.setCurrentItem(H);
        }
        if (wepdrop == 1)
        {
          p.getWorld().playEffect(p.getLocation(), 
            Effect.EXTINGUISH, 0);
          ParticleEffect.LAVA.display(p.getEyeLocation(), 0.0F, 0.0F, 0.0F, 5.0F, 10);
        }
        else
        {
          Firework fw = (Firework)p.getWorld().spawn(p.getLocation(), 
            Firework.class);
          FireworkMeta data = fw.getFireworkMeta();
          data.addEffects(new FireworkEffect[] {FireworkEffect.builder()
            .withColor(Color.YELLOW).with(FireworkEffect.Type.BALL).build() });
          data.setPower(0);
          fw.setFireworkMeta(data);
        }
      }
      else if ((e.getCurrentItem().getType() == Material.IRON_SWORD) && 
        (e.getCurrentItem().getItemMeta().getLore() != null))
      {
        e.setCancelled(true);
        List<String> curlore = e.getCurrentItem().getItemMeta()
          .getLore();
        if (e.getCursor().getAmount() > 1) {
          e.getCursor().setAmount(e.getCursor().getAmount() - 1);
        } else if (e.getCursor().getAmount() == 1) {
          e.setCursor(null);
        }
        String plus = "�b";
        String name = "Magic Sword";
        if (e.getCurrentItem().getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+")) {
          plus = 
          
            e.getCurrentItem().getItemMeta().getDisplayName().substring(0, 7) + plus;
        }
        Random random = new Random();
        int elemamt = random.nextInt(6) + 5;
        int lifeamt = random.nextInt(20) + 1;
        int critamt = random.nextInt(9) + 1;
        int elem = random.nextInt(9) + 1;
        int life = random.nextInt(3) + 1;
        int crit = random.nextInt(3) + 1;
        List<String> lore = new ArrayList<String>();
        lore.add((String)curlore.get(0));
        if (life == 1)
        {
          lore.add(ChatColor.RED + "LIFE STEAL: " + lifeamt + "%");
          name = "Vampyric " + name;
        }
        if (crit == 1)
        {
          lore.add(ChatColor.RED + "CRITICAL HIT: " + critamt + 
            "%");
          name = "Deadly " + name;
        }
        if (elem == 3)
        {
          lore.add(ChatColor.RED + "ICE DMG: +" + elemamt);
          name = name + " of Ice";
        }
        if (elem == 2)
        {
          lore.add(ChatColor.RED + "POISON DMG: +" + elemamt);
          name = name + " of Poison";
        }
        if (elem == 1)
        {
          lore.add(ChatColor.RED + "FIRE DMG: +" + elemamt);
          name = name + " of Fire";
        }
        lore.add((String)curlore.get(curlore.size() - 1));
        ItemStack is = new ItemStack(Material.IRON_SWORD);
        ItemMeta meta = is.getItemMeta();
        meta.setDisplayName(plus + name);
        meta.setLore(lore);
        is.setItemMeta(meta);
        e.setCurrentItem(is);
        if (lore.size() <= curlore.size())
        {
          p.getWorld().playEffect(p.getLocation(), 
            Effect.EXTINGUISH, 0);
          ParticleEffect.LAVA.display(p.getEyeLocation(), 0.0F, 0.0F, 0.0F, 5.0F, 10);
        }
        else
        {
          Firework fw = (Firework)p.getWorld().spawn(p.getLocation(), 
            Firework.class);
          FireworkMeta data = fw.getFireworkMeta();
          data.addEffects(new FireworkEffect[] {FireworkEffect.builder()
            .withColor(Color.YELLOW).with(FireworkEffect.Type.BALL).build() });
          data.setPower(0);
          fw.setFireworkMeta(data);
        }
      }
    }
  }
}
