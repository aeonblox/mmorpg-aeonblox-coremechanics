package com.aeonblox.rpg.events;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

import com.aeonblox.rpg.Aeonblox;

public class BookDrop
  implements Listener
{
	  Aeonblox plugin;
	  
  @EventHandler(ignoreCancelled=true)
  public void drop(PlayerDropItemEvent e)
  {
	  
    if (e.getItemDrop().getItemStack().getType() == Material.WRITTEN_BOOK)
    {
      if ((e.getItemDrop().getItemStack().getItemMeta() != null) && 
        (e.getItemDrop().getItemStack().getItemMeta().getDisplayName() != null) && (e.getItemDrop().getItemStack().getItemMeta().getDisplayName().contains(ChatColor.GREEN + "" + ChatColor.BOLD + "Character Journal")))
      {
        e.setCancelled(true);
        e.getPlayer().sendMessage(ChatColor.GOLD + "You can't drop the book!");
        e.getPlayer().getInventory().setItem(8, e.getItemDrop().getItemStack());
      }
    }
    else if ((e.getItemDrop().getItemStack().getType() == Material.EMERALD) && 
      (e.getItemDrop().getItemStack().getItemMeta().getDisplayName() != null) && (e.getItemDrop().getItemStack().getItemMeta().getDisplayName().contains("Rune")))
    {
      e.setCancelled(true);
      e.getPlayer().getInventory().setItem(7, e.getItemDrop().getItemStack());
      e.getPlayer().sendMessage(ChatColor.GOLD + "You can't drop the telportation rune!");
    }
  }
}
