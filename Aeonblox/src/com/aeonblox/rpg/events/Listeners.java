package com.aeonblox.rpg.events;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Animals;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.RPGMakerDev.RPGMaker.EntityData.RPGEntity;
import com.aeonblox.rpg.Aeonblox;
import com.mewin.WGRegionEvents.events.RegionEnterEvent;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.StateFlag;

public class Listeners
  implements Listener
{
  Aeonblox plugin;
  
  public Listeners(Aeonblox instance)
  {
    plugin = instance;
  }
  /*
  @EventHandler
  public void onJoin(PlayerJoinEvent e)
  {
    Player p = e.getPlayer();
    p.setHealthScale(20.0D);
    p.setHealthScaled(true);
    if (p.hasPlayedBefore())
    {
      p.sendMessage(ChatColor.WHITE + "" + ChatColor.BOLD + "                                Patch 1.1 Beta");
      p.sendMessage(ChatColor.GRAY + "" + ChatColor.ITALIC + "               http://store.aeonblox.com/");
      p.sendMessage(ChatColor.GRAY + "If you are a subscriber, you can toggle your particle trail using " + ChatColor.YELLOW + ChatColor.UNDERLINE + "/toggletrail");
      e.setJoinMessage(null);
    }
    else
    {
      Kit(e.getPlayer());
      p.sendMessage(ChatColor.WHITE + "" +ChatColor.BOLD + "                                Patch 1.1 Beta");
      p.sendMessage(ChatColor.GRAY + "" + ChatColor.ITALIC + "               http://drpracticestore.buycraft.net/");
      p.sendMessage(ChatColor.GRAY + "If you are a subscriber, you can toggle your particle trail using " + ChatColor.YELLOW + ChatColor.UNDERLINE + "/toggletrail");
      e.setJoinMessage(null);
    }
  }
  
  @EventHandler
  public void onLeave(PlayerKickEvent e)
  {
    e.setLeaveMessage(null);
  }
  
  @EventHandler
  public void onPlayerLeave(PlayerQuitEvent e)
  {
    e.setQuitMessage(null);
  }
  */
  /**
   * This is a listener for Player Respawns.
   * @param e - The event passed to the method by Bukkit
   */
  @EventHandler
  public void onRespawn(PlayerRespawnEvent e)
  {
    Player p = e.getPlayer();
    for (int i = 0; i < Aeonblox.tagged.size(); i++) {
      Aeonblox.tagged.remove(p);
    }
    for (int i = 0; i < this.combat.size(); i++) {
      this.combat.remove(p);
    }
  }
  
  @EventHandler
  public void onLogoutCancelDamager(EntityDamageByEntityEvent e)
  {
    if (((e.getDamager() instanceof Player)) && ((e.getEntity() instanceof LivingEntity)))
    {
      Player p = (Player)e.getDamager();
      if (Aeonblox.bank.contains(p.getName()))
      {
        Aeonblox.bank.remove(p.getName());
        p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "Bank Open - CANCELLED");
      }
    }
  }
  
  @EventHandler
  public void onLogoutCancelDamage(EntityDamageEvent e)
  {
    if ((e.getEntity() instanceof Player))
    {
      Player p = (Player)e.getEntity();
      if (Aeonblox.bank.contains(p.getName()))
      {
        Aeonblox.bank.remove(p.getName());
        p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "Bank Open - CANCELLED");
      }
    }
  }
 /*
  @EventHandler
  public void onLogoutCancelMove(PlayerMoveEvent e)
  {
    Player p = e.getPlayer();
    if (Aeonblox.bank.contains(p.getName()))
    {
      int fromX = (int)e.getFrom().getX();
      int fromY = (int)e.getFrom().getY();
      int fromZ = (int)e.getFrom().getZ();
      int toX = (int)e.getTo().getX();
      int toY = (int)e.getTo().getY();
      int toZ = (int)e.getTo().getZ();
      if ((fromX != toX) || (fromZ != toZ) || (fromY != toY))
      {
        Aeonblox.bank.remove(p.getName());
        p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "Bank Open - CANCELLED");
      }
    }
   if (Aeonblox.teleporting.contains(p.getName()))
    {
      int fromX = (int)e.getFrom().getX();
      int fromY = (int)e.getFrom().getY();
      int fromZ = (int)e.getFrom().getZ();
      int toX = (int)e.getTo().getX();
      int toY = (int)e.getTo().getY();
      int toZ = (int)e.getTo().getZ();
      if ((fromX != toX) || (fromZ != toZ) || (fromY != toY))
      {
        Aeonblox.teleporting.remove(p.getName());
        p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "Teleporation - CANCELLED");
      }
    } 
  }*/
  
  @EventHandler
  public void onSuicide(AsyncPlayerChatEvent e)
  {
    Player p = e.getPlayer();
    if (Aeonblox.suicide.contains(p.getName()))
    {
      e.setCancelled(true);
      if (e.getMessage().equalsIgnoreCase("y"))
      {
        for (int i = 0; i < Aeonblox.tagged.size(); i++) {
          Aeonblox.tagged.remove(p);
        }
        for (int i = 0; i < this.combat.size(); i++) {
          this.combat.remove(p);
        }
        p.setHealth(0.0D);
        Bukkit.getServer().broadcastMessage(
          p.getDisplayName() + ChatColor.WHITE + 
          " ended their own life.");
        for (int i = 0; i < Aeonblox.suicide.size(); i++) {
          Aeonblox.suicide.remove(p.getName());
        }
      }
      else
      {
        p.sendMessage(ChatColor.YELLOW + "/suicide - " + ChatColor.BOLD + 
          "CANCELLED");
        for (int i = 0; i < Aeonblox.suicide.size(); i++) {
          Aeonblox.suicide.remove(p.getName());
        }
      }
    }
  }
  /**
  @EventHandler
  public void onFireTrail(PlayerMoveEvent e)
  {
    Player p = e.getPlayer();
    if (Aeonblox.toggletrail.contains(p.getName()))
    {
      if ((p.hasPermission("aeonblox.subtrail")) && 
        (!p.hasPermission("aeonblox.subplustrail")) && 
        (!p.hasPermission("aeonblox.subplusplustrail"))) {
        ParticleEffect.HAPPY_VILLAGER.display(p.getLocation(), 0.25F, 
          0.125F, 0.25F, 0.0F, 1);
      }
      if ((p.hasPermission("aeonblox.subplustrail")) && 
        (!p.hasPermission("aeonblox.subplusplustrail"))) {
        ParticleEffect.FLAME.display(p.getLocation(), 0.25F, 0.125F, 
          0.25F, 0.0F, 1);
      }
      if (p.hasPermission("aeonblox.subplusplustrail")) {
        ParticleEffect.WITCH_MAGIC.display(p.getLocation(), 0.0F, 0.0F, 0.0F, 5.0F, 
          5);
      }
    }
  }*/
/**  
  @EventHandler
  public void onEnergyUse(PlayerInteractEvent e)
  {
    if ((e.getAction() == Action.LEFT_CLICK_AIR) || (e.getAction() == Action.LEFT_CLICK_BLOCK))
    {
      Player p = e.getPlayer();
      if (p.getExp() >= -0.5D)
      {
        Random random = new Random();
        int amt = 0;
        if (p.getItemInHand().getType().name().contains("AXE")) {
          amt = random.nextInt(3) + 9;
        } else if (p.getItemInHand().getType().name().contains("SWORD")) {
          amt = random.nextInt(3) + 8;
        } else {
          amt = random.nextInt(3) + 6;
        }
        p.setExp((float)(p.getExp() - amt * 0.01D));
        p.setLevel(p.getLevel() - amt);
      }
      if (p.getExp() <= 0.0F)
      {
        p.setExp(-0.5F);
        p.setLevel(-50);
        p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 25, 5), true);
      }
    }
  }*/
  
  List<String> combat = new ArrayList<String>();
  
  @EventHandler
  public void onCombatTag(EntityDamageByEntityEvent e)
  {
    if (((e.getDamager() instanceof Player)) && ((e.getEntity() instanceof Player)))
    {
      if (e.getDamage() <= 0.0D) {
        return;
      }
      final Player p = (Player)e.getDamager();
      this.combat.add(p.getName());
      new BukkitRunnable()
      {
        public void run()
        {
          Listeners.this.combat.remove(p.getName());
        }
      }.runTaskLater(plugin, 100L);
    }
  }
  
  @SuppressWarnings("deprecation")
@EventHandler
  public void onRegionHit(EntityDamageByEntityEvent e)
  {
    if (((e.getDamager() instanceof Player)) && ((e.getEntity() instanceof Player)))
    {
      if (e.getDamage() <= 0.0D) {
        return;
      }
     Player p = (Player)e.getDamager();
     if (plugin.util_WorldGuard != null)
     {
    if (plugin.util_WorldGuard.playerInPVPRegion(p))
     {
    	e.setCancelled(true);
    	e.setDamage(0);
    	p.sendMessage(ChatColor.RED + "Try not to " + ChatColor.UNDERLINE + "HIT" + ChatColor.RED + " others in safezones!");
    	
     }else{
    	 
     }
    	 
     }
     
    }
  }
  
  public void onPlayerTeleport(PlayerTeleportEvent event)
  {
	  if((!Aeonblox.teleporting.contains(event.getPlayer().getName())) && ((event.getCause() == PlayerTeleportEvent.TeleportCause.COMMAND))) {
		  event.setCancelled(true);
	  }else{
		  
	  }
  }
  
  @EventHandler
  public void onEnter(RegionEnterEvent e)
  {
	
    if ((e.getRegion().getFlag(DefaultFlag.PVP) == StateFlag.State.DENY) && (e.getRegion().getFlag(DefaultFlag.MOB_DAMAGE) == StateFlag.State.DENY) && 
      (this.combat.contains(e.getPlayer().getName())))
    {
    if (plugin.util_WorldGuard != null)
    {
    if (!plugin.util_WorldGuard.playerInSafety(e.getPlayer()))
    {
      e.setCancelled(true);
      e.getPlayer().sendMessage(ChatColor.RED + "You " + ChatColor.UNDERLINE + 
        "cannot" + ChatColor.RED + " leave a chaotic zone while in combat.");
    }else{
    	
    }
    }
    }else{
    	
    }
  }
  
  
  @EventHandler
  public void onCombatEnter(PlayerMoveEvent e)
  {
    Player p = e.getPlayer();
    if (this.combat.contains(p.getName()))
    {
      int fromX = (int)e.getFrom().getX();
      int fromY = (int)e.getFrom().getY();
      int fromZ = (int)e.getFrom().getZ();
      int toX = (int)e.getTo().getX();
      int toY = (int)e.getTo().getY();
      int toZ = (int)e.getTo().getZ();
      if ((fromX != toX) || (fromZ != toZ) || (fromY != toY)) {
        if ((plugin.wg != null) && (plugin.wg.getRegionManager(p.getWorld()) != null) && (plugin.wg.getRegionManager(p.getWorld()).getApplicableRegions(p.getLocation()) != null))
        {
          ApplicableRegionSet set = plugin.wg.getRegionManager(
            e.getTo().getWorld()).getApplicableRegions(e.getTo());
          if (!set.allows(DefaultFlag.PVP))
          {
            Vector unitVector = p.getLocation().toVector().subtract(e.getTo().toVector()).normalize();
            p.setVelocity(unitVector.multiply(0.5D).setY(0));
          }
        }
      }
    }
  }
  
  List<LivingEntity> nodamage = new ArrayList<LivingEntity>();
  
  @EventHandler(priority=EventPriority.LOWEST)
  public void onNoAutoclick(EntityDamageByEntityEvent e)
  {
    if (((e.getEntity() instanceof LivingEntity)) && ((e.getDamager() instanceof Player)))
    {
      final LivingEntity p = (LivingEntity)e.getEntity();
      if (this.nodamage.contains(p)) {
        e.setDamage(0.0D);
      }
      e.setCancelled(true);
      this.nodamage.add(p);
      new BukkitRunnable()
      {
        public void run()
        {
          Listeners.this.nodamage.remove(p);
        }
      }.runTaskLater(plugin, 2L);
    }
  }
  
  /**@EventHandler(priority=EventPriority.LOWEST)
  public void onNoEnergyDamage(EntityDamageByEntityEvent e)
  {
    if (((e.getEntity() instanceof LivingEntity)) && ((e.getDamager() instanceof Player)))
    {
      Player d = (Player)e.getDamager();
      if (d.getExp() <= 0.0F)
      {
        e.setDamage(0.0D);
        e.setCancelled(true);
      }
    }
  }*/
  
  @EventHandler(priority=EventPriority.HIGHEST)
  public void onNoDamager(EntityDamageEvent e)
  {
    if (e.getEntity().hasMetadata("NPC"))
    {
      e.setDamage(0.0D);
      e.setCancelled(true);
    }
  }
  
  @EventHandler
  public void onLoginShiny(PlayerJoinEvent e)
  {
    Player p = e.getPlayer();
    PlayerInventory pi = p.getInventory();
    for (int i = 0; i < pi.getSize(); i++) {
      if ((pi.getItem(i) != null) && (pi.getItem(i).getItemMeta().hasDisplayName()) && 
        (!pi.getItem(i).getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+4] ")) && (!pi.getItem(i).getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+5] ")) && (!pi.getItem(i).getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+6] ")) && (!pi.getItem(i).getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+7] "))) {
        pi.getItem(i).getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+8] ");
      }
    }
    if ((pi.getHelmet() != null) && (pi.getHelmet().getItemMeta().hasDisplayName()) && 
      (!pi.getHelmet().getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+4] ")) && (!pi.getHelmet().getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+5] ")) && (!pi.getHelmet().getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+6] ")) && (!pi.getHelmet().getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+7] "))) {
      pi.getHelmet().getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+8] ");
    }
    if ((pi.getChestplate() != null) && (pi.getChestplate().getItemMeta().hasDisplayName()) && 
      (!pi.getChestplate().getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+4] ")) && (!pi.getChestplate().getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+5] ")) && (!pi.getChestplate().getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+6] ")) && (!pi.getChestplate().getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+7] "))) {
      pi.getChestplate().getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+8] ");
    }
    if ((pi.getLeggings() != null) && (pi.getLeggings().getItemMeta().hasDisplayName()) && 
      (!pi.getLeggings().getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+4] ")) && (!pi.getLeggings().getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+5] ")) && (!pi.getLeggings().getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+6] ")) && (!pi.getLeggings().getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+7] "))) {
      pi.getLeggings().getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+8] ");
    }
    if ((pi.getBoots() != null) && (pi.getBoots().getItemMeta().hasDisplayName()) && 
      (!pi.getBoots().getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+4] ")) && (!pi.getBoots().getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+5] ")) && (!pi.getBoots().getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+6] ")) && (!pi.getBoots().getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+7] "))) {
      pi.getBoots().getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+8] ");
    }
  }
  
  @EventHandler
  public void onOpenShinyShiny(InventoryOpenEvent e)
  {
    Inventory pi = e.getInventory();
    if (e.getInventory().getType() == InventoryType.ENDER_CHEST) {
      for (int i = 0; i < pi.getSize(); i++) {
        if ((pi.getItem(i) != null) && (pi.getItem(i).getItemMeta().hasDisplayName()) && 
          (!pi.getItem(i).getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+4] ")) && (!pi.getItem(i).getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+5] ")) && (!pi.getItem(i).getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+6] ")) && (!pi.getItem(i).getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+7] "))) {
          pi.getItem(i).getItemMeta().getDisplayName().startsWith(ChatColor.RED + "[+8] ");
        }
      }
    }
  }
  
  @SuppressWarnings("deprecation")
@EventHandler
  public void onMapOpen(PlayerInteractEvent e)
  {
    Player p = e.getPlayer();
    if (((e.getAction() == Action.RIGHT_CLICK_AIR) || (e.getAction() == Action.RIGHT_CLICK_BLOCK)) && 
      ((p.getItemInHand().getType() == Material.EMPTY_MAP) && (p.getItemInHand().getItemMeta().getLore() != null)))
    {
      e.setCancelled(true);
      p.updateInventory();
    }
  }
  
  @EventHandler
  public void onGoldBlockPickup(PlayerPickupItemEvent e)
  {
    Player p = e.getPlayer();
    if (e.getItem().getItemStack().getType() == Material.GOLD_BLOCK)
    {
      p.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "            +" + ChatColor.GOLD + e.getItem().getItemStack().getAmount() * 81 + ChatColor.GOLD + ChatColor.BOLD + "G");
      p.playSound(p.getLocation(), Sound.SUCCESSFUL_HIT, 1.0F, 1.0F);
    }
  }
  
  @EventHandler
  public void onGoldIngotPickup(PlayerPickupItemEvent e)
  {
    Player p = e.getPlayer();
    if (e.getItem().getItemStack().getType() == Material.GOLD_INGOT)
    {
      p.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "            +" + ChatColor.GOLD + e.getItem().getItemStack().getAmount() * 9 + ChatColor.GOLD + ChatColor.BOLD + "G");
      p.playSound(p.getLocation(), Sound.SUCCESSFUL_HIT, 1.0F, 1.0F);
    }
  }
  
  @EventHandler
  public void onGoldNuggetPickup(PlayerPickupItemEvent e)
  {
    Player p = e.getPlayer();
    if (e.getItem().getItemStack().getType() == Material.GOLD_NUGGET)
    {
      p.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "            +" + ChatColor.GOLD + e.getItem().getItemStack().getAmount() + ChatColor.GOLD + ChatColor.BOLD + "G");
      p.playSound(p.getLocation(), Sound.SUCCESSFUL_HIT, 1.0F, 1.0F);
    }
  }
  
  @EventHandler
  public void onDamagePercent(EntityDamageEvent e)
  {
    if ((e.getEntity() instanceof Player))
    {
      if (e.getDamage() <= 0.0D) {
        return;
      }
      Player p = (Player)e.getEntity();
      if ((e.getCause().equals(EntityDamageEvent.DamageCause.POISON)) || (e.getCause().equals(EntityDamageEvent.DamageCause.FIRE_TICK)) || (e.getCause().equals(EntityDamageEvent.DamageCause.FIRE)) || (e.getCause().equals(EntityDamageEvent.DamageCause.LAVA)))
      {
        Damageable d = (Player) p;
    	  if (d.getMaxHealth() / 80.0D < 1.0D) {
          e.setDamage(1.0D);
        } else {
          e.setDamage(d.getMaxHealth() / 80.0D);
        }
      }
      else if (e.getCause().equals(EntityDamageEvent.DamageCause.FALL)) {
          Player p1 = (Player)e.getEntity();
          Damageable d = (Player) p1;
    	  if (e.getDamage() * d.getMaxHealth() / 80.0D >= d.getHealth()) {
          e.setDamage(d.getHealth() - 1.0D);
        } else if (e.getDamage() * d.getMaxHealth() / 80.0D < 1.0D) {
          e.setDamage(1.0D);
        } else {
          e.setDamage(e.getDamage() * d.getMaxHealth() / 80.0D);
        }
      }
    }
  }
  
  @EventHandler
  public void onEntityDamageTick(EntityDamageByEntityEvent e)
  {
    if (((e.getEntity() instanceof LivingEntity)) && 
      ((e.getDamager() instanceof LivingEntity)) && (!(e.getDamager() instanceof Player)))
    {
      final LivingEntity p = (LivingEntity)e.getEntity();
      final LivingEntity d = (LivingEntity)e.getDamager();
      if (e.getDamage() <= 0.0D) {
        return;
      }
      if (p.getNoDamageTicks() < p.getMaximumNoDamageTicks() / 2)
      {
        Random random = new Random();
        final int kb = random.nextInt(2) + 1;
        new BukkitRunnable()
        {
          public void run()
          {
            if (kb == 1) {
              p.setVelocity(new Vector(d.getLocation().getDirection().getX() * 0.25D, 0.175D, d.getLocation().getDirection().getZ() * 0.25D));
            } else {
              p.setVelocity(new Vector(d.getLocation().getDirection().getX() * 0.2D, 0.0D, d.getLocation().getDirection().getZ() * 0.2D));
            }
          }
        }.runTaskLater(plugin, 1L);
      }
    }
  }
  
  @EventHandler
  public void onArrowKnock(EntityDamageByEntityEvent e)
  {
    if (((e.getEntity() instanceof LivingEntity)) && 
      ((e.getDamager() instanceof Arrow)) && (!(e.getDamager() instanceof Player)))
    {
      LivingEntity p = (LivingEntity)e.getEntity();
      Arrow d = (Arrow)e.getDamager();
      if (e.getDamage() <= 0.0D) {
        return;
      }
      d.remove();
      if (p.getNoDamageTicks() < p.getMaximumNoDamageTicks() / 2)
      {
        Random random = new Random();
        int kb = random.nextInt(2) + 1;
        if (kb == 1) {
          p.setVelocity(new Vector(d.getLocation().getDirection().getX() * 0.25D, 0.175D, d.getLocation().getDirection().getZ() * 0.25D));
        } else {
          p.setVelocity(new Vector(d.getLocation().getDirection().getX() * 0.2D, 0.0D, d.getLocation().getDirection().getZ() * 0.2D));
        }
      }
    }
  }
  
  @EventHandler
  public void onEntityDamageKnock(EntityDamageByEntityEvent e)
  {
    if (((e.getEntity() instanceof LivingEntity)) && 
      ((e.getDamager() instanceof Player)))
    {
      final Player d = (Player)e.getDamager();
      final LivingEntity p = (LivingEntity)e.getEntity();
      if (e.getDamage() <= 0.0D) {
        return;
      }
      p.setNoDamageTicks(0);
      Random random = new Random();
      final int kb = random.nextInt(2) + 1;
      new BukkitRunnable()
      {
        public void run()
        {
          if (kb == 1) {
            p.setVelocity(new Vector(d.getLocation().getDirection().getX() * 0.25D, 0.175D, d.getLocation().getDirection().getZ() * 0.25D));
          } else {
            p.setVelocity(new Vector(d.getLocation().getDirection().getX() * 0.2D, 0.0D, d.getLocation().getDirection().getZ() * 0.2D));
          }
        }
      }.runTaskLater(plugin, 1L);
    }
  }
  
  public static int getMinValueFromLore(ItemStack item, String value)
  {
    int returnVal = 1;
    ItemMeta meta = item.getItemMeta();
    try
    {
      List<String> lore = meta.getLore();
      if ((lore != null) && 
        (((String)lore.get(0)).contains(value)))
      {
        String vals = ((String)lore.get(0)).split(": ")[1];
        vals = ChatColor.stripColor(vals);
        vals = vals.split(" - ")[0];
        returnVal = Integer.parseInt(vals.trim());
      }
    }
    catch (Exception localException) {}
    return returnVal;
  }
  
  public static int getMaxValueFromLore(ItemStack item, String value)
  {
    int returnVal = 1;
    ItemMeta meta = item.getItemMeta();
    try
    {
      List<String> lore = meta.getLore();
      if ((lore != null) && 
        (((String)lore.get(0)).contains(value)))
      {
        String vals = ((String)lore.get(0)).split(": ")[1];
        vals = ChatColor.stripColor(vals);
        vals = vals.split(" - ")[1];
        returnVal = Integer.parseInt(vals.trim());
      }
    }
    catch (Exception localException) {}
    return returnVal;
  }
  
  @EventHandler(priority=EventPriority.LOWEST)
  public void onPlayerDamage(EntityDamageByEntityEvent e)
  {
    if (((e.getDamager() instanceof Player)) && ((e.getEntity() instanceof LivingEntity)))
    {
      Player p = (Player)e.getDamager();
      if (e.getDamage() <= 0.0D) {
        return;
      }
      if ((p.getItemInHand() != null) && (p.getItemInHand().getType() != Material.AIR) && (p.getItemInHand().getItemMeta().hasLore()))
      {
        int damageMin = getMinValueFromLore(p.getItemInHand(), "DMG");
        int damageMax = getMaxValueFromLore(p.getItemInHand(), "DMG");
        Random random = new Random();
        double dmg = random.nextInt(damageMax - damageMin + 1) + damageMin;
        e.setDamage(dmg);
      }
    }
  }
  
  @SuppressWarnings("deprecation")
@EventHandler(priority=EventPriority.LOWEST)
  public void onNPCDamage(EntityDamageByEntityEvent e)
  {
    if ((e.getDamager() instanceof Player) && (!(e.getEntity() instanceof Monster)) && (!(e.getEntity() instanceof Animals)))
    {
      Player p = (Player)e.getDamager();
      if (e.getDamage() <= 0.0D) {
        return;
      }
      /**
      Entity e1 = e.getEntity();
	   boolean bool = true;
	   for (Player plz: Bukkit.getOnlinePlayers()){
		   if (plz.getUniqueId() == e1.getUniqueId()){
			   bool =false;
		   }*/
		   if(!RPGEntity.players.containsKey(e.getEntity().getUniqueId())) {
			   e.setCancelled(true);
			   e.setDamage(0);
			   p.sendMessage(ChatColor.RED + "Try not to " + ChatColor.UNDERLINE + "HIT" + ChatColor.RED + " the people of the land(NPC's).");
		   
			   
		   
      }else{
    	  
      }
    }else{
    	
    }
  }
  
  
  public static int getLifestealFromLore(ItemStack item, String value)
  {
    int returnVal = 0;
    ItemMeta meta = item.getItemMeta();
    try
    {
      List<String> lore = meta.getLore();
      if (lore != null) {
        for (int i = 0; i < lore.size(); i++) {
          if (((String)lore.get(i)).contains(value))
          {
            String vals = ((String)lore.get(i)).split(": ")[1];
            vals = ChatColor.stripColor(vals);
            vals = vals.replace("%", "").trim().toString();
            returnVal = Integer.parseInt(vals.trim());
          }
        }
      }
    }
    catch (Exception localException) {}
    return returnVal;
  }
  
  public static int getElemFromLore(ItemStack item, String value)
  {
    int returnVal = 0;
    ItemMeta meta = item.getItemMeta();
    try
    {
      List<String> lore = meta.getLore();
      if (lore != null) {
        for (int i = 0; i < lore.size(); i++) {
          if (((String)lore.get(i)).contains(value))
          {
            String vals = ((String)lore.get(i)).split(": +")[1];
            vals = ChatColor.stripColor(vals);
            returnVal = Integer.parseInt(vals.trim());
          }
        }
      }
    }
    catch (Exception localException) {}
    return returnVal;
  }
  
  @EventHandler
  public void onWeaponStats(EntityDamageByEntityEvent e)
  {
    if (((e.getDamager() instanceof Player)) && ((e.getEntity() instanceof LivingEntity)))
    {
      double dmg = e.getDamage();
      Player p = (Player)e.getDamager();
      LivingEntity li = (LivingEntity)e.getEntity();
      if (e.getDamage() <= 0.0D) {
        return;
      }
      if ((p.getItemInHand() != null) && (p.getItemInHand().getType() != Material.AIR) && (p.getItemInHand().getItemMeta().hasLore()))
      {
        List<String> lore = p.getItemInHand().getItemMeta().getLore();
        for (int i = 0; i < lore.size(); i++)
        {
          if (((String)lore.get(i)).contains("ICE DMG"))
          {
            li.getWorld().playEffect(li.getEyeLocation(), Effect.POTION_BREAK, 8194);
            li.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 20, 0));
            double eldmg = getElemFromLore(p.getItemInHand(), "ICE DMG");
            dmg += eldmg;
          }
          if (((String)lore.get(i)).contains("POISON DMG"))
          {
            li.getWorld().playEffect(li.getEyeLocation(), Effect.POTION_BREAK, 8196);
            li.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 20, 1));
            double eldmg = getElemFromLore(p.getItemInHand(), "POISON DMG");
            dmg += eldmg;
          }
          if (((String)lore.get(i)).contains("FIRE DMG"))
          {
            li.setFireTicks(40);
            double eldmg = getElemFromLore(p.getItemInHand(), "FIRE DMG");
            dmg += eldmg;
          }
          if (((String)lore.get(i)).contains("PURE DMG"))
          {
            double eldmg = getElemFromLore(p.getItemInHand(), "PURE DMG");
            dmg += eldmg;
          }
          if (((String)lore.get(i)).contains("CRITICAL HIT"))
          {
            int crit = getLifestealFromLore(p.getItemInHand(), "CRITICAL HIT");
            Random random = new Random();
            int drop = random.nextInt(25) + 1;
            if (drop <= crit) {
              dmg *= 2.0D;
            }
          }
          if (((String)lore.get(i)).contains("LIFE STEAL"))
          {
            li.getWorld().playEffect(li.getEyeLocation(), Effect.STEP_SOUND, Material.REDSTONE_BLOCK);
            double base = getLifestealFromLore(p.getItemInHand(), "LIFE STEAL");
            double pcnt = base / 100.0D;
            int life = 0;
            if ((int)(pcnt * dmg) > 0) {
              life = (int)(pcnt * dmg);
            } else {
              life = 1;
            }
            Damageable p1 = (Damageable) e.getEntity();
            //Damageable d = p1;
            p.sendMessage(ChatColor.GREEN + "" + ChatColor.BOLD + "            +" + ChatColor.GREEN + life + ChatColor.GREEN + ChatColor.BOLD + " HP " + ChatColor.GRAY + "[" + (int)p1.getHealth() + "/" + (int)p1.getMaxHealth() + "HP]");
            if (p1.getHealth() < p1.getMaxHealth() - life) {
              p.setHealth(p1.getHealth() + life);
            } else if (p1.getHealth() >= p1.getMaxHealth() - life) {
              p.setHealth(p1.getMaxHealth());
            }
          }
        }
      }
      e.setDamage(dmg);
    }
  }
  
  @EventHandler(priority=EventPriority.LOWEST)
  public void onPlayerDeath(PlayerDeathEvent e)
  {
    Player p = e.getEntity();
    p.playSound(p.getLocation(), Sound.WITHER_SPAWN, 1.0F, 1.0F);
    e.setDroppedExp(0);
    e.setDeathMessage(null);
    
    new BukkitRunnable()
    {
      public void run()
      {
        for (Player p : Bukkit.getServer().getOnlinePlayers()) {
        plugin.listeners.hpCheck(p);
          }
      }
    }.runTaskLater(plugin, 1L);
    
    for (int i = 0; i < Aeonblox.tagged.size(); i++) {
      Aeonblox.tagged.remove(p);
    }
    for (int i = 0; i < this.combat.size(); i++) {
      this.combat.remove(p);
    }
  }
  
  @EventHandler(priority=EventPriority.HIGH)
  public void onSafetyMobDamage(EntityDamageEvent e)
  {
    if ((e.getEntity() instanceof Player))
    {
      Player p = (Player)e.getEntity();
      if (plugin.util_WorldGuard != null)
      {
       if (plugin.util_WorldGuard.playerInSafety(p))
      {
    	e.setCancelled(true);
    	e.setDamage(0);
    	   
        }else{
        	
        }
      }
    }else{
    	
    }
  }
  
  @EventHandler(priority=EventPriority.HIGH)
  public void onDeathMessageElem(EntityDamageEvent e)
  {
    if ((e.getEntity() instanceof Player))
    {
      Player p = (Player)e.getEntity();
      Damageable d = (Player) p;
      if ((e.getDamage() >= d.getHealth()) && (p.getNoDamageTicks() <= p.getMaximumNoDamageTicks() / 2) && (d.getHealth() > 0.0D) && (
        (e.getCause().equals(EntityDamageEvent.DamageCause.LAVA)) || (e.getCause().equals(EntityDamageEvent.DamageCause.FIRE)) || (e.getCause().equals(EntityDamageEvent.DamageCause.FIRE_TICK)))) {
        Bukkit.getServer().broadcastMessage(p.getDisplayName() + ChatColor.GRAY + " burned to death");
      }
    }
  }
  
  @EventHandler(priority=EventPriority.HIGH)
  public void onDeathMessage(EntityDamageByEntityEvent e)
  {
    if (((e.getEntity() instanceof Player)) && ((e.getDamager() instanceof LivingEntity)))
    {
      Player p = (Player)e.getEntity();
      Damageable hd = (Player) p;
      if ((e.getDamage() >= hd.getHealth()) && (p.getNoDamageTicks() <= p.getMaximumNoDamageTicks() / 2) && (hd.getHealth() > 0.0D)) {
        if ((e.getDamager() instanceof Player))
        {
          Player d = (Player)e.getDamager();
          if ((d.getItemInHand() != null) && (d.getItemInHand().getType() != Material.AIR)) {
            Bukkit.getServer().broadcastMessage(p.getDisplayName() + ChatColor.GRAY + " was killed by " + ChatColor.RESET + d.getDisplayName() + ChatColor.WHITE + " with a(n) " + d.getItemInHand().getItemMeta().getDisplayName());
          } else {
            Bukkit.getServer().broadcastMessage(p.getDisplayName() + ChatColor.GRAY + " was killed by " + ChatColor.RESET + d.getDisplayName() + ChatColor.WHITE + " with a(n) Air");
          }
        }
        else if ((e.getDamager() instanceof LivingEntity))
        {
          LivingEntity l = (LivingEntity)e.getDamager();
          if (l.getCustomName() != null) {
            Bukkit.getServer().broadcastMessage(p.getDisplayName() + ChatColor.GRAY + " was killed by a(n) " + ChatColor.RESET + l.getCustomName());
          }
        }
      }
    }
  }
  
  @EventHandler
  public void onDamageSound(EntityDamageByEntityEvent e)
  {
    if (((e.getDamager() instanceof Player)) && ((e.getEntity() instanceof LivingEntity)))
    {
      Player d = (Player)e.getDamager();
      if (e.getDamage() <= 0.0D) {
        return;
      }
      d.playSound(d.getLocation(), Sound.HURT_FLESH, 1.0F, 1.0F);
    }
  }
 


  @EventHandler
  public void onArmorDura(EntityDamageEvent e)
  {
    if ((e.getEntity() instanceof Player))
    {
      Player p = (Player)e.getEntity();
      final PlayerInventory i = p.getInventory();
      if (i.getHelmet() != null) {
        i.getHelmet().setDurability((short)0);
      }
      if (i.getChestplate() != null) {
        i.getChestplate().setDurability((short)0);
      }
      if (i.getLeggings() != null) {
        i.getLeggings().setDurability((short)0);
      }
      if (i.getBoots() != null) {
        i.getBoots().setDurability((short)0);
      }
      new BukkitRunnable()
      {
        public void run()
        {
          if (i.getHelmet() != null) {
            i.getHelmet().setDurability((short)0);
          }
          if (i.getChestplate() != null) {
            i.getChestplate().setDurability((short)0);
          }
          if (i.getLeggings() != null) {
            i.getLeggings().setDurability((short)0);
          }
          if (i.getBoots() != null) {
            i.getBoots().setDurability((short)0);
          }
        }
      }.runTaskLater(plugin, 1L);
    }
  } 
  
  @SuppressWarnings("deprecation")
@EventHandler
  public void onEntityDamage(EntityDamageByEntityEvent e)
  {
    if (((e.getDamager() instanceof Player)) && ((e.getEntity() instanceof LivingEntity)))
    {
      Player p = (Player)e.getDamager();
      if ((p.getItemInHand().getType() == Material.WOOD_SWORD) || (p.getItemInHand().getType() == Material.WOOD_AXE) || (p.getItemInHand().getType() == Material.STONE_SWORD) || (p.getItemInHand().getType() == Material.STONE_AXE) || (p.getItemInHand().getType() == Material.IRON_SWORD) || (p.getItemInHand().getType() == Material.IRON_AXE) || (p.getItemInHand().getType() == Material.DIAMOND_SWORD) || (p.getItemInHand().getType() == Material.DIAMOND_AXE) || (p.getItemInHand().getType() == Material.GOLD_SWORD) || (p.getItemInHand().getType() == Material.GOLD_AXE) || (p.getItemInHand().getType() == Material.GOLD_PICKAXE))
      {
        p.getInventory().getItemInHand().setDurability((short)0);
        p.updateInventory();
        p.saveData();
      }
    }
  }
  
  public static int getValueFromLore(ItemStack item, String value)
  {
    int returnVal = 0;
    ItemMeta meta = item.getItemMeta();
    try
    {
      List<String> lore = meta.getLore();
      if ((lore != null) && 
        (((String)lore.get(1)).contains(value)))
      {
        String vals = ((String)lore.get(1)).split(": +")[1];
        vals = ChatColor.stripColor(vals);
        returnVal = Integer.parseInt(vals.trim());
      }
    }
    catch (Exception localException) {}
    return returnVal;
  }
  
  public static int getVitFromLore(ItemStack item, String value)
  {
    int returnVal = 0;
    ItemMeta meta = item.getItemMeta();
    try
    {
      List<String> lore = meta.getLore();
      if ((lore != null) && 
        (((String)lore.get(2)).contains(value)))
      {
        String vals = ((String)lore.get(2)).split(": +")[1];
        vals = ChatColor.stripColor(vals);
        returnVal = Integer.parseInt(vals.trim());
      }
    }
    catch (Exception localException) {}
    return returnVal;
  }
  
  @SuppressWarnings("deprecation")
public void hpCheck(Player p)
  {
    PlayerInventory i = p.getInventory();
    double a = 50.0D;
    double vital = 0.0D;
    if ((i.getHelmet() != null) && (i.getHelmet().getItemMeta().hasLore()))
    {
      double health = getValueFromLore(i.getHelmet(), "HP");
      int vit = getVitFromLore(i.getHelmet(), "VIT");
      a += health;
      vital += vit;
    }
    if ((i.getChestplate() != null) && (i.getChestplate().getItemMeta().hasLore()))
    {
      double health = getValueFromLore(i.getChestplate(), "HP");
      int vit = getVitFromLore(i.getChestplate(), "VIT");
      a += health;
      vital += vit;
    }
    if ((i.getLeggings() != null) && (i.getLeggings().getItemMeta().hasLore()))
    {
      double health = getValueFromLore(i.getLeggings(), "HP");
      int vit = getVitFromLore(i.getLeggings(), "VIT");
      a += health;
      vital += vit;
    }
    if ((i.getBoots() != null) && (i.getBoots().getItemMeta().hasLore()))
    {
      double health = getValueFromLore(i.getBoots(), "HP");
      int vit = getVitFromLore(i.getBoots(), "VIT");
      a += health;
      vital += vit;
    }
    if (vital > 0.0D)
    {
      double divide = vital / 2000.0D;
      double pre = a * divide;
      int cleaned = (int)(a + pre);
      Damageable d = (Player) p;
      if (d.getHealth() > cleaned) {
        d.setHealth(cleaned);
      }
      d.setMaxHealth(cleaned);
    }
    else
    {
      p.setMaxHealth(a);
    }
    /** HEALTHSCALE */
    p.setHealthScale(20.0D);
    p.setHealthScaled(true);
  }
  
  @EventHandler
  public void onInventoryClick(InventoryClickEvent e)
  {
    final Player p = (Player)e.getWhoClicked();
    new BukkitRunnable()
    {
      public void run()
      {
        Listeners.this.hpCheck(p);
      }
    }.runTaskLater(plugin, 1L);
  }
  
  @SuppressWarnings("deprecation")
@EventHandler(priority=EventPriority.NORMAL)
  public void onVitSword(EntityDamageByEntityEvent e)
  {
    if (((e.getDamager() instanceof Player)) && ((e.getEntity() instanceof LivingEntity)))
    {
      if (e.getDamage() <= 0.0D) {
        return;
      }
      Player p = (Player)e.getDamager();
      PlayerInventory i = p.getInventory();
      if ((p.getItemInHand() != null) && (p.getItemInHand().getType().name().contains("SWORD")) && (p.getItemInHand().getItemMeta().hasLore()))
      {
        double dmg = e.getDamage();
        double vital = 0.0D;
        if ((i.getHelmet() != null) && (i.getHelmet().getItemMeta().hasLore()))
        {
          int vit = getVitFromLore(i.getHelmet(), "VIT");
          vital += vit;
        }
        if ((i.getChestplate() != null) && (i.getChestplate().getItemMeta().hasLore()))
        {
          int vit = getVitFromLore(i.getChestplate(), "VIT");
          vital += vit;
        }
        if ((i.getLeggings() != null) && (i.getLeggings().getItemMeta().hasLore()))
        {
          int vit = getVitFromLore(i.getLeggings(), "VIT");
          vital += vit;
        }
        if ((i.getBoots() != null) && (i.getBoots().getItemMeta().hasLore()))
        {
          int vit = getVitFromLore(i.getBoots(), "VIT");
          vital += vit;
        }
        if (vital > 0.0D)
        {
          double divide = vital / 7500.0D;
          double pre = dmg * divide;
          int cleaned = (int)(dmg + pre);
          e.setDamage(cleaned);
        }
        else
        {
          e.setDamage(dmg);
        }
      }
    }
  }
  
  @SuppressWarnings("deprecation")
@EventHandler(priority=EventPriority.NORMAL)
  public void onDpsDamage(EntityDamageByEntityEvent e)
  {
    if (((e.getDamager() instanceof Player)) && ((e.getEntity() instanceof LivingEntity)))
    {
      if (e.getDamage() <= 0.0D) {
        return;
      }
      Player p = (Player)e.getDamager();
      PlayerInventory i = p.getInventory();
      if ((p.getItemInHand() != null) && (p.getItemInHand().getType() != Material.AIR) && (p.getItemInHand().getItemMeta().hasLore()))
      {
        double dmg = e.getDamage();
        double dps = 0.0D;
        if ((i.getHelmet() != null) && (i.getHelmet().getItemMeta().hasLore()))
        {
          int adddps = getMinValueFromLore(i.getHelmet(), "DPS");
          dps += adddps;
        }
        if ((i.getChestplate() != null) && (i.getChestplate().getItemMeta().hasLore()))
        {
          int adddps = getMinValueFromLore(i.getChestplate(), "DPS");
          dps += adddps;
        }
        if ((i.getLeggings() != null) && (i.getLeggings().getItemMeta().hasLore()))
        {
          int adddps = getMinValueFromLore(i.getLeggings(), "DPS");
          dps += adddps;
        }
        if ((i.getBoots() != null) && (i.getBoots().getItemMeta().hasLore()))
        {
          int adddps = getMinValueFromLore(i.getBoots(), "DPS");
          dps += adddps;
        }
        if (dps > 0.0D)
        {
          double divide = dps / 100.0D;
          double pre = dmg * divide;
          int cleaned = (int)(dmg + pre);
          e.setDamage(cleaned);
        }
        else
        {
          e.setDamage(dmg);
        }
      }
    }
  }
  
  @EventHandler
  public void onTag(EntityDamageEvent e)
  {
    if ((e.getEntity() instanceof Player))
    {
      if (e.getDamage() <= 0.0D) {
        return;
      }
      final Player p = (Player)e.getEntity();
      Aeonblox.tagged.add(p.getName());
      new BukkitRunnable()
      {
        public void run()
        {
          Aeonblox.tagged.remove(p.getName());
        }
      }.runTaskLater(plugin, 200L);
    }
  }
  
  @EventHandler
  public void onHitTag(EntityDamageByEntityEvent e)
  {
    if (((e.getDamager() instanceof Player)) && ((e.getEntity() instanceof LivingEntity)))
    {
      if (e.getDamage() <= 0.0D) {
        return;
      }
      final Player p = (Player)e.getDamager();
      Aeonblox.tagged.add(p.getName());
      new BukkitRunnable()
      {
        public void run()
        {
          Aeonblox.tagged.remove(p.getName());
        }
      }.runTaskLater(plugin, 200L);
    }
  }
  
  @EventHandler(priority=EventPriority.LOWEST)
  public void onKickLog(PlayerKickEvent e)
  {
    Player p = e.getPlayer();
    if (e.getReason().equals("Illegal characters in chat")) {
      e.setCancelled(true);
    } else {
      for (int i = 0; i < Aeonblox.tagged.size(); i++) {
        Aeonblox.tagged.remove(p.getName());
      }
    }
  }
  
  @EventHandler(priority=EventPriority.LOWEST)
  public void onQuitLog(PlayerQuitEvent e)
  {
    Player p = e.getPlayer();
    if ((plugin.wg != null) && (plugin.wg.getRegionManager(p.getWorld()) != null) && (plugin.wg.getRegionManager(p.getWorld()).getApplicableRegions(p.getLocation()) != null))
    {
      ApplicableRegionSet set = plugin.wg.getRegionManager(p.getWorld()).getApplicableRegions(p.getLocation());
      if (!set.allows(DefaultFlag.PVP)) {
        return;
      }
      if (Aeonblox.tagged.contains(p.getName()))
      {
       Damageable d = (Player) p;
    	p.damage(d.getHealth());
        p.setHealth(0.0D);
        Bukkit.getServer().broadcastMessage(p.getDisplayName() + ChatColor.RED + " logged out whilst in combat, what a whimp.");
      }
    }
  }

  /**I DUNNO WHETHER TO KEEP THIS */

  @EventHandler
  public void onHealthRegen(EntityRegainHealthEvent e)
  {
    e.setCancelled(true);
  }
  
  @EventHandler(priority=EventPriority.HIGHEST)
  public void onBypassArmor(EntityDamageEvent e)
  {
    if ((e.getEntity() instanceof LivingEntity))
    {
      LivingEntity li = (LivingEntity)e.getEntity();
      if (e.getDamage() <= 0.0D) {
        return;
      }
      double dmg = e.getDamage();
      e.setDamage(0.0D);
      li.damage(dmg);
    }
  }
  
  public void Kit(Player p)
  {
    PlayerInventory i = p.getInventory();
    Random random = new Random();
    int min = random.nextInt(2) + 2;
    int max = random.nextInt(2) + 4;
    int wep = random.nextInt(2) + 1;
    if (wep == 1)
    {
      ItemStack S = new ItemStack(Material.WOOD_SWORD);
      ItemMeta smeta = S.getItemMeta();
      smeta.setDisplayName(ChatColor.WHITE + "Training Sword");
      List<String> slore = new ArrayList<String>();
      slore.add(ChatColor.RED + "DMG: " + min + " - " + max);
      slore.add(ChatColor.GRAY + "Untradeable");
      smeta.setLore(slore);
      S.setItemMeta(smeta);
      i.addItem(new ItemStack[] { S });
      
      ItemStack B = new ItemStack(Material.BREAD);
      ItemMeta bmeta = B.getItemMeta();
      bmeta.setDisplayName(ChatColor.WHITE + "Bread");
      List<String> blore = new ArrayList<String>();
      blore.add(ChatColor.GRAY + "Bread, specially baked by KingPsychopath");
      blore.add(ChatColor.GRAY + "Untradeable");
      bmeta.setLore(blore);
      B.setItemMeta(bmeta);
      i.addItem(new ItemStack[] { B });
    }
    if (wep == 2)
    {
      ItemStack S = new ItemStack(Material.WOOD_AXE);
      ItemMeta smeta = S.getItemMeta();
      smeta.setDisplayName(ChatColor.WHITE + "Training Hatchet");
      List<String> slore = new ArrayList<String>();
      slore.add(ChatColor.RED + "DMG: " + min + " - " + max);
      slore.add(ChatColor.GRAY + "Untradeable");
      smeta.setLore(slore);
      S.setItemMeta(smeta);
      i.addItem(new ItemStack[] { S });
      
      ItemStack B = new ItemStack(Material.BREAD);
      ItemMeta bmeta = B.getItemMeta();
      bmeta.setDisplayName(ChatColor.WHITE + "Bread");
      List<String> blore = new ArrayList<String>();
      blore.add(ChatColor.GRAY + "Bread, specially baked by KingPsychopath");
      blore.add(ChatColor.GRAY + "Untradeable");
      bmeta.setLore(blore);
      B.setItemMeta(bmeta);
      i.addItem(new ItemStack[] { B });
    }
    
    p.setMaxHealth(50.0D);
    p.setHealth(50.0D);
    p.setHealthScale(20.0D);
    p.setHealthScaled(true); 
  }
}