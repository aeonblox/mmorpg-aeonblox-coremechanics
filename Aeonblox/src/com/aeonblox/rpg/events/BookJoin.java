package com.aeonblox.rpg.events;

import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.aeonblox.rpg.Aeonblox;

public class BookJoin
  implements Listener
{
	  Aeonblox plugin;
	  
  @EventHandler
  public void join(PlayerJoinEvent e)
  {
    Material material = Material.EMERALD;
    int ammount = 1;
    String name = ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "Teleportation Rune";
    
    ItemStack item = new ItemStack(material, ammount);
    ItemMeta itemMeta = item.getItemMeta();
    itemMeta.setDisplayName(name);
    if (!e.getPlayer().hasPermission("aeonblox.rune.donor")) {
      itemMeta.setLore(Arrays.asList(new String[] { ChatColor.GREEN + "Left Click:" + ChatColor.GRAY + " Teleports to Skyte", ChatColor.GREEN + "Sneak-Left Click:" + ChatColor.GRAY + " Set-home at your current position", ChatColor.GREEN + "Sneak-Right Click:" + ChatColor.GRAY + " Teleport to your home", ChatColor.GREEN + "Right click " + ChatColor.GRAY + "will teleport you to the wild!" }));
    } else {
      itemMeta.setLore(Arrays.asList(new String[] { ChatColor.GREEN + "Left Click:" + ChatColor.GRAY + " Teleports to Spawn", ChatColor.GREEN + "Sneak-Left Click:" + ChatColor.GRAY + " Set-home at your current position", ChatColor.GREEN + "Sneak-Right Click:" + ChatColor.GRAY + " Teleport to your home", ChatColor.GREEN + "Right click " + ChatColor.GRAY + "will teleport you to the wild!" }));
    }
    item.setItemMeta(itemMeta);
    e.getPlayer().getInventory().setItem(7, new ItemStack(Material.AIR));
    e.getPlayer().getInventory().setItem(7, item);
  }
}
