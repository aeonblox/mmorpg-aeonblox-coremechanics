package com.aeonblox.rpg.events;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

import com.aeonblox.rpg.Aeonblox;

public class Untradeable
  implements Listener
{
  Aeonblox plugin;
  
  public Untradeable(Aeonblox instance)
  {
    plugin = instance;
  }
  
  @EventHandler
  public void onDropItem(PlayerDropItemEvent e)
  {
    String p = e.getPlayer().getName();
	  if ((e.getItemDrop().getItemStack().getItemMeta().hasLore()) && (e.getItemDrop().getItemStack().getItemMeta().getLore().contains(ChatColor.GRAY + "Untradeable")))
    {
      if(!Aeonblox.invarray.contains(p)) {
      e.getItemDrop().remove();
      e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.FIZZ, 1.0F, 0.0F);
      e.getPlayer().sendMessage(ChatColor.GRAY + "This item was" + ChatColor.GRAY + ChatColor.ITALIC + " untradeable" + ChatColor.GRAY + ", therefore it has " + ChatColor.GRAY + ChatColor.UNDERLINE + "vanished.");
    }
  }
  }
  
  @EventHandler
  public void onPickup(PlayerPickupItemEvent e)
  {
	 String p = e.getPlayer().getName();
	  if ((e.getItem().getItemStack().getItemMeta().hasLore()) && (e.getItem().getItemStack().getItemMeta().getLore().contains(ChatColor.GRAY + "Untradeable"))) {
    	if(!Aeonblox.invarray.contains(p)) {
    	e.setCancelled(true);
    }
  }
}
  
  
  @EventHandler
  public void onPickup2(PlayerPickupItemEvent e)
  {
	 String p = e.getPlayer().getName();
	  if ((e.getItem().getItemStack().getItemMeta().hasLore()) && (e.getItem().getItemStack().getItemMeta().getLore().contains(ChatColor.GREEN + "A book that displays"))) {
    	if(!Aeonblox.invarray.contains(p)) {
    	e.setCancelled(true);
    }
  }
}
  
  @EventHandler
  public void onClick(InventoryClickEvent e)
  {
	    String pz = e.getWhoClicked().getName();
	  if ((e.getCurrentItem() != null) && (e.getCurrentItem().getType() != Material.AIR) && (e.getCurrentItem().getItemMeta().hasLore()) && (e.getCurrentItem().getItemMeta().getLore().contains(ChatColor.GRAY + "Untradeable")) && 
      (e.getInventory().getHolder() != e.getWhoClicked()))
    {
    	if(!Aeonblox.invarray.contains(pz)) {
    	Player p = (Player)e.getWhoClicked();
      e.setCancelled(true);
      p.sendMessage(ChatColor.RED + "You " + ChatColor.RED + ChatColor.UNDERLINE + "cannot" + ChatColor.RED + " bank this item, as it is untradeable.");
    }
  }
 }
}