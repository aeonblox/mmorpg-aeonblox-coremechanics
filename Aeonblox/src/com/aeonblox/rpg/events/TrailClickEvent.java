package com.aeonblox.rpg.events;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import com.aeonblox.rpg.Aeonblox;

public class TrailClickEvent 
implements Listener {
	
	Aeonblox plugin;
	public TrailClickEvent(Aeonblox instance){	
		this.plugin = instance;
	}
	
	
	  @EventHandler
	  public void onClick(InventoryClickEvent e)
	  {
	    Player p = (Player)e.getWhoClicked();
	    if (e.getInventory().getName().equals(Aeonblox.Itrail.getName()))
	    {
	      if (e.getCurrentItem().getType() == Material.RED_ROSE) {
	        if ((p.hasPermission("trail.heart")) || (p.hasPermission("trail.all")))
	        {
	          p.sendMessage(ChatColor.YELLOW + "You selected " + ChatColor.GREEN + "Heart trail");
	          plugin.cfgHandler.getCustomConfig(plugin.trailConfig).set("Players." + p.toString(), Integer.valueOf(1));
	          p.closeInventory();
	        }
	        else
	        {
	          p.sendMessage(ChatColor.YELLOW + "You dont have permission to this trail");
	          p.closeInventory();
	        }
	      }
	      if (e.getCurrentItem().getType() == Material.PAPER) {
	        if ((p.hasPermission("trail.allow")) || (p.hasPermission("trail.all")))
	        {
	          p.sendMessage(ChatColor.YELLOW + "You selected " + ChatColor.GREEN + "None trails");
	          plugin.cfgHandler.getCustomConfig(plugin.trailConfig).set("Players." + p.toString(), null);
	          p.closeInventory();
	        }
	        else
	        {
	          p.sendMessage(ChatColor.YELLOW + "You dont have permission to this trail");
	          p.closeInventory();
	        }
	      }
	      if (e.getCurrentItem().getType() == Material.BLAZE_POWDER) {
	        if ((p.hasPermission("trail.angry")) || (p.hasPermission("trail.all")))
	        {
	          p.sendMessage(ChatColor.YELLOW + "You selected " + ChatColor.GREEN + "Angry trail");
	          plugin.cfgHandler.getCustomConfig(plugin.trailConfig).set("Players." + p.toString(), Integer.valueOf(2));
	          p.closeInventory();
	        }
	        else
	        {
	          p.sendMessage(ChatColor.YELLOW + "You dont have permission to this trail");
	          p.closeInventory();
	        }
	      }
	      if (e.getCurrentItem().getType() == Material.BOOK) {
	        if ((p.hasPermission("trail.magic")) || (p.hasPermission("trail.all")))
	        {
	          p.sendMessage(ChatColor.YELLOW + "You selected " + ChatColor.GREEN + "Magic trail");
	          plugin.cfgHandler.getCustomConfig(plugin.trailConfig).set("Players." + p.toString(), Integer.valueOf(3));
	          p.closeInventory();
	        }
	        else
	        {
	          p.sendMessage(ChatColor.YELLOW + "You dont have permission to this trail");
	          p.closeInventory();
	        }
	      }
	      if (e.getCurrentItem().getType() == Material.REDSTONE) {
	        if ((p.hasPermission("trail.fun")) || (p.hasPermission("trail.all")))
	        {
	          p.sendMessage(ChatColor.YELLOW + "You selected " + ChatColor.GREEN + "Fun trail");
	          plugin.cfgHandler.getCustomConfig(plugin.trailConfig).set("Players." + p.toString(), Integer.valueOf(4));
	          p.closeInventory();
	        }
	        else
	        {
	          p.sendMessage(ChatColor.YELLOW + "You dont have permission to this trail");
	          p.closeInventory();
	        }
	      }
	      if (e.getCurrentItem().getType() == Material.WEB) {
	        if ((p.hasPermission("trail.cloud")) || (p.hasPermission("trail.all")))
	        {
	          p.sendMessage(ChatColor.YELLOW + "You selected " + ChatColor.GREEN + "Cloud trail");
	          plugin.cfgHandler.getCustomConfig(plugin.trailConfig).set("Players." + p.toString(), Integer.valueOf(5));
	          p.closeInventory();
	        }
	        else
	        {
	          p.sendMessage(ChatColor.YELLOW + "You dont have permission to this trail");
	          p.closeInventory();
	        }
	      }
	      if (e.getCurrentItem().getType() == Material.SKULL_ITEM) {
	        if ((p.hasPermission("trail.wmagic")) || (p.hasPermission("trail.all")))
	        {
	          p.sendMessage(ChatColor.YELLOW + "You selected " + ChatColor.GREEN + "Witch Magic trail");
	          plugin.cfgHandler.getCustomConfig(plugin.trailConfig).set("Players." + p.toString(), Integer.valueOf(6));
	          p.closeInventory();
	        }
	        else
	        {
	          p.sendMessage(ChatColor.YELLOW + "You dont have permission to this trail");
	          p.closeInventory();
	        }
	      }
	      if (e.getCurrentItem().getType() == Material.ENDER_PEARL) {
	        if ((p.hasPermission("trail.ender")) || (p.hasPermission("trail.all")))
	        {
	          p.sendMessage(ChatColor.YELLOW + "You selected " + ChatColor.GREEN + "Ender trail");
	          plugin.cfgHandler.getCustomConfig(plugin.trailConfig).set("Players." + p.toString(), Integer.valueOf(7));
	          p.closeInventory();
	        }
	        else
	        {
	          p.sendMessage(ChatColor.YELLOW + "You dont have permission to this trail");
	          p.closeInventory();
	        }
	      }
	      if (e.getCurrentItem().getType() == Material.EMERALD) {
	        if ((p.hasPermission("trail.green")) || (p.hasPermission("trail.all")))
	        {
	          p.sendMessage(ChatColor.YELLOW + "You selected " + ChatColor.GREEN + "Green trail");
	          plugin.cfgHandler.getCustomConfig(plugin.trailConfig).set("Players." + p.toString(), Integer.valueOf(8));
	          p.closeInventory();
	        }
	        else
	        {
	          p.sendMessage(ChatColor.YELLOW + "You dont have permission to this trail");
	          p.closeInventory();
	        }
	      }
	      if (e.getCurrentItem().getType() == Material.FLINT) {
	        if ((p.hasPermission("trail.spark")) || (p.hasPermission("trail.all")))
	        {
	          p.sendMessage(ChatColor.YELLOW + "You selected " + ChatColor.GREEN + "Spark trail");
	          plugin.cfgHandler.getCustomConfig(plugin.trailConfig).set("Players." + p.toString(), Integer.valueOf(9));
	          p.closeInventory();
	        }
	        else
	        {
	          p.sendMessage(ChatColor.YELLOW + "You dont have permission to this trail");
	          p.closeInventory();
	        }
	      }
	      if (e.getCurrentItem().getType() == Material.FLINT_AND_STEEL) {
	        if ((p.hasPermission("trail.flame")) || (p.hasPermission("trail.all")))
	        {
	          p.sendMessage(ChatColor.YELLOW + "You selected " + ChatColor.GREEN + "Flame trail");
	          plugin.cfgHandler.getCustomConfig(plugin.trailConfig).set("Players." + p.toString(), Integer.valueOf(10));
	          p.closeInventory();
	        }
	        else
	        {
	          p.sendMessage(ChatColor.YELLOW + "You dont have permission to this trail");
	          p.closeInventory();
	        }
	      }
	      if (e.getCurrentItem().getType() == Material.SPIDER_EYE) {
	        if ((p.hasPermission("trail.white")) || (p.hasPermission("trail.all")))
	        {
	          p.sendMessage(ChatColor.YELLOW + "You selected " + ChatColor.GREEN + "White Magic trail");
	          plugin.cfgHandler.getCustomConfig(plugin.trailConfig).set("Players." + p.toString(), Integer.valueOf(11));
	          p.closeInventory();
	        }
	        else
	        {
	          p.sendMessage(ChatColor.YELLOW + "You dont have permission to this trail");
	          p.closeInventory();
	        }
	      }
	      if (e.getCurrentItem().getType() == Material.RECORD_3) {
	        if ((p.hasPermission("trail.note")) || (p.hasPermission("trail.all")))
	        {
	          p.sendMessage(ChatColor.YELLOW + "You selected " + ChatColor.GREEN + "Note trail");
	          plugin.cfgHandler.getCustomConfig(plugin.trailConfig).set("Players." + p.toString(), Integer.valueOf(12));
	          p.closeInventory();
	        }
	        else
	        {
	          p.sendMessage(ChatColor.YELLOW + "You dont have permission to this trail");
	          p.closeInventory();
	        }
	      }
	      if (e.getCurrentItem().getType() == Material.SNOW_BALL) {
	        if ((p.hasPermission("trail.snow")) || (p.hasPermission("trail.all")))
	        {
	          p.sendMessage(ChatColor.YELLOW + "You selected " + ChatColor.GREEN + "Snow trail");
	          plugin.cfgHandler.getCustomConfig(plugin.trailConfig).set("Players." + p.toString(), Integer.valueOf(13));
	          p.closeInventory();
	        }
	        else
	        {
	          p.sendMessage(ChatColor.YELLOW + "You dont have permission to this trail");
	          p.closeInventory();
	        }
	      }
	      if (e.getCurrentItem().getType() == Material.WATER_BUCKET) {
	        if ((p.hasPermission("trail.water")) || (p.hasPermission("trail.all")))
	        {
	          p.sendMessage(ChatColor.YELLOW + "You selected " + ChatColor.GREEN + "Water Line trail");
	          plugin.cfgHandler.getCustomConfig(plugin.trailConfig).set("Players." + p.toString(), Integer.valueOf(14));
	          p.closeInventory();
	        }
	        else
	        {
	          p.sendMessage(ChatColor.YELLOW + "You dont have permission to this trail");
	          p.closeInventory();
	        }
	      }
	      if (e.getCurrentItem().getType() == Material.LAVA_BUCKET) {
	        if ((p.hasPermission("trail.lava")) || (p.hasPermission("trail.all")))
	        {
	          p.sendMessage(ChatColor.YELLOW + "You selected " + ChatColor.GREEN + "Lava Line trail");
	          plugin.cfgHandler.getCustomConfig(plugin.trailConfig).set("Players." + p.toString(), Integer.valueOf(15));
	          p.closeInventory();
	        }
	        else
	        {
	          p.sendMessage(ChatColor.YELLOW + "You dont have permission to this trail");
	          p.closeInventory();
	        }
	      }
	      if (e.getCurrentItem().getType() == Material.IRON_SWORD) {
	        if ((p.hasPermission("trail.crit")) || (p.hasPermission("trail.all")))
	        {
	          p.sendMessage(ChatColor.YELLOW + "You selected " + ChatColor.GREEN + "Crit trail");
	          plugin.cfgHandler.getCustomConfig(plugin.trailConfig).set("Players." + p.toString(), Integer.valueOf(16));
	          p.closeInventory();
	        }
	        else
	        {
	          p.sendMessage(ChatColor.YELLOW + "You dont have permission to this trail");
	          p.closeInventory();
	        }
	      }
	      if (e.getCurrentItem().getType() == Material.OBSIDIAN) {
	        if ((p.hasPermission("trail.smoke")) || (p.hasPermission("trail.all")))
	        {
	          p.sendMessage(ChatColor.YELLOW + "You selected " + ChatColor.GREEN + "Smoke trail");
	          plugin.cfgHandler.getCustomConfig(plugin.trailConfig).set("Players." + p.toString(), Integer.valueOf(17));
	          p.closeInventory();
	        }
	        else
	        {
	          p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + "You dont have permission to use this trail.");
	          p.closeInventory();
	        }
	      }
	    }
	  }

}
