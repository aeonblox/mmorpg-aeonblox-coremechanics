package com.aeonblox.rpg.events;

import java.text.DecimalFormat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;

import com.aeonblox.rpg.Aeonblox;

public class Runner
  extends BukkitRunnable
  implements Listener
{
	Aeonblox plugin;
  
  public void run()
  {
    for (Player player : Bukkit.getOnlinePlayers()) {
      if (player.hasPermission("aeonblox.bar"))
      {
        HeadsUpDisplay.setStatusBar(player, ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "HP " + ChatColor.LIGHT_PURPLE + getHealth(player) + " / " + getMaxHealth(player), 1);
       //player.setHealthScaled(true);
        //player.setHealthScale(20);
        /**player.setMaxHealth(50);*/
      }
    }
  }
  
  public double roundTwoDecimals(double d) {
	  
      DecimalFormat twoDForm = new DecimalFormat("#.##");
      return Double.valueOf(twoDForm.format(d));

  }
  private String getHealth(Player player)
  {
//      Location loc = ((Player)getDefender).getLocation();
//      loc.getWorld().playEffect(loc, Effect.POTION_BREAK, 9);
	  Damageable d = (Damageable) player;
    return String.valueOf(roundTwoDecimals(d.getHealth()));
  }
  
  private String getMaxHealth(Player player)
  {
	  Damageable d = (Damageable) player;
    return String.valueOf(roundTwoDecimals(d.getMaxHealth()));
  }
}
