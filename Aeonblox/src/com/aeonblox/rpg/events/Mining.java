package com.aeonblox.rpg.events;

import java.util.Arrays;
import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import com.aeonblox.rpg.Aeonblox;

public class Mining
  implements Listener
{
  Aeonblox plugin;
  
  public Mining(Aeonblox instance)
  {
    this.plugin = instance;
  }
  
  @EventHandler
  public void onInteract(PlayerInteractEvent e)
  {
    Player p = e.getPlayer(); //(isWeapon(p.getItemInHand().getType()))
    if ((e.getAction() == Action.LEFT_CLICK_BLOCK) && 
    		(isTool(p.getItemInHand().getType()))) {
      if (e.getClickedBlock().getType() == Material.IRON_ORE) {
        p.addPotionEffect(new PotionEffect(
          PotionEffectType.SLOW_DIGGING, 40, 0), true);
      } else if (e.getClickedBlock().getType() == Material.DIAMOND_ORE) {
        p.addPotionEffect(new PotionEffect(
          PotionEffectType.SLOW_DIGGING, 80, 2), true);
      } else if (e.getClickedBlock().getType() == Material.GOLD_ORE) {
        p.addPotionEffect(new PotionEffect(
          PotionEffectType.SLOW_DIGGING, 160, 3), true);
      }
    }
  }
  
  @SuppressWarnings("deprecation")
@EventHandler
  public void onBreak(final BlockBreakEvent e)
  {
    Player p = e.getPlayer();
    if (p.getItemInHand().getType() == Material.GOLD_PICKAXE) {
   /** int chance = getChance(p.getItemInHand().getType()); //named it wrong, sorry about taht
    int rand = new Random().nextInt(100);
    if (!(rand < chance)){
    	return;
    	//and for the gold pick it would be 75% chance, so it would be more likely to be below that, right? 
    }*/
    if (e.getBlock().getType() == Material.IRON_ORE)
    {
      e.setCancelled(true);
      Random random = new Random();
      int amt = random.nextInt(9) + 8;
      ItemStack Blox = new ItemStack(Material.GOLD_NUGGET, amt);
      ItemMeta im = Blox.getItemMeta();
      im.setDisplayName(ChatColor.WHITE + "Blox");
      im.setLore(Arrays.asList(new String[] {ChatColor.GRAY + 
        "The currency of Aeonblox" }));
      Blox.setItemMeta(im);
      p.getWorld().dropItemNaturally(e.getBlock().getLocation(), Blox);
      p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + 
        "          Found " + amt + " Blox(s)");
      int treasure = random.nextInt(75) + 1;
      if (treasure == 1)
      {
        p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + 
          "          Found 1x " + ChatColor.LIGHT_PURPLE + 
          "Orb of Alteration" + ChatColor.YELLOW + 
          ChatColor.BOLD + " embedded in the ore");
        ItemStack orb = new ItemStack(Material.MAGMA_CREAM);
        ItemMeta orbmeta = orb.getItemMeta();
        orbmeta.setDisplayName(ChatColor.LIGHT_PURPLE + 
          "Orb of Alteration");
        orbmeta.setLore(Arrays.asList(new String[] {ChatColor.GRAY + 
          "Randomizes stats of selected equipment." }));
        orb.setItemMeta(orbmeta);
        p.getWorld().dropItemNaturally(e.getBlock().getLocation(), orb);
      }
      else if (treasure == 2)
      {
        p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + 
          "          Found 1x " + ChatColor.WHITE + 
          ChatColor.BOLD + "Scroll:" + ChatColor.AQUA + 
          " Enchant Iron Armor" + ChatColor.YELLOW + 
          ChatColor.BOLD + " embedded in the ore");
        ItemStack scroll = new ItemStack(Material.EMPTY_MAP);
        ItemMeta scrollmeta = scroll.getItemMeta();
        scrollmeta.setDisplayName(ChatColor.WHITE + "" + ChatColor.BOLD + 
          "Scroll:" + ChatColor.AQUA + " Enchant Iron Armor");
        scrollmeta
          .setLore(
          Arrays.asList(new String[] { ChatColor.RED + "+5% HP", 
          ChatColor.RED + "+5% HP REGEN", 
          ChatColor.GRAY + "   - OR -", 
          ChatColor.RED + "+5% VIT", 
          
          ChatColor.GRAY + "" + 
          ChatColor.ITALIC + 
          "Armor will VANISH if enchant above +3 FAILS." }));
        scroll.setItemMeta(scrollmeta);
        p.getWorld().dropItemNaturally(e.getBlock().getLocation(), 
          scroll);
      }
      else if (treasure == 3)
      {
        p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + 
          "          Found 1x " + ChatColor.WHITE + 
          ChatColor.BOLD + "Scroll:" + ChatColor.AQUA + 
          " Enchant Iron Weapon" + ChatColor.YELLOW + 
          ChatColor.BOLD + " embedded in the ore");
        ItemStack scroll = new ItemStack(Material.EMPTY_MAP);
        ItemMeta scrollmeta = scroll.getItemMeta();
        scrollmeta.setDisplayName(ChatColor.WHITE + "" + ChatColor.BOLD + 
          "Scroll:" + ChatColor.AQUA + " Enchant Iron Weapon");
        scrollmeta.setLore(Arrays.asList(new String[] { ChatColor.RED + "+5% DMG", 
          ChatColor.GRAY + "" + ChatColor.ITALIC + 
          "Weapon will VANISH if enchant above +3 FAILS." }));
        scroll.setItemMeta(scrollmeta);
        p.getWorld().dropItemNaturally(e.getBlock().getLocation(), 
          scroll);
      }
      e.getBlock().setType(Material.STONE);
      new BukkitRunnable()
      {
        public void run()
        {
          e.getBlock().setType(Material.IRON_ORE);
        }
      }.runTaskLater(this.plugin, 1200L);
      p.getItemInHand().setDurability((short)0);
      p.updateInventory();
    }
    else if (e.getBlock().getType() == Material.DIAMOND_ORE)
    {
      e.setCancelled(true);
      Random random = new Random();
      int amt = random.nextInt(17) + 16;
      ItemStack Blox = new ItemStack(Material.GOLD_NUGGET, amt);
      ItemMeta im = Blox.getItemMeta();
      im.setDisplayName(ChatColor.WHITE + "Blox");
      im.setLore(Arrays.asList(new String[] {ChatColor.GRAY + 
        "The currency of Aeonblox" }));
      Blox.setItemMeta(im);
      p.getWorld().dropItemNaturally(e.getBlock().getLocation(), Blox);
      int treasure = random.nextInt(75) + 1;
      if (treasure == 1)
      {
        p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + 
          "          Found 1x " + ChatColor.LIGHT_PURPLE + 
          "Orb of Alteration" + ChatColor.YELLOW + 
          ChatColor.BOLD + " embedded in the ore");
        ItemStack orb = new ItemStack(Material.MAGMA_CREAM);
        ItemMeta orbmeta = orb.getItemMeta();
        orbmeta.setDisplayName(ChatColor.LIGHT_PURPLE + 
          "Orb of Alteration");
        orbmeta.setLore(Arrays.asList(new String[] {ChatColor.GRAY + 
          "Randomizes stats of selected equipment." }));
        orb.setItemMeta(orbmeta);
        p.getWorld().dropItemNaturally(e.getBlock().getLocation(), orb);
      }
      else if (treasure == 2)
      {
        p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + 
          "          Found 1x " + ChatColor.WHITE + 
          ChatColor.BOLD + "Scroll:" + ChatColor.LIGHT_PURPLE + 
          " Enchant Diamond Armor" + ChatColor.YELLOW + 
          ChatColor.BOLD + " embedded in the ore");
        ItemStack scroll = new ItemStack(Material.EMPTY_MAP);
        ItemMeta scrollmeta = scroll.getItemMeta();
        scrollmeta.setDisplayName(ChatColor.WHITE + "" + ChatColor.BOLD + 
          "Scroll:" + ChatColor.LIGHT_PURPLE + 
          " Enchant Diamond Armor");
        scrollmeta
          .setLore(
          Arrays.asList(new String[] { ChatColor.RED + "+5% HP", 
          ChatColor.RED + "+5% HP REGEN", 
          ChatColor.GRAY + "   - OR -", 
          ChatColor.RED + "+5% VIT", 
          
          ChatColor.GRAY + "" +
          ChatColor.ITALIC + 
          "Armor will VANISH if enchant above +3 FAILS." }));
        scroll.setItemMeta(scrollmeta);
        p.getWorld().dropItemNaturally(e.getBlock().getLocation(), 
          scroll);
      }
      else if (treasure == 3)
      {
        p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + 
          "          Found 1x " + ChatColor.WHITE + 
          ChatColor.BOLD + "Scroll:" + ChatColor.LIGHT_PURPLE + 
          " Enchant Diamond Weapon" + ChatColor.YELLOW + 
          ChatColor.BOLD + " embedded in the ore");
        ItemStack scroll = new ItemStack(Material.EMPTY_MAP);
        ItemMeta scrollmeta = scroll.getItemMeta();
        scrollmeta.setDisplayName(ChatColor.WHITE + "" + ChatColor.BOLD + 
          "Scroll:" + ChatColor.LIGHT_PURPLE + 
          " Enchant Diamond Weapon");
        scrollmeta.setLore(Arrays.asList(new String[] { ChatColor.RED + "+5% DMG", 
          ChatColor.GRAY + "" + ChatColor.ITALIC + 
          "Weapon will VANISH if enchant above +3 FAILS." }));
        scroll.setItemMeta(scrollmeta);
        p.getWorld().dropItemNaturally(e.getBlock().getLocation(), 
          scroll);
      }
      p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + 
        "          Found " + amt + " Blox(s)");
      e.getBlock().setType(Material.STONE);
      new BukkitRunnable()
      {
        public void run()
        {
          e.getBlock().setType(Material.DIAMOND_ORE);
        }
      }.runTaskLater(this.plugin, 2400L);
      p.getItemInHand().setDurability((short)0);
      p.updateInventory();
    }
    else if (e.getBlock().getType() == Material.GOLD_ORE)
    {
      e.setCancelled(true);
      Random random = new Random();
      int fail = random.nextInt(4) + 1;
      if (fail == 1)
      {
        p.sendMessage(ChatColor.GRAY + "" + ChatColor.ITALIC + 
          "You failed to find any Blox.");
      }
      else
      {
        int amt = random.nextInt(17) + 32;
        ItemStack Blox = new ItemStack(Material.GOLD_NUGGET, amt);
        ItemMeta im = Blox.getItemMeta();
        im.setDisplayName(ChatColor.WHITE + "Blox");
        im.setLore(Arrays.asList(new String[] {ChatColor.GRAY + 
          "The currency of Aeonblox" }));
        Blox.setItemMeta(im);
        p.getWorld().dropItemNaturally(e.getBlock().getLocation(), Blox);
        p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + 
          "          Found " + amt + " Blox(s)");
        int treasure = random.nextInt(75) + 1;
        if (treasure == 1)
        {
          p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + 
            "          Found 1x " + ChatColor.LIGHT_PURPLE + 
            "Orb of Alteration" + ChatColor.YELLOW + 
            ChatColor.BOLD + " embedded in the ore");
          ItemStack orb = new ItemStack(Material.MAGMA_CREAM);
          ItemMeta orbmeta = orb.getItemMeta();
          orbmeta.setDisplayName(ChatColor.LIGHT_PURPLE + 
            "Orb of Alteration");
          orbmeta.setLore(Arrays.asList(new String[] {ChatColor.GRAY + 
            "Randomizes stats of selected equipment." }));
          orb.setItemMeta(orbmeta);
          p.getWorld().dropItemNaturally(e.getBlock().getLocation(), 
            orb);
        }
        else if (treasure == 2)
        {
          p.sendMessage(ChatColor.YELLOW + "" +  ChatColor.BOLD + 
            "          Found 1x " + ChatColor.WHITE + 
            ChatColor.BOLD + "Scroll:" + ChatColor.YELLOW + 
            " Enchant Gold Armor" + ChatColor.YELLOW + 
            ChatColor.BOLD + " embedded in the ore");
          ItemStack scroll = new ItemStack(Material.EMPTY_MAP);
          ItemMeta scrollmeta = scroll.getItemMeta();
          scrollmeta.setDisplayName(ChatColor.WHITE + "" +
            ChatColor.BOLD + "Scroll:" + ChatColor.YELLOW + 
            " Enchant Gold Armor");
          scrollmeta
            .setLore(
            Arrays.asList(new String[] { ChatColor.RED + "+5% HP", 
            ChatColor.RED + "+5% HP REGEN", 
            ChatColor.GRAY + "   - OR -", 
            ChatColor.RED + "+5% VIT", 
            
            ChatColor.GRAY + "" +
            ChatColor.ITALIC + 
            "Armor will VANISH if enchant above +3 FAILS." }));
          scroll.setItemMeta(scrollmeta);
          p.getWorld().dropItemNaturally(e.getBlock().getLocation(), 
            scroll);
        }
        else if (treasure == 3)
        {
          p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + 
            "          Found 1x " + ChatColor.WHITE + 
            ChatColor.BOLD + "Scroll:" + ChatColor.YELLOW + 
            " Enchant Gold Weapon" + ChatColor.YELLOW + 
            ChatColor.BOLD + " embedded in the ore");
          ItemStack scroll = new ItemStack(Material.EMPTY_MAP);
          ItemMeta scrollmeta = scroll.getItemMeta();
          scrollmeta.setDisplayName(ChatColor.WHITE + "" +
            ChatColor.BOLD + "Scroll:" + ChatColor.YELLOW + 
            " Enchant Gold Weapon");
          scrollmeta
            .setLore(
            Arrays.asList(new String[] { ChatColor.RED + "+5% DMG", 
            
            ChatColor.GRAY +  "" +
            ChatColor.ITALIC + 
            "Weapon will VANISH if enchant above +3 FAILS." }));
          scroll.setItemMeta(scrollmeta);
          p.getWorld().dropItemNaturally(e.getBlock().getLocation(), 
            scroll);
        }
      }
      e.getBlock().setType(Material.STONE);
      new BukkitRunnable()
      {
        public void run()
        {
          e.getBlock().setType(Material.GOLD_ORE);
        }
      }.runTaskLater(this.plugin, 6000L);
      p.getItemInHand().setDurability((short)0);
      p.updateInventory();
    }
  }else
	  // From here, the drop PERCENTILE GOES DOWN NOT THE AMOUNT // THE AMOUNT IS AFTER
	  if (p.getItemInHand().getType() == Material.DIAMOND_PICKAXE) {
		    if (e.getBlock().getType() == Material.IRON_ORE)
		    {
		      e.setCancelled(true);
		      Random random = new Random();
		      int amt = random.nextInt(4) + 5;
		      ItemStack Blox = new ItemStack(Material.GOLD_NUGGET, amt);
		      ItemMeta im = Blox.getItemMeta();
		      im.setDisplayName(ChatColor.WHITE + "Blox");
		      im.setLore(Arrays.asList(new String[] {ChatColor.GRAY + 
		        "The currency of Aeonblox" }));
		      Blox.setItemMeta(im);
		      p.getWorld().dropItemNaturally(e.getBlock().getLocation(), Blox);
		      p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + 
		        "          Found " + amt + " Blox(s)");
		      int treasure = random.nextInt(115) + 1;
		      if (treasure == 1)
		      {
		        p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + 
		          "          Found 1x " + ChatColor.LIGHT_PURPLE + 
		          "Orb of Alteration" + ChatColor.YELLOW + 
		          ChatColor.BOLD + " embedded in the ore");
		        ItemStack orb = new ItemStack(Material.MAGMA_CREAM);
		        ItemMeta orbmeta = orb.getItemMeta();
		        orbmeta.setDisplayName(ChatColor.LIGHT_PURPLE + 
		          "Orb of Alteration");
		        orbmeta.setLore(Arrays.asList(new String[] {ChatColor.GRAY + 
		          "Randomizes stats of selected equipment." }));
		        orb.setItemMeta(orbmeta);
		        p.getWorld().dropItemNaturally(e.getBlock().getLocation(), orb);
		      }
		      else if (treasure == 2)
		      {
		        p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + 
		          "          Found 1x " + ChatColor.WHITE + 
		          ChatColor.BOLD + "Scroll:" + ChatColor.AQUA + 
		          " Enchant Iron Armor" + ChatColor.YELLOW + 
		          ChatColor.BOLD + " embedded in the ore");
		        ItemStack scroll = new ItemStack(Material.EMPTY_MAP);
		        ItemMeta scrollmeta = scroll.getItemMeta();
		        scrollmeta.setDisplayName(ChatColor.WHITE + "" + ChatColor.BOLD + 
		          "Scroll:" + ChatColor.AQUA + " Enchant Iron Armor");
		        scrollmeta
		          .setLore(
		          Arrays.asList(new String[] { ChatColor.RED + "+5% HP", 
		          ChatColor.RED + "+5% HP REGEN", 
		          ChatColor.GRAY + "   - OR -", 
		          ChatColor.RED + "+5% VIT", 
		          
		          ChatColor.GRAY + "" + 
		          ChatColor.ITALIC + 
		          "Armor will VANISH if enchant above +3 FAILS." }));
		        scroll.setItemMeta(scrollmeta);
		        p.getWorld().dropItemNaturally(e.getBlock().getLocation(), 
		          scroll);
		      }
		      else if (treasure == 3)
		      {
		        p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + 
		          "          Found 1x " + ChatColor.WHITE + 
		          ChatColor.BOLD + "Scroll:" + ChatColor.AQUA + 
		          " Enchant Iron Weapon" + ChatColor.YELLOW + 
		          ChatColor.BOLD + " embedded in the ore");
		        ItemStack scroll = new ItemStack(Material.EMPTY_MAP);
		        ItemMeta scrollmeta = scroll.getItemMeta();
		        scrollmeta.setDisplayName(ChatColor.WHITE + "" + ChatColor.BOLD + 
		          "Scroll:" + ChatColor.AQUA + " Enchant Iron Weapon");
		        scrollmeta.setLore(Arrays.asList(new String[] { ChatColor.RED + "+5% DMG", 
		          ChatColor.GRAY + "" + ChatColor.ITALIC + 
		          "Weapon will VANISH if enchant above +3 FAILS." }));
		        scroll.setItemMeta(scrollmeta);
		        p.getWorld().dropItemNaturally(e.getBlock().getLocation(), 
		          scroll);
		      }
		      e.getBlock().setType(Material.STONE);
		      new BukkitRunnable()
		      {
		        public void run()
		        {
		          e.getBlock().setType(Material.IRON_ORE);
		        }
		      }.runTaskLater(this.plugin, 1200L);
		      p.getItemInHand().setDurability((short)0);
		      p.updateInventory();
		    }
		    
		    
		    
		    
		    else if (e.getBlock().getType() == Material.DIAMOND_ORE)
		    {
		      e.setCancelled(true);
		      Random random = new Random();
		      int amt = random.nextInt(7) + 6;
		      ItemStack Blox = new ItemStack(Material.GOLD_NUGGET, amt);
		      ItemMeta im = Blox.getItemMeta();
		      im.setDisplayName(ChatColor.WHITE + "Blox");
		      im.setLore(Arrays.asList(new String[] {ChatColor.GRAY + 
		        "The currency of Aeonblox" }));
		      Blox.setItemMeta(im);
		      p.getWorld().dropItemNaturally(e.getBlock().getLocation(), Blox);
		      int treasure = random.nextInt(130) + 1;
		      if (treasure == 1)
		      {
		        p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + 
		          "          Found 1x " + ChatColor.LIGHT_PURPLE + 
		          "Orb of Alteration" + ChatColor.YELLOW + 
		          ChatColor.BOLD + " embedded in the ore");
		        ItemStack orb = new ItemStack(Material.MAGMA_CREAM);
		        ItemMeta orbmeta = orb.getItemMeta();
		        orbmeta.setDisplayName(ChatColor.LIGHT_PURPLE + 
		          "Orb of Alteration");
		        orbmeta.setLore(Arrays.asList(new String[] {ChatColor.GRAY + 
		          "Randomizes stats of selected equipment." }));
		        orb.setItemMeta(orbmeta);
		        p.getWorld().dropItemNaturally(e.getBlock().getLocation(), orb);
		      }
		      else if (treasure == 2)
		      {
		        p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + 
		          "          Found 1x " + ChatColor.WHITE + 
		          ChatColor.BOLD + "Scroll:" + ChatColor.LIGHT_PURPLE + 
		          " Enchant Diamond Armor" + ChatColor.YELLOW + 
		          ChatColor.BOLD + " embedded in the ore");
		        ItemStack scroll = new ItemStack(Material.EMPTY_MAP);
		        ItemMeta scrollmeta = scroll.getItemMeta();
		        scrollmeta.setDisplayName(ChatColor.WHITE + "" + ChatColor.BOLD + 
		          "Scroll:" + ChatColor.LIGHT_PURPLE + 
		          " Enchant Diamond Armor");
		        scrollmeta
		          .setLore(
		          Arrays.asList(new String[] { ChatColor.RED + "+5% HP", 
		          ChatColor.RED + "+5% HP REGEN", 
		          ChatColor.GRAY + "   - OR -", 
		          ChatColor.RED + "+5% VIT", 
		          
		          ChatColor.GRAY + "" +
		          ChatColor.ITALIC + 
		          "Armor will VANISH if enchant above +3 FAILS." }));
		        scroll.setItemMeta(scrollmeta);
		        p.getWorld().dropItemNaturally(e.getBlock().getLocation(), 
		          scroll);
		      }
		      else if (treasure == 3)
		      {
		        p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + 
		          "          Found 1x " + ChatColor.WHITE + 
		          ChatColor.BOLD + "Scroll:" + ChatColor.LIGHT_PURPLE + 
		          " Enchant Diamond Weapon" + ChatColor.YELLOW + 
		          ChatColor.BOLD + " embedded in the ore");
		        ItemStack scroll = new ItemStack(Material.EMPTY_MAP);
		        ItemMeta scrollmeta = scroll.getItemMeta();
		        scrollmeta.setDisplayName(ChatColor.WHITE + "" + ChatColor.BOLD + 
		          "Scroll:" + ChatColor.LIGHT_PURPLE + 
		          " Enchant Diamond Weapon");
		        scrollmeta.setLore(Arrays.asList(new String[] { ChatColor.RED + "+5% DMG", 
		          ChatColor.GRAY + "" + ChatColor.ITALIC + 
		          "Weapon will VANISH if enchant above +3 FAILS." }));
		        scroll.setItemMeta(scrollmeta);
		        p.getWorld().dropItemNaturally(e.getBlock().getLocation(), 
		          scroll);
		      }
		      p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + 
		        "          Found " + amt + " Blox(s)");
		      e.getBlock().setType(Material.STONE);
		      new BukkitRunnable()
		      {
		        public void run()
		        {
		          e.getBlock().setType(Material.DIAMOND_ORE);
		        }
		      }.runTaskLater(this.plugin, 2400L);
		      p.getItemInHand().setDurability((short)0);
		      p.updateInventory();
		    }
		    else if (e.getBlock().getType() == Material.GOLD_ORE)
		    {
		      e.setCancelled(true);
		      Random random = new Random();
		      int fail = random.nextInt(4) + 1;
		      if (fail < 2)
		      {
		        p.sendMessage(ChatColor.GRAY + "" + ChatColor.ITALIC + 
		          "You failed to find any Blox.");
		      }
		      else
		      {
		        int amt = random.nextInt(12) + 8;
		        ItemStack Blox = new ItemStack(Material.GOLD_NUGGET, amt);
		        ItemMeta im = Blox.getItemMeta();
		        im.setDisplayName(ChatColor.WHITE + "Blox");
		        im.setLore(Arrays.asList(new String[] {ChatColor.GRAY + 
		          "The currency of Aeonblox" }));
		        Blox.setItemMeta(im);
		        p.getWorld().dropItemNaturally(e.getBlock().getLocation(), Blox);
		        p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + 
		          "          Found " + amt + " Blox(s)");
		        int treasure = random.nextInt(145) + 1;
		        if (treasure == 1)
		        {
		          p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + 
		            "          Found 1x " + ChatColor.LIGHT_PURPLE + 
		            "Orb of Alteration" + ChatColor.YELLOW + 
		            ChatColor.BOLD + " embedded in the ore");
		          ItemStack orb = new ItemStack(Material.MAGMA_CREAM);
		          ItemMeta orbmeta = orb.getItemMeta();
		          orbmeta.setDisplayName(ChatColor.LIGHT_PURPLE + 
		            "Orb of Alteration");
		          orbmeta.setLore(Arrays.asList(new String[] {ChatColor.GRAY + 
		            "Randomizes stats of selected equipment." }));
		          orb.setItemMeta(orbmeta);
		          p.getWorld().dropItemNaturally(e.getBlock().getLocation(), 
		            orb);
		        }
		        else if (treasure == 2)
		        {
		          p.sendMessage(ChatColor.YELLOW + "" +  ChatColor.BOLD + 
		            "          Found 1x " + ChatColor.WHITE + 
		            ChatColor.BOLD + "Scroll:" + ChatColor.YELLOW + 
		            " Enchant Gold Armor" + ChatColor.YELLOW + 
		            ChatColor.BOLD + " embedded in the ore");
		          ItemStack scroll = new ItemStack(Material.EMPTY_MAP);
		          ItemMeta scrollmeta = scroll.getItemMeta();
		          scrollmeta.setDisplayName(ChatColor.WHITE + "" +
		            ChatColor.BOLD + "Scroll:" + ChatColor.YELLOW + 
		            " Enchant Gold Armor");
		          scrollmeta
		            .setLore(
		            Arrays.asList(new String[] { ChatColor.RED + "+5% HP", 
		            ChatColor.RED + "+5% HP REGEN", 
		            ChatColor.GRAY + "   - OR -", 
		            ChatColor.RED + "+5% VIT", 
		            
		            ChatColor.GRAY + "" +
		            ChatColor.ITALIC + 
		            "Armor will VANISH if enchant above +3 FAILS." }));
		          scroll.setItemMeta(scrollmeta);
		          p.getWorld().dropItemNaturally(e.getBlock().getLocation(), 
		            scroll);
		        }
		        else if (treasure == 3)
		        {
		          p.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + 
		            "          Found 1x " + ChatColor.WHITE + 
		            ChatColor.BOLD + "Scroll:" + ChatColor.YELLOW + 
		            " Enchant Gold Weapon" + ChatColor.YELLOW + 
		            ChatColor.BOLD + " embedded in the ore");
		          ItemStack scroll = new ItemStack(Material.EMPTY_MAP);
		          ItemMeta scrollmeta = scroll.getItemMeta();
		          scrollmeta.setDisplayName(ChatColor.WHITE + "" +
		            ChatColor.BOLD + "Scroll:" + ChatColor.YELLOW + 
		            " Enchant Gold Weapon");
		          scrollmeta
		            .setLore(
		            Arrays.asList(new String[] { ChatColor.RED + "+5% DMG", 
		            
		            ChatColor.GRAY +  "" +
		            ChatColor.ITALIC + 
		            "Weapon will VANISH if enchant above +3 FAILS." }));
		          scroll.setItemMeta(scrollmeta);
		          p.getWorld().dropItemNaturally(e.getBlock().getLocation(), 
		            scroll);
		        }
		      }
		      e.getBlock().setType(Material.STONE);
		      new BukkitRunnable()
		      {
		        public void run()
		        {
		          e.getBlock().setType(Material.GOLD_ORE);
		        }
		      }.runTaskLater(this.plugin, 6000L);
		      p.getItemInHand().setDurability((short)0);
		      p.updateInventory();
		    }
  }else
	  if (p.getItemInHand().getType() == Material.IRON_PICKAXE) {
		  
	  }else
		  if (p.getItemInHand().getType() == Material.STONE_PICKAXE) {
			  
		  }else
			  if (p.getItemInHand().getType() == Material.WOOD_PICKAXE) {
				  
			  }
            
}
  
	 public boolean isTool(Material material) {
		 switch(material) {
		 case GOLD_PICKAXE:
		 case DIAMOND_PICKAXE:
		 case IRON_PICKAXE:
		 case STONE_PICKAXE:
		 case WOOD_PICKAXE:
		 case GOLD_SPADE:
		 case DIAMOND_SPADE:
		 case IRON_SPADE:
		 case STONE_SPADE:
		 case WOOD_SPADE:
			 return true;
			 default:
			 return false;
			 }
			 }
	 public int getChance(Material mat){
		 int amp = 0;
		 if (mat == Material.GOLD_PICKAXE){
			 return 75;
		 }
		 if (mat == Material.IRON_PICKAXE){
			 return 50;
		 }
		 return amp;
	 }
}