package com.aeonblox.rpg.events;

import java.util.Random;

import org.bukkit.Effect;
import org.bukkit.EntityEffect;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.aeonblox.rpg.Aeonblox;

public class TPListener
  implements Listener
{
  Aeonblox plugin;
  
  public TPListener(Aeonblox instance)
  {
    this.plugin = instance;
  }
  
  @EventHandler
  public void onPlayerTeleport(PlayerTeleportEvent event)
  {
    if ((event.getTo() == null) || (event.getFrom() == null) || (event.getPlayer() == null)) {
      return;
    }
    Player p = event.getPlayer();
    
    if (p == null) {
      return;
    }
    if ((event.getCause() == PlayerTeleportEvent.TeleportCause.UNKNOWN)) {
      return;
    }
    Location from = event.getFrom().clone();
    final Location to = event.getTo().clone();
    
    boolean spawnParticles = p.hasPermission("aeonblox.particles");
    boolean playSound = p.hasPermission("aeonblox.sounds");
    
    boolean forceParticles = false;
    boolean forceSound = false;
    boolean denyParticles = false;
    boolean denySound = false;
    if ((event.getCause() == PlayerTeleportEvent.TeleportCause.COMMAND))
    {
        Location l = p.getLocation();
        l.setY(400);
 /*       final LivingEntity kludgeE = (LivingEntity) p.getWorld().spawnEntity(l, EntityType.WOLF);
        kludgeE.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY,100,100));
        kludgeE.teleport(p.getLocation());

        plugin.getServer().getScheduler().runTaskLater(plugin,  new Runnable() {
            @Override
            public void run()
            {
                kludgeE.playEffect(EntityEffect.WOLF_HEARTS);
                kludgeE.remove();
            }
        },3); */
        
    String fromWorldName = event.getFrom().getWorld().getName();
    String toWorldName = event.getTo().getWorld().getName();
//  playSound = (!denySound) && ((forceSound) || (playSound));
    p.playSound(p.getLocation(), Sound.WITHER_SHOOT, 1.0F, -0.0F);   
    spawnParticles = (!denyParticles) && ((forceParticles) || (spawnParticles));
    if (playSound) {
      if (from.getWorld().equals(to.getWorld()))
      {
        if (from.distanceSquared(to) >= 2)
        {
          from.getWorld().playSound(from, Sound.ENDERMAN_TELEPORT, 0.3F, 1.0F);
          
          this.plugin.getServer().getScheduler().scheduleSyncDelayedTask(this.plugin, new Runnable()
          {
            public void run()
            {
              to.getWorld().playSound(to, Sound.ENDERMAN_TELEPORT, 0.3F, 1.0F);
            }
          }, from.distance(to) >= 5.0D ? 1L : 0L);
        }
      }
      else
      {
        from.getWorld().playSound(from, Sound.ENDERMAN_TELEPORT, 0.3F, 1.0F);
        
        this.plugin.getServer().getScheduler().scheduleSyncDelayedTask(this.plugin, new Runnable()
        {
          public void run()
          {
            to.getWorld().playSound(to, Sound.ENDERMAN_TELEPORT, 0.3F, 1.0F);
          }
        }, 1L);
      }
    }
    if (spawnParticles) {
      if ((!from.getWorld().equals(to.getWorld())) || (from.distanceSquared(to) >= 2))
      {
        spawnSmoke(from, 5.0F);
        spawnSmoke(from.add(0.0D, 1.0D, 0.0D), 5.0F);
        from.getWorld().playEffect(from, Effect.MOBSPAWNER_FLAMES, null);
        
        this.plugin.getServer().getScheduler().scheduleSyncDelayedTask(this.plugin, new Runnable()
        {
          public void run()
          {
            for (int i = 0; i < 3; i++)
            {
              to.getWorld().playEffect(to, Effect.ENDER_SIGNAL, null);
              to.getWorld().playEffect(to, Effect.ENDER_SIGNAL, null);
              to.add(0.0D, 1.0D, 0.0D);
            }
          }
        }, 1L);
      }
    }
    }
  }
  
  public static Random random = new Random();
  
  public static void spawnSmoke(Location location, float thickness)
  {
    int singles = (int)Math.floor(thickness * 9.0F);
    for (int i = 0; i < singles; i++)
    {
      if (location == null) {
        return;
      }
      location.getWorld().playEffect(location, Effect.SMOKE, random.nextInt(9));
    }
  }
}
