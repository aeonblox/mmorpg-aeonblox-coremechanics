package com.aeonblox.rpg.events;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Enderman;
import org.bukkit.entity.Sheep;
import org.bukkit.entity.Silverfish;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.EntityBlockFormEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.hanging.HangingBreakEvent;

import com.aeonblox.rpg.Aeonblox;

public class NoEntityGrief
  implements Listener
{
  public List<String>[] EWs = new ArrayList[6];
  
  Aeonblox plugin;
  public NoEntityGrief(Aeonblox instance) {
	  this.plugin = instance;
  }
  
  @EventHandler
  public void onBlockChange(EntityChangeBlockEvent e)
  {
    if (((e.getEntity() instanceof Enderman)) && ((this.EWs[0] == null) || (!this.EWs[0].contains(e.getBlock().getLocation().getWorld().getName()))))
    {
      e.setCancelled(true);
    }
    else if (((e.getEntity() instanceof Silverfish)) && ((this.EWs[5] == null) || (!this.EWs[5].contains(e.getBlock().getLocation().getWorld().getName()))))
    {
      e.setCancelled(true);
    }
    else if (((e.getEntity() instanceof Sheep)) && ((this.EWs[1] == null) || (!this.EWs[1].contains(e.getBlock().getLocation().getWorld().getName()))))
    {
      e.setCancelled(true);
      ((Sheep)e.getEntity()).setSheared(false);
    }
  }
  
  @EventHandler
  public void onExplosion(EntityExplodeEvent e)
  {
    if ((e.getEntity() instanceof TNTPrimed))
    {
      if ((this.EWs[3] == null) || (!this.EWs[3].contains(e.getLocation().getWorld().getName()))) {
        e.blockList().clear();
      }
    }
    else if ((this.EWs[2] == null) || (!this.EWs[2].contains(e.getLocation().getWorld().getName()))) {
      e.blockList().clear();
    }
  }
  
  @EventHandler
  public void onPaintingBreak(HangingBreakEvent e)
  {
    if ((e.getCause() == HangingBreakEvent.RemoveCause.EXPLOSION) && ((e.getEntity() instanceof TNTPrimed)))
    {
      if ((this.EWs[3] == null) || (!this.EWs[3].contains(e.getEntity().getWorld().getName()))) {
        e.setCancelled(true);
      }
    }
    else if ((e.getCause() == HangingBreakEvent.RemoveCause.EXPLOSION) && ((this.EWs[2] == null) || (!this.EWs[2].contains(e.getEntity().getWorld().getName())))) {
      e.setCancelled(true);
    }
  }
  
  @EventHandler
  public void onEntityForm(EntityBlockFormEvent e)
  {
    if ((this.EWs[4] == null) || (!this.EWs[4].contains(e.getBlock().getLocation().getWorld().getName()))) {
      e.setCancelled(true);
    }
  }
}
