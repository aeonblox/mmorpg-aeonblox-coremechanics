package com.aeonblox.rpg.events;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import com.aeonblox.rpg.Aeonblox;
import com.aeonblox.rpg.utils.StringUtils;

public class WhitelistMessage
  implements Listener, CommandExecutor
{
  public static String whitelistMessage;
  WhitelistPlayerListener playerListener;
  Aeonblox plugin;
  
  public WhitelistMessage(Aeonblox instance) {
	  this.plugin = instance;
  }
  
  public boolean onCommand(CommandSender sender, Command command, String c, String[] args)
  {
    if (sender.hasPermission("aeonblox.whitelistmsg")) {
    if (command.getName().equalsIgnoreCase("whitelistmessage")) {
      if (args.length > 0)
      {
        if (args[0].equalsIgnoreCase("reload"))
        {
        plugin.cfgHandler.reloadCustomConfig(plugin.whitelistConfig);
          if ((sender instanceof Player)) {
            sender.sendMessage("[Aeonblox] WhitelistMessage >> Config reloaded!");
          }
          System.out.println(ChatColor.GRAY + "WhitelistMechanics >> Config reloaded!");
        }
        else if (args[0].equalsIgnoreCase("set"))
        {
          if (args.length > 1)
          {
            String message = "";
            for (int i = 1; i < args.length; i++) {
              message = message + args[i] + " ";
            }
            whitelistMessage = StringUtils.replaceChatColors(message.trim());
            if ((sender instanceof Player)) {
              sender.sendMessage("[Aeonblox] Whitelist message set!");
            }
            System.out.println("Whitelist message set!");
            
            plugin.cfgHandler.saveCustomConfig(plugin.whitelistConfig);
          }
          else
          {
            sender.sendMessage("[Aeonblox] Invalid argument.  Please provide a valid whitelist message. /whitelistmessage set <message>");
          }
        }
        else if (args[0].equalsIgnoreCase("help"))
        {
          sender.sendMessage(ChatColor.GRAY + "[Aeonblox] You can reload the config from file using /whitelistmessage reload");
          sender.sendMessage(ChatColor.GRAY + "You can set the whitelist message using /whitelistmessage set <message>");
        }
        return true;
      }
    }
  }
	return false;
  }
}
