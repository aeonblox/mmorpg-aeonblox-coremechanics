package com.aeonblox.rpg.events;

import java.util.ArrayList;
import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import com.aeonblox.rpg.Aeonblox;

public class BookClicks
  implements Listener
{
  public static ArrayList<String> pls = new ArrayList();
  Aeonblox plugin;
  
	public BookClicks(Aeonblox plugin){	
		this.plugin = plugin;
	}
  
  
  PotionEffect confusion = new PotionEffect(PotionEffectType.CONFUSION, 150, 3);
  PotionEffect blindness = new PotionEffect(PotionEffectType.BLINDNESS, 150, 3);
  
  @EventHandler(ignoreCancelled=true)
  public void click(final PlayerInteractEvent e)
  {
    Material material = Material.EMERALD;
    int ammount = 1;
    String name = ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "Teleportation Rune";
    
    ItemStack item = new ItemStack(material, ammount);
    ItemMeta itemMeta = item.getItemMeta();
    itemMeta.setDisplayName(name);
    if (!e.getPlayer().hasPermission("aeonblox.rune.donor")) {
      itemMeta.setLore(Arrays.asList(new String[] { ChatColor.GREEN + "Left Click:" + ChatColor.GRAY + " Teleports to Skyte", ChatColor.GREEN + "Sneak-Left Click:" + ChatColor.GRAY + " Set-home at your current position", ChatColor.GREEN + "Sneak-Right Click:" + ChatColor.GRAY + " Teleport to your home" }));
    } else {
      itemMeta.setLore(Arrays.asList(new String[] { ChatColor.GREEN + "Left Click:" + ChatColor.GRAY + " Teleports to Spawn", ChatColor.GREEN + "Sneak-Left Click:" + ChatColor.GRAY + " Set-home at your current position", ChatColor.GREEN + "Sneak-Right Click:" + ChatColor.GRAY + " Teleport to your home" }));
    }
    item.setItemMeta(itemMeta);
    if (e.getPlayer().getInventory().getItemInHand().equals(item))
    {
      e.setCancelled(true);
      if ((e.getAction() == Action.RIGHT_CLICK_AIR) || (e.getAction() == Action.RIGHT_CLICK_BLOCK) && (!Aeonblox.teleporting.contains(e.getPlayer().getName())))
      {
      	Aeonblox.teleporting.add(e.getPlayer().getName());
        if (e.getPlayer().isSneaking())
        {
          if (e.getPlayer().hasPermission("aeonblox.rune.donor"))
          {
            if (!pls.contains(e.getPlayer().getName()))
            {
              e.getPlayer().addPotionEffect(this.confusion);
              e.getPlayer().addPotionEffect(this.blindness);
              e.getPlayer().sendMessage(ChatColor.GOLD + "To which home do you want to go?");
              pls.add(e.getPlayer().getName());
            }
            else
            {
              e.getPlayer().sendMessage(ChatColor.GOLD + "You are already in queue to be teleported!");
            }
          }
          else
          {
        	  final Location oldLoc = e.getPlayer().getLocation();
        	final BukkitTask s =  new BukkitRunnable(){
        	  int ten = 10;
        	  public void run(){
                	Aeonblox.teleporting.add(e.getPlayer().getName());
        		  if (!e.getPlayer().getLocation().equals(oldLoc)){
        			  Aeonblox.teleporting.remove(e.getPlayer().getName());
        		  }
        		  if (ten == 10){
                  	e.getPlayer().sendMessage(
                      ChatColor.RED + "" + ChatColor.BOLD + ">>" + 
                      ChatColor.RED + 
                      " You feel energy rush into you " + 
                      ChatColor.UNDERLINE + "TELEPORTING" + 
                      ChatColor.RED + " in " + 
                      ChatColor.BOLD + "10s...");
                    e.getPlayer().addPotionEffect(plugin.bookclick.confusion);
                    e.getPlayer().addPotionEffect(plugin.bookclick.blindness);
        		  }
        		  else if (ten <= 5){
                      /**e.getPlayer().sendMessage(
                      ChatColor.RED + "" + ChatColor.BOLD + ">>" + 
                      ChatColor.RED + 
                      " You will be " + 
                      ChatColor.UNDERLINE + "TELEPORTING" + 
                      ChatColor.RED + " in " + 
                      ChatColor.BOLD + "5s..."); */
        			  if (ten == 0){
        	                e.getPlayer().performCommand("home");
        	                e.getPlayer().sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "     >> Teleporting...");
        	                
        	            	Aeonblox.teleporting.remove(e.getPlayer().getName());
        			  } else {
        				  e.getPlayer().sendMessage(
        	                      ChatColor.RED + "" + ChatColor.BOLD + ">>" + 
        	                      ChatColor.RED + 
        	                      " You will be " + 
        	                      ChatColor.UNDERLINE + "TELEPORTING" + 
        	                      ChatColor.RED + " in " + 
        	                      ChatColor.BOLD + ten + "s...");
        			  }
        		  }
        		  ten--;//substract one from ten
        	  }
        	  
          }.runTaskTimer(plugin, 0L, 20L);
          new BukkitRunnable(){
        	  public void run(){
        		  s.cancel();
        	  }
          }.runTaskLater(plugin, 101L);
          }
        }
        else {
      	  final Location oldLoc = e.getPlayer().getLocation();
      	final BukkitTask s =  new BukkitRunnable(){
      	  int ten = 10;
      	  public void run(){
              	Aeonblox.teleporting.add(e.getPlayer().getName());
      		  if (!e.getPlayer().getLocation().equals(oldLoc)){
      			  Aeonblox.teleporting.remove(e.getPlayer().getName());
      		  }
      		  if (ten == 10){
                	e.getPlayer().sendMessage(
                    ChatColor.RED + "" + ChatColor.BOLD + ">>" + 
                    ChatColor.RED + 
                    " You feel energy rush into you " + 
                    ChatColor.UNDERLINE + "TELEPORTING" + 
                    ChatColor.RED + " in " + 
                    ChatColor.BOLD + "10s...");
                  e.getPlayer().addPotionEffect(plugin.bookclick.confusion);
                  e.getPlayer().addPotionEffect(plugin.bookclick.blindness);
      		  }
      		  else if (ten <= 5){
                    /**e.getPlayer().sendMessage(
                    ChatColor.RED + "" + ChatColor.BOLD + ">>" + 
                    ChatColor.RED + 
                    " You will be " + 
                    ChatColor.UNDERLINE + "TELEPORTING" + 
                    ChatColor.RED + " in " + 
                    ChatColor.BOLD + "5s..."); */
      			  if (ten == 0){
      	                e.getPlayer().performCommand("tpr");
      	                e.getPlayer().sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "     >> Teleporting...");
      	                
      	            	Aeonblox.teleporting.remove(e.getPlayer().getName());
      			  } else {
      				  e.getPlayer().sendMessage(
      	                      ChatColor.RED + "" + ChatColor.BOLD + ">>" + 
      	                      ChatColor.RED + 
      	                      " You will be " + 
      	                      ChatColor.UNDERLINE + "TELEPORTING" + 
      	                      ChatColor.RED + " in " + 
      	                      ChatColor.BOLD + ten + "s...");
      			  }
      		  }
      		  ten--;//substract one from ten
      	  }
      	  
        }.runTaskTimer(plugin, 0L, 20L);
        new BukkitRunnable(){
      	  public void run(){
      		  s.cancel();
      	  }
        }.runTaskLater(plugin, 101L);
            }
        	
        	//------
      }
      else if ((e.getAction() == Action.LEFT_CLICK_AIR) || (e.getAction() == Action.LEFT_CLICK_BLOCK)) {
        if (!e.getPlayer().isSneaking())
        {
          if (e.getPlayer().hasPermission("aeonblox.rune.donor"))
          {
        	  
        	  final Location oldLoc = e.getPlayer().getLocation();
        	final BukkitTask s =  new BukkitRunnable(){
        	  int ten = 10;
        	  public void run(){
                	Aeonblox.teleporting.add(e.getPlayer().getName());
        		  if (!e.getPlayer().getLocation().equals(oldLoc)){
        			  Aeonblox.teleporting.remove(e.getPlayer().getName());
        		  }
        		  if (ten == 10){
                  	e.getPlayer().sendMessage(
                      ChatColor.RED + "" + ChatColor.BOLD + ">>" + 
                      ChatColor.RED + 
                      " You feel energy rush into you " + 
                      ChatColor.UNDERLINE + "TELEPORTING" + 
                      ChatColor.RED + " in " + 
                      ChatColor.BOLD + "10s...");
                    e.getPlayer().addPotionEffect(plugin.bookclick.confusion);
                    e.getPlayer().addPotionEffect(plugin.bookclick.blindness);
        		  }
        		  else if (ten <= 5){
                      /**e.getPlayer().sendMessage(
                      ChatColor.RED + "" + ChatColor.BOLD + ">>" + 
                      ChatColor.RED + 
                      " You will be " + 
                      ChatColor.UNDERLINE + "TELEPORTING" + 
                      ChatColor.RED + " in " + 
                      ChatColor.BOLD + "5s..."); */
        			  if (ten == 0){
        	                e.getPlayer().performCommand("spawn");
        	                e.getPlayer().sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "     >> Teleporting...");
        	                
        	            	Aeonblox.teleporting.remove(e.getPlayer().getName());
        			  } else {
        				  e.getPlayer().sendMessage(
        	                      ChatColor.RED + "" + ChatColor.BOLD + ">>" + 
        	                      ChatColor.RED + 
        	                      " You will be " + 
        	                      ChatColor.UNDERLINE + "TELEPORTING" + 
        	                      ChatColor.RED + " in " + 
        	                      ChatColor.BOLD + ten + "s...");
        			  }
        		  }
        		  ten--;//substract one from ten
        	  }
        	  
          }.runTaskTimer(plugin, 0L, 20L);
          new BukkitRunnable(){
        	  public void run(){
        		  s.cancel();
        	  }
          }.runTaskLater(plugin, 101L);
              
            }else{
            	
          	  final Location oldLoc = e.getPlayer().getLocation();
          	final BukkitTask s =  new BukkitRunnable(){
          	  int ten = 10;
          	  public void run(){
                  	Aeonblox.teleporting.add(e.getPlayer().getName());
          		  if (!e.getPlayer().getLocation().equals(oldLoc)){
          			  Aeonblox.teleporting.remove(e.getPlayer().getName());
          		  }
          		  if (ten == 10){
                    	e.getPlayer().sendMessage(
                        ChatColor.RED + "" + ChatColor.BOLD + ">>" + 
                        ChatColor.RED + 
                        " You feel energy rush into you " + 
                        ChatColor.UNDERLINE + "TELEPORTING" + 
                        ChatColor.RED + " in " + 
                        ChatColor.BOLD + "10s...");
                      e.getPlayer().addPotionEffect(plugin.bookclick.confusion);
                      e.getPlayer().addPotionEffect(plugin.bookclick.blindness);
          		  }
          		  else if (ten <= 5){
                        /**e.getPlayer().sendMessage(
                        ChatColor.RED + "" + ChatColor.BOLD + ">>" + 
                        ChatColor.RED + 
                        " You will be " + 
                        ChatColor.UNDERLINE + "TELEPORTING" + 
                        ChatColor.RED + " in " + 
                        ChatColor.BOLD + "5s..."); */
          			  if (ten == 0){
          	                e.getPlayer().performCommand("spawn");
          	                e.getPlayer().sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "     >> Teleporting...");
          	                
          	            	Aeonblox.teleporting.remove(e.getPlayer().getName());
          			  } else {
          				  e.getPlayer().sendMessage(
          	                      ChatColor.RED + "" + ChatColor.BOLD + ">>" + 
          	                      ChatColor.RED + 
          	                      " You will be " + 
          	                      ChatColor.UNDERLINE + "TELEPORTING" + 
          	                      ChatColor.RED + " in " + 
          	                      ChatColor.BOLD + ten + "s...");
          			  }
          		  }
          		  ten--;//substract one from ten
          	  }
          	  
            }.runTaskTimer(plugin, 0L, 20L);
            new BukkitRunnable(){
          	  public void run(){
          		  s.cancel();
          	  }
            }.runTaskLater(plugin, 101L);
              }
        }
        else {
          e.getPlayer().performCommand("sethome");
        }
      }
      }
    }
  
  /**
  @EventHandler(priority=EventPriority.MONITOR)
  public void move(PlayerMoveEvent e) { 
	  String p = e.getPlayer().getName();
	  if(Aeonblox.teleporting.contains(p)) {
		  e.setCancelled(true);
	  }
  } */
  
  
  
  
  @EventHandler
  public void chat(final AsyncPlayerChatEvent e)
  {
    if (pls.contains(e.getPlayer().getName()))
    {
        e.setCancelled(true);
        e.getPlayer().performCommand("home " + e.getMessage());
        e.getPlayer().sendMessage(ChatColor.GOLD + "You are now teleporting to the home: " + e.getMessage());
        pls.remove(e.getPlayer().getName());
    	Aeonblox.teleporting.remove(e.getPlayer().getName());
      
    }
  }
  
  
  @SuppressWarnings("deprecation")
@EventHandler
  public void onPlayerDropItem(PlayerDropItemEvent e) {
   
  Player player = e.getPlayer();
  PlayerInventory playerInventory = player.getInventory();
  ItemStack[] contents = playerInventory.getContents();
   
  if(!Aeonblox.invarray.contains(player)) {
  if(player.hasPermission("aeonblox.journalblock")) {  
  if(playerInventory.getHeldItemSlot() == 7 || playerInventory.getHeldItemSlot() == 8 ) {
  e.setCancelled(true);
  playerInventory.setContents(contents);
  player.updateInventory();
  player.saveData();
  }
  }
  }else{
	  
  }
  }
  
  @EventHandler
  public void onClickSlot(InventoryClickEvent e) {
  String p = e.getWhoClicked().getName();
  HumanEntity player = e.getWhoClicked();
  if(e.getInventory().getType() == InventoryType.PLAYER) { return; }
  if(!Aeonblox.invarray.contains(p)) {
  if(player.hasPermission("aeonblox.journalblock")) {
  if(e.getSlot() == 7 || e.getSlot() == 8) {
  e.setResult(Result.DENY);
  e.setCancelled(true); 
  }
  }
}else{
}
}
}