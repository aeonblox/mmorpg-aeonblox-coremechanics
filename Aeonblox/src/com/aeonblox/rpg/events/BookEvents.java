package com.aeonblox.rpg.events;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.ItemMeta;

import com.aeonblox.rpg.Aeonblox;

public class BookEvents
  implements Listener
{
  ItemStack book;
  ItemStack emerald = new ItemStack(Material.EMERALD, 1);
  Aeonblox plugin;
  
  public BookEvents(Aeonblox instance)
  {
    this.plugin = instance;
    this.book = new ItemStack(Material.WRITTEN_BOOK);
    this.book.setAmount(1);
    BookMeta meta = (BookMeta)this.book.getItemMeta();
    meta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "Character Journal");
    
    List<String> lore = new ArrayList<String>();
    lore.add(ChatColor.RESET + "" + ChatColor.GREEN + "A book that displays ");
    lore.add(ChatColor.GREEN + "your characters data");
    lore.add(ChatColor.GREEN + "Sneak-Right Click: "+ ChatColor.GRAY + "Open's Race Selection Screen");
    lore.add(ChatColor.GREEN + "Sneak-Left Click: " + ChatColor.GRAY + "Open's Bank Account");
    

    meta.setLore(lore);
    this.book.setItemMeta(meta);
    String name = ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "Teleportation Rune";
    
    ItemMeta itemMeta = this.emerald.getItemMeta();
    itemMeta.setDisplayName(name);
    this.emerald.setItemMeta(itemMeta);
  }
  
  public String getAliance(Player player) {
      if (plugin.respawn.chaotic.contains(player.getName())) {
    	  String bandit = ChatColor.RED + "" + ChatColor.BOLD + "" + ChatColor.UNDERLINE + "Evil";
    	  return bandit;
      }else if ((plugin.respawn.neutral.contains(player.getName()))) { 
    	  String neutral = ChatColor.RED + "" + ChatColor.BOLD + "" + ChatColor.UNDERLINE + "Neutral";
    	  return neutral;
	  }else if ((!plugin.respawn.chaotic.contains(player.getName())) && (!plugin.respawn.neutral.contains(player.getName()))) {
    	  String lawful = ChatColor.GREEN + "" + ChatColor.BOLD + "" + ChatColor.UNDERLINE + "Lawful";
    	  return lawful;  
	  }
	return null;
  }
  
  @EventHandler(ignoreCancelled=true)
  public void click(final InventoryClickEvent e)
  {
	Player player = (Player)e.getWhoClicked();
    if ((e.getInventory() instanceof PlayerInventory))
    {
        PlayerInventory inv = player.getInventory();
        if ((inv.getHelmet() != null) && 
                (inv.getHelmet().getItemMeta().hasLore()))
              {
            double helm = Aeonblox.getHpgenFromLore(inv.getHelmet(), "HP REGEN");
              
        
        if ((inv.getChestplate() != null) && 
                (inv.getChestplate().getItemMeta().hasLore()))
              {
            double chest = Aeonblox.getHpgenFromLore(inv.getChestplate(), "HP REGEN");
              
        
        if ((inv.getLeggings() != null) && 
                (inv.getLeggings().getItemMeta().hasLore()))
              {
            double legs = Aeonblox.getHpgenFromLore(inv.getLeggings(), "HP REGEN");
              
        
        if ((inv.getBoots() != null) && 
                (inv.getBoots().getItemMeta().hasLore()))
              {
            double boots = Aeonblox.getHpgenFromLore(inv.getBoots(), "HP REGEN");
              
        double hpgen = helm + chest + legs + boots;
      List<String> page = new ArrayList<String>();
      page.add(ChatColor.BOLD + "Character Journal:\n----------------" + "\nALIANCE: " + getAliance(player) + "\nRACE: " + Aeonblox.RMMain.utils.getPlayerRace(e.getWhoClicked().getUniqueId()) + 
      "" + "\n  DAMAGE " + String.valueOf(plugin.bookmech.getExtraDamage(e.getWhoClicked())) 
      + "%\n  RESIST " + String.valueOf(new StringBuilder(String.valueOf(plugin.bookmech.getResistance(e.getWhoClicked()))).append("%").toString())
      + ChatColor.RESET + "\n   " + hpgen + ChatColor.BOLD + " HP/s");
      ((BookMeta)this.book.getItemMeta()).setPages(page);
      if (!e.getWhoClicked().hasPermission("aeonblox.rune.donor")) {
        this.emerald.getItemMeta().setLore(Arrays.asList(new String[] { ChatColor.GREEN + "Left Click:" + ChatColor.GRAY + " Teleports to Skyte", ChatColor.GREEN + "Sneak-Left Click:" + ChatColor.GRAY + " Set-home at your current position", ChatColor.GREEN + "Sneak-Right Click:" + ChatColor.GRAY + " Teleport to your home", ChatColor.GREEN + "Right click " + ChatColor.GRAY + "will teleport you to the wild!" }));
      } else {
        this.emerald.getItemMeta().setLore(Arrays.asList(new String[] { ChatColor.GREEN + "Left Click:" + ChatColor.GRAY + " Teleports to Spawn", ChatColor.GREEN + "Sneak-Left Click:" + ChatColor.GRAY + " Set-home at your current position", ChatColor.GREEN + "Sneak-Right Click:" + ChatColor.GRAY + " Teleport to your home", ChatColor.GREEN + "Right click " + ChatColor.GRAY + "will teleport you to the wild!" }));
      }
      if (e.getSlot() == 8)
      {
        e.setCancelled(true);
        e.setCurrentItem(new ItemStack(Material.AIR));
        ((Player)e.getWhoClicked()).sendMessage(ChatColor.GOLD + "A magical force forces the book back into your hands!");
      }
      if (e.getSlot() == 7)
      {
        e.setCancelled(true);
        ((Player)e.getWhoClicked()).sendMessage(ChatColor.GOLD + "A magical force has bound this rune to your soul, you struggle to remove it!");
        Bukkit.getScheduler().runTaskLater(this.plugin, new java.lang.Runnable()
        {
          public void run()
          {
            e.getWhoClicked().getInventory().removeItem(new ItemStack[] { BookEvents.this.emerald });
            e.getWhoClicked().getInventory().setItem(7, BookEvents.this.emerald);
          }
        }, 1L);
      }
              }
              }
              }
              }
    }
  }
}
