package com.aeonblox.rpg.events;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

public class WhitelistPlayerListener
  implements Listener
{
  @EventHandler(priority=EventPriority.LOWEST)
  public void onPlayerLogin(PlayerLoginEvent event)
  {
    if ((Bukkit.getServer().hasWhitelist()) && (!event.getPlayer().isWhitelisted())) {
      event.disallow(PlayerLoginEvent.Result.KICK_WHITELIST, WhitelistMessage.whitelistMessage);
    }
  }
}
