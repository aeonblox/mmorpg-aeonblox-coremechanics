package com.aeonblox.rpg.events;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.util.Vector;

import com.aeonblox.rpg.Aeonblox;

public class BowPush implements Listener {
	
	Aeonblox plugin;
	public BowPush(Aeonblox instance) {
		this.plugin = instance;
	}

    
/**
* Controls the bow knockback.
*/
	@SuppressWarnings("deprecation")
	@EventHandler
    public void onInteract(PlayerInteractEntityEvent event) {
    final Player player = event.getPlayer();
    Player p = event.getPlayer();
    Entity entity = event.getRightClicked();
    if(entity instanceof Player == true) {   
    Player clicked = (Player) entity;
    if (player.getItemInHand().getType() == Material.BOW) {
    	//Checking if the player is already in the cooldown, then cancelling
    if(plugin.cooldown.contains(event.getPlayer().getName())) {
    player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "You must wait for the cooldown to end to re-use this ability!");
    event.setCancelled(true);
     }else{
     if (player.hasPermission("aeonblox.bowpush")) {
     if (clicked.isOnGround()) {
   if (plugin.util_WorldGuard != null)
   {
	   boolean bool = true;
	   for (Player plz: Bukkit.getOnlinePlayers()){
		   if (plz.getUniqueId() == clicked.getUniqueId()){
			   bool =false;
		   }
	   }
   if (!plugin.util_WorldGuard.playerInPVPRegion(p) && !bool){
   {
//clicked.setVelocity(clicked.getVelocity().add(new Vector(0, 1, 0)));
// Get velocity unit vector:
     Vector unitVector = clicked.getLocation().toVector().subtract(player.getLocation().toVector()).normalize();
// Set speed and push entity:
     clicked.setVelocity(unitVector.multiply(2));
     player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "*OPPONET SMITTEN* <[" + ChatColor.RED + "" + ChatColor.BOLD + (clicked.getName() + ChatColor.RED + "" + ChatColor.BOLD + "]" ));
//Adding player to the cooldown if they aren't in it already.
     plugin.cooldown.add(player.getName());
      }
      plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
      public void run() {;
//Removing the cooldown after 15 seconds
      plugin.cooldown.remove(player.getName());
         }
         }, 80);
       }
      }
    }
   }
    }
    
    }
}
	}
}
