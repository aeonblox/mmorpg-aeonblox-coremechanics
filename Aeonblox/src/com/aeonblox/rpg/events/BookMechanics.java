package com.aeonblox.rpg.events;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.scheduler.BukkitRunnable;

import com.aeonblox.rpg.Aeonblox;

public class BookMechanics
  extends BukkitRunnable
{
  Aeonblox plugin;
  
	public BookMechanics(Aeonblox instance){	
		this.plugin = instance;
	}
  
	  public double roundTwoDecimals(double d) {
		  
	      DecimalFormat twoDForm = new DecimalFormat("#.##");
	      return Double.valueOf(twoDForm.format(d));

	  }
/**	public double getHp(Player p) {
        PlayerInventory i = p.getInventory();
        if ((i.getHelmet() != null) && 
                (i.getHelmet().getItemMeta().hasLore()))
              {
            double helm = Aeonblox.getHpgenFromLore(i.getHelmet(), "HP REGEN");
              return helm++;
              }
        
        if ((i.getChestplate() != null) && 
                (i.getChestplate().getItemMeta().hasLore()))
              {
            double chest = Aeonblox.getHpgenFromLore(i.getChestplate(), "HP REGEN");
            return chest++;
              }
        
        if ((i.getLeggings() != null) && 
                (i.getLeggings().getItemMeta().hasLore()))
              {
            double legs = Aeonblox.getHpgenFromLore(i.getLeggings(), "HP REGEN");
            return legs++;
              }
        
        if ((i.getBoots() != null) && 
                (i.getBoots().getItemMeta().hasLore()))
              {
            double boots = Aeonblox.getHpgenFromLore(i.getBoots(), "HP REGEN");
            return boots++;
              }
        //double hpgen = helm + chest + legs + boots;
	}*/
	  
	  
  public void run()
  {
    for (Player p : Bukkit.getOnlinePlayers())
    {
      ItemStack i = new ItemStack(Material.WRITTEN_BOOK);
      i.setAmount(1);
      BookMeta meta = (BookMeta)i.getItemMeta();
      meta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "Character Journal");
      List<String> page = new ArrayList<String>();
      Damageable d = (Player) p;
      
      PlayerInventory inv = p.getInventory();
      if ((inv.getHelmet() != null) && 
              (inv.getHelmet().getItemMeta().hasLore()))
            {
          double helm = Aeonblox.getHpgenFromLore(inv.getHelmet(), "HP REGEN");
            
      
      if ((inv.getChestplate() != null) && 
              (inv.getChestplate().getItemMeta().hasLore()))
            {
          double chest = Aeonblox.getHpgenFromLore(inv.getChestplate(), "HP REGEN");
            
      
      if ((inv.getLeggings() != null) && 
              (inv.getLeggings().getItemMeta().hasLore()))
            {
          double legs = Aeonblox.getHpgenFromLore(inv.getLeggings(), "HP REGEN");
            
      
      if ((inv.getBoots() != null) && 
              (inv.getBoots().getItemMeta().hasLore()))
            {
          double boots = Aeonblox.getHpgenFromLore(inv.getBoots(), "HP REGEN");
            
      double hpgen = helm + chest + legs + boots;
      
      page.add(ChatColor.BOLD + "Character Journal:\n----------------" + "\nALIANCE: " + getAliance(p) + ChatColor.RESET + "" + ChatColor.BOLD + "\nRACE: " + ChatColor.BLACK + Aeonblox.RMMain.utils.getPlayerRace(p.getUniqueId()) + 
      "" + "\n\n   " + roundTwoDecimals(d.getHealth()) + " / " + d.getMaxHealth()  + ChatColor.BOLD + " HP" + ChatColor.RESET + "\n   " + String.valueOf(plugin.bookmech.getExtraDamage(p) + ChatColor.BOLD + " DAMAGE ") 
      + ChatColor.RESET + "\n   " + String.valueOf(new StringBuilder(String.valueOf(plugin.bookmech.getResistance(p))).append("%").toString()) + ChatColor.BOLD + " RESIST "
      + ChatColor.RESET + "\n   " + hpgen + ChatColor.BOLD + " HP/s");
      
      List<String> lore = new ArrayList<String>();
      lore.add(ChatColor.RESET + "" + ChatColor.GREEN + "A book that displays ");
      lore.add(ChatColor.GREEN + "your characters data");
      lore.add(ChatColor.GREEN + "Sneak-Right Click: "+ ChatColor.GRAY + "Open's Race Selection Screen");
      lore.add(ChatColor.GREEN + "Sneak-Left Click: " + ChatColor.GRAY + "Open's Bank Account");
      

      meta.setLore(lore);
      meta.setPages(page);
      i.setItemMeta(meta);
      p.getInventory().setItem(8, new ItemStack(Material.AIR));
      p.getInventory().setItem(8, i);
    }
            }
            }
            }
    }
  }
  
  
  public String getAliance(Player player) {
      if (plugin.respawn.chaotic.contains(player.getName())) {
    	  String bandit = ChatColor.RED + "" + ChatColor.UNDERLINE + "Evil" + ChatColor.BLACK + "\n 100% Item drop chance";
    	  return bandit;
      }else if ((plugin.respawn.neutral.contains(player.getName()))) { 
    	  String neutral = ChatColor.GOLD + " "+ ChatColor.UNDERLINE + "Neutral" + ChatColor.BLACK + "\n 45% Item drop chance";
    	  return neutral;
	  }else if ((!plugin.respawn.chaotic.contains(player.getName())) && (!plugin.respawn.neutral.contains(player.getName()))) {
    	  String lawful = ChatColor.GREEN + "" + ChatColor.UNDERLINE + "Lawful" + ChatColor.BLACK + "\n 15% Item drop chance";
    	  return lawful;  
	  }
	return null;
  }
  // public static - switch if it doesnt work - for both
  public String getExtraDamage(HumanEntity p1)
  {
    String returns = null;
    Player p = (Player)p1;
    Double damage = Double.valueOf(plugin.cfgHandler.getCustomConfig(plugin.expConfig).getDouble("extra damage"));
    if (p.getLevel() > 0) {
      returns = String.valueOf(damage.doubleValue() * p.getLevel());
    }
    if (returns == null) {
      returns = "0.0";
    }
    String[] retunsArray = returns.split("\\.");
    if (retunsArray[1].length() >= 2) {
      returns = retunsArray[0] + "." + retunsArray[1].substring(0, 2);
    }
    return returns;
  }
  
  public String getResistance(HumanEntity p1)
  {
    String returns = null;
    Player p = (Player)p1;
    Double resist = Double.valueOf(plugin.cfgHandler.getCustomConfig(plugin.expConfig).getDouble("extra resistance"));
    Double maxResist = Double.valueOf(plugin.cfgHandler.getCustomConfig(plugin.expConfig).getDouble("max resistance"));
    if (p.getLevel() > 0) {
      if (resist.doubleValue() * p.getLevel() <= maxResist.doubleValue()) {
        returns = String.valueOf(100.0D - (1.0D - resist.doubleValue() / 100.0D * p.getLevel()) * 100.0D);
      } else {
        returns = String.valueOf(100.0D - (1.0D - maxResist.doubleValue() / 100.0D) * 100.0D);
      }
    }
    if (returns == null) {
      returns = "0.0";
    }
    String[] returnsArray = returns.split("\\.");
    if (returnsArray[1].length() >= 2) {
      returns = returnsArray[0] + "." + returnsArray[1].substring(0, 2);
    }
    return returns;
  }
}
