package com.aeonblox.rpg.entitydata;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.RPGMakerDev.RPGMaker.Inventory.ItemDrops;
import com.RPGMakerDev.RPGMaker.Inventory.ItemDrops.ItemType;
import com.RPGMakerDev.RPGMaker.Inventory.RPGItem;
import com.aeonblox.rpg.Aeonblox;

public class MobDrops2
  implements Listener
{
  Aeonblox plugin;
  
  public MobDrops2(Aeonblox instance)
  {
    plugin = instance;
  }
  
  @EventHandler
  public void onMobDeath(EntityDeathEvent e)
  {
    if (!(e.getEntity() instanceof Player)) {
      e.getDrops().clear();
    }
    e.setDroppedExp(0);
    if ((e.getEntity() instanceof Skeleton))
    {
      Skeleton s = (Skeleton)e.getEntity();
      if (s.getCustomName() == null) {
        return;
      }
      //if (s.getCustomName().replaceAll("�7", "").replaceAll("�e", "").replaceAll("�c", "").replaceAll("�a", "").contains("Infernal Skeleton"))
      if (s.getCustomName().contains(ChatColor.RED + "Infernal Skeleton"))
      {
        Random random = new Random();
        int drop = random.nextInt(425) + 1;
        int helm = 0;
        int chest = 0;
        int min = 0;
        int max = 0;
        int regen = random.nextInt(21) + 90;
        String common = "";
        int vit = random.nextInt(161) + 150;
        int rarity = random.nextInt(2) + 1; //CHANGE THIS SHIT LATER!!!!! 
        int Bloxs = random.nextInt(2) + 1;
        int stacks = random.nextInt(2) + 1;
        int amt = random.nextInt(17) + 16;
        if (Bloxs == 1)
        {
          for (int i = 0; i < stacks; i++)
          {
            ItemStack Blox = new ItemStack(Material.GOLD_NUGGET, 64);
            ItemMeta im = Blox.getItemMeta();
            im.setDisplayName(ChatColor.WHITE + "Blox");
            im.setLore(Arrays.asList(new String[] {ChatColor.GRAY + 
              "The currency of Aeonblox" }));
            Blox.setItemMeta(im);
            e.getDrops().add(Blox);
          }
          ItemStack Blox = new ItemStack(Material.GOLD_NUGGET, amt);
          ItemMeta im = Blox.getItemMeta();
          im.setDisplayName(ChatColor.WHITE + "Blox");
          im.setLore(Arrays.asList(new String[] {ChatColor.GRAY + 
            "The currency of Aeonblox" }));
          Blox.setItemMeta(im);
          e.getDrops().add(Blox);
        }
        if((rarity == 2) | (rarity == 3)) { 
        	ItemType helm2 = ItemType.HELMET;
        	int tier = 1;
            ItemDrops give = new ItemDrops(1, 1, tier, helm2);
            give.updateItemLore();
            give.updateItemMeta();
            e.getDrops().add(give.getItemStack());
            System.out.println("I HAZ DAH POWAHHHHHHHHHHHHHHHHhHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH!");
            System.out.println("I HAZ DAH POWAHHHHHHHHHHHHHHHHhHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH!");
            System.out.println("I HAZ DAH POWAHHHHHHHHHHHHHHHHhHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH!");
            System.out.println("I HAZ DAH POWAHHHHHHHHHHHHHHHHhHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH!");
        }

      }
      else if (s.getCustomName() == ChatColor.translateAlternateColorCodes('&', "Skeletal Guardian"))
      {
        Random random = new Random();
        int drop = random.nextInt(250) + 1;
        int helm = 0;
        int chest = 0;
        int min = 0;
        int max = 0;
        int regen = random.nextInt(6) + 60;
        int vit = random.nextInt(91) + 60;
        String common = "";
        int rarity = random.nextInt(50) + 1;
        int Bloxs = random.nextInt(2) + 1;
        int amt = random.nextInt(33) + 32;
        if (Bloxs == 1)
        {
          ItemStack Blox = new ItemStack(Material.GOLD_NUGGET, amt);
          ItemMeta im = Blox.getItemMeta();
          im.setDisplayName(ChatColor.WHITE + "Blox");
          im.setLore(Arrays.asList(new String[] {ChatColor.GRAY + 
            "The currency of Aeonblox" }));
          Blox.setItemMeta(im);
          e.getDrops().add(Blox);
        }
        if (rarity <= 43)
        {
          common = ChatColor.GRAY + "" + ChatColor.ITALIC + "Common";
          helm = random.nextInt(200) + 300;
          chest = random.nextInt(200) + 600;
          min = random.nextInt(21) + 60;
          max = random.nextInt(99 - min + 1) + min;
        }
        else if ((rarity == 44) || (rarity == 45) || (rarity == 46) || 
          (rarity == 47))
        {
          common = 
            ChatColor.GREEN + "" + ChatColor.ITALIC + "Uncommon";
          helm = random.nextInt(200) + 500;
          chest = random.nextInt(700) + 800;
          min = random.nextInt(26) + 65;
          max = random.nextInt(21) + 100;
        }
        else if ((rarity == 48) || (rarity == 49))
        {
          common = ChatColor.AQUA + "" + ChatColor.ITALIC + "Rare";
          helm = random.nextInt(300) + 700;
          chest = random.nextInt(800) + 1500;
          min = random.nextInt(21) + 80;
          max = random.nextInt(79) + 121;
        }
        else if (rarity == 50)
        {
          common = 
            ChatColor.YELLOW + "" + ChatColor.ITALIC + "Unique";
          helm = random.nextInt(201) + 1000;
          chest = random.nextInt(201) + 2300;
          min = random.nextInt(51) + 100;
          max = random.nextInt(51) + 200;
        }
        if (drop == 1)
        {
          ItemStack H = new ItemStack(Material.DIAMOND_HELMET);
          ItemMeta helmmeta = H.getItemMeta();
          helmmeta.setDisplayName(ChatColor.LIGHT_PURPLE + 
            "Mending Ancient Full Helmet");
          List<String> hlore = new ArrayList<String>();
          hlore.add(ChatColor.RED + "DPS: 6 - 6%");
          hlore.add(ChatColor.RED + "HP: +" + helm);
          hlore.add(ChatColor.RED + "HP REGEN: +" + regen + " HP/s");
          hlore.add(common);
          helmmeta.setLore(hlore);
          H.setItemMeta(helmmeta);
          e.getDrops().add(H);
        }
        if (drop == 2)
        {
          ItemStack C = new ItemStack(Material.DIAMOND_CHESTPLATE);
          ItemMeta chestmeta = C.getItemMeta();
          chestmeta.setDisplayName(ChatColor.LIGHT_PURPLE + 
            "Mending Magic Platemail");
          List<String> clore = new ArrayList<String>();
          clore.add(ChatColor.RED + "DPS: 11 - 11%");
          clore.add(ChatColor.RED + "HP: +" + chest);
          clore.add(ChatColor.RED + "HP REGEN: +" + regen + " HP/s");
          clore.add(common);
          chestmeta.setLore(clore);
          C.setItemMeta(chestmeta);
          e.getDrops().add(C);
        }
        if (drop == 3)
        {
          ItemStack L = new ItemStack(Material.DIAMOND_LEGGINGS);
          ItemMeta legmeta = L.getItemMeta();
          legmeta.setDisplayName(ChatColor.LIGHT_PURPLE + 
            "Mending Magic Platemail Leggings");
          List<String> llore = new ArrayList<String>();
          llore.add(ChatColor.RED + "DPS: 11 - 11%");
          llore.add(ChatColor.RED + "HP: +" + chest);
          llore.add(ChatColor.RED + "HP REGEN: +" + regen + " HP/s");
          llore.add(common);
          legmeta.setLore(llore);
          L.setItemMeta(legmeta);
          e.getDrops().add(L);
        }
        if (drop == 4)
        {
          ItemStack B = new ItemStack(Material.DIAMOND_BOOTS);
          ItemMeta bootmeta = B.getItemMeta();
          bootmeta.setDisplayName(ChatColor.LIGHT_PURPLE + 
            "Mending Magic Platemail Boots");
          List<String> blore = new ArrayList<String>();
          blore.add(ChatColor.RED + "DPS: 6 - 6%");
          blore.add(ChatColor.RED + "HP: +" + helm);
          blore.add(ChatColor.RED + "HP REGEN: +" + regen + " HP/s");
          blore.add(common);
          bootmeta.setLore(blore);
          B.setItemMeta(bootmeta);
          e.getDrops().add(B);
        }
        if (drop == 5)
        {
          ItemStack H = new ItemStack(Material.DIAMOND_HELMET);
          ItemMeta helmmeta = H.getItemMeta();
          helmmeta.setDisplayName(ChatColor.LIGHT_PURPLE + 
            "Ancient Full Helmet of Fortitude");
          List<String> hlore = new ArrayList<String>();
          hlore.add(ChatColor.RED + "DPS: 6 - 6%");
          hlore.add(ChatColor.RED + "HP: +" + helm);
          hlore.add(ChatColor.RED + "VIT: +" + vit);
          hlore.add(common);
          helmmeta.setLore(hlore);
          H.setItemMeta(helmmeta);
          e.getDrops().add(H);
        }
        if (drop == 6)
        {
          ItemStack C = new ItemStack(Material.DIAMOND_CHESTPLATE);
          ItemMeta chestmeta = C.getItemMeta();
          chestmeta.setDisplayName(ChatColor.LIGHT_PURPLE + 
            "Magic Platemail of Fortitude");
          List<String> clore = new ArrayList<String>();
          clore.add(ChatColor.RED + "DPS: 11 - 11%");
          clore.add(ChatColor.RED + "HP: +" + chest);
          clore.add(ChatColor.RED + "VIT: +" + vit);
          clore.add(common);
          chestmeta.setLore(clore);
          C.setItemMeta(chestmeta);
          e.getDrops().add(C);
        }
        if (drop == 7)
        {
          ItemStack L = new ItemStack(Material.DIAMOND_LEGGINGS);
          ItemMeta legmeta = L.getItemMeta();
          legmeta.setDisplayName(ChatColor.LIGHT_PURPLE + 
            "Magic Platemail Leggings of Fortitude");
          List<String> llore = new ArrayList<String>();
          llore.add(ChatColor.RED + "DPS: 11 - 11%");
          llore.add(ChatColor.RED + "HP: +" + chest);
          llore.add(ChatColor.RED + "VIT: +" + vit);
          llore.add(common);
          legmeta.setLore(llore);
          L.setItemMeta(legmeta);
          e.getDrops().add(L);
        }
        if (drop == 8)
        {
          ItemStack B = new ItemStack(Material.DIAMOND_BOOTS);
          ItemMeta bootmeta = B.getItemMeta();
          bootmeta.setDisplayName(ChatColor.LIGHT_PURPLE + 
            "Magic Platemail Boots of Fortitude");
          List<String> blore = new ArrayList<String>();
          blore.add(ChatColor.RED + "DPS: 6 - 6%");
          blore.add(ChatColor.RED + "HP: +" + helm);
          blore.add(ChatColor.RED + "VIT: +" + vit);
          blore.add(common);
          bootmeta.setLore(blore);
          B.setItemMeta(bootmeta);
          e.getDrops().add(B);
        }
        if ((drop == 9) || (drop == 10))
        {
          ItemStack GA = new ItemStack(Material.DIAMOND_AXE);
          ItemMeta gameta = GA.getItemMeta();
          gameta.setDisplayName(ChatColor.LIGHT_PURPLE + 
            "Ancient Axe");
          List<String> galore = new ArrayList<String>();
          galore.add(ChatColor.RED + "DMG: " + min + " - " + max);
          galore.add(common);
          gameta.setLore(galore);
          GA.setItemMeta(gameta);
          e.getDrops().add(GA);
        }
        if ((drop == 11) || (drop == 12))
        {
          ItemStack S = new ItemStack(Material.DIAMOND_SWORD);
          ItemMeta swordmeta = S.getItemMeta();
          swordmeta.setDisplayName(ChatColor.LIGHT_PURPLE + 
            "Ancient Sword");
          List<String> slore = new ArrayList<String>();
          slore.add(ChatColor.RED + "DMG: " + min + " - " + max);
          slore.add(common);
          swordmeta.setLore(slore);
          S.setItemMeta(swordmeta);
          e.getDrops().add(S);
        }
      }
      else if (s.getCustomName() == ChatColor.translateAlternateColorCodes('&', "Mountain Bandit"))
      {
        Random random = new Random();
        int drop = random.nextInt(150) + 1;
        int helm = 0;
        int chest = 0;
        int min = 0;
        int max = 0;
        int regen = random.nextInt(11) + 40;
        int vit = random.nextInt(41) + 20;
        String common = "";
        int rarity = random.nextInt(50) + 1;
        int Bloxs = random.nextInt(2) + 1;
        int amt = random.nextInt(17) + 16;
        if (Bloxs == 1)
        {
          ItemStack Blox = new ItemStack(Material.GOLD_NUGGET, amt);
          ItemMeta im = Blox.getItemMeta();
          im.setDisplayName(ChatColor.WHITE + "Blox");
          im.setLore(Arrays.asList(new String[] {ChatColor.GRAY + 
            "The currency of Aeonblox" }));
          Blox.setItemMeta(im);
          e.getDrops().add(Blox);
        }
        if (rarity <= 43)
        {
          common = ChatColor.GRAY + "" + ChatColor.ITALIC + "Common";
          helm = random.nextInt(100) + 100;
          chest = random.nextInt(150) + 200;
          min = random.nextInt(8) + 25;
          max = random.nextInt(49 - min + 1) + min;
        }
        else if ((rarity == 44) || (rarity == 45) || (rarity == 46) || 
          (rarity == 47))
        {
          common = 
            ChatColor.GREEN + "" + ChatColor.ITALIC + "Uncommon";
          helm = random.nextInt(100) + 200;
          chest = random.nextInt(150) + 350;
          min = random.nextInt(10) + 30;
          max = random.nextInt(31) + 40;
        }
        else if ((rarity == 48) || (rarity == 49))
        {
          common = ChatColor.AQUA + "" + ChatColor.ITALIC + "Rare";
          helm = random.nextInt(50) + 300;
          chest = random.nextInt(300) + 500;
          min = random.nextInt(11) + 35;
          max = random.nextInt(30) + 71;
        }
        else if (rarity == 50)
        {
          common = 
            ChatColor.YELLOW + "" + ChatColor.ITALIC + "Unique";
          helm = random.nextInt(81) + 350;
          chest = random.nextInt(51) + 800;
          min = random.nextInt(6) + 45;
          max = random.nextInt(51) + 100;
        }
        if (drop == 1)
        {
          ItemStack H = new ItemStack(Material.IRON_HELMET);
          ItemMeta helmmeta = H.getItemMeta();
          helmmeta.setDisplayName(ChatColor.AQUA + 
            "Mending Full Helmet");
          List<String> hlore = new ArrayList<String>();
          hlore.add(ChatColor.RED + "DPS: 4 - 4%");
          hlore.add(ChatColor.RED + "HP: +" + helm);
          hlore.add(ChatColor.RED + "HP REGEN: +" + regen + " HP/s");
          hlore.add(common);
          helmmeta.setLore(hlore);
          H.setItemMeta(helmmeta);
          e.getDrops().add(H);
        }
        if (drop == 2)
        {
          ItemStack C = new ItemStack(Material.IRON_CHESTPLATE);
          ItemMeta chestmeta = C.getItemMeta();
          chestmeta.setDisplayName(ChatColor.AQUA + 
            "Mending Platemail");
          List<String> clore = new ArrayList<String>();
          clore.add(ChatColor.RED + "DPS: 8 - 8%");
          clore.add(ChatColor.RED + "HP: +" + chest);
          clore.add(ChatColor.RED + "HP REGEN: +" + regen + " HP/s");
          clore.add(common);
          chestmeta.setLore(clore);
          C.setItemMeta(chestmeta);
          e.getDrops().add(C);
        }
        if (drop == 3)
        {
          ItemStack L = new ItemStack(Material.IRON_LEGGINGS);
          ItemMeta legmeta = L.getItemMeta();
          legmeta.setDisplayName(ChatColor.AQUA + 
            "Mending Platemail Leggings");
          List<String> llore = new ArrayList<String>();
          llore.add(ChatColor.RED + "DPS: 8 - 8%");
          llore.add(ChatColor.RED + "HP: +" + chest);
          llore.add(ChatColor.RED + "HP REGEN: +" + regen + " HP/s");
          llore.add(common);
          legmeta.setLore(llore);
          L.setItemMeta(legmeta);
          e.getDrops().add(L);
        }
        if (drop == 4)
        {
          ItemStack B = new ItemStack(Material.IRON_BOOTS);
          ItemMeta bootmeta = B.getItemMeta();
          bootmeta.setDisplayName(ChatColor.AQUA + 
            "Mending Platemail Boots");
          List<String> blore = new ArrayList<String>();
          blore.add(ChatColor.RED + "DPS: 4 - 4%");
          blore.add(ChatColor.RED + "HP: +" + helm);
          blore.add(ChatColor.RED + "HP REGEN: +" + regen + " HP/s");
          blore.add(common);
          bootmeta.setLore(blore);
          B.setItemMeta(bootmeta);
          e.getDrops().add(B);
        }
        if (drop == 5)
        {
          ItemStack H = new ItemStack(Material.IRON_HELMET);
          ItemMeta helmmeta = H.getItemMeta();
          helmmeta.setDisplayName(ChatColor.AQUA + 
            "Full Helmet of Fortitude");
          List<String> hlore = new ArrayList<String>();
          hlore.add(ChatColor.RED + "DPS: 4 - 4%");
          hlore.add(ChatColor.RED + "HP: +" + helm);
          hlore.add(ChatColor.RED + "VIT: +" + vit);
          hlore.add(common);
          helmmeta.setLore(hlore);
          H.setItemMeta(helmmeta);
          e.getDrops().add(H);
        }
        if (drop == 6)
        {
          ItemStack C = new ItemStack(Material.IRON_CHESTPLATE);
          ItemMeta chestmeta = C.getItemMeta();
          chestmeta.setDisplayName(ChatColor.AQUA + 
            "Platemail of Fortitude");
          List<String> clore = new ArrayList<String>();
          clore.add(ChatColor.RED + "DPS: 8 - 8%");
          clore.add(ChatColor.RED + "HP: +" + chest);
          clore.add(ChatColor.RED + "VIT: +" + vit);
          clore.add(common);
          chestmeta.setLore(clore);
          C.setItemMeta(chestmeta);
          e.getDrops().add(C);
        }
        if (drop == 7)
        {
          ItemStack L = new ItemStack(Material.IRON_LEGGINGS);
          ItemMeta legmeta = L.getItemMeta();
          legmeta.setDisplayName(ChatColor.AQUA + 
            "Platemail Leggings of Fortitude");
          List<String> llore = new ArrayList<String>();
          llore.add(ChatColor.RED + "DPS: 8 - 8%");
          llore.add(ChatColor.RED + "HP: +" + chest);
          llore.add(ChatColor.RED + "VIT: +" + vit);
          llore.add(common);
          legmeta.setLore(llore);
          L.setItemMeta(legmeta);
          e.getDrops().add(L);
        }
        if (drop == 8)
        {
          ItemStack B = new ItemStack(Material.IRON_BOOTS);
          ItemMeta bootmeta = B.getItemMeta();
          bootmeta.setDisplayName(ChatColor.AQUA + 
            "Platemail Boots of Fortitude");
          List<String> blore = new ArrayList<String>();
          blore.add(ChatColor.RED + "DPS: 4 - 4%");
          blore.add(ChatColor.RED + "HP: +" + helm);
          blore.add(ChatColor.RED + "VIT: +" + vit);
          blore.add(common);
          bootmeta.setLore(blore);
          B.setItemMeta(bootmeta);
          e.getDrops().add(B);
        }
        if ((drop == 9) || (drop == 10))
        {
          ItemStack GA = new ItemStack(Material.IRON_AXE);
          ItemMeta gameta = GA.getItemMeta();
          gameta.setDisplayName(ChatColor.AQUA + "War Axe");
          List<String> galore = new ArrayList<String>();
          galore.add(ChatColor.RED + "DMG: " + min + " - " + max);
          galore.add(common);
          gameta.setLore(galore);
          GA.setItemMeta(gameta);
          e.getDrops().add(GA);
        }
        if ((drop == 11) || (drop == 12))
        {
          ItemStack S = new ItemStack(Material.IRON_SWORD);
          ItemMeta swordmeta = S.getItemMeta();
          swordmeta.setDisplayName(ChatColor.AQUA + "Magic Sword");
          List<String> slore = new ArrayList<String>();
          slore.add(ChatColor.RED + "DMG: " + min + " - " + max);
          slore.add(common);
          swordmeta.setLore(slore);
          S.setItemMeta(swordmeta);
          e.getDrops().add(S);
        }
      }
      else if (s.getCustomName() == ChatColor.translateAlternateColorCodes('&', "Old Bandit"))
      {
        Random random = new Random();
        int drop = random.nextInt(60) + 1;
        int helm = 0;
        int chest = 0;
        int min = 0;
        int max = 0;
        int regen = random.nextInt(11) + 10;
        int vit = random.nextInt(6) + 5;
        String common = "";
        int rarity = random.nextInt(50) + 1;
        int Bloxs = random.nextInt(2) + 1;
        int amt = random.nextInt(7) + 10;
        if (Bloxs == 1)
        {
          ItemStack Blox = new ItemStack(Material.GOLD_NUGGET, amt);
          ItemMeta im = Blox.getItemMeta();
          im.setDisplayName(ChatColor.WHITE + "Blox");
          im.setLore(Arrays.asList(new String[] {ChatColor.GRAY + 
            "The currency of Aeonblox" }));
          Blox.setItemMeta(im);
          e.getDrops().add(Blox);
        }
        if (rarity <= 43)
        {
          common = ChatColor.GRAY + "" + ChatColor.ITALIC + "Common";
          helm = random.nextInt(13) + 32;
          chest = random.nextInt(40) + 60;
          min = random.nextInt(6) + 10;
          max = random.nextInt(20 - min + 1) + min;
        }
        else if ((rarity == 44) || (rarity == 45) || (rarity == 46) || 
          (rarity == 47))
        {
          common = 
            ChatColor.GREEN + "" + ChatColor.ITALIC + "Uncommon";
          helm = random.nextInt(40) + 45;
          chest = random.nextInt(100) + 100;
          min = random.nextInt(6) + 15;
          max = random.nextInt(19) + 21;
        }
        else if ((rarity == 48) || (rarity == 49))
        {
          common = ChatColor.AQUA + "" + ChatColor.ITALIC + "Rare";
          helm = random.nextInt(35) + 85;
          chest = random.nextInt(100) + 200;
          min = random.nextInt(7) + 24;
          max = random.nextInt(25) + 40;
        }
        else if (rarity == 50)
        {
          common = 
            ChatColor.YELLOW + "" + ChatColor.ITALIC + "Unique";
          helm = random.nextInt(31) + 120;
          chest = random.nextInt(101) + 300;
          min = random.nextInt(7) + 24;
          max = random.nextInt(6) + 65;
        }
        if (drop == 1)
        {
          ItemStack H = new ItemStack(Material.CHAINMAIL_HELMET);
          ItemMeta helmmeta = H.getItemMeta();
          helmmeta.setDisplayName(ChatColor.GREEN + 
            "Mending Medium Helmet");
          List<String> hlore = new ArrayList<String>();
          hlore.add(ChatColor.RED + "DPS: 2 - 2%");
          hlore.add(ChatColor.RED + "HP: +" + helm);
          hlore.add(ChatColor.RED + "HP REGEN: +" + regen + " HP/s");
          hlore.add(common);
          helmmeta.setLore(hlore);
          H.setItemMeta(helmmeta);
          e.getDrops().add(H);
        }
        if (drop == 2)
        {
          ItemStack C = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
          ItemMeta chestmeta = C.getItemMeta();
          chestmeta.setDisplayName(ChatColor.GREEN + 
            "Mending Chainmail");
          List<String> clore = new ArrayList<String>();
          clore.add(ChatColor.RED + "DPS: 5 - 5%");
          clore.add(ChatColor.RED + "HP: +" + chest);
          clore.add(ChatColor.RED + "HP REGEN: +" + regen + " HP/s");
          clore.add(common);
          chestmeta.setLore(clore);
          C.setItemMeta(chestmeta);
          e.getDrops().add(C);
        }
        if (drop == 3)
        {
          ItemStack L = new ItemStack(Material.CHAINMAIL_LEGGINGS);
          ItemMeta legmeta = L.getItemMeta();
          legmeta.setDisplayName(ChatColor.GREEN + 
            "Mending Chainmail Leggings");
          List<String> llore = new ArrayList<String>();
          llore.add(ChatColor.RED + "DPS: 5 - 5%");
          llore.add(ChatColor.RED + "HP: +" + chest);
          llore.add(ChatColor.RED + "HP REGEN: +" + regen + " HP/s");
          llore.add(common);
          legmeta.setLore(llore);
          L.setItemMeta(legmeta);
          e.getDrops().add(L);
        }
        if (drop == 4)
        {
          ItemStack B = new ItemStack(Material.CHAINMAIL_BOOTS);
          ItemMeta bootmeta = B.getItemMeta();
          bootmeta.setDisplayName(ChatColor.GREEN + 
            "Mending Chainmail Boots");
          List<String> blore = new ArrayList<String>();
          blore.add(ChatColor.RED + "DPS: 2 - 2%");
          blore.add(ChatColor.RED + "HP: +" + helm);
          blore.add(ChatColor.RED + "HP REGEN: +" + regen + " HP/s");
          blore.add(common);
          bootmeta.setLore(blore);
          B.setItemMeta(bootmeta);
          e.getDrops().add(B);
        }
        if (drop == 5)
        {
          ItemStack H = new ItemStack(Material.CHAINMAIL_HELMET);
          ItemMeta helmmeta = H.getItemMeta();
          helmmeta.setDisplayName(ChatColor.GREEN + 
            "Medium Helmet of Fortitude");
          List<String> hlore = new ArrayList<String>();
          hlore.add(ChatColor.RED + "DPS: 2 - 2%");
          hlore.add(ChatColor.RED + "HP: +" + helm);
          hlore.add(ChatColor.RED + "VIT: +" + vit);
          hlore.add(common);
          helmmeta.setLore(hlore);
          H.setItemMeta(helmmeta);
          e.getDrops().add(H);
        }
        if (drop == 6)
        {
          ItemStack C = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
          ItemMeta chestmeta = C.getItemMeta();
          chestmeta.setDisplayName(ChatColor.GREEN + 
            "Chainmail of Fortitude");
          List<String> clore = new ArrayList<String>();
          clore.add(ChatColor.RED + "DPS: 5 - 5%");
          clore.add(ChatColor.RED + "HP: +" + chest);
          clore.add(ChatColor.RED + "VIT: +" + vit);
          clore.add(common);
          chestmeta.setLore(clore);
          C.setItemMeta(chestmeta);
          e.getDrops().add(C);
        }
        if (drop == 7)
        {
          ItemStack L = new ItemStack(Material.CHAINMAIL_LEGGINGS);
          ItemMeta legmeta = L.getItemMeta();
          legmeta.setDisplayName(ChatColor.GREEN + 
            "Chainmail Leggings of Fortitude");
          List<String> llore = new ArrayList<String>();
          llore.add(ChatColor.RED + "DPS: 5 - 5%");
          llore.add(ChatColor.RED + "HP: +" + chest);
          llore.add(ChatColor.RED + "VIT: +" + vit);
          llore.add(common);
          legmeta.setLore(llore);
          L.setItemMeta(legmeta);
          e.getDrops().add(L);
        }
        if (drop == 8)
        {
          ItemStack B = new ItemStack(Material.CHAINMAIL_BOOTS);
          ItemMeta bootmeta = B.getItemMeta();
          bootmeta.setDisplayName(ChatColor.GREEN + 
            "Chainmail Boots of Fortitude");
          List<String> blore = new ArrayList<String>();
          blore.add(ChatColor.RED + "DPS: 2 - 2%");
          blore.add(ChatColor.RED + "HP: +" + helm);
          blore.add(ChatColor.RED + "VIT: +" + vit);
          blore.add(common);
          bootmeta.setLore(blore);
          B.setItemMeta(bootmeta);
          e.getDrops().add(B);
        }
        if ((drop == 9) || (drop == 10))
        {
          ItemStack GA = new ItemStack(Material.STONE_AXE);
          ItemMeta gameta = GA.getItemMeta();
          gameta.setDisplayName(ChatColor.GREEN + "Great Axe");
          List<String> galore = new ArrayList<String>();
          galore.add(ChatColor.RED + "DMG: " + min + " - " + max);
          galore.add(common);
          gameta.setLore(galore);
          GA.setItemMeta(gameta);
          e.getDrops().add(GA);
        }
        if ((drop == 11) || (drop == 12))
        {
          ItemStack S = new ItemStack(Material.STONE_SWORD);
          ItemMeta swordmeta = S.getItemMeta();
          swordmeta.setDisplayName(ChatColor.GREEN + "Broadsword");
          List<String> slore = new ArrayList<String>();
          slore.add(ChatColor.RED + "DMG: " + min + " - " + max);
          slore.add(common);
          swordmeta.setLore(slore);
          S.setItemMeta(swordmeta);
          e.getDrops().add(S);
        }
      }
      else if (s.getCustomName() == ChatColor.translateAlternateColorCodes('&', "Starving Bandit"))
      {
        Random random = new Random();
        int drop = random.nextInt(40) + 1;
        int helm = 0;
        int chest = 0;
        int min = 0;
        int max = 0;
        int regen = random.nextInt(11) + 5;
        int vit = random.nextInt(5) + 1;
        String common = "";
        int rarity = random.nextInt(50) + 1;
        int Bloxs = random.nextInt(2) + 1;
        int amt = random.nextInt(5) + 1;
        if (Bloxs == 1)
        {
          ItemStack Blox = new ItemStack(Material.GOLD_NUGGET, amt);
          ItemMeta im = Blox.getItemMeta();
          im.setDisplayName(ChatColor.WHITE + "Blox");
          im.setLore(Arrays.asList(new String[] {ChatColor.GRAY + 
            "The currency of Aeonblox" }));
          Blox.setItemMeta(im);
          e.getDrops().add(Blox);
        }
        if (rarity <= 43)
        {
          common = ChatColor.GRAY + "" + ChatColor.ITALIC + "Common";
          helm = random.nextInt(5) + 5;
          chest = random.nextInt(11) + 10;
          min = random.nextInt(4) + 1;
          max = random.nextInt(5 - min + 1) + min;
        }
        else if ((rarity == 44) || (rarity == 45) || (rarity == 46) || 
          (rarity == 47))
        {
          common = 
            ChatColor.GREEN + "" + ChatColor.ITALIC + "Uncommon";
          helm = random.nextInt(10) + 10;
          chest = random.nextInt(39) + 21;
          min = random.nextInt(2) + 3;
          max = random.nextInt(6 - min + 1) + min;
        }
        else if ((rarity == 48) || (rarity == 49))
        {
          common = ChatColor.AQUA + "" + ChatColor.ITALIC + "Rare";
          helm = random.nextInt(30) + 20;
          chest = random.nextInt(50) + 50;
          min = random.nextInt(4) + 6;
          max = random.nextInt(12) + 9;
        }
        else if (rarity == 50)
        {
          common = 
            ChatColor.YELLOW + "" + ChatColor.ITALIC + "Unique";
          helm = random.nextInt(11) + 50;
          chest = random.nextInt(51) + 100;
          min = random.nextInt(2) + 9;
          max = random.nextInt(7) + 24;
        }
        if (drop == 1)
        {
          ItemStack H = new ItemStack(Material.LEATHER_HELMET);
          ItemMeta helmmeta = H.getItemMeta();
          helmmeta.setDisplayName(ChatColor.WHITE + 
            "Mending Leather Coif");
          List<String> hlore = new ArrayList<String>();
          hlore.add(ChatColor.RED + "DPS: 1 - 1%");
          hlore.add(ChatColor.RED + "HP: +" + helm);
          hlore.add(ChatColor.RED + "HP REGEN: +" + regen + " HP/s");
          hlore.add(common);
          helmmeta.setLore(hlore);
          H.setItemMeta(helmmeta);
          e.getDrops().add(H);
        }
        if (drop == 2)
        {
          ItemStack C = new ItemStack(Material.LEATHER_CHESTPLATE);
          ItemMeta chestmeta = C.getItemMeta();
          chestmeta.setDisplayName(ChatColor.WHITE + 
            "Mending Leather Chestplate");
          List<String> clore = new ArrayList<String>();
          clore.add(ChatColor.RED + "DPS: 1 - 1%");
          clore.add(ChatColor.RED + "HP: +" + chest);
          clore.add(ChatColor.RED + "HP REGEN: +" + regen + " HP/s");
          clore.add(common);
          chestmeta.setLore(clore);
          C.setItemMeta(chestmeta);
          e.getDrops().add(C);
        }
        if (drop == 3)
        {
          ItemStack L = new ItemStack(Material.LEATHER_LEGGINGS);
          ItemMeta legmeta = L.getItemMeta();
          legmeta.setDisplayName(ChatColor.WHITE + 
            "Mending Leather Leggings");
          List<String> llore = new ArrayList<String>();
          llore.add(ChatColor.RED + "DPS: 1 - 1%");
          llore.add(ChatColor.RED + "HP: +" + chest);
          llore.add(ChatColor.RED + "HP REGEN: +" + regen + " HP/s");
          llore.add(common);
          legmeta.setLore(llore);
          L.setItemMeta(legmeta);
          e.getDrops().add(L);
        }
        if (drop == 4)
        {
          ItemStack B = new ItemStack(Material.LEATHER_BOOTS);
          ItemMeta bootmeta = B.getItemMeta();
          bootmeta.setDisplayName(ChatColor.WHITE + 
            "Mending Leather Boots");
          List<String> blore = new ArrayList<String>();
          blore.add(ChatColor.RED + "DPS: 1 - 1%");
          blore.add(ChatColor.RED + "HP: +" + helm);
          blore.add(ChatColor.RED + "HP REGEN: +" + regen + " HP/s");
          blore.add(common);
          bootmeta.setLore(blore);
          B.setItemMeta(bootmeta);
          e.getDrops().add(B);
        }
        if (drop == 5)
        {
          ItemStack H = new ItemStack(Material.LEATHER_HELMET);
          ItemMeta helmmeta = H.getItemMeta();
          helmmeta.setDisplayName(ChatColor.WHITE + 
            "Leather Coif of Fortitude");
          List<String> hlore = new ArrayList<String>();
          hlore.add(ChatColor.RED + "DPS: 1 - 1%");
          hlore.add(ChatColor.RED + "HP: +" + helm);
          hlore.add(ChatColor.RED + "VIT: +" + vit);
          hlore.add(common);
          helmmeta.setLore(hlore);
          H.setItemMeta(helmmeta);
          e.getDrops().add(H);
        }
        if (drop == 6)
        {
          ItemStack C = new ItemStack(Material.LEATHER_CHESTPLATE);
          ItemMeta chestmeta = C.getItemMeta();
          chestmeta.setDisplayName(ChatColor.WHITE + 
            "Leather Chestplate of Fortitude");
          List<String> clore = new ArrayList<String>();
          clore.add(ChatColor.RED + "DPS: 1 - 1%");
          clore.add(ChatColor.RED + "HP: +" + chest);
          clore.add(ChatColor.RED + "VIT: +" + vit);
          clore.add(common);
          chestmeta.setLore(clore);
          C.setItemMeta(chestmeta);
          e.getDrops().add(C);
        }
        if (drop == 7)
        {
          ItemStack L = new ItemStack(Material.LEATHER_LEGGINGS);
          ItemMeta legmeta = L.getItemMeta();
          legmeta.setDisplayName(ChatColor.WHITE + 
            "Leather Leggings of Fortitude");
          List<String> llore = new ArrayList<String>();
          llore.add(ChatColor.RED + "DPS: 1 - 1%");
          llore.add(ChatColor.RED + "HP: +" + chest);
          llore.add(ChatColor.RED + "VIT: +" + vit);
          llore.add(common);
          legmeta.setLore(llore);
          L.setItemMeta(legmeta);
          e.getDrops().add(L);
        }
        if (drop == 8)
        {
          ItemStack B = new ItemStack(Material.LEATHER_BOOTS);
          ItemMeta bootmeta = B.getItemMeta();
          bootmeta.setDisplayName(ChatColor.WHITE + 
            "Leather Boots of Fortitude");
          List<String> blore = new ArrayList<String>();
          blore.add(ChatColor.RED + "DPS: 1 - 1%");
          blore.add(ChatColor.RED + "HP: +" + helm);
          blore.add(ChatColor.RED + "VIT: +" + vit);
          blore.add(common);
          bootmeta.setLore(blore);
          B.setItemMeta(bootmeta);
          e.getDrops().add(B);
        }
        if ((drop == 9) || (drop == 10))
        {
          ItemStack GA = new ItemStack(Material.WOOD_AXE);
          ItemMeta gameta = GA.getItemMeta();
          gameta.setDisplayName(ChatColor.WHITE + "Hatchet");
          List<String> galore = new ArrayList<String>();
          galore.add(ChatColor.RED + "DMG: " + min + " - " + max);
          galore.add(common);
          gameta.setLore(galore);
          GA.setItemMeta(gameta);
          e.getDrops().add(GA);
        }
        if ((drop == 11) || (drop == 12))
        {
          ItemStack S = new ItemStack(Material.WOOD_SWORD);
          ItemMeta swordmeta = S.getItemMeta();
          swordmeta.setDisplayName(ChatColor.WHITE + "Shortsword");
          List<String> slore = new ArrayList<String>();
          slore.add(ChatColor.RED + "DMG: " + min + " - " + max);
          slore.add(common);
          swordmeta.setLore(slore);
          S.setItemMeta(swordmeta);
          e.getDrops().add(S);
        }
      }
    }
  }
}