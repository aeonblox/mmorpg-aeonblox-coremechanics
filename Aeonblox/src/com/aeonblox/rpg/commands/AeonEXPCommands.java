package com.aeonblox.rpg.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;

import com.aeonblox.rpg.Aeonblox;

/**
 * This class deals with all Aeonblox EXP related commands.
 * @docsince 1.9.0-SNAPSHOT
 *
 */
public class AeonEXPCommands 
implements Listener, CommandExecutor{
	Aeonblox plugin;
	/**
	 * This is the only constructor for the AeonEXPCommands class.
	 * @param instance - An instance on the plugin (Aeonblox)
	 */
	public AeonEXPCommands(Aeonblox instance){	
		this.plugin = instance;
	}
		
	
	/**
	 * This the command handler called by bukkit.
	 */
	  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	  {
	    if (cmd.getName().equalsIgnoreCase("aeonexp"))
	    {
	      if (args.length == 0)
	      {
	        sender.sendMessage(ChatColor.GOLD + "Info about Aeonexp");
	        sender.sendMessage(ChatColor.GOLD + "by: " + plugin.getDescription().getAuthors().toString().replace("[", "").replace("]", ""));
	        sender.sendMessage(ChatColor.GOLD + "version: " + plugin.getDescription().getVersion());
	        return true;
	      }
	      if (args.length == 1)
	      {
	        if (args[0].equalsIgnoreCase("reload"))
	        {
	          if (sender.hasPermission("aeonexp.reload"))
	          {
	            plugin.cfgHandler.reloadCustomConfig(plugin.expConfig);
	            sender.sendMessage(ChatColor.GOLD + "Reloaded the AeonEXP config!");
	            return true;
	          }
	          sender.sendMessage(ChatColor.RED + "You don't have permission to do that!");
	          return true;
	        }
	        sender.sendMessage(ChatColor.RED + "Wrong usage!");
	        return true;
	      }
	      sender.sendMessage(ChatColor.RED + "Wrong usage!");
	      return true;
	    }
	    return false;
	  }

}
