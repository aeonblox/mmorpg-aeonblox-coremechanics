package com.aeonblox.rpg.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import com.aeonblox.rpg.Aeonblox;

public class Suicide implements Listener, CommandExecutor{
	Aeonblox plugin;
	public Suicide(Aeonblox instance){	
		this.plugin = instance;
	}
	
	  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	  {
	      if ((sender instanceof Player))
	      {
	      final Player p = (Player)sender;
	      if (cmd.getName().equalsIgnoreCase("suicide") && (p.hasPermission("aeonblox.suicide")))
	      {
	      Aeonblox.suicide.add(p.getName());
	      p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "Warning: " + ChatColor.GRAY + 
          "This command will KILL you, you will LOSE every thing you are carrying. It you are sure, type '" + ChatColor.GREEN + "Y" + ChatColor.GRAY + 
	      "', if not, type '" + ChatColor.RED + "cancel'.");
	      return true;
	      }
	      }
		  return false;

}
}
