package com.aeonblox.rpg.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import com.aeonblox.rpg.Aeonblox;

public class InventoryArray 
implements Listener, CommandExecutor{
	Aeonblox plugin;
	public InventoryArray(Aeonblox instance){	
		this.plugin = instance;
	}
	
	  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	  {
	    if ((sender instanceof Player))
	    {
	      final Player p = (Player)sender;
	      if ((cmd.getName().equalsIgnoreCase("toggleblock")) && (p.hasPermission("aeonblox.toggleblock"))) {
	      if (Aeonblox.invarray.contains(p.getName()))
	      {
	      Aeonblox.invarray.remove(p.getName());
	      p.sendMessage(ChatColor.RED + "Inventory Lock Bypass - " + ChatColor.BOLD + "DISABLED");
	      return true;
	      }else{
	      Aeonblox.invarray.add(p.getName());
	      p.sendMessage(ChatColor.GREEN + "Inventory Lock Bypass - " + ChatColor.BOLD + "ENABLED");
	      return true;
	      }
	      }
	      }
		  return false;
	  }
}
