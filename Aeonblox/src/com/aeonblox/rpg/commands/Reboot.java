package com.aeonblox.rpg.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import com.aeonblox.rpg.Aeonblox;

public class Reboot
implements Listener, CommandExecutor {
	Aeonblox plugin;
	public Reboot(Aeonblox plugin){	
		this.plugin = plugin;
	}
	

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2,
			String[] arg3) {
		Player player =  (Player)sender;
	    if (arg1.getName().equalsIgnoreCase("reboot")) {
	    	if(player.hasPermission("aeonblox.reboot")) {
		    	plugin.reboot();
		    	return true;
	    	}else{
	    		return true;
	    	}
	    }
		return false;
	}
	

}
