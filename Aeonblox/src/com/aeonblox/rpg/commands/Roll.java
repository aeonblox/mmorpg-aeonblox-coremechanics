package com.aeonblox.rpg.commands;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import com.aeonblox.rpg.Aeonblox;

public class Roll 
implements Listener, CommandExecutor{
	Aeonblox plugin;
	public Roll(Aeonblox instance){	
		this.plugin = instance;
	}
	
	  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	  {
	      if ((sender instanceof Player))
	      {
	      final Player p = (Player)sender;
	      if (cmd.getName().equalsIgnoreCase("roll") && (p.hasPermission("aeonblox.roll"))) {
	          if ((args.length < 1) || (args.length > 1))
	          {
	            p.sendMessage(ChatColor.RED + "" +  ChatColor.BOLD + 
	              "Incorrect Syntax." + ChatColor.GRAY + 
	              " /roll <1 - 10000>");
	            //Remove if fails
	            return true;
	          }
	          else if (args.length == 1)
	          {
	            int max = 0;
	            try
	            {
	              max = Integer.parseInt(args[0]);
	            }
	            catch (NumberFormatException e)
	            {
	              p.sendMessage(ChatColor.RED + "" +  ChatColor.BOLD + 
	                "Non-Numeric Max Number. /roll <1 - 10000>");
	              return true;
	            }
	            if ((max < 1) || (max > 10000))
	            {
	              p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + 
	                "Incorrect Syntax." + ChatColor.GRAY + 
	                " /roll <1 - 10000>");
	              //Remove if fails
	              return true;
	            }
	            else
	            {
	              Random random = new Random();
	              int roll = random.nextInt(max + 1);
	              Bukkit.broadcastMessage(ChatColor.GRAY + p.getName() + " has rolled a " + ChatColor.GRAY + 
	                ChatColor.BOLD + ChatColor.UNDERLINE + roll + ChatColor.GRAY + " out of " + ChatColor.GRAY + ChatColor.BOLD + ChatColor.UNDERLINE + max + ".");
	              return true;
	            }
	          }
	        }
	      }
		  return false;

}

}
