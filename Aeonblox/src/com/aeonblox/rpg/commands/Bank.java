package com.aeonblox.rpg.commands;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;

import com.aeonblox.rpg.Aeonblox;

/**
 * This class deals with banking within the Aeonblox plugin.
 *
 */
public class Bank
implements Listener, CommandExecutor{
	Aeonblox plugin;
	/**
	 * This is the only constructor for the Bank class.
	 * @param instance - An instance of the plugin (Aeonblox)
	 */
	public Bank(Aeonblox instance){	
		this.plugin = instance;
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	  {
	    if ((sender instanceof Player))
	    {
	      final Player p = (Player)sender;
    	  final Location oldLoc = p.getLocation();
	      if ((cmd.getName().equalsIgnoreCase("bank")) && 
	      (p.hasPermission("aeonblox.bank")) && 
	      (!Aeonblox.bank.contains(p.getName())))
	      {
	    	  
	    	Aeonblox.bank.add(p.getName());
	    	p.sendMessage(ChatColor.RED + "Your bank will be " + 
	    	ChatColor.BOLD + "OPENED" + ChatColor.RED + " shortly.");
	    	p.sendMessage(ChatColor.RED + "Bank opening in ... " + ChatColor.BOLD + "5s");
	    	        
	    	new BukkitRunnable()
	    	{
	    	public void run()
	    	{
    		if (!p.getLocation().equals(oldLoc) && (Aeonblox.bank.contains(p.getName()))){
            Aeonblox.bank.remove(p.getName());
            p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "Bank Open - CANCELLED");
    		}
	    	}
	    	}.runTaskTimer(plugin, 20L, 20L);
	    	new BukkitRunnable()
	    	{
	    	public void run()
	    	{
	    	if (Aeonblox.bank.contains(p.getName()))
	    	{
	    	Aeonblox.bank.remove(p.getName());
	    	p.openInventory(p.getEnderChest());
	    	}
	    	}
	    	}.runTaskLater(plugin, 100L);
	    	return true;
	    	}
	    }
		return false;
	  }
}