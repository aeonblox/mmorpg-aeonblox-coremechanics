package com.aeonblox.rpg.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import com.aeonblox.rpg.Aeonblox;

public class ToggleTrails 
implements Listener, CommandExecutor{
	Aeonblox plugin;
	public ToggleTrails(Aeonblox instance){	
		this.plugin = instance;
	}
	
	  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	  {
	    if ((sender instanceof Player))
	    {
	      final Player p = (Player)sender;
	      if ((cmd.getName().equalsIgnoreCase("toggletrail")) && (p.hasPermission("aeonblox.toggletrail"))) {
	      if (Aeonblox.toggletrail.contains(p.getName()))
	      {
	      Aeonblox.toggletrail.remove(p.getName());
	      plugin.cfgHandler.getCustomConfig(plugin.trailConfig).set("Players." + p.toString(), null);
	      p.sendMessage(ChatColor.RED + "Particle Trail - " + ChatColor.BOLD + "DISABLED");
	      return true;
	      }else{
	      Aeonblox.toggletrail.add(p.getName());
	      p.openInventory(Aeonblox.Itrail);
	      p.sendMessage(ChatColor.GREEN + "Particle Trail - " + ChatColor.BOLD + "ENABLED");
	      return true;
	      }
	      }
	      }
		  return false;
	  }
}
