package com.aeonblox.rpg.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import com.aeonblox.rpg.Aeonblox;

public class RandomTPMechanics
implements Listener, CommandExecutor
{
Aeonblox plugin;	
public RandomTPMechanics(Aeonblox plugin){	
	this.plugin = plugin;
}
	
private int[] blockBlackListArray;
private double[] testLocation = { 0.0D, 0.0D, 0.0D, 0.0D };
private int xRadius;
private int zRadius;
private int maxTries;
//private String[] jarVersion = { "0.8", "0.9", "0.9.1", "0.9.2", "0.9.3" };
private String errorReason = "";
private double[] portalPos1 = { 0.0D, 0.0D, 0.0D };
private double[] portalPos2 = { 0.0D, 0.0D, 0.0D };
private String portalWorld;
private double[] playerPos = { 0.0D, 0.0D, 0.0D };
private boolean checked = false;
BukkitTask timing;
public static boolean update = false;


@SuppressWarnings("deprecation")
public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
{
  if ((sender instanceof BlockCommandSender))
  {
    BlockCommandSender blockSender = (BlockCommandSender)sender;
    if (cmd.getName().equalsIgnoreCase("tpr")) {
      if (args.length < 1)
      {
        plugin.getLogger().info("Not enough arguments from CommandBlock at (" + blockSender.getBlock().getX() + "," + blockSender.getBlock().getY() + "," + blockSender.getBlock().getZ() + ")");
      }
      else if (args.length == 1)
      {
        tpr(Bukkit.getPlayer(args[0]), blockSender.getBlock().getLocation(), sender, this.xRadius, this.zRadius);
      }
      else
      {
        String[] coords = args[1].split(",");
        tpr(Bukkit.getPlayer(args[0]), blockSender.getBlock().getLocation(), sender, Integer.parseInt(coords[0]), Integer.parseInt(coords[1]));
      }
    }
    return true;
  }
  if (!(sender instanceof Player))
  {
    sender.sendMessage(ChatColor.GOLD + "You cannot run this command from the console!");
    return true;
  }
  ArrayList<String> argsList = new ArrayList<String>();
  for (String arg : args) {
    argsList.add(arg);
  }
  Player target = (Player)sender;
  for (int i = 0; i < argsList.size(); i++)
  {
    String string = (String)argsList.get(i);
    if (string.toLowerCase().startsWith("p:"))
    {
      argsList.remove(i);
      if (sender.hasPermission("aeonblox.randomtp.use.others"))
      {
        if (plugin.getServer().getPlayer(string.substring(2)) != null)
        {
          target = plugin.getServer().getPlayer(string.substring(2));
        }
        else
        {
          sender.sendMessage(ChatColor.GOLD +  string.substring(2) + ChatColor.GOLD + " is not online.");
          return true;
        }
      }
      else
      {
        sender.sendMessage(ChatColor.RED + "You are not allowed to do this.");
        return true;
      }
    }
  }
  Location center = target.getLocation();
  for (int i = 0; i < argsList.size(); i++)
  {
    String string = (String)argsList.get(i);
    if (string.toLowerCase().startsWith("c:")) {
      if (sender.hasPermission("aeonblox.randomtp.use.center"))
      {
        try
        {
          if (string.equals("c:"))
          {
            center = target.getLocation();
          }
          else
          {
            String[] centers = string.substring(2).split(",");
            if (centers.length == 1)
            {
              center = plugin.getServer().getPlayer(string.substring(2)).getLocation();
            }
            else
            {
              center.setX(Integer.parseInt(centers[0]));
              center.setZ(Integer.parseInt(centers[1]));
            }
          }
        }
        catch (NullPointerException e)
        {
          sender.sendMessage(ChatColor.GOLD +  string.substring(2) + ChatColor.GOLD + " is not online.");
          return true;
        }
        argsList.remove(i);
      }
      else
      {
        sender.sendMessage(ChatColor.RED + "You do not have sufficient permission to do this.");
        return true;
      }
    }
  }
  if (cmd.getName().equalsIgnoreCase("tpr")) {
    if (argsList.size() == 0)
    {
      if (sender.hasPermission("aeonblox.randomtp.use.default"))
      {
        sender.sendMessage(ChatColor.GOLD + "Teleporting " + ChatColor.GOLD + target.getName() + ChatColor.GOLD + " in a " + ChatColor.GOLD + this.xRadius + ChatColor.GOLD + " x " + ChatColor.GOLD + this.zRadius + ChatColor.GOLD + " radius around " + ChatColor.GOLD + center.getX() + ChatColor.GOLD + ", " + ChatColor.GOLD + center.getZ());
        tpr(target, center, (Player)sender, this.xRadius, this.zRadius);
      }
      else
      {
        sender.sendMessage(ChatColor.RED + "You are not allowed to do this.");
      }
    }
    else if (argsList.size() == 1)
    {
      String str1;
      switch ((str1 = (String)argsList.get(0)).hashCode())
      {
      case -934641255: 
        if (str1.equals("reload")) {
          if (sender.hasPermission("aeonblox.randomtp.reload"))
          {
            // reloadConfiguration();
        	  plugin.cfgHandler.reloadCustomConfig(plugin.randomConfig);
        	  getBlocksBlackList();
        	  getRadius();
        	  getMaxTryCount();
        	  getPortal();
        	  plugin.getLogger().info("Config Reloaded");
            sender.sendMessage(ChatColor.GOLD + "Config Reloaded!");
          }
          else
          {
            sender.sendMessage(ChatColor.RED + "You are not allowed to do this.");
          }
        }
        break;
      }
      if (sender.hasPermission("aeonblox.randomtp.use.custom"))
      {
        if (!testIfNumber((String)argsList.get(0)))
        {
          sender.sendMessage(ChatColor.GOLD +  (String)argsList.get(0) + ChatColor.GOLD +  "' is not a valid number.");
        }
        else
        {
          sender.sendMessage(ChatColor.GOLD + "Teleporting " + ChatColor.GOLD + target.getName() + ChatColor.GOLD + " in a " + ChatColor.GOLD + (String)argsList.get(0) + ChatColor.GOLD + " radius around " + ChatColor.GOLD + center.getX() + ChatColor.GOLD + ", " + ChatColor.GOLD + center.getY());
          tpr(target, center, (Player)sender, Integer.parseInt((String)argsList.get(0)), Integer.parseInt((String)argsList.get(0)));
        }
      }
      else {
        sender.sendMessage(ChatColor.RED + "You are not allowed to do this.");
      }
    }
    else if (argsList.size() == 2)
    {
      if (sender.hasPermission("aeonblox.randomtp.custom"))
      {
        if ((!testIfNumber((String)argsList.get(0))) || (!testIfNumber((String)argsList.get(1))))
        {
          sender.sendMessage(ChatColor.GOLD + (String)argsList.get(0) + ChatColor.GOLD + "' or '" + ChatColor.GOLD + (String)argsList.get(1) + ChatColor.GOLD + "' is not a valid number.");
        }
        else
        {
          sender.sendMessage(ChatColor.GOLD + "Teleporting " + target.getName() + ChatColor.GOLD + " in a " + ChatColor.GOLD + (String)argsList.get(0) + ChatColor.GOLD + " x " + ChatColor.GOLD + (String)argsList.get(1) + ChatColor.GOLD + " radius around " + ChatColor.GOLD + center.getX() + ChatColor.GOLD + ", " + ChatColor.GOLD + center.getY());
          tpr(target, center, (Player)sender, Integer.parseInt((String)argsList.get(0)), Integer.parseInt((String)argsList.get(1)));
        }
      }
      else {
        sender.sendMessage(ChatColor.RED + "You are not allowed to do this.");
      }
    }
    else
    {
      sender.sendMessage(ChatColor.RED + "Too many Arguments");
    }
  }
  return true;
}

private boolean testIfNumber(String s)
{
  try
  {
    int n = Integer.parseInt(s);
    if (n < 1) {
      return false;
    }
  }
  catch (NumberFormatException nfe)
  {
    return false;
  }
  return true;
}
/*
public void loadConfiguration()
{
  getConfig().options().copyDefaults(true);
  saveDefaultConfig();
  getBlocksBlackList();
  getMaxTryCount();
  getRadius();
  getPortal();
  checkConfigCompatibility();
}*/
/*
public void reloadConfiguration()
{
// reloadConfig();
  plugin.cfgHandler.reloadCustomConfig(plugin.chatConfig);
  getBlocksBlackList();
  getRadius();
  getMaxTryCount();
  getPortal();
  checkConfigCompatibility();
  plugin.getLogger().info("Config Reloaded");
}*/

public void getBlocksBlackList()
{
  List<Integer> blockBlackList = plugin.cfgHandler.getCustomConfig(plugin.randomConfig).getIntegerList("blockBlackList");
  this.blockBlackListArray = new int[blockBlackList.size()];
  for (int i = 0; i < blockBlackList.size(); i++)
  {
    int x = ((Integer)blockBlackList.get(i)).intValue();
    this.blockBlackListArray[i] = x;
  }
}

public void getMaxTryCount()
{
  this.maxTries = plugin.cfgHandler.getCustomConfig(plugin.randomConfig).getInt("maximumTries");
  if (this.maxTries == -1) {
    this.maxTries = 2147483647;
  } else if (this.maxTries < -1) {
    this.maxTries = 0;
  }
}

public void getRadius()
{
  this.xRadius = plugin.cfgHandler.getCustomConfig(plugin.randomConfig).getInt("xRadius");
  this.zRadius = plugin.cfgHandler.getCustomConfig(plugin.randomConfig).getInt("zRadius");
}

public void getPortal()
{
  this.portalWorld = plugin.cfgHandler.getCustomConfig(plugin.randomConfig).getString("portal.world");
  this.portalPos1[0] = plugin.cfgHandler.getCustomConfig(plugin.randomConfig).getDouble("portal.pos1.X");
  this.portalPos1[1] = plugin.cfgHandler.getCustomConfig(plugin.randomConfig).getDouble("portal.pos1.Y");
  this.portalPos1[2] = plugin.cfgHandler.getCustomConfig(plugin.randomConfig).getDouble("portal.pos1.Z");
  this.portalPos2[0] = plugin.cfgHandler.getCustomConfig(plugin.randomConfig).getDouble("portal.pos2.X");
  this.portalPos2[1] = plugin.cfgHandler.getCustomConfig(plugin.randomConfig).getDouble("portal.pos2.Y");
  this.portalPos2[2] = plugin.cfgHandler.getCustomConfig(plugin.randomConfig).getDouble("portal.pos2.Z");
}
/**
public void checkConfigCompatibility()
{
  String configVersion = plugin.cfgHandler.getCustomConfig(plugin.randomConfig).getString("Version");
  boolean configIsCompatible = false;
  for (String compatibleVersion : this.jarVersion) {
    if (configVersion.equals(compatibleVersion)) {
      configIsCompatible = true;
    }
  }
  if (!configIsCompatible)
  {
    plugin.getLogger().info("Config is incompatible. Please delete config and Reload!");
    plugin.getLogger().info("Using Default values!");
  }
}*/

private void loadChunk(World w, double x, double z)
{
  Chunk chunk = w.getChunkAt((int)x, (int)z);
  chunk.load(true);
  plugin.getLogger().info("Loading Chunk");
}

private void tpr(Player target, Location center, CommandSender sender, int xRadius, int zRadius)
{
  Location currentLocation = center;
  World w = currentLocation.getWorld();
  Random random = new Random();
  
  int tooManyTimes = 0;
  while ((this.testLocation[0] != 1.0D) && (tooManyTimes < this.maxTries))
  {
    double x = random.nextInt(xRadius * 2) - xRadius;
    double z = random.nextInt(zRadius * 2) - zRadius;
    
    this.testLocation[1] = (x + currentLocation.getX());
    this.testLocation[3] = (z + currentLocation.getZ());
    this.testLocation[2] = (w.getHighestBlockYAt((int)this.testLocation[1], (int)this.testLocation[3]) - 1);
    
    @SuppressWarnings("deprecation")
	int testBlock = w.getBlockTypeIdAt((int)this.testLocation[1], (int)this.testLocation[2], (int)this.testLocation[3]);
    
    int isSafe = 0;
    for (int i = 0; i < this.blockBlackListArray.length; i++) {
      if (testBlock != this.blockBlackListArray[i]) {
        isSafe++;
      }
    }
    if (isSafe == this.blockBlackListArray.length) {
      this.testLocation[0] = 1.0D;
    } else {
      this.testLocation[0] = 0.0D;
    }
    tooManyTimes++;
  }
  if (tooManyTimes >= this.maxTries) {
    this.errorReason = ChatColor.RED + "Couldn't find a safe teleportation location";
  }
  if (this.testLocation[0] == 1.0D)
  {
    Location finalLocation = currentLocation;
    if (this.testLocation[1] < 0.0D) {
      finalLocation.setX(this.testLocation[1] + 1.0D);
    } else {
      finalLocation.setX(this.testLocation[1]);
    }
    finalLocation.setY(this.testLocation[2] + 1.0D);
    if (this.testLocation[3] < 0.0D) {
      finalLocation.setZ(this.testLocation[3] + 1.0D);
    } else {
      finalLocation.setZ(this.testLocation[3]);
    }
    loadChunk(w, finalLocation.getX(), finalLocation.getZ());
    target.teleport(finalLocation);
    checkSafe(target, finalLocation);
  }
  else
  {
    sender.sendMessage(ChatColor.RED + "Failed: " + this.errorReason);
  }
  this.testLocation[0] = 0.0D;
}

public void checkSafe(final Player targetCheck, final Location finalLocationCheck)
{
  if (this.checked)
  {
    this.checked = false;
    if (targetCheck.getLocation().getY() < finalLocationCheck.getY() - 1.0D)
    {
      plugin.getLogger().info("Saving Player!");
      Location currentLocation = targetCheck.getLocation();
      currentLocation.setY(finalLocationCheck.getY());
      targetCheck.teleport(currentLocation);
    }
  }
  else
  {
    this.checked = true;
    this.timing = new BukkitRunnable()
    {
      public void run()
      {
        RandomTPMechanics.this.checkSafe(targetCheck, finalLocationCheck);
//      RandomTPMechanics.this.getLogger().info("Check done"); 
        plugin.getLogger().info("Check done"); 
      }
    }.runTaskLater(plugin, 5L);
  }
}

@EventHandler
private void atPortal(PlayerPortalEvent event)
{
  Player player = event.getPlayer();
  this.playerPos[0] = player.getLocation().getX();
  this.playerPos[1] = player.getLocation().getY();
  this.playerPos[2] = player.getLocation().getZ();
  if ((player.getLocation().getWorld().getName().toString().toLowerCase().equals(this.portalWorld)) && (
    ((this.portalPos1[0] <= this.playerPos[0]) && (this.playerPos[0] <= this.portalPos2[0])) || ((this.portalPos2[0] <= this.playerPos[0]) && (this.playerPos[0] <= this.portalPos1[0]) && (
    ((this.portalPos1[1] <= this.playerPos[1]) && (this.playerPos[1] <= this.portalPos2[1])) || ((this.portalPos2[1] <= this.playerPos[1]) && (this.playerPos[1] <= this.portalPos1[1]) && (
    ((this.portalPos1[2] <= this.playerPos[2]) && (this.playerPos[2] <= this.portalPos2[2])) || ((this.portalPos2[2] <= this.playerPos[2]) && (this.playerPos[2] <= this.portalPos1[2]))))))))
  {
    event.setCancelled(true);
    if (player.hasPermission("aeonblox.randomtp.use.portal")) {
      tpr(player, player.getLocation(), player, this.xRadius, this.zRadius);
    } else {
      player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "I will not let you do this.");
    }
  }
}
}
