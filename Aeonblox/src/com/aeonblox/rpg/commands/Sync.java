package com.aeonblox.rpg.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import com.aeonblox.rpg.Aeonblox;

public class Sync
implements Listener, CommandExecutor{
	Aeonblox plugin;
	public Sync(Aeonblox instance){	
		this.plugin = instance;
	}
	
	  @SuppressWarnings("deprecation")
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	  {
	    if ((sender instanceof Player))
	    {
	      final Player p = (Player)sender;
	      if (cmd.getName().equalsIgnoreCase("sync") && (p.hasPermission("aeonblox.sync"))) 
	      {
	        p.updateInventory();
	        p.saveData();
	        p.sendMessage(ChatColor.GREEN + "Synced player data to " + 
	          ChatColor.GREEN + ChatColor.UNDERLINE + "HIVE" + 
	          ChatColor.GREEN + " server.");
	        return true;
	      }
	}
		return false;

}
}