package com.aeonblox.rpg.social;

import java.security.acl.Permission;
import java.util.ArrayList;

import net.milkbowl.vault.chat.Chat;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;

import com.aeonblox.rpg.Aeonblox;

public class ChatAlert implements Listener, CommandExecutor{
	
	public static Chat chat = null;
	public static Permission perms = null;
	public String msgColor;
	public ChatAlert ca;
	public ArrayList<String> onCooldown = new ArrayList<String>();
	public String chatPluginPrefix = ChatColor.GOLD + "[" + ChatColor.AQUA + "ChatPingMechanics"+ ChatColor.GOLD + "] " + ChatColor.RESET;
	private String tagColor;
	private int cooldownTime;
    Aeonblox plugin; 	
    public ChatAlert(Aeonblox instance){
    	this.plugin = instance; // you forgot this, hence the plugin + the config are null
    }
	@Override
	public boolean onCommand(CommandSender sender, Command cmd,	String commandLabel, String[] args) {
		if (commandLabel.equalsIgnoreCase("cpm") && sender.hasPermission("aeonblox.chatalert.reload")){
			/*if (args.length == 2){
				if (args[0].equalsIgnoreCase("check")){
					String targetName = args[1];
					Player targetPlayer = Bukkit.getPlayer(targetName);
					if (targetPlayer != null ){
						String displayName = targetPlayer.getDisplayName();
						sender.sendMessage(displayName);
						return true;
					} else {
						sender.sendMessage("Player is not online");
						return true;
					}
				}
				if (args[0].equalsIgnoreCase("match")){
					String phrase = args[1];
					if (phrase.length() >= 3){
						for (Player player : Bukkit.getOnlinePlayers()){
							String nick = player.getDisplayName();
							String nickName = nick.replaceAll("[]", " "); // this lines needs revising
							String playerName = player.getName();
							if (nickName.startsWith(phrase)){
								Bukkit.broadcastMessage(nickName + " Match found.");
								log.info(nickName + " Match found.");
								return true;
							} else if (playerName.startsWith(phrase)){
								Bukkit.broadcastMessage(playerName + "Match found.");
								log.info(playerName + " Match found.");
								return true;
							} else {
								Bukkit.broadcastMessage("No match found.");
								log.info("No match found.");
								return true;
							}
						}
					}
					sender.sendMessage("Too short");
					return true;
				}
			}*/
			if (args.length == 0){
				plugin.cfgHandler.reloadCustomConfig(plugin.chatConfig);
				sender.sendMessage(chatPluginPrefix + ChatColor.GREEN + "All in check, Files reloaded, ChatColor:" + 
						plugin.cfgHandler.getCustomConfig(plugin.chatConfig).getString("MsgColor") + " Tagging color: " + plugin.cfgHandler.getCustomConfig(plugin.chatConfig).getString("Color"));
			}
			return true;
		}
		return false;
	}
	public String getMsgColor(){
		String msgColor = ChatColor.translateAlternateColorCodes('&', plugin.cfgHandler.getCustomConfig(plugin.chatConfig).getString("MsgColor")); 
		
		return msgColor;
	}
	
	public String getTagColor(){
		tagColor = ChatColor.translateAlternateColorCodes('&', plugin.cfgHandler.getCustomConfig(plugin.chatConfig).getString("Color"));
		return tagColor;
	}
	
	public Integer getCooldownTime(){
		String cdTime = plugin.cfgHandler.getCustomConfig(plugin.chatConfig).getString("CooldownTime","10");
		cooldownTime = Integer.parseInt(cdTime)*20;
		return cooldownTime;
	}
	
/**	 private boolean setupChat() {
		 if (getServer().getPluginManager().getPlugin("Vault") == null) {
			 return false;
		 }
		 RegisteredServiceProvider<Chat> rsp = getServer().getServicesManager().getRegistration(Chat.class);
		 if (rsp == null) {
			 return false;
		 }
		 chat = rsp.getProvider();
		 return chat != null;
	 } */

}
