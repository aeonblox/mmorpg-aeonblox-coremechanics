package com.aeonblox.rpg.social;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class Party {
//objects and stuff
private UUID owner;
private PartyManager partyManager;
private ArrayList<UUID> players;
private String name;
private Team team;
private Team ownerTeam;
public Objective healthObjective;
private Scoreboard ScoreBoard;
enum RemoveReason{
	KICK,
	CLOSE,
	COMMAND,
	QUIT
}
public Party(String name, UUID owner, PartyManager manager){
	this.owner = owner;
	this.partyManager = manager;
	this.players = new ArrayList<UUID>();
	players.add(owner);
	this.name = name;
	this.partyManager.addParty(this);
	this.ScoreBoard = partyManager.ScoreBoardManager.getNewScoreboard();
	this.team = this.ScoreBoard.registerNewTeam(name);
	this.healthObjective = this.ScoreBoard.registerNewObjective("HealthObjective", "health");
	this.healthObjective.setDisplaySlot(DisplaySlot.SIDEBAR);
	this.healthObjective.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + name);
	this.ownerTeam = this.ScoreBoard.registerNewTeam(Bukkit.getPlayer(owner).getName());
	this.ownerTeam.setPrefix(ChatColor.GOLD + "" + ChatColor.BOLD + "");
	this.team.setPrefix(ChatColor.RED + "" + ChatColor.BOLD + "");
	this.ownerTeam.addPlayer(Bukkit.getPlayer(owner));
}
public String getName(){
	return name;
}
public void setName(String s){
	this.name = s;
}
public Scoreboard getScoreBoard(){
	return this.ScoreBoard;
}
public String getPartyPlayersString(){
	String s = "";
	for (UUID p: this.getPlayers()){
			if (this.getOwner() == p){
			s = s + ChatColor.GOLD + Bukkit.getPlayer(p).getName() + "\n";
			} else {
				s = s + ChatColor.GRAY + Bukkit.getPlayer(p).getName() + "\n";
			}
	}
	if (s.equals("")){
		s = "thats weird, there are no people in your party?! (Not even you!)";
	}
	return s;
}
public UUID getOwner(){
	return owner;
}
public ArrayList<UUID> getPlayers(){
	return players;
}
public void sendMessage(String s){
	for (UUID u: players){
		Bukkit.getPlayer(u).sendMessage(s);
	}
}
public void removePlayer(UUID player, RemoveReason r){
	this.players.remove(player);
	if (r == RemoveReason.COMMAND){
	this.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + Bukkit.getPlayer(player).getName() + ChatColor.RESET + ChatColor.GRAY + " has left the party");
	} else if (r == RemoveReason.KICK){
		this.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + Bukkit.getPlayer(player).getName() + " has been kicked from the party!");
	} else if (r == RemoveReason.QUIT){
		this.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + Bukkit.getPlayer(player).getName() + " has left the party, because they quit!!");
	}
	if (players.size() == 0){
		this.close();
	}
	if (this.getOwner() == player){
		this.setOwner(this.players.get(0));
	}
	try {
	this.team.removePlayer(Bukkit.getPlayer(player));
	this.ownerTeam.removePlayer(Bukkit.getPlayer(player));
	} catch (Exception ex){
		
	}
}
public void close(){
	this.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "The party has closed!");
	try{
	for (int i = 0; i < this.players.size(); i++){
		UUID u = this.players.get(i);
		this.removePlayer(u, RemoveReason.CLOSE);
	}
	this.partyManager.removeParty(this);
	this.ScoreBoard = this.partyManager.emptyBoard;
	for(OfflinePlayer p: this.team.getPlayers()){
		this.team.removePlayer(p);
	}
	this.ownerTeam.removePlayer(Bukkit.getPlayer(this.getOwner()));
	this.owner = null;
	this.ownerTeam = null;
	this.ScoreBoard = null;
	this.team = null;
	this.name = null;
	this.partyManager = null;
	this.players = null;
	}  catch (Exception ex){
	}
}
public void setOwner(UUID u){
	this.ownerTeam.removePlayer(Bukkit.getPlayer(this.getOwner()));
	this.team.addPlayer(Bukkit.getPlayer(this.getOwner()));
	this.team.removePlayer(Bukkit.getPlayer(u));
	this.ownerTeam.addPlayer(Bukkit.getPlayer(u));
	this.owner = u;
	Bukkit.getPlayer(u).sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "You are now the leader of the " + name+ " party!");
	this.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + Bukkit.getPlayer(u).getName() + " is now the leader of the " + name+ " party!");
}
public void addPlayer(UUID u){
	this.players.add(u);
	this.team.addPlayer(Bukkit.getPlayer(u));
}
}
