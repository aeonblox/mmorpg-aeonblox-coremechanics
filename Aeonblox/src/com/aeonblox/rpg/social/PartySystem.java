package com.aeonblox.rpg.social;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.aeonblox.rpg.social.Party.RemoveReason;

public class PartySystem implements CommandExecutor, Listener{
	public PartyManager partyManager;
	public PartySystem(PartyManager manager){
		this.partyManager = manager;
	}
	public String cmdFormat(String uno, String dosso){
		return ChatColor.RED + "" + ChatColor.BOLD + uno + ChatColor.RESET + " " + ChatColor.GRAY + dosso;
	}
	@EventHandler
	public void onEntityHit(EntityDamageByEntityEvent e){
		if (partyManager.getPartyWithPlayer(e.getEntity().getUniqueId()) != null){
			if (partyManager.getPartyWithPlayer(e.getEntity().getUniqueId()).getPlayers().contains(e.getDamager().getUniqueId())){
				((Player) e.getDamager()).sendMessage(ChatColor.RED + "Try not to hit party members!");
				((Player) e.getEntity()).sendMessage(ChatColor.RED + "" + ChatColor.BOLD + Bukkit.getPlayer(e.getEntity().getUniqueId()).getName() +" attempted to hurt you!");
				e.setCancelled(true);
			}
		}
	}
	@EventHandler
	public void leave(PlayerQuitEvent e){
		if (this.partyManager.getPartyWithPlayer(e.getPlayer().getUniqueId()) != null){
			Party party = this.partyManager.getPartyWithPlayer(e.getPlayer().getUniqueId());
			party.removePlayer(e.getPlayer().getUniqueId(), RemoveReason.QUIT);
		}
	}
	@SuppressWarnings("deprecation")
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if (cmd.getName().equalsIgnoreCase("party") || cmd.getName().equalsIgnoreCase("p")){
			if (sender instanceof ConsoleCommandSender){
				sender.sendMessage(ChatColor.RED + "You can't do this as console!");
				return true;
			}
			Player player = (Player) sender;
			if (partyManager.getPartyWithPlayer(player.getUniqueId()) != null){
				Party party = partyManager.getPartyWithPlayer(player.getUniqueId());
				if (args.length == 0){
					if (party.getOwner() == player.getUniqueId()){
						player.sendMessage(ChatColor.GRAY + "Party Leader: Here are some available commands:");
						player.sendMessage(ChatColor.GRAY + "Party Name: " + ChatColor.RED + "" + ChatColor.BOLD + party.getName());
						player.sendMessage(ChatColor.GRAY + "Party Members:");
						player.sendMessage(party.getPartyPlayersString());
						player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "/p invite <name> " + ChatColor.RESET + "" + ChatColor.GRAY + "invite a new player to your party");
						player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "/p kick <name> " + ChatColor.RESET + "" + ChatColor.GRAY + "kick a player from your party");
						player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "/p quit " + ChatColor.RESET + "" + ChatColor.GRAY + "quit your party. This will assign a new owner");
						player.sendMessage(ChatColor.RED+ "" + ChatColor.BOLD + "/p leader <name> " + ChatColor.RESET + "" + ChatColor.GRAY + "give another player the leadership over the party");
						player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "/p close " + ChatColor.RESET + "" + ChatColor.GRAY + "closes the party, everyone will be kicked from it");
						return true;
					} else {
						player.sendMessage(ChatColor.GRAY + "Party member: Here are the available commands");
						player.sendMessage(ChatColor.GRAY + "Party Name: " + ChatColor.RED + "" + ChatColor.BOLD + party.getName());
						player.sendMessage(ChatColor.GRAY + "Party Members:");
						player.sendMessage(party.getPartyPlayersString());
						player.sendMessage(cmdFormat("/p <message>", "send a certain message to the party"));
						player.sendMessage(cmdFormat("/p quit", "quit the party"));
						return true;
					}
				} else if (args.length == 1){
					if (args[0].equalsIgnoreCase("quit")){
						player.sendMessage(ChatColor.RED +"" + ChatColor.BOLD + "You quit the party!");
						party.sendMessage(cmdFormat(player.getName(), "left the party!"));
							party.removePlayer(player.getUniqueId(), RemoveReason.QUIT);
						return true;
					} else if (args[0].equalsIgnoreCase("close")){
						if (party.getOwner() == player.getUniqueId()){
							party.close();
							return true;
						} else {
							player.sendMessage(ChatColor.RED + "You can't do this, you aren't the owner of this party!");
							return true;
						}
					} if (args.length == 1 && args[0].equalsIgnoreCase("list")){
						sender.sendMessage(cmdFormat("The current parties:\n", ""));
						for (Party p: this.partyManager.getParties()){
							sender.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + p.getName());
						}
						return true;
					}else {
						party.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + player.getName() + ": " + args[0]);
						return true;
					}
				} else if (args.length == 2){
					if (args[0].equalsIgnoreCase("invite")){
						if (party.getOwner() == player.getUniqueId()){
							try {
								if(!this.partyManager.pendingInvitations.containsKey(Bukkit.getPlayer(args[1]).getUniqueId())){
									if (!(this.partyManager.getPartyWithPlayer(Bukkit.getPlayer(args[1]).getUniqueId()) != null)){
								this.partyManager.invitePlayerToParty(party, player.getUniqueId(), Bukkit.getPlayer(args[1]).getUniqueId());
								player.sendMessage(ChatColor.GRAY + "Succesfully sent an invitation to " + Bukkit.getPlayer(args[1]).getName());
								return true;
									} else {
										player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "This player already is in a party!");
										return true;
									}
									} else {
									player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "This player already has a pending invitation!");
								return true;
									}
							} catch(NullPointerException ex){
								player.sendMessage(ChatColor.RED + "That player is not online!");
								return true;
							} catch (Exception ex){
								player.sendMessage(ChatColor.RED + "Something went wrong! Report this to the staff please!");
								return true;
							}
						} else {
							player.sendMessage(ChatColor.RED + "You can't do this, you aren't the owner of this party!");
							return true;
						}
					} else if (args[0].equalsIgnoreCase("kick")){
						if (party.getOwner() == player.getUniqueId()){
							this.partyManager.kickPlayerFromParty(party, Bukkit.getPlayer(args[1]).getUniqueId());
							player.sendMessage(ChatColor.RED +  "" + ChatColor.BOLD + "Succesfully kicked " + Bukkit.getPlayer(args[1]).getName());
							return true;
						} else {
							player.sendMessage(ChatColor.RED + "You can't do this, you aren't the owner of this party!");
							return true;
						}
					} else if(args[0].equalsIgnoreCase("leader")){
						if (party.getOwner() == player.getUniqueId()){
							party.setOwner(Bukkit.getPlayer(args[1]).getUniqueId());
							player.sendMessage(ChatColor.GRAY + args[1] + " is now the leader of your party!");
							return true;
						} else {
							player.sendMessage(ChatColor.RED + "You can't do this, you aren't the owner of this party!");
							return true;
						}
					} else {
						party.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + player.getName() + ": " + args[0] + args[1]);
					}
					return true;
				}else {
					String s = "";
					for (int i = 0; i < args.length; i++){
						if (s.equals("")){
							s = args[i];
						} else {
							s = s + " " + args[i];
						}
					}
					party.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + player.getName() + ": " +  s);
					return true;
				}
			} else {
				if (args.length > 1 && args[0].equalsIgnoreCase("create")){
					if (partyManager.partyForName(args[1]) == null){
					Party prt = new Party(args[1], player.getUniqueId(), partyManager);
					player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "You are now the leader of the " + prt.getName() + " party!");
					return true;
					} else {
						player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "There already is a party with the same name!");
						return true;
					}
				} else if (args.length > 0 && args[0].equalsIgnoreCase("list")){
					sender.sendMessage(cmdFormat("The current parties:\n", ""));
					for (Party p: this.partyManager.getParties()){
						sender.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + p.getName());
					}
					return true;
				}
				else if (args.length == 0 || (args.length > 0 && !args[0].equalsIgnoreCase("accept"))){
					player.sendMessage(ChatColor.GRAY + "Hello player, you are not in a party yet!");
					if (!this.partyManager.pendingInvitations.containsKey(player.getUniqueId())){
						player.sendMessage(ChatColor.GRAY + "use " + cmdFormat("/p create <name>", "to create a party"));
					} else {
						player.sendMessage(ChatColor.GRAY + "use " + cmdFormat("/p create <name>", "to create a party"));
						player.sendMessage(ChatColor.GRAY + "use " + cmdFormat("/p accept", "to join the party that invited you!"));
					}
					return true;
				}   else {
					if (this.partyManager.pendingInvitations.containsKey(player.getUniqueId())){
						this.partyManager.pendingInvitations.get(player.getUniqueId()).addPlayer(player.getUniqueId());
						player.sendMessage(ChatColor.GRAY + "You just joined the " + this.partyManager.pendingInvitations.get(player.getUniqueId()).getName() + " party");
						this.partyManager.pendingInvitations.get(player.getUniqueId()).sendMessage(cmdFormat(player.getName(), "joined the party"));
						this.partyManager.pendingInvitations.remove(player.getUniqueId());
						return true;
					} else {
						player.sendMessage(ChatColor.RED + "You don't have any actives invitations!");
						return true;
					}
				}
			}
		}
		return false;
	}
}
