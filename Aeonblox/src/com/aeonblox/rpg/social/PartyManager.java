package com.aeonblox.rpg.social;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

import com.aeonblox.rpg.Aeonblox;
import com.aeonblox.rpg.social.Party.RemoveReason;

public class PartyManager {
public ArrayList<Party> parties;
public HashMap<UUID, Party> pendingInvitations;
public Scoreboard emptyBoard;
public Aeonblox plugin;
public ScoreboardManager ScoreBoardManager;
public PartyManager(Aeonblox instance){
	this.plugin = instance;
	pendingInvitations = new HashMap<UUID, Party>();
	parties = new ArrayList<Party>();
	this.ScoreBoardManager = Bukkit.getScoreboardManager();
	this.emptyBoard = this.ScoreBoardManager.getNewScoreboard();
	Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable(){
		public void run(){
			for (Player p: Bukkit.getOnlinePlayers()){
				boolean doe = false;
				for (Party pr: parties){
					@SuppressWarnings("deprecation")
					Score score = pr.healthObjective.getScore(p);
					if (pr.getPlayers().contains(p.getUniqueId())){
				score.setScore(Integer.valueOf((int) ((Damageable)p).getHealth()));
				p.setScoreboard(pr.getScoreBoard());
				doe = true;
				}
				}
					if (!doe){
					p.setScoreboard(emptyBoard);
					}
			}
		}
	}, 5L, 5L);
}
public ArrayList<Party> getParties(){
	return parties;
}
public void addParty(Party party){
	parties.add(party);
}
public Party getPartyWithPlayer(UUID player){
	if (parties.size() != 0){
	for (Party party: parties){
		if (party.getPlayers() != null && party.getPlayers().contains(player)){
			return party;
		}
	}
	}
	return null;
}
public void invitePlayerToParty(Party party, UUID sender, UUID player){
	this.pendingInvitations.put(player, party);
	Bukkit.getPlayer(player).sendMessage(ChatColor.RED +  "" +ChatColor.BOLD + Bukkit.getPlayer(sender).getName() + ChatColor.RESET + ChatColor.GRAY + " wants you to join their party! Type /p accept to accept the invitation!");
}
public void removeParty(Party p){
	this.parties.remove(p);
}
public void kickPlayerFromParty(Party party, UUID player) {
	party.removePlayer(player, RemoveReason.KICK);
	try {
	party.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + Bukkit.getPlayer(player).getName() + " has left the party because he was kicked!");
	}catch (Exception ex){
		
	}
	}
public Party partyForName(String string) {
	Party p = null;
	for (Party pr: parties){
		if(pr.getName().equalsIgnoreCase(string)){
			p = pr;
		}
	}
	return p;
}
}
