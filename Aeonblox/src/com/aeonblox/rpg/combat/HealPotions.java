package com.aeonblox.rpg.combat;

import org.bukkit.ChatColor;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class HealPotions
implements Listener
{
@EventHandler
public void drinkHealPotion(PlayerItemConsumeEvent event)
{
    ItemStack item = event.getItem();
	ItemMeta itemMeta = item.getItemMeta();
    if (itemMeta == null || itemMeta.getLore() == null)
	{
		return;
	}
	if((itemMeta.hasLore() &&
    (itemMeta.getLore().contains((ChatColor.LIGHT_PURPLE + "Heals 15% HP"))))) {
    Player player = event.getPlayer();
	  Damageable d = (Damageable)event.getPlayer();
      double potionHeal = d.getMaxHealth() / 100.0D * 15;
      if (d.getHealth() - 5.0D + potionHeal > d.getMaxHealth()) {
        player.setHealth(d.getMaxHealth());
      } else {
        player.setHealth(d.getHealth() - 5.0D + potionHeal);
      }
    }
    else if (itemMeta.getLore().contains((ChatColor.LIGHT_PURPLE + "Heals 25% HP"))) {
        Player player = event.getPlayer();
  	  Damageable d = (Damageable)event.getPlayer();
      double potionHeal = d.getMaxHealth() / 100.0D * 25;
      if (d.getHealth() - 5.0D + potionHeal > d.getMaxHealth()) {
        player.setHealth(d.getMaxHealth());
      } else {
        player.setHealth(d.getHealth() - 5.0D + potionHeal);
      }
    }
    //final Player playerFinal = event.getPlayer();
}

/**@EventHandler
public void useSplashHealPotion(PotionSplashEvent event)
{
  if (ItemLoreStats.plugin.getBarAPI() != null) {
    for (PotionEffect e : event.getPotion().getEffects()) {
      if (e.getType().equals(PotionEffectType.HEAL)) {
        for (LivingEntity entity : event.getAffectedEntities()) {
          if ((entity instanceof Player))
          {
            if (event.getPotion().getItem().getDurability() == 16453)
            {
              double potionHeal = entity.getMaxHealth() / 100.0D * ItemLoreStats.plugin.getConfig().getInt("potions.splashHealthI");
              if (entity.getHealth() - 5.0D + potionHeal > entity.getMaxHealth()) {
                entity.setHealth(entity.getMaxHealth());
              } else {
                entity.setHealth(entity.getHealth() - 5.0D + potionHeal);
              }
            }
            else if (event.getPotion().getItem().getDurability() == 16421)
            {
              double potionHeal = entity.getMaxHealth() / 100.0D * ItemLoreStats.plugin.getConfig().getInt("potions.splashHealthII");
              if (entity.getHealth() - 5.0D + potionHeal > entity.getMaxHealth()) {
                entity.setHealth(entity.getMaxHealth());
              } else {
                entity.setHealth(entity.getHealth() - 5.0D + potionHeal);
              }
            }
            final Player playerFinal = (Player)entity;
            ItemLoreStats.plugin.getServer().getScheduler().scheduleSyncDelayedTask(ItemLoreStats.plugin, new Runnable()
            {
              public void run()
              {
                ItemLoreStats.plugin.updateBarAPI(playerFinal);
              }
            }, 2L);
          }
        }
      }
    }
  }
}*/
}

