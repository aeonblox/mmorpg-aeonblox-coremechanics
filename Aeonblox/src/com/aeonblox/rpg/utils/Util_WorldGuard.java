package com.aeonblox.rpg.utils;

import org.bukkit.entity.Player;

import com.aeonblox.rpg.Aeonblox;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.StateFlag;

public class Util_WorldGuard
{
  Aeonblox plugin;
  
  public Util_WorldGuard(Aeonblox instance)
  {
    this.plugin = instance;
  }
  
  public boolean playerInPVPRegion(Player player)
  {
    ApplicableRegionSet set = plugin.getWorldGuard().getRegionManager(player.getWorld()).getApplicableRegions(player.getLocation());
    if (set.getFlag(DefaultFlag.PVP) != null)
    {
      if (((StateFlag.State)set.getFlag(DefaultFlag.PVP)).equals(StateFlag.State.DENY)) {
        return true;
      }
      return false;
    }
    return false;
  }
  
  public boolean playerInSafety(Player player)
  {
    ApplicableRegionSet set = plugin.getWorldGuard().getRegionManager(player.getWorld()).getApplicableRegions(player.getLocation());
    if ((set.getFlag(DefaultFlag.PVP) != null) && (set.getFlag(DefaultFlag.MOB_DAMAGE) != null))
    {
      if ((((StateFlag.State)set.getFlag(DefaultFlag.PVP)).equals(StateFlag.State.DENY)) && ((StateFlag.State)set.getFlag(DefaultFlag.MOB_DAMAGE)).equals(StateFlag.State.DENY)) {
        return true;
      }
      return false;
    }
    return false;
  }
  
  public boolean playerInInvincibleRegion(Player player)
  {
    ApplicableRegionSet set = plugin.getWorldGuard().getRegionManager(player.getWorld()).getApplicableRegions(player.getLocation());
    if (set.getFlag(DefaultFlag.INVINCIBILITY) != null)
    {
      if (((StateFlag.State)set.getFlag(DefaultFlag.INVINCIBILITY)).equals(StateFlag.State.ALLOW)) {
        return true;
      }
      return false;
    }
    return false;
  }
  /*
	 void damageTool(ItemStack item) {
	 System.out.println(item.getDurability() + "" + ChatColor.RED + "is the durability.");
	 short maxDurability = (short) (item.getDurability());
	 int randomnum = (maxDurability - 8);
  short newDurability = (short) (randomnum);
	 System.out.println((newDurability) + "" + ChatColor.RED + "is the new durability.");
	 item.setDurability(newDurability);
	 } */
}

