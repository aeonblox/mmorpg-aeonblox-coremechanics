package com.aeonblox.rpg.utils;

import org.bukkit.ChatColor;

public class StringUtils
{
  public static String replaceChatColors(String s)
  {
	  for(ChatColor c : ChatColor.values()) {
          s = s.replaceAll("&" + c.getChar(), ChatColor.getByChar(c.getChar()) + "");
    }
    return s;
  }
}
/**
* Convert color hex values
*
* @param msg
* @return
*/
/**
public static String parseColors(String msg)
{
    return msg.replace("&", "\u00a7");
}

https://forums.bukkit.org/threads/simple-colors-parsing-method.32058/
*/