package com.aeonblox.rpg.spellsystem.spells;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.Animals;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import com.aeonblox.rpg.Aeonblox;
import com.aeonblox.rpg.spellsystem.Magic;

public class WhirlwindSpell extends Spell {

	private int radius;

	public WhirlwindSpell(Player caster, Aeonblox plugin, String[] spellCommands, Magic parent, int radius) {
		super(caster, plugin, spellCommands, parent);
		this.radius = radius;
	}

	@Override
	public boolean castSpell(Magic magic) {
		try {
			new BukkitRunnable() {

				@Override
				public void run() {
					List<Entity> entities = caster.getNearbyEntities(radius, radius, radius);
					Location to = caster.getLocation();
					to.setX(to.getX() + 3);
					for (Entity e : entities) {
						e.teleport(to);
						if (e instanceof Animals) {
							((Animals) e).addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 10, 10));
						}
						if (e instanceof Monster) {
							((Monster) e).addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 10, 10));
						}
						if (e instanceof Player) {
							((Player) e).addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 10, 10));
						}
					}
					caster.setNoDamageTicks(200);
					Magic.takeHunger(entities.size(), caster);
				}
			}.runTask(plugin);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
