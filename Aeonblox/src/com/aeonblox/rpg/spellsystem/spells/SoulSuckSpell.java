package com.aeonblox.rpg.spellsystem.spells;

import java.util.List;

import org.bukkit.GameMode;
import org.bukkit.entity.Animals;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;

import com.aeonblox.rpg.Aeonblox;
import com.aeonblox.rpg.spellsystem.Magic;

/**
 * This class holds all the Soul Suck Spell methods.
 * 
 * @since 1.9.0-SNAPSHOT
 */
public class SoulSuckSpell extends Spell {

	private Thread thread;
	private int radius;

	/**
	 * Default constructor for the Soul Suck spell class.
	 * 
	 * @param caster
	 *            - The player that cast the spell.
	 * @param plugin
	 *            - The owning plugin.
	 * @param spellCommands
	 *            - The string list of spell commands.
	 * @param parent
	 *            - The Magic class parent.
	 * @param radius
	 *            - The radius in which to check for entities.
	 */
	public SoulSuckSpell(Player caster, Aeonblox plugin, String[] spellCommands, Magic parent, int radius) {
		super(caster, plugin, spellCommands, parent);
		this.radius = radius;
	}

	@Override
	public boolean castSpell(Magic magic) {
		try {
			thread = new Thread() {
				@Override
				public void run() {
					double radiusSquared = radius * radius;
					List<Entity> entities = caster.getNearbyEntities(radius, radius, radius);
					for (Entity entity : entities) {
						if (entity.getLocation().distanceSquared(caster.getLocation()) > radiusSquared) continue;
						if (entity instanceof Player) {
							if (!(((Player) entity).getGameMode() == GameMode.CREATIVE)) damage(entity);
						}
						if (entity instanceof Animals) {
							kill(entity, true);
						}
						if (entity instanceof Monster) {
							kill(entity, false);
						}
					}
					try {
						Damageable d = (Player) caster;
						caster.setHealth(d.getHealth() + entities.size());
					} catch (Exception e) {
						caster.setHealth(20D);
					}
					try {
						caster.setFoodLevel(caster.getFoodLevel() - entities.size());
					} catch (Exception e) {
						caster.setFoodLevel(0);
					}
					try {
						join();
						thread = null;
					} catch (InterruptedException e) {
						interrupt();
						thread = null;
						e.printStackTrace();
					}
				}

				private void kill(Entity entity, boolean animal) {
					if (animal) {
						((Animals) entity).setHealth(0D);
					} else {
						((Monster) entity).setHealth(0D);
					}
				}

				private void damage(Entity entity) {
					try {
						((Player) entity).damage(10D);
					} catch (Exception e) {
						((Player) entity).setHealth(0D);
					}
				}
			};
			thread.start();
			magic.spellCompleted(caster);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Override of super nullify - null-ing more vars.
	 */
	@Override
	public void nullify() {
		thread = null;
		radius = 0;
		super.nullify();
	}

}
