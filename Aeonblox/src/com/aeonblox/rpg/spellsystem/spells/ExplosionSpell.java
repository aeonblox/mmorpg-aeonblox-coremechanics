package com.aeonblox.rpg.spellsystem.spells;

import org.bukkit.entity.Player;

import com.aeonblox.rpg.Aeonblox;
import com.aeonblox.rpg.spellsystem.Magic;

/**
 * This class holds all the Explosion Spell methods.
 * @since 1.9.0-SNAPSHOT
 */
public class ExplosionSpell extends Spell {

	private int hungerCost;

	/**
	 * This is the default constructor for the ExplosionSpell class.
	 * 
	 * @param caster
	 *            - The player that cast the spell.
	 * @param plugin
	 *            - The owning plugin.
	 * @param spellCommands
	 *            - The string list of spell commands.
	 * @param parent
	 *            - The Magic class parent.
	 * @param hungerCost
	 *            - How much hunger the spell takes away.
	 */
	public ExplosionSpell(Player caster, Aeonblox plugin, String[] spellCommands, Magic parent, int hungerCost) {
		super(caster, plugin, spellCommands, parent);
		this.hungerCost = hungerCost;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean castSpell(Magic magic) {
		try {
			Magic.createExplosion(caster.getTargetBlock(null, 20).getLocation(), 3, false, false, caster);
			Magic.takeHunger(hungerCost, caster);
			magic.spellCompleted(caster);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	

}
