package com.aeonblox.rpg.spellsystem.spells;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.bukkit.entity.Player;

import com.aeonblox.rpg.Aeonblox;
import com.aeonblox.rpg.spellsystem.Magic;

public class Spell {

	Player caster = null;
	String spellName = null;
	Integer spellID = null;
	Aeonblox plugin = null;
	String[] spellCommands = null;
	List<String> spellCommandList = null;
	Magic parent = null;

	/**
	 * This is the constructor for the spell class.
	 * 
	 * @param caster
	 *            - The player that cast the spell.
	 * @param plugin
	 *            - The owning plugin.
	 * @param spellCommands
	 *            - The string list of spell commands.
	 * @param parent
	 *            - The Magic class parent.
	 */
	public Spell(Player caster, Aeonblox plugin, String[] spellCommands, Magic parent) {
		this.caster = caster;
		this.plugin = plugin;
		this.spellCommands = spellCommands;
		spellCommandList = new ArrayList<String>(Arrays.asList(spellCommands));
		this.parent = parent;
	}

	/**
	 * This method will cast the spell if it has been overwritten - otherwise it
	 * will print an info message to the console.
	 * 
	 * @param magic
	 *            - The magic class (Will be fixing this later)
	 * @return <b>boolean</b> - whether the spell cast successfully.
	 */
	public boolean castSpell(Magic magic) {
		plugin.getLogger().info("Failed spell cast attempt: Cast method not overwritten");
		return false;
	}

	/**
	 * This will return the {@link Player} that cast the spell.
	 * 
	 * @return <b>{@link Player}</b> - The player that cast the spell.
	 */
	public Player getCaster() {
		return caster;
	}

	/**
	 * This will return the casters name.
	 * 
	 * @return <b>{@link String}</b> - the casters name.
	 */
	public String getCasterName() {
		return caster.getName();
	}

	/**
	 * This will return the casters UUID for storage.
	 * 
	 * @return <b>{@link UUID}</b> - the casters UUID for storage.
	 */
	public UUID getCasterUUID() {
		return caster.getUniqueId();
	}

	/**
	 * This returns the name of the spell.
	 * 
	 * @return <b>{@link String}</b> - the name of the spell.
	 */
	public String getSpellName() {
		return spellName;
	}

	/**
	 * This returns the id of the spell.
	 * 
	 * @return <b>int</b> - the id of the spell.
	 */
	public int getSpellID() {
		return spellID;
	}

	/**
	 * This sets all values in the instance to null.
	 */
	public void nullify() {
		caster = null;
		spellName = null;
		spellID = null;
		plugin = null;
		spellCommands = null;
		spellCommandList = null;
		parent = null;
	}

	/**
	 * This will return a {@link List<String>} with the spell commands stored
	 * within.
	 * 
	 * @return <b>{@link #List<String>}</b> - A list with the spell commands
	 *         stored within.
	 */
	public List<String> getSpellList() {
		return spellCommandList;
	}

	/**
	 * This will return whether the spell commands given match the spell
	 * commands within this class.
	 * 
	 * @param spell
	 *            - The spell to compare
	 * @return <b>boolean</b> - Whether they are the same.
	 */
	public boolean compareSpell(List<String> spell) {
		return spell.equals(spellCommandList);
	}

	/**
	 * This method will set the caster.
	 * 
	 * @param player
	 *            - the player to set.
	 */
	public void setPlayer(Player player) {
		caster = player;
	}

}
