package com.aeonblox.rpg.spellsystem.spells;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import com.aeonblox.rpg.Aeonblox;
import com.aeonblox.rpg.spellsystem.Magic;

public class TeleportSpell extends Spell {

	private int hungerCost;

	public TeleportSpell(Player caster, Aeonblox plugin, String[] spellCommands, Magic parent, int hungerCost) {
		super(caster, plugin, spellCommands, parent);
		this.hungerCost = hungerCost;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean castSpell(Magic magic) {
		try {
			Block target = caster.getTargetBlock(null, 25);
			Location location = target.getLocation();
			caster.playSound(caster.getLocation(), Sound.ENDERMAN_TELEPORT, 10.0F, 1.0F);
			caster.teleport(location);
			Magic.takeHunger(hungerCost, caster);
			magic.spellCompleted(caster);
			magic.hashmap.remove(caster.getName());
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
