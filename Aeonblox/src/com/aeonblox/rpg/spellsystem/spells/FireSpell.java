package com.aeonblox.rpg.spellsystem.spells;

import org.bukkit.Material;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.aeonblox.rpg.Aeonblox;
import com.aeonblox.rpg.spellsystem.ItemTools;
import com.aeonblox.rpg.spellsystem.Magic;
/**
 * This class holds all the Fire Spell methods.
 * @since 1.9.0-SNAPSHOT
 */
public class FireSpell extends Spell {

	int minHungerLevel;
	int hungerCost;

	public FireSpell(Player caster, Aeonblox plugin, String[] spellCommands, Magic magic, int hungerCost, int cancelHungerValue) {
		super(caster, plugin, spellCommands, magic);
		this.spellName = "Firespell";
		this.spellID = new Integer(0);
		this.hungerCost = hungerCost;
		this.minHungerLevel = cancelHungerValue;
	}

	@Override
	public boolean castSpell(final Magic magic) {
		try {
			new BukkitRunnable() {
				@SuppressWarnings("deprecation")
				public void run() {
					if (caster.getFoodLevel() > minHungerLevel) {
						FallingBlock fire = caster.getWorld().spawnFallingBlock(caster.getEyeLocation().add(caster.getLocation().getDirection().normalize()), Material.FIRE, (byte) 0);
						fire.setVelocity(caster.getLocation().getDirection().multiply(2));
						Magic.takeHunger(hungerCost, caster);
					} else {
						ItemTools.resetWandItemName(caster);
						magic.hashmap.remove(caster.getName());
						cancel();
					}
				}
			}.runTaskTimer(this.plugin, 0L, 1L);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
