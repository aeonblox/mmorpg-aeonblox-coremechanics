package com.aeonblox.rpg.spellsystem.spells;

import java.util.List;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import com.aeonblox.rpg.Aeonblox;
import com.aeonblox.rpg.spellsystem.Magic;

/**
 * This class holds all the Lightning Spell methods.
 * 
 * @since 1.9.0-SNAPSHOT
 */
public class LightningSpell extends Spell {

	private int hungerCost;
	private int minHungerLevel;

	/**
	 * Default constructor for the Lightning Spell class.
	 * 
	 * @param caster
	 *            - The player that cast the spell.
	 * @param plugin
	 *            - The owning plugin.
	 * @param spellCommands
	 *            - The string list of spell commands.
	 * @param parent
	 *            - The Magic class parent.
	 * @param hungerCost
	 *            - How much hunger the spell takes away.
	 * @param cancelHungerLevel
	 *            - The hunger level at which to cancel the spell.
	 */
	public LightningSpell(Player caster, Aeonblox plugin, String[] spellCommands, Magic parent, int hungerCost, int cancelHungerLevel) {
		super(caster, plugin, spellCommands, parent);
		this.hungerCost = hungerCost;
		this.minHungerLevel = cancelHungerLevel;
	}

	@Override
	public boolean castSpell(final Magic magic) {
		try {
			caster.setNoDamageTicks(1200);
			new BukkitRunnable() {
				public void run() {
					if (caster.getFoodLevel() > minHungerLevel) {
						List<Block> blocks = Magic.getNearbyBlocks(caster.getLocation(), 3);
						for (Block block2 : blocks) {
							if (block2.getLocation() != caster.getLocation()) {
								caster.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 1200, 1));
								caster.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 1200, 1));
								caster.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 1200, 1));

								caster.getWorld().strikeLightning(block2.getLocation());
							}
						}
						Magic.takeHunger(hungerCost, caster);
					} else {
						magic.hashmap.remove(caster.getName());
						cancel();
						magic.spellCompleted(caster);
					}
				}
			}.runTaskTimer(this.plugin, 0L, 1L);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
