package com.aeonblox.rpg.spellsystem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Animals;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.aeonblox.rpg.Aeonblox;
import com.aeonblox.rpg.spellsystem.spells.ExplosionSpell;
import com.aeonblox.rpg.spellsystem.spells.FireSpell;
import com.aeonblox.rpg.spellsystem.spells.LightningSpell;
import com.aeonblox.rpg.spellsystem.spells.SoulSuckSpell;
import com.aeonblox.rpg.spellsystem.spells.TeleportSpell;
import com.aeonblox.rpg.spellsystem.spells.WhirlwindSpell;

/**
 * This class deals with the magic side of the plugin.
 * 
 * @docsince 1.9.0-SNAPSHOT
 * 
 */
@SuppressWarnings({ "unchecked", "rawtypes", "unused", "deprecation" })
public class Magic implements Listener {
	public HashMap<String, ArrayList> hashmap = new HashMap();
	HashMap<String, Long> lastattack = new HashMap();
	HashMap<String, Long> ticksSinceLastClick = new HashMap();
	Aeonblox plugin;
	Thread thread;
	ExplosionSpell explosion = new ExplosionSpell(null, plugin, new String[] { "Right", "Left", "Right", "Right" }, this, 6);
	FireSpell fire = new FireSpell(null, plugin, new String[] { "Right", "Left", "Left", "Left" }, this, 1, 3);
	LightningSpell lightning = new LightningSpell(null, plugin, new String[] { "Right", "Left", "Right", "Right", "Right" }, this, 5, 4);
	SoulSuckSpell soulsuck = new SoulSuckSpell(null, plugin, new String[] { "Left", "Left", "Right", "Left" }, this, 10);
	TeleportSpell teleport = new TeleportSpell(null, plugin, new String[] { "Right", "Right", "Right", "Right" }, this, 12);
	WhirlwindSpell whirlwind = new WhirlwindSpell(null, plugin, new String[] { "Right", "Left", "Right", "Left" }, this, 10);

	boolean shouldRun = false;
	public static int SCHEDULE_ID;

	/**
	 * This is the only constructor for the Magic class.
	 * 
	 * @param instance
	 *            - An instance of the plugin.
	 */
	public Magic(Aeonblox instance) {
		this.plugin = instance;
		ItemTools.setupClass(plugin);
		SCHEDULE_ID = startScheduler(30);
	}

	/**
	 * This will remove a given arrow
	 * 
	 * @param arrow
	 *            - The arrow to remove
	 * @return - If the action completed successfully.
	 */
	public boolean removeArrow(Arrow arrow) {
		List<MetadataValue> values = arrow.getMetadata("Remove");
		for (MetadataValue val : values) {
			if (val.getOwningPlugin().getName().equals(this.plugin.getName())) {
				return val.asBoolean();
			}
		}
		return false;
	}

	/**
	 * This will return a {@link List} containing all of the nearby blocks
	 * within a given radius.
	 * 
	 * @param location
	 *            - The location to search from.
	 * @param Radius
	 *            - The radius to search in.
	 * @return a list of blocks
	 */
	public static List<Block> getNearbyBlocks(Location location, int Radius) {
		List<Block> Blocks = new ArrayList();
		for (int X = location.getBlockX() - Radius; X <= location.getBlockX() + Radius; X++) {
			for (int Y = location.getBlockY() - Radius; Y <= location.getBlockY() + Radius; Y++) {
				for (int Z = location.getBlockZ() - Radius; Z <= location.getBlockZ() + Radius; Z++) {
					Block block = location.getWorld().getBlockAt(X, Y, Z);
					if (!block.isEmpty()) {
						Blocks.add(block);
					}
				}
			}
		}
		return Blocks;
	}

	/**
	 * This is an event listener that will be called when a projectile hits an
	 * entity or object.
	 * 
	 * @param event
	 *            - The event passed to it by Bukkit
	 */
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onProjectileHit(ProjectileHitEvent event) {
		Entity entity = event.getEntity();
		if (entity.getType() == EntityType.ARROW) {
			entity.setMetadata("Remove", new FixedMetadataValue(this.plugin, Boolean.valueOf(true)));
		}
	}

	/**
	 * This is an event listener that will be called when an {@link Entity} is
	 * damaged by another {@link Entity}.
	 * 
	 * @param event
	 *            - The event passed to it by Bukkit
	 */
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onmagicdamageevent(EntityDamageByEntityEvent event) {
		if ((event.getDamager() instanceof Arrow)) {
			Arrow arrow = (Arrow) event.getDamager();
			if ((arrow.getShooter() instanceof Player)) {
				Player player = (Player) arrow.getShooter();
				String name = player.getName();
				if (player == event.getEntity()) {
					event.setCancelled(true);
					return;
				}
				if (player.getItemInHand().getType() == Material.STICK) {
					int s = (player.getLevel() + 1) * 2;
					event.setDamage(4 + s);
				}
			}
		}
	}

	/**
	 * This method deals with the whole combo spell system.
	 * 
	 * @param name
	 *            - The name of the {@link Player} who clicked.
	 * @param player
	 *            - The {@link Player} who clicked.
	 */
	public void ComboSystem(String name, final Player player) {
		ArrayList list = ((ArrayList) this.hashmap.get(name));
		if (list.equals(whirlwind.getSpellList())) {
			whirlwind.setPlayer(player);
			whirlwind.castSpell(this);
			plugin.getLogger().info("Spells - Whirlwind");
		} else if (list.equals(teleport.getSpellList())) {
			teleport.setPlayer(player);
			teleport.castSpell(this);
			plugin.getLogger().info("Spells - Teleport");
		} else if (list.equals(lightning.getSpellList())) {
			lightning.setPlayer(player);
			lightning.castSpell(this);
			plugin.getLogger().info("Spells - Lightning");
		} else if (list.equals(fire.getSpellList())) {
			fire.setPlayer(player);
			fire.castSpell(this);
			plugin.getLogger().info("Spells - Fire");
		} else if (list.equals(explosion.getSpellList())) {
			explosion.setPlayer(player);
			explosion.castSpell(this);
			plugin.getLogger().info("Spells - Expolosion");
		} else if (list.equals(soulsuck.getSpellList())) {
			soulsuck.setPlayer(player);
			soulsuck.castSpell(this);
			plugin.getLogger().info("Spells - SoulSuck");
		} else if (list.size() > 5) {
			ItemTools.resetWandItemName(player);
			this.hashmap.remove(player.getName());
		}
	}

	public void spellCompleted(Player player) {
		ItemTools.resetWandItemName(player);
	}

	/**
	 * This is an event listener that will be called when a {@link Player} uses
	 * an item that is not a stick.
	 * 
	 * @param event
	 *            - The event passed to it by Bukkit
	 */
	@EventHandler
	public void notastickevent(PlayerItemHeldEvent event) {
		if (!event.getPlayer().getItemInHand().equals(Material.STICK)) {
			this.hashmap.remove(event.getPlayer().getName());
		}
	}

	/**
	 * This is an event listener that will be called when a {@link Player}
	 * leaves the server half way through a cast.
	 * 
	 * @param event
	 *            - The event passed to it by Bukkit
	 */
	@EventHandler
	public void magicuserdisconnect(PlayerQuitEvent event) {
		ItemTools.resetWandItemName(event.getPlayer());
		this.lastattack.remove(event.getPlayer().getName());
		this.hashmap.remove(event.getPlayer().getName());
	}

	/**
	 * This is an event listener that will be called when a {@link Player} is
	 * kicked from the server half way through a cast.
	 * 
	 * @param event
	 *            - The event passed to it by Bukkit
	 */
	@EventHandler
	public void magicuserkickdisconnect(PlayerKickEvent event) {
		ItemTools.resetWandItemName(event.getPlayer());
		this.lastattack.remove(event.getPlayer().getName());
		this.hashmap.remove(event.getPlayer().getName());
	}

	/**
	 * This is an event listener that will be called when a {@link Player} uses
	 * a wand.
	 * 
	 * @param event
	 *            - The event passed to it by Bukkit
	 */
	@EventHandler
	public void magicstickhandler(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		String name = player.getName();
		if (player.getItemInHand().getType() == Material.STICK) {
			if ((event.getAction() == Action.RIGHT_CLICK_AIR) || (event.getAction().equals(Action.RIGHT_CLICK_BLOCK))) {
				long time = new Date().getTime();
				if ((this.lastattack.get(name) == null) || (((Long) this.lastattack.get(name)).longValue() + 1000L <= time)) {
					processClick(false, player, event);
				} else {
					event.setCancelled(true);
				}
			}
			if ((event.getAction() == Action.LEFT_CLICK_AIR) || (event.getAction().equals(Action.LEFT_CLICK_BLOCK))) {
				long time = new Date().getTime();
				if ((this.lastattack.get(name) == null) || (((Long) this.lastattack.get(name)).longValue() + 1000L <= time)) {
					processClick(true, player, event);
				} else {
					event.setCancelled(true);
				}
			}
		} else {
			ArrayList arraylist = (ArrayList) this.hashmap.get(name);
			if (arraylist != null) {
				if (arraylist.size() > 0) {
					this.hashmap.remove(player.getName());
				}
			}
		}
	}

	/**
	 * This method process the action when a {@link Player} left or right clicks
	 * with a stick.
	 * 
	 * @param isLeft
	 *            - If the {@link Player} left clicked.
	 * @param player
	 *            - The {@link Player} who clicked.
	 * @param event
	 *            - The click event.
	 */
	private void processClick(boolean isLeft, Player player, PlayerInteractEvent event) {
		String name = player.getName();
		this.lastattack.put(name, Long.valueOf(new Date().getTime()));
		ArrayList<String> list = (ArrayList) this.hashmap.get(name);
		if (list == null) {
			ArrayList<String> list2 = new ArrayList();
			if (isLeft) {
				list2.add("Left");
				ItemTools.setDisplayName(true, event);
			} else {
				list2.add("Right");
				ItemTools.setDisplayName(false, event);
			}
			this.hashmap.put(name, list2);
			this.ticksSinceLastClick.put(name, 0L);
		} else {
			if (isLeft) {
				list.add("Left");
				ItemTools.setDisplayName(true, event);
			} else {
				list.add("Right");
				ItemTools.setDisplayName(false, event);
			}
			this.hashmap.put(name, list);
			this.ticksSinceLastClick.put(name, 0L);
		}
		createClickArrow(player, isLeft);
		if (isLeft) {
			player.playSound(player.getLocation(), Sound.CLICK, 10.0F, 1.0F);
		} else {
			player.playSound(player.getLocation(), Sound.WOOD_CLICK, 10.0F, 1.0F);
		}
		ComboSystem(name, player);
	}

	/**
	 * This method creates an arrow when a {@link Player} clicks.
	 * 
	 * @param player
	 *            - The {@link Player} that clicked.
	 */
	private void createClickArrow(Player player, boolean isLeft) {
		if (isLeft) {
			final Arrow arrow = (Arrow) player.getWorld().spawn(player.getEyeLocation(), Arrow.class);
			arrow.setShooter(player);
			arrow.setVelocity(player.getEyeLocation().getDirection().multiply(2));

			new BukkitRunnable() {
				public void run() {
					processArrow(arrow, this);
				}
			}.runTaskTimer(this.plugin, 0L, 1L);
		}
	}

	/**
	 * This method checks whether an {@link Effect} should be spawned or if an
	 * {@link Arrow} should be removed.
	 * 
	 * @param arrow
	 *            - The {@link Arrow} to check
	 * @param runnable
	 *            - The {@link BukkitRunnable} that the check if being run in.
	 */
	private void processArrow(Arrow arrow, BukkitRunnable runnable) {
		if ((!arrow.isOnGround()) || (!arrow.isDead()) || (arrow.isValid())) {
			arrow.getWorld().playEffect(arrow.getLocation(), Effect.MOBSPAWNER_FLAMES, 2);
		} else if ((arrow.isOnGround()) || (!arrow.isValid()) || (arrow.isDead()) || (arrow.isEmpty()) || (arrow == null) || ((arrow.getVelocity().getX() == 0.0D) && (arrow.getVelocity().getY() == 0.0D)) || (arrow.getVelocity().getZ() == 0.0D) || (arrow.getVelocity() == null) || (Magic.this.removeArrow(arrow))) {
			arrow.removeMetadata("Remove", Magic.this.plugin);
			runnable.cancel();
		}
	}

	/**
	 * This creates an explosion at the given {@link Location} with the given
	 * parameters
	 * 
	 * @param location
	 *            - Where to create the explosion
	 * @param power
	 *            - The power of the explosion
	 * @param setFire
	 *            - Whether to set fire to the blocks
	 * @param breaksBlocks
	 *            - Whether to break blocks
	 * @param player
	 *            - The {@link Player} who cast the spell.
	 */
	public static void createExplosion(Location location, int power, boolean setFire, boolean breaksBlocks, Player player) {
		player.getWorld().createExplosion(location.getX(), location.getY(), location.getZ(), power, setFire, breaksBlocks);
	}

	/**
	 * This takes the given amount of hunger from the {@link Player} or if the
	 * hunger to take is greater than the amount they have, it will just set the
	 * hunger to 0.
	 * 
	 * @param amount
	 *            - The amount of hunger to take
	 * @param player
	 *            - The {@link Player} to take the hunger from.
	 */
	public static void takeHunger(int amount, Player player) {
		if (player.getFoodLevel() - amount < 0) {
			player.setFoodLevel(0);
		} else {
			player.setFoodLevel(player.getFoodLevel() - amount);
		}
	}

	/**
	 * This method starts the scheduler to handle click delays and spell
	 * removals after 3 seconds.
	 * 
	 * @param ticks
	 *            - The delay in between runs in ticks.
	 * @return the schedule id so that the task can be cancelled by the main
	 *         thread.
	 */
	private int startScheduler(int ticks) {
		return plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
			@Override
			public void run() {
				Set<String> names = ticksSinceLastClick.keySet();
				String[] nameArray = new String[names.size()];
				names.toArray(nameArray);

				Collection<Long> tick = ticksSinceLastClick.values();
				Long[] tickArray = new Long[tick.size()];
				tick.toArray(tickArray);

				for (int i = 0; i < names.size(); i++) {
					String name = nameArray[i];
					Long time = tickArray[i];
					if (time > 60) {
						ItemTools.resetWandItemName(Bukkit.getPlayer(name));
						hashmap.remove(name);
					}
				}
			}
		}, 0, ticks);
	}
}