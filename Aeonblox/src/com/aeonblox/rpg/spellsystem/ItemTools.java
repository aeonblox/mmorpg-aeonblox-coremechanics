package com.aeonblox.rpg.spellsystem;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;

import com.aeonblox.rpg.Aeonblox;

/**
 * This class deals with all of the naming of items.
 * 
 * @author Ryan (ZXSkelobrine)
 * @since 1.0.0
 * 
 */
public class ItemTools {

	/**
	 * This is the instance of the {@link Aeonblox} that is used within this
	 * class
	 */
	static Aeonblox plugin;

	/**
	 * This will setup the class by setting the plugin instance.
	 * 
	 * @param plugin
	 */
	public static void setupClass(Aeonblox plugin) {
		ItemTools.plugin = plugin;
	}

	/**
	 * This will update the display name of an item when they click with a wand.
	 * 
	 * @param left
	 *            - Whether the click was a left click.
	 * @param event
	 *            - The interaction event to get {@link ItemMeta} from.
	 */
	public static void setDisplayName(boolean left, PlayerInteractEvent event) {
		String currentDisplay = getName(event.getPlayer());
		event.getPlayer().setMetadata("aeon.itemname", new FixedMetadataValue(plugin, getName(event.getPlayer())));
		if (left) {
			if (currentDisplay == null || !currentDisplay.contains("Right") || !currentDisplay.contains("Left") || currentDisplay == "Wand") {
				setName("Left ", event.getPlayer());
			} else {
				updateName(" - Left ", event.getPlayer());
			}
		} else {
			if (currentDisplay == null || !currentDisplay.contains("Right") || !currentDisplay.contains("Left") || currentDisplay == "Wand") {
				setName("Right ", event.getPlayer());
			} else {
				updateName(" - Right ", event.getPlayer());
			}
		}
	}

	/**
	 * This gets the name of the item that a {@link Player} is currently
	 * holding.
	 * 
	 * @param player
	 *            - The {@link Player} to get the current held item from.
	 * @return the name of the item
	 */
	public static String getName(Player player) {
		return player.getItemInHand().getItemMeta().getDisplayName();
	}

	/**
	 * This will override the current name of the item and set it to the given
	 * name
	 * 
	 * @param name
	 *            - The name to set the item to
	 * @param player
	 *            - The {@link Player} to get the current item to set the name.
	 */
	public static void setName(String name, Player player) {
		ItemMeta im = player.getItemInHand().getItemMeta();
		im.setDisplayName(ChatColor.GOLD + name);
		player.getItemInHand().setItemMeta(im);
	}

	/**
	 * This will override the current name of the item and set it to the given
	 * name
	 * 
	 * @param name
	 *            - The name to set the item to
	 * @param stack
	 *            - The {@link ItemStack} to set the name.
	 */
	public static void setName(String name, ItemStack stack) {
		ItemMeta im = stack.getItemMeta();
		im.setDisplayName(ChatColor.GOLD + name);
		stack.setItemMeta(im);
	}

	/**
	 * This will take the current name and append the given ending to it.
	 * 
	 * @param append
	 *            - The string to append to the end
	 * @param player
	 *            - The {@link Player} to get the current item to append the
	 *            name.
	 */
	public static void updateName(String append, Player player) {
		ItemMeta im = player.getItemInHand().getItemMeta();
		im.setDisplayName(ChatColor.GOLD + im.getDisplayName() + append);
		player.getItemInHand().setItemMeta(im);
	}

	/**
	 * This will reset the name of a wand back to its stored name, or if one has
	 * not been saved, it will be set to 'Wand'
	 * 
	 * @param player
	 *            - The {@link Player} to get the item from.
	 */
	public static void resetWandItemName(Player player) {
		ItemStack i = player.getItemInHand();
		if (i.getType() == Material.STICK) {
			if (player.hasMetadata("aeon.itemname")) {
				setName(player.getMetadata("aeon.itemname").get(0).asString(), i);
			} else {
				setName("Wand", i);
			}
		}
	}
}
