package com.aeonblox.rpg.spellsystem;

import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.SmallFireball;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.aeonblox.rpg.Aeonblox;

public class StaffMechanics implements Listener {
	Aeonblox plugin;

	public StaffMechanics(Aeonblox instance) {
		this.plugin = instance;
	}

	/**
	 * This is a listener called when a {@link Player} interacts. This will be
	 * called by Bukkit with the given parameter. This deals with magic based
	 * around {@link ItemMeta}.
	 * 
	 * @param e
	 *            - The event passed to it by Bukkit.
	 */
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		Player nuller = e.getPlayer();
		if (nuller.getItemInHand().getType().equals(null)) return;
		if (e.getPlayer().getItemInHand().getType() != Material.WOOD_HOE || e.getPlayer().getItemInHand().getType() != Material.STONE_HOE || e.getPlayer().getItemInHand().getType() != Material.IRON_HOE || e.getPlayer().getItemInHand().getType() != Material.DIAMOND_HOE || e.getPlayer().getItemInHand().getType() != Material.GOLD_HOE) {
			if ((e.getAction() == Action.RIGHT_CLICK_AIR)) {
				if (plugin.util_WorldGuard != null) {
					if (!plugin.util_WorldGuard.playerInSafety(nuller)) {
						ItemStack item = e.getItem();
						ItemMeta itemMeta = item.getItemMeta();
						if (itemMeta == null || itemMeta.getLore() == null) {
							return;
						}
						if ((itemMeta.hasLore() && (itemMeta.getLore().contains((ChatColor.RED + "Tier 1"))))) {
							if (e.getPlayer().getFoodLevel() > 3) {
								Player p = e.getPlayer();
								// StaffMechanics.plugin.util_WorldGuard.damageTool(item);
								Snowball f = p.launchProjectile(Snowball.class);
								plugin.balls.add(f);
								Location loc = ((Player) e.getPlayer()).getLocation();
								loc.getWorld().playEffect(loc, Effect.BOW_FIRE, 2);
								int change = e.getPlayer().getFoodLevel() - 2;
								e.getPlayer().setFoodLevel(change);
							} else {
								Player p = e.getPlayer();
								p.playSound(p.getLocation(), Sound.WOOD_CLICK, 1.0F, 0.0F);
							}

						}
						ItemStack item2 = e.getItem();
						ItemMeta itemMeta2 = item2.getItemMeta();
						if (itemMeta == null || itemMeta.getLore() == null) {
							return;
						}
						if (itemMeta2.hasLore()) if (itemMeta2.getLore().contains((ChatColor.RED + "Tier 2"))) {
							if (e.getPlayer().getFoodLevel() > 3) {
								Player p = e.getPlayer();
								SmallFireball f2 = p.launchProjectile(SmallFireball.class);
								// StaffMechanics.plugin.util_WorldGuard.damageTool(item2);
							plugin.fballs.add(f2);
							Location loc = ((Player) e.getPlayer()).getLocation();
							loc.getWorld().playEffect(loc, Effect.BOW_FIRE, 2);
							f2.setVelocity(f2.getVelocity().multiply(10));
							int change = e.getPlayer().getFoodLevel() - 2;
							e.getPlayer().setFoodLevel(change);
						} else {
							Player p = e.getPlayer();
							p.playSound(p.getLocation(), Sound.WOOD_CLICK, 1.0F, 0.0F);
						}
					}
						ItemStack item3 = e.getItem();
						ItemMeta itemMeta3 = item3.getItemMeta();
						if (itemMeta3 == null || itemMeta3.getLore() == null) {
							return;
						}
						if (itemMeta3.hasLore()) if (itemMeta3.getLore().contains((ChatColor.RED + "Tier 3"))) {
							if (e.getPlayer().getFoodLevel() > 3) {
								Player p = e.getPlayer();
								// StaffMechanics.plugin.util_WorldGuard.damageTool(item3);
							Arrow f3 = p.launchProjectile(Arrow.class);
							plugin.arrow.add(f3);
							Location loc = ((Player) e.getPlayer()).getLocation();
							loc.getWorld().playEffect(loc, Effect.BOW_FIRE, 2);
							int change = e.getPlayer().getFoodLevel() - 2;
							e.getPlayer().setFoodLevel(change);
						} else {
							Player p = e.getPlayer();
							p.playSound(p.getLocation(), Sound.WOOD_CLICK, 1.0F, 0.0F);
						}
					}
						ItemStack item4 = e.getItem();
						ItemMeta itemMeta4 = item4.getItemMeta();
						if (itemMeta4 == null || itemMeta4.getLore() == null) {
							return;
						}
						if (itemMeta4.hasLore()) if (itemMeta4.getLore().contains((ChatColor.RED + "Tier 4"))) {
							if (e.getPlayer().getFoodLevel() > 4) {
								Player p = e.getPlayer();
								// StaffMechanics.plugin.util_WorldGuard.damageTool(item4);
							EnderPearl f4 = p.launchProjectile(EnderPearl.class);
							plugin.enderpearl.add(f4);
							Location loc = ((Player) e.getPlayer()).getLocation();
							loc.getWorld().playEffect(loc, Effect.BOW_FIRE, 2);
							String name = ((p)).getName();
							Aeonblox.character.add(name);
							int change = e.getPlayer().getFoodLevel() - 2;
							e.getPlayer().setFoodLevel(change);
						} else {
							Player p = e.getPlayer();
							p.playSound(p.getLocation(), Sound.WOOD_CLICK, 1.0F, 0.0F);
						}
					}

					} else {
						e.setCancelled(true);
					}
				}
			}
		} else {

		}
	}

	/**
	 * This is a listener called when an entity is damaged by another entity. It
	 * detects whether the entity that damages was a snowball, fireball, arrow
	 * or enderpearl. and whether is should apply damage to the target. If so it
	 * applies the damage necessary.
	 * 
	 * @param e
	 *            - The event passed to it by Bukkit.
	 */
	@EventHandler
	public void onEntityDamage(EntityDamageByEntityEvent e) {

		if ((e.getDamager() instanceof Snowball)) {
			Snowball f = (Snowball) e.getDamager();
			if (plugin.balls.contains(f)) {
				Location loc = e.getEntity().getLocation();
				loc.getWorld().playEffect(loc, Effect.POTION_BREAK, 8194);
				e.setDamage(5.0D);
				plugin.balls.remove(e);
			}
		}
		if ((e.getDamager() instanceof SmallFireball)) {
			Entity p = e.getEntity();
			SmallFireball f2 = (SmallFireball) e.getDamager();
			if (plugin.fballs.contains(f2)) {
				e.setDamage(7.0D);
				Location loc = e.getEntity().getLocation();
				loc.getWorld().playEffect(loc, Effect.POTION_BREAK, 10);
				p.setFireTicks(40); // Use FireTimeInt for the lore fire dmg
									// boost
				plugin.fballs.remove(e);

			}
		}
		if ((e.getDamager() instanceof Arrow)) {
			Arrow f3 = (Arrow) e.getDamager();
			if (plugin.arrow.contains(f3)) {
				e.setDamage(9.0D);
				LivingEntity victim = (LivingEntity) e.getEntity();
				victim.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 30, 1));
				Location loc = ((Player) e.getEntity()).getLocation();
				loc.getWorld().playEffect(loc, Effect.POTION_BREAK, 8196);
				plugin.arrow.remove(e);
			}
		}
		if ((e.getDamager() instanceof EnderPearl)) {
			EnderPearl f4 = (EnderPearl) e.getDamager();
			if (plugin.enderpearl.contains(f4)) {
				e.setDamage(16.0D);
				Location loc = e.getEntity().getLocation();
				loc.getWorld().playEffect(loc, Effect.POTION_BREAK, 2);
				plugin.enderpearl.remove(e);

			}
		}
	}

	/**
	 * This will check if a teleport was from an enderpearl then determine
	 * whether it should be allowed based upon {@link Aeonblox#character}.
	 * 
	 * @param event
	 *            - The event passed by Bukkit.
	 */
	@EventHandler
	public void onTeleport(PlayerTeleportEvent event) {
		if (event.getCause().equals(TeleportCause.ENDER_PEARL)) {
			Player p = event.getPlayer();
			String name = ((p)).getName();
			if (Aeonblox.character.contains(name)) {
				event.setCancelled(true);
			}
		}
	}

	/**
	 * This is a listener called when a projectile hits a block/entity. It will
	 * check whether {@link Aeonblox#balls} contains the entity - if so it will
	 * remove the entity.
	 * 
	 * @param e
	 *            - The event passed by Bukkit.
	 */
	@EventHandler
	public void hit(ProjectileHitEvent e) {
		if ((plugin.balls).contains(e.getEntity()))
		;
		{
			(plugin.balls).remove(e.getEntity());
		}
		if ((plugin.fballs).contains(e.getEntity()))
		;
		{
			(plugin.fballs).remove(e.getEntity());
		}
		if ((plugin.enderpearl).contains(e.getEntity()))
		;
		{
			(plugin.enderpearl).remove(e.getEntity());
		}
	}

}
