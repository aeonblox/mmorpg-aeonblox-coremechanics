package com.aeonblox.rpg.economy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class PlayerUtil {
private File players;
private ArrayList<String> toggled;
 
	public PlayerUtil(File players) {
		this.players = players;
		this.toggled = new ArrayList<String>();
	
		if (this.players.exists() == false){
			try{
				this.players.createNewFile();
			}catch (Exception e){
			e.printStackTrace();
			}
		}
	}
 
	public boolean contains(String user){
		return this.toggled.contains(user);
	}

	public ArrayList<String> getList(){
		return this.toggled;
	}

	public void add(String user){
		if (!this.toggled.contains(user)){
			this.toggled.add(user);
		}
	}

	public void remove(String user){
	if (this.toggled.contains(user)){
		this.toggled.remove(user);
		}
	}

	public void load(){
	try {
		DataInputStream input = new DataInputStream(new FileInputStream(this.players));
		BufferedReader reader = new BufferedReader(new InputStreamReader(input));
		String line;
		
		while ((line = reader.readLine()) != null){
			if (!this.toggled.contains(line)){
				this.toggled.add(line);
			}
		}

		reader.close();
		input.close();
	} catch (Exception e) {
		e.printStackTrace();
	}
	}

	public void save(){
		try {
			FileWriter stream = new FileWriter(this.players);
			BufferedWriter out = new BufferedWriter(stream);

			for (String value : this.toggled){
				out.write(value);
				out.newLine();
			}
  
			out.close();
			stream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}