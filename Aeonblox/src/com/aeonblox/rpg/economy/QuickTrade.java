package com.aeonblox.rpg.economy;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

import com.aeonblox.rpg.Aeonblox;

public class QuickTrade implements Listener{
	Aeonblox plugin;
	public static Permission perms = null;
	public TradeCommand tradecommand;
	public TradeListener tradelistener;
	public TradeInventory tradeinventory;
	public TradeItemStack tradeitemstack;
	public PlayerUtil playersStorage;
	public RightClickTrade rightclicktrade;
	public EconomyListener economylistener;
	public EXPListener explistener;
	public Economy economy = null;
	public static HashMap<String, String> messageData = new HashMap<String, String>();
	public File f = null;
	public QuickTrade(Aeonblox instance) {
		this.plugin = instance;
    	tradecommand = new TradeCommand (this);
    	tradelistener = new TradeListener(this);
    	tradeinventory = new TradeInventory(this);
    	tradeitemstack = new TradeItemStack(this);
    	rightclicktrade = new RightClickTrade(this);
    	economylistener = new EconomyListener(this);
    	explistener = new EXPListener(this);
	}
    
    public boolean getEconomy(){
    	if(economy == null){
    		return false;
    	} else {
    		return true;
    	}
    }
	
	public int getSize(){
		int rows = plugin.cfgHandler.getCustomConfig(plugin.tradeConfig).getInt("Rows") * 9;
		return rows;
	}
	
	
	//******************************************ITEMSTACKS***********************************************************//
	
	public ItemStack getAcceptedItem(){
		String accepted = plugin.cfgHandler.getCustomConfig(plugin.tradeConfig).getString("Accepted");
		ItemStack acceptedItem = null;
		if(accepted.contains(":")){
			int acceptedId = Integer.parseInt(accepted.split(":")[0]);
			byte acceptedValue = Byte.parseByte(accepted.split(":")[1]);
			acceptedItem = new ItemStack(acceptedId, 1, acceptedValue);
		} else {
			int acceptedId = Integer.parseInt(accepted);
			acceptedItem = new ItemStack(acceptedId);
		}
		return acceptedItem;
	}
	
	public ItemStack getDeclineItem(){
		String decline = plugin.cfgHandler.getCustomConfig(plugin.tradeConfig).getString("Decline");
		ItemStack declineItem = null;
		if(decline.contains(":")){
			int declineId = Integer.parseInt(decline.split(":")[0]);
			byte declineValue = Byte.parseByte(decline.split(":")[1]);
			declineItem = new ItemStack(declineId, 1, declineValue);
		} else {
			int declineId = Integer.parseInt(decline);
			declineItem = new ItemStack(declineId);
		}
		return declineItem;
	}
	
	public String getInventoryName(){
		String inventoryname = plugin.cfgHandler.getCustomConfig(plugin.tradeConfig).getString("InventoryName");
		return inventoryname;
	}
	
	public ItemStack getSeparatorItem(){
		String separator = plugin.cfgHandler.getCustomConfig(plugin.tradeConfig).getString("Separator");
		ItemStack separatorItem = null;
		if(separator.contains(":")){
			int separatorId = Integer.parseInt(separator.split(":")[0]);
			byte separatorValue = Byte.parseByte(separator.split(":")[1]);
			separatorItem = new ItemStack(separatorId, 1, separatorValue);
		} else {
			int separatorId = Integer.parseInt(separator);
			separatorItem = new ItemStack(separatorId);
		}
		return separatorItem;
	}
	
	public ItemStack getAcceptItem(){
		String accept = plugin.cfgHandler.getCustomConfig(plugin.tradeConfig).getString("Accept");
		ItemStack acceptItem = null;
		if(accept.contains(":")){
			int acceptId = Integer.parseInt(accept.split(":")[0]);
			byte acceptValue = Byte.parseByte(accept.split(":")[1]);
			acceptItem = new ItemStack(acceptId, 1, acceptValue);
		} else {
			int acceptId = Integer.parseInt(accept);
			acceptItem = new ItemStack(acceptId);
		}
	return acceptItem;
	}
	
	public ItemStack getEconomyIndicator(){
		String economyIndicator = plugin.cfgHandler.getCustomConfig(plugin.tradeConfig).getString("EconomyIndicator");
		ItemStack economyIndicatorItem = null;
		if(economyIndicator.contains(":")){
			int acceptedId = Integer.parseInt(economyIndicator.split(":")[0]);
			byte acceptedValue = Byte.parseByte(economyIndicator.split(":")[1]);
			economyIndicatorItem = new ItemStack(acceptedId, 1, acceptedValue);
		} else {
			int acceptedId = Integer.parseInt(economyIndicator);
			economyIndicatorItem = new ItemStack(acceptedId);
		}
		return economyIndicatorItem;
	} 
	
	//**************************************************************************************************************************************//
	
	
	public int getDistance(){
		int distance = plugin.cfgHandler.getCustomConfig(plugin.tradeConfig).getInt("Distance");
		return distance;
	}
	
	public boolean noChat(){
		boolean nochat = plugin.cfgHandler.getCustomConfig(plugin.tradeConfig).getBoolean("NoChat");
		return nochat;
	}
	
	public boolean expTrading(){
		boolean expTrading = plugin.cfgHandler.getCustomConfig(plugin.tradeConfig).getBoolean("EXPTrading");
		return expTrading;
	}
	
	public int expOption(){
		int expOption = plugin.cfgHandler.getCustomConfig(plugin.tradeConfig).getInt("ExpOption");
		return expOption;
	}
	
	public boolean allowShiftRightClick(){
		boolean shiftrightclick = plugin.cfgHandler.getCustomConfig(plugin.tradeConfig).getBoolean("ShiftRightClick");
		return shiftrightclick;
	}
	
	public int getTradeDistanceLimit(){
		int tradedistancelimit = plugin.cfgHandler.getCustomConfig(plugin.tradeConfig).getInt("TradeDistanceLimit");
		return tradedistancelimit;
	}
	
	public boolean allowCreative(){
		boolean creativetrade = plugin.cfgHandler.getCustomConfig(plugin.tradeConfig).getBoolean("AllowCreative");
		return creativetrade;
	}
	
	public TradeCommand getTradeCommand(){
		return tradecommand;
	}
	
	public TradeInventory getTradeInventory(){
		return tradeinventory;
	}
	
	public EconomyListener getEconomyListener(){
		return economylistener;
	}
	
	public EXPListener getEXPListener(){
		return explistener;
	}
	
	public TradeItemStack getTradeItemStack(){
		return tradeitemstack;
	}
	
	public PlayerUtil getPlayerUtil(){
		return playersStorage;
	}
	
	public List<Integer> getBlacklist(){
		List<Integer> blacklist = plugin.cfgHandler.getCustomConfig(plugin.tradeConfig).getIntegerList("blacklist");
		return blacklist;
		
	}
	
	public int getTimeout(){
		int timeout = plugin.cfgHandler.getCustomConfig(plugin.tradeConfig).getInt("Delay") * 20;
		return timeout;
	}
	
	public int getEco1(){
		int eco1 = plugin.cfgHandler.getCustomConfig(plugin.tradeConfig).getInt("EcoOptionOne");
		return eco1;
	}
	
	public int getEco2(){
		int eco2 = plugin.cfgHandler.getCustomConfig(plugin.tradeConfig).getInt("EcoOptionTwo");
		return eco2;
	}
	
	public int getEco3(){
		int eco3 = plugin.cfgHandler.getCustomConfig(plugin.tradeConfig).getInt("EcoOptionThree");
		return eco3;
	}
	
	public void addMessages(){
        setMessage("NO_PERMISSION_COMMAND", "&4You do not have permission to do that command.");
        setMessage("OTHER_PLAYER_NO_PERMISSION_COMMAND", "&4%player% does not have permission to trade.");
        setMessage("NO_PERMISSION_SHIFT_CLICK", "&4You do not have permission to right click trade.");
        setMessage("OTHER_PLAYER_NO_PERMISSION_SHIFT_CLICK", "&4%player% does not have permission to right click trade.");
        setMessage("NO_CREATIVE_TRADE", "&cYou cannot trade in creative mode.");
        setMessage("OTHER_PLAYER_NO_CREATIVE_TRADE", "&c%player% cannot trade while in creative mode.");
        setMessage("DISABLE_SHIFT_CLICK", "&aShift-Right-Click trading has been disabled.");
        setMessage("ENABLE_SHIFT_CLICK", "&aShift-Right-Click trading has been enabled.");
        setMessage("NO_SELF_TRADE", "&cYou cannot trade with yourself.");
        setMessage("PLAYER_NOT_FOUND", "&c%player% was not found!");
        setMessage("TOO_FAR", "&cYou are too far away from the player to make a trade request. You must be within %distance% blocks.");
        setMessage("TOO_FAR_SHIFT_CLICK", "&cTrade request cancelled, too far away");
        setMessage("SENT_REQUEST", "&aYou have sent a trade request to %player%.");
        setMessage("RECEIVED_REQUEST", "&a%player% has requested to trade with you!");
        setMessage("RECEIVED_REQUEST_P2", "&aDo /trade accept, or /trade decline");
        setMessage("SHIFT_CLICK_RECEIVED_REQUEST", "&aShift-Right-Click them to accept");
        setMessage("ALREADY_IN_TRADE", "&c%player% is already in a trade! Please try later.");
        setMessage("ALREADY_REQUESTED", "&c%player% has already been sent a request! Please try later.");
        setMessage("NO_REPLY", "&c%player% did not reply to your trade request.");
        setMessage("YOU_NO_REPLY", "&cYou did not reply to %player%'s trade request.");
        setMessage("TRADE_DECLINED", "&c%player% declined the trade request.");
        setMessage("YOU_TRADE_DECLINED", "&cYou declined the trade request.");
        setMessage("BLACKLISTED_ITEM", "&cYou cannot place this item into trades.");
        setMessage("YOU_CHANGED_TRADE", "&cYou have changed the trade, please re-accept.");
        setMessage("OTHER_PLAYER_CHANGED_TRADE", "&c%player% has changed the trade, please re-accept.");
        setMessage("ACCEPTED_TRADE", "&aTrade accepted, please wait for %player% to accept.");
        setMessage("OTHER_PLAYER_ACCEPTED_TRADE", "&a%player% has accepted the trade!");
        setMessage("ALREADY_ACCEPTED_TRADE", "&cYou have already accepted the trade.");
        setMessage("INVALID_SLOT", "&cYou cannot place items there.");
        setMessage("YOU_DECLINED_TRADE", "&cYou declined the trade");
        setMessage("OTHER_PLAYER_DECLINED_TRADE", "&c%player% declined the trade");
        setMessage("SUCCESSFUL_TRADE", "&5Trade successful!");
        setMessage("TRADE_INITIATED", "&aTrade initiated with %player%");
        setMessage("NOT_REQUESTED", "&cYou were not requested for a trade.");
        setMessage("NOT_SAME_WORLD", "&cYou must be in the same world to trade.");
        setMessage("INVENTORY_FULL", "&b**&aYour inventory was full, so the items have been dropped next to you.&b**");
        setMessage("ECONOMY_YOU_ADDED", "&aYou have added &b$%amount% &ato the trade. $%totalamount% in total");
        setMessage("ECONOMY_OTHER_ADDED", "&a%player% has added &b$%amount% &ato the trade. $%totalamount% in total");
        setMessage("ECONOMY_NOT_ENOUGH_MONEY_ADD", "&cYou do not have enough money");
        setMessage("ECONOMY_NOT_ENOUGH_MONEY_WITHDRAW", "&cNot enough money in trade");
        setMessage("ECONOMY_YOU_WITHDREW", "&a$%amount% &ahas been removed from the trade. $%totalamount% left");
        setMessage("ECONOMY_OTHER_WITHDREW", "&a%player% has removed &b$%amount% &afrom the trade. $%totalamount% left");
        setMessage("EXP_YOU_ADDED", "&aYou have added &b%amount% &3levels &a to the trade. %totalamount% in total");
        setMessage("EXP_OTHER_ADDED", "&a%player% has added &b%amount% &3levels &a to the trade. %totalamount% in total");
        setMessage("EXP_NOT_ENOUGH_MONEY_ADD", "&cYou do not have enough EXP");
        setMessage("EXP_NOT_ENOUGH_MONEY_WITHDRAW", "&cNot enough EXP in trade");
        setMessage("EXP_YOU_WITHDREW", "&a%amount% &3levels &ahave been removed from the trade. %totalamount% left");
        setMessage("EXP_OTHER_WITHDREW", "&a%player% has removed &b%amount% &3levels &a from the trade. %totalamount% left");
	}
	
	
    private void setMessage(String name, String message) {
        File f = new File(plugin.getDataFolder()+File.separator+"messages.yml");
        FileConfiguration config = YamlConfiguration.loadConfiguration(f);
        if (!config.isSet(name)) {
            config.set(name, message);
            try {
                config.save(f);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    public void reloadMessages() {
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        addMessages();
        
        FileConfiguration config = YamlConfiguration.loadConfiguration(f);
        for (String message : config.getConfigurationSection("").getKeys(false)) {
            messageData.put(message, config.getString(message));
        }
    }
}
