package com.aeonblox.rpg.economy;

import java.util.ArrayList;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class TradeInventory{
	

	public static ArrayList<Integer> rightside = new ArrayList<Integer>();
	public static ArrayList<Integer> leftside = new ArrayList<Integer>();
	private QuickTrade plugin;
	
	
	 public TradeInventory(QuickTrade plugin) {
		 this.plugin = plugin;
		 setSlots();
	 	}
	 
	 public void setSlots(){
		int rows = this.plugin.getSize();
		switch (rows) {
		
		case 18:
			leftside.add(0);
			leftside.add(1);
			leftside.add(2);
			leftside.add(9);
			leftside.add(10);
			leftside.add(11);
			leftside.add(12); 	
			
			rightside.add(6);
			rightside.add(7);
			rightside.add(8);
			rightside.add(14);
			rightside.add(15);
			rightside.add(16);
			rightside.add(17);
			break;
		case 27:
			leftside.add(0);
			leftside.add(1);
			leftside.add(2);
			leftside.add(9);
			leftside.add(10);
			leftside.add(11);
			leftside.add(12); 
			leftside.add(18); 
			leftside.add(19);
			leftside.add(20);
			leftside.add(21); 
			
			rightside.add(6);
			rightside.add(7);
			rightside.add(8);
			rightside.add(14);
			rightside.add(15);
			rightside.add(16);
			rightside.add(17);
			rightside.add(23);
			rightside.add(24);
			rightside.add(25);
			rightside.add(26);
			break;
		case 36:
			leftside.add(0);
			leftside.add(1);
			leftside.add(2);
			leftside.add(9);
			leftside.add(10);
			leftside.add(11);
			leftside.add(12); 
			leftside.add(18); 
			leftside.add(19);
			leftside.add(20);
			leftside.add(21); 
			leftside.add(27); 
			leftside.add(28); 
			leftside.add(29); 
			leftside.add(30); 
			
			rightside.add(6);
			rightside.add(7);
			rightside.add(8);
			rightside.add(14);
			rightside.add(15);
			rightside.add(16);
			rightside.add(17);
			rightside.add(23);
			rightside.add(24);
			rightside.add(25);
			rightside.add(26);
			rightside.add(32);
			rightside.add(33);
			rightside.add(34);
			rightside.add(35);
			break;
		case 45:
			leftside.add(0);
			leftside.add(1);
			leftside.add(2);
			leftside.add(9);
			leftside.add(10);
			leftside.add(11);
			leftside.add(12); 
			leftside.add(18); 
			leftside.add(19);
			leftside.add(20);
			leftside.add(21); 
			leftside.add(27); 
			leftside.add(28); 
			leftside.add(29); 
			leftside.add(30);
			leftside.add(36); 
			leftside.add(37); 
			leftside.add(38); 
			leftside.add(39); 
			
			rightside.add(6);
			rightside.add(7);
			rightside.add(8);
			rightside.add(14);
			rightside.add(15);
			rightside.add(16);
			rightside.add(17);
			rightside.add(23);
			rightside.add(24);
			rightside.add(25);
			rightside.add(26);
			rightside.add(32);
			rightside.add(33);
			rightside.add(34);
			rightside.add(35);
			rightside.add(41);
			rightside.add(42);
			rightside.add(43);
			rightside.add(44);
			break;
		case 54:
			leftside.add(0);
			leftside.add(1);
			leftside.add(2);
			leftside.add(9);
			leftside.add(10);
			leftside.add(11);
			leftside.add(12); 
			leftside.add(18); 
			leftside.add(19);
			leftside.add(20);
			leftside.add(21); 
			leftside.add(27); 
			leftside.add(28); 
			leftside.add(29); 
			leftside.add(30);
			leftside.add(36); 
			leftside.add(37); 
			leftside.add(38); 
			leftside.add(39); 
			leftside.add(45); 
			leftside.add(46); 
			leftside.add(47); 
			leftside.add(48); 
			
			rightside.add(6);
			rightside.add(7);
			rightside.add(8);
			rightside.add(14);
			rightside.add(15);
			rightside.add(16);
			rightside.add(17);
			rightside.add(23);
			rightside.add(24);
			rightside.add(25);
			rightside.add(26);
			rightside.add(32);
			rightside.add(33);
			rightside.add(34);
			rightside.add(35);
			rightside.add(41);
			rightside.add(42);
			rightside.add(43);
			rightside.add(44);
			rightside.add(50);
			rightside.add(51);
			rightside.add(52);
			rightside.add(53);
			break;
			}
		
		if(this.plugin.economy != null){
			switch (rows) {
			case 18:
				leftside.remove(new Integer(12));
				rightside.remove(new Integer(14));
				break;
			case 27:
				leftside.remove(new Integer(21));
				rightside.remove(new Integer(23));
				break;
			case 36:
				leftside.remove(new Integer(30));
				rightside.remove(new Integer(32));
				break;
			case 45:
				leftside.remove(new Integer(39));
				rightside.remove(new Integer(41));
				break;
			case 54:
				leftside.remove(new Integer(48));
				rightside.remove(new Integer(50));
				break;
				}
		}
		
	 }
	 
		public void fillInventory1(Inventory inventory, Player player){
			//Left Side
			int rows = this.plugin.getSize();
			TradeItemStack tis = this.plugin.getTradeItemStack();
			ItemStack separator = tis.getSeparator();
			ItemStack accept = tis.getAccept();
			ItemStack decline = tis.getDecline();
		
				switch(rows){
				
				case 9:
					rows = rows + 9;
					break;
				
				case 18:
					inventory.setItem(3, accept);
					inventory.setItem(5, decline);
					inventory.setItem(4, separator);
					inventory.setItem(13, separator);
					break;
				case 27:
					inventory.setItem(3, accept);
					inventory.setItem(5, decline);
					inventory.setItem(4, separator);
					inventory.setItem(13, separator);
					inventory.setItem(22, separator);
					break;
				case 36:
					inventory.setItem(3, accept);
					inventory.setItem(5, decline);
					inventory.setItem(4, separator);
					inventory.setItem(13, separator);
					inventory.setItem(22, separator);
					inventory.setItem(31, separator);
					break;
				case 45:
					inventory.setItem(3, accept);
					inventory.setItem(5, decline);
					inventory.setItem(4, separator);
					inventory.setItem(13, separator);
					inventory.setItem(22, separator);
					inventory.setItem(31, separator);
					inventory.setItem(40, separator);

					break;
				case 54:
					inventory.setItem(3, accept);
					inventory.setItem(5, decline);
					inventory.setItem(4, separator);
					inventory.setItem(13, separator);
					inventory.setItem(22, separator);
					inventory.setItem(31, separator);
					inventory.setItem(40, separator);
					inventory.setItem(49, separator);
					break;
					
				}
				
				if(this.plugin.economy != null){
					ItemStack eco1 = tis.getEcoSeparator1();
					ItemStack eco2 = tis.getEcoSeparator2();
					ItemStack eco3 = tis.getEcoSeparator3();
					ItemStack ecoIndicator = tis.getEconomyIndicator(player);
					inventory.setItem(4, ecoIndicator);
					switch(rows){
					
					case 9:
						rows = rows + 9;
						break;
					
					case 18:
						inventory.setItem(12, eco1);
						inventory.setItem(13, eco2);
						inventory.setItem(14, eco3);
						break;
					case 27:
						inventory.setItem(21, eco1);
						inventory.setItem(22, eco2);
						inventory.setItem(23, eco3);
						break;
					case 36:
						inventory.setItem(30, eco1);
						inventory.setItem(31, eco2);
						inventory.setItem(32, eco3);
						break;
					case 45:
						inventory.setItem(39, eco1);
						inventory.setItem(40, eco2);
						inventory.setItem(41, eco3);
						break;
					case 54:
						inventory.setItem(48, eco1);
						inventory.setItem(49, eco2);
						inventory.setItem(50, eco3);
						break;
					}				
				}	
				if(this.plugin.expTrading() != false){
					ItemStack exp = tis.getEXP1();
					switch(rows){
					
					case 9:
						rows = rows + 9;
						break;
					
					case 18:
						inventory.setItem(4, exp);
						break;
					case 27:
						inventory.setItem(13, exp);
						break;
					case 36:
						inventory.setItem(22, exp);
						break;
					case 45:
						inventory.setItem(31, exp);
						break;
					case 54:
						inventory.setItem(40, exp);
						break;
					}				
				}
			}
		
		public void fillInventory2(Inventory inventory, Player player){
			//Right Side
			int rows = this.plugin.getSize();
			TradeItemStack tis = this.plugin.getTradeItemStack();
			ItemStack separator = tis.getSeparator();
			ItemStack accept = tis.getAccept();
			ItemStack decline = tis.getDecline();
					
			switch(rows){
			
			case 9:
				rows = rows + 9;
				break;
		
			case 18:
				inventory.setItem(5, accept);
				inventory.setItem(3, decline);
				inventory.setItem(4, separator);
				inventory.setItem(13, separator);
				break;
			case 27:
				inventory.setItem(5, accept);
				inventory.setItem(3, decline);
				inventory.setItem(4, separator);
				inventory.setItem(13, separator);
				inventory.setItem(22, separator);
				break;
			case 36:
				inventory.setItem(5, accept);
				inventory.setItem(3, decline);
				inventory.setItem(4, separator);
				inventory.setItem(13, separator);
				inventory.setItem(22, separator);
				inventory.setItem(31, separator);
				break;
			case 45:
				inventory.setItem(5, accept);
				inventory.setItem(3, decline);
				inventory.setItem(4, separator);
				inventory.setItem(13, separator);
				inventory.setItem(22, separator);
				inventory.setItem(31, separator);
				inventory.setItem(40, separator);
				break;
			case 54:
				inventory.setItem(5, accept);
				inventory.setItem(3, decline);
				inventory.setItem(4, separator);
				inventory.setItem(13, separator);
				inventory.setItem(22, separator);
				inventory.setItem(31, separator);
				inventory.setItem(40, separator);
				inventory.setItem(49, separator);
				break;
			}	
			
			if(this.plugin.economy != null){
				ItemStack eco1 = tis.getEcoSeparator1();
				ItemStack eco2 = tis.getEcoSeparator2();
				ItemStack eco3 = tis.getEcoSeparator3();
				ItemStack ecoIndicator = tis.getEconomyIndicator(player);
				inventory.setItem(4, ecoIndicator);
				switch(rows){
				
				case 9:
					rows = rows + 9;
					break;
			
				case 18:
					inventory.setItem(12, eco1);
					inventory.setItem(13, eco2);
					inventory.setItem(14, eco3);
					break;
				case 27:
					inventory.setItem(21, eco1);
					inventory.setItem(22, eco2);
					inventory.setItem(23, eco3);
					break;
				case 36:
					inventory.setItem(30, eco1);
					inventory.setItem(31, eco2);
					inventory.setItem(32, eco3);
					break;
				case 45:
					inventory.setItem(39, eco1);
					inventory.setItem(40, eco2);
					inventory.setItem(41, eco3);
					break;
				case 54:
					inventory.setItem(48, eco1);
					inventory.setItem(49, eco2);
					inventory.setItem(50, eco3);
					break;
					}
				}	
			
			if(this.plugin.expTrading() != false){
				ItemStack exp = tis.getEXP1();
				switch(rows){
				
				case 9:
					rows = rows + 9;
					break;
				
				case 18:
					inventory.setItem(4, exp);
					break;
				case 27:
					inventory.setItem(13, exp);
					break;
				case 36:
					inventory.setItem(22, exp);
					break;
				case 45:
					inventory.setItem(31, exp);
					break;
				case 54:
					inventory.setItem(40, exp);
					break;
				}				
			}
		}

	 
		
		public static ArrayList<Integer> getLeftSlots(){
			return leftside;
		}
		
		public static ArrayList<Integer> getRightSlots(){
			return rightside;
		}
}
