package com.aeonblox.rpg.economy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.Event.Result;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.util.ChatPaginator;

public class EXPListener implements Listener {
	
	private QuickTrade plugin;
	public static Map<Player, Integer> exp = new HashMap<Player, Integer>();
	public Map<String, Player> trades = TradeCommand.trades;
	public ArrayList<Player> accepted = TradeListener.accepted;
	public static HashMap<String, String> messageData = QuickTrade.messageData;
	
	 public EXPListener(QuickTrade plugin) {
		 this.plugin = plugin;
	 }
	
	 
		public void onEXPClick(InventoryClickEvent event){
			Player player = (Player) event.getWhoClicked();
			ArrayList<Integer> rightside = TradeInventory.rightside;
			ArrayList<Integer> leftside = TradeInventory.leftside;
			String inventoryname = this.plugin.getInventoryName();				
			if(!event.getInventory().getName().equals(inventoryname)){
				return;
			}

			Player b = trades.get(player.getName());
			if(b == null){
				getSecondPlayerOnEXPClick(event);
			} else {
				int rows = this.plugin.getSize();
				switch (rows) {
				case 18:
					if(event.getSlot() == 4){
						event.setCancelled(true);
						event.setResult(Result.DENY);
						onEXPClick(event, player, b);
						return;
						}
					break;
				case 27:
					if(event.getSlot() == 13){
						event.setCancelled(true);
						event.setResult(Result.DENY);
						onEXPClick(event, player, b);
						return;
						}
					break;
				case 36:
					if(event.getSlot() == 22){
						event.setCancelled(true);
						event.setResult(Result.DENY);
						onEXPClick(event, player, b);
						return;
						}
					break;
				case 45:
					if(event.getSlot() == 31){
						event.setCancelled(true);
						event.setResult(Result.DENY);
						onEXPClick(event, player, b);
						return;
						}
					break;
				case 54:
					if(event.getSlot() == 40){
						event.setCancelled(true);
						event.setResult(Result.DENY);
						onEXPClick(event, player, b);
						return;
						}
					break;
				}
			}
		}

		public void getSecondPlayerOnEXPClick(InventoryClickEvent event){
	        String key= null;
			Player player = (Player) event.getWhoClicked();
	        Player value = player;
	        for(Map.Entry entry: trades.entrySet()){
	            if(value.equals(entry.getValue())){
	            	key = (String) entry.getKey();
	            	Player e = Bukkit.getServer().getPlayerExact(key);
	            	int rows = this.plugin.getSize();
					switch (rows) {
					case 18:
						if(event.getSlot() == 4){
							event.setCancelled(true);
							event.setResult(Result.DENY);
							onEXPClick(event, player, e);
							return;
							}
						break;
					case 27:
						if(event.getSlot() == 13){
							event.setCancelled(true);
							event.setResult(Result.DENY);
							onEXPClick(event, player, e);
							return;
							}
						break;
					case 36:
						if(event.getSlot() == 22){
							event.setCancelled(true);
							event.setResult(Result.DENY);
							onEXPClick(event, player, e);
							return;
							}
						break;
					case 45:
						if(event.getSlot() == 31){
							event.setCancelled(true);
							event.setResult(Result.DENY);
							onEXPClick(event, player, e);
							return;
							}
						break;
					case 54:
						if(event.getSlot() == 40){
							event.setCancelled(true);
							event.setResult(Result.DENY);
							onEXPClick(event, player, e);
							return;
							}
						break;
					}
				break;
	        }
		}
	}
		
		public void expAccept(Player player, Player b){
			if(this.plugin.expTrading() != false){
				if(exp.get(player) == null && exp.get(b) != null){
					int c = exp.get(b);
					int playerXP = player.getLevel();
					int bXP = b.getLevel();
					bXP = bXP - c;
					playerXP = playerXP + c;
					b.setLevel(bXP);
					player.setLevel(playerXP);
					exp.remove(b);
					return;
				} else {
					if(exp.get(b) == null && exp.get(player) != null){
						int a = exp.get(player);
						int playerXP = player.getLevel();
						int bXP = b.getLevel();
						bXP = bXP + a;
						playerXP = playerXP - a;
						b.setLevel(bXP);
						player.setLevel(playerXP);
						exp.remove(player);
						return;
					}
				}
				if(exp.get(b) != null && exp.get(player) != null){
					int c = exp.get(b);
					int a = exp.get(player);
					int playerXP = player.getLevel();
					int bXP = b.getLevel();
					bXP = bXP + a;
					bXP = bXP - c;
					playerXP = playerXP + c;
					playerXP = playerXP - a;
					b.setLevel(bXP);
					player.setLevel(playerXP);
					exp.remove(player);
					exp.remove(b);
				}
			}
		}
		
		
		
		public void onEXPClick(InventoryClickEvent event, Player player, Player b){
			event.setCancelled(true);
			event.setResult(Result.DENY);
			if(accepted.contains(player)){
				accepted.remove(player);
				player.sendMessage(ChatPaginator.wordWrap(ChatColor.translateAlternateColorCodes('&', messageData.get("YOU_CHANGED_TRADE")), 21));
			}
			if(accepted.contains(b)){
				accepted.remove(b);
				b.sendMessage(ChatPaginator.wordWrap(getReplacedMessage("OTHER_PLAYER_CHANGED_TRADE", player, null, null), 21));
			}
			int exp1 = this.plugin.expOption();
			if(event.isLeftClick()){
				
				
				int f = player.getLevel();
				if(f >= exp1) {
					if(exp.get(player) == null){
						exp.put(player, exp1);	
						player.sendMessage(ChatPaginator.wordWrap(getReplacedMessage("EXP_YOU_ADDED", player, exp1, exp1), 21));
						b.sendMessage(ChatPaginator.wordWrap(getReplacedMessage("EXP_OTHER_ADDED", player, exp1, exp1), 21));
					} else {
						int a = exp.get(player);
						int g = f-a;
						if(g >= exp1){
							int c = a + exp1;
							exp.put(player, c);	
							int d = exp.get(player);
							player.sendMessage(ChatPaginator.wordWrap(getReplacedMessage("EXP_YOU_ADDED", player, exp1, d), 21));
							b.sendMessage(ChatPaginator.wordWrap(getReplacedMessage("EXP_OTHER_ADDED", player, exp1, d), 21));	
							return;
						} else {
							player.sendMessage(ChatColor.translateAlternateColorCodes('&', messageData.get("EXP_NOT_ENOUGH_MONEY_ADD")));
							return;	
						}
					}
				} else {
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', messageData.get("EXP_NOT_ENOUGH_MONEY_ADD")));
					return;	
				}
				
				
			} else {
				if(event.isRightClick()){
					
					if(exp.get(player) == null){
						player.sendMessage(ChatColor.translateAlternateColorCodes('&', messageData.get("EXP_NOT_ENOUGH_MONEY_WITHDRAW")));
						return;
					}
					int a = exp.get(player);
					int g = a-exp1;
					if(g < 0){
						player.sendMessage(ChatColor.translateAlternateColorCodes('&', messageData.get("EXP_NOT_ENOUGH_MONEY_WITHDRAW")));
					} else {
						exp.put(player, g);
						player.sendMessage(ChatPaginator.wordWrap(getReplacedMessage("EXP_YOU_WITHDREW", player, exp1, g), 21));
						b.sendMessage(ChatPaginator.wordWrap(getReplacedMessage("EXP_OTHER_WITHDREW", player, exp1, g), 21));	
					}
				}
			}
		}
			
			private String getReplacedMessage(String string, Player player, Integer amount, Integer totalamount){
				Integer distance = this.plugin.getDistance();
				String a = messageData.get(string);
				if(a.contains("%player%")){
					a = a.replaceAll("%player%", player.getName());
				}
				if(a.contains("%distance%")){
					a = a.replaceAll("%distance%", distance.toString());
				}
				if(amount != null && totalamount != null){
					if(a.contains("%amount%")){
						a = a.replaceAll("%amount%", amount.toString());
					}
					if(a.contains("%totalamount%")){
						a = a.replaceAll("%totalamount%", totalamount.toString());
					}
				}
				return ChatColor.translateAlternateColorCodes('&', a);
			}
}
