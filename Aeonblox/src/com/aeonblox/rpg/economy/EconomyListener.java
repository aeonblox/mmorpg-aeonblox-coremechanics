package com.aeonblox.rpg.economy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.ChatPaginator;

public class EconomyListener
  implements Listener
{
  public static Map<Player, Double> money = new HashMap<Player, Double>();
  public Map<String, Player> trades = TradeCommand.trades;
  public ArrayList<Player> accepted = TradeListener.accepted;
  public static HashMap<String, String> messageData = QuickTrade.messageData;
  QuickTrade plugin;
  
  public EconomyListener(QuickTrade quickTrade)
  {
	    this.plugin = quickTrade;
	    plugin.plugin.getServer().getPluginManager().registerEvents(this, plugin.plugin);
	  }
  
  public void onEconomyClick(InventoryClickEvent event)
  {
    Player player = (Player)event.getWhoClicked();
    ArrayList<Integer> rightside = TradeInventory.rightside;
    ArrayList<Integer> leftside = TradeInventory.leftside;
    String inventoryname = this.plugin.plugin.quicktrade.getInventoryName();
    if (this.plugin.plugin.quicktrade.economy != null)
    {
      if (!event.getInventory().getName().equals(inventoryname)) {
        return;
      }
      Player b = (Player)this.trades.get(player.getName());
      if (b == null)
      {
        getSecondPlayerOnEconomyClick(event);
      }
      else
      {
        int rows = this.plugin.plugin.quicktrade.getSize();
        switch (rows)
        {
        case 18: 
          if (event.getSlot() == 12)
          {
            event.setCancelled(true);
            event.setResult(Result.DENY);
            onEco1Click(event, player, b);
            return;
          }
          if (event.getSlot() == 13)
          {
            event.setCancelled(true);
            event.setResult(Result.DENY);
            onEco2Click(event, player, b);
            return;
          }
          if (event.getSlot() == 14)
          {
            event.setCancelled(true);
            event.setResult(Result.DENY);
            onEco3Click(event, player, b); return;
          }
          break;
        case 27: 
          if (event.getSlot() == 21)
          {
            event.setCancelled(true);
            event.setResult(Result.DENY);
            onEco1Click(event, player, b);
            return;
          }
          if (event.getSlot() == 22)
          {
            event.setCancelled(true);
            event.setResult(Result.DENY);
            onEco2Click(event, player, b);
            return;
          }
          if (event.getSlot() == 23)
          {
            event.setCancelled(true);
            event.setResult(Result.DENY);
            onEco3Click(event, player, b); return;
          }
          break;
        case 36: 
          if (event.getSlot() == 30)
          {
            event.setCancelled(true);
            event.setResult(Result.DENY);
            onEco1Click(event, player, b);
            return;
          }
          if (event.getSlot() == 31)
          {
            event.setCancelled(true);
            event.setResult(Result.DENY);
            onEco2Click(event, player, b);
            return;
          }
          if (event.getSlot() == 32)
          {
            event.setCancelled(true);
            event.setResult(Result.DENY);
            onEco3Click(event, player, b); return;
          }
          break;
        case 45: 
          if (event.getSlot() == 39)
          {
            event.setCancelled(true);
            event.setResult(Result.DENY);
            onEco1Click(event, player, b);
            return;
          }
          if (event.getSlot() == 40)
          {
            event.setCancelled(true);
            event.setResult(Result.DENY);
            onEco2Click(event, player, b);
            return;
          }
          if (event.getSlot() == 41)
          {
            event.setCancelled(true);
            event.setResult(Result.DENY);
            onEco3Click(event, player, b); return;
          }
          break;
        case 54: 
          if (event.getSlot() == 48)
          {
            event.setCancelled(true);
            event.setResult(Result.DENY);
            onEco1Click(event, player, b);
            return;
          }
          if (event.getSlot() == 49)
          {
            event.setCancelled(true);
            event.setResult(Result.DENY);
            onEco2Click(event, player, b);
            return;
          }
          if (event.getSlot() == 50)
          {
            event.setCancelled(true);
            event.setResult(Result.DENY);
            onEco3Click(event, player, b); return;
          }
          break;
        }
      }
    }
  }
  
  public void getSecondPlayerOnEconomyClick(InventoryClickEvent event)
  {
    String key = null;
    Player player = (Player)event.getWhoClicked();
    Player value = player;
    for (Map.Entry entry : this.trades.entrySet()) {
      if (value.equals(entry.getValue()))
      {
        key = (String)entry.getKey();
        Player e = Bukkit.getServer().getPlayerExact(key);
        int rows = this.plugin.plugin.quicktrade.getSize();
        switch (rows)
        {
        case 18: 
          if (event.getSlot() == 12)
          {
            event.setCancelled(true);
            event.setResult(Result.DENY);
            onEco1Click(event, player, e);
            return;
          }
          if (event.getSlot() == 13)
          {
            event.setCancelled(true);
            event.setResult(Result.DENY);
            onEco2Click(event, player, e);
            return;
          }
          if (event.getSlot() != 14) {
            break;
          }
          event.setCancelled(true);
          event.setResult(Result.DENY);
          onEco3Click(event, player, e);
          return;
        case 27: 
          if (event.getSlot() == 21)
          {
            event.setCancelled(true);
            event.setResult(Result.DENY);
            onEco1Click(event, player, e);
            return;
          }
          if (event.getSlot() == 22)
          {
            event.setCancelled(true);
            event.setResult(Result.DENY);
            onEco2Click(event, player, e);
            return;
          }
          if (event.getSlot() != 23) {
            break;
          }
          event.setCancelled(true);
          event.setResult(Result.DENY);
          onEco3Click(event, player, e);
          return;
        case 36: 
          if (event.getSlot() == 30)
          {
            event.setCancelled(true);
            event.setResult(Result.DENY);
            onEco1Click(event, player, e);
            return;
          }
          if (event.getSlot() == 31)
          {
            event.setCancelled(true);
            event.setResult(Result.DENY);
            onEco2Click(event, player, e);
            return;
          }
          if (event.getSlot() != 32) {
            break;
          }
          event.setCancelled(true);
          event.setResult(Result.DENY);
          onEco3Click(event, player, e);
          return;
        case 45: 
          if (event.getSlot() == 39)
          {
            event.setCancelled(true);
            event.setResult(Result.DENY);
            onEco1Click(event, player, e);
            return;
          }
          if (event.getSlot() == 40)
          {
            event.setCancelled(true);
            event.setResult(Result.DENY);
            onEco2Click(event, player, e);
            return;
          }
          if (event.getSlot() != 41) {
            break;
          }
          event.setCancelled(true);
          event.setResult(Result.DENY);
          onEco3Click(event, player, e);
          return;
        case 54: 
          if (event.getSlot() == 48)
          {
            event.setCancelled(true);
            event.setResult(Result.DENY);
            onEco1Click(event, player, e);
            return;
          }
          if (event.getSlot() == 49)
          {
            event.setCancelled(true);
            event.setResult(Result.DENY);
            onEco2Click(event, player, e);
            return;
          }
          if (event.getSlot() != 50) {
            break;
          }
          event.setCancelled(true);
          event.setResult(Result.DENY);
          onEco3Click(event, player, e); return;
        }
      }
    }
  }
  
  public void economyAccept(Player player, Player b)
  {
    if (this.plugin.plugin.quicktrade.economy != null)
    {
      if ((money.get(player) == null) && (money.get(b) != null))
      {
        double c = ((Double)money.get(b)).doubleValue();
        this.plugin.plugin.quicktrade.economy.withdrawPlayer(b.getName(), c);
        this.plugin.plugin.quicktrade.economy.depositPlayer(player.getName(), c);
        money.remove(b);
        return;
      }
      if ((money.get(b) == null) && (money.get(player) != null))
      {
        double a = ((Double)money.get(player)).doubleValue();
        this.plugin.plugin.quicktrade.economy.withdrawPlayer(player.getName(), a);
        this.plugin.plugin.quicktrade.economy.depositPlayer(b.getName(), a);
        money.remove(player);
        return;
      }
      if ((money.get(b) != null) && (money.get(player) != null))
      {
        double c = ((Double)money.get(b)).doubleValue();
        double a = ((Double)money.get(player)).doubleValue();
        this.plugin.plugin.quicktrade.economy.withdrawPlayer(player.getName(), a);
        this.plugin.plugin.quicktrade.economy.depositPlayer(b.getName(), a);
        this.plugin.plugin.quicktrade.economy.withdrawPlayer(b.getName(), c);
        this.plugin.plugin.quicktrade.economy.depositPlayer(player.getName(), c);
        money.remove(player);
        money.remove(b);
      }
    }
  }
  
  public void onEco1Click(InventoryClickEvent event, Player player, Player b)
  {
    event.setCancelled(true);
    event.setResult(Result.DENY);
    if (this.accepted.contains(player))
    {
      this.accepted.remove(player);
      player.sendMessage(ChatPaginator.wordWrap(ChatColor.translateAlternateColorCodes('&', (String)messageData.get("YOU_CHANGED_TRADE")), 21));
    }
    if (this.accepted.contains(b))
    {
      this.accepted.remove(b);
      b.sendMessage(ChatPaginator.wordWrap(getReplacedMessage("OTHER_PLAYER_CHANGED_TRADE", player, null, null), 21));
    }
    double eco1op = this.plugin.plugin.quicktrade.getEco1();
    if (event.isLeftClick())
    {
      double f = this.plugin.plugin.quicktrade.economy.getBalance(player.getName());
      if (f >= eco1op)
      {
        if (money.get(player) == null)
        {
          money.put(player, Double.valueOf(eco1op));
          newEcoIndicator(player.getOpenInventory().getTopInventory(), b, player);
          newEcoIndicator(b.getOpenInventory().getTopInventory(), player, b);
          player.sendMessage(ChatPaginator.wordWrap(getReplacedMessage("ECONOMY_YOU_ADDED", player, new Double(eco1op), new Double(eco1op)), 21));
          b.sendMessage(ChatPaginator.wordWrap(getReplacedMessage("ECONOMY_OTHER_ADDED", player, new Double(eco1op), new Double(eco1op)), 21));
        }
        else
        {
          double a = ((Double)money.get(player)).doubleValue();
          double g = f - a;
          if (g >= eco1op)
          {
            double c = a + eco1op;
            money.put(player, Double.valueOf(c));
            double d = ((Double)money.get(player)).doubleValue();
            newEcoIndicator(player.getOpenInventory().getTopInventory(), b, player);
            newEcoIndicator(b.getOpenInventory().getTopInventory(), player, b);
            player.sendMessage(ChatPaginator.wordWrap(getReplacedMessage("ECONOMY_YOU_ADDED", player, new Double(eco1op), new Double(d)), 21));
            b.sendMessage(ChatPaginator.wordWrap(getReplacedMessage("ECONOMY_OTHER_ADDED", player, new Double(eco1op), new Double(d)), 21));
            return;
          }
          player.sendMessage(ChatColor.translateAlternateColorCodes('&', (String)messageData.get("ECONOMY_NOT_ENOUGH_MONEY_ADD")));
        }
      }
      else {
        player.sendMessage(ChatColor.translateAlternateColorCodes('&', (String)messageData.get("ECONOMY_NOT_ENOUGH_MONEY_ADD")));
      }
    }
    else if (event.isRightClick())
    {
      if (money.get(player) == null)
      {
        player.sendMessage(ChatColor.translateAlternateColorCodes('&', (String)messageData.get("ECONOMY_NOT_ENOUGH_MONEY_WITHDRAW")));
        return;
      }
      double a = ((Double)money.get(player)).doubleValue();
      double g = a - eco1op;
      if (g < 0.0D)
      {
        player.sendMessage(ChatColor.translateAlternateColorCodes('&', (String)messageData.get("ECONOMY_NOT_ENOUGH_MONEY_WITHDRAW")));
      }
      else
      {
        money.put(player, Double.valueOf(g));
        newEcoIndicator(player.getOpenInventory().getTopInventory(), b, player);
        newEcoIndicator(b.getOpenInventory().getTopInventory(), player, b);
        player.sendMessage(ChatPaginator.wordWrap(getReplacedMessage("ECONOMY_YOU_WITHDREW", player, new Double(eco1op), new Double(g)), 21));
        b.sendMessage(ChatPaginator.wordWrap(getReplacedMessage("ECONOMY_OTHER_WITHDREW", player, new Double(eco1op), new Double(g)), 21));
      }
    }
  }
  
  public void onEco2Click(InventoryClickEvent event, Player player, Player b)
  {
    event.setCancelled(true);
    event.setResult(Result.DENY);
    if (this.accepted.contains(player))
    {
      this.accepted.remove(player);
      player.sendMessage(ChatPaginator.wordWrap(ChatColor.translateAlternateColorCodes('&', (String)messageData.get("YOU_CHANGED_TRADE")), 21));
    }
    if (this.accepted.contains(b))
    {
      this.accepted.remove(b);
      b.sendMessage(ChatPaginator.wordWrap(getReplacedMessage("OTHER_PLAYER_CHANGED_TRADE", player, null, null), 21));
    }
    double eco2op = this.plugin.plugin.quicktrade.getEco2();
    if (event.isLeftClick())
    {
      double f = this.plugin.plugin.quicktrade.economy.getBalance(player.getName());
      if (f >= eco2op)
      {
        if (money.get(player) == null)
        {
          money.put(player, Double.valueOf(eco2op));
          newEcoIndicator(player.getOpenInventory().getTopInventory(), b, player);
          newEcoIndicator(b.getOpenInventory().getTopInventory(), player, b);
          player.sendMessage(ChatPaginator.wordWrap(getReplacedMessage("ECONOMY_YOU_ADDED", player, new Double(eco2op), new Double(eco2op)), 21));
          b.sendMessage(ChatPaginator.wordWrap(getReplacedMessage("ECONOMY_OTHER_ADDED", player, new Double(eco2op), new Double(eco2op)), 21));
        }
        else
        {
          double a = ((Double)money.get(player)).doubleValue();
          double g = f - a;
          if (g >= eco2op)
          {
            double c = a + eco2op;
            money.put(player, Double.valueOf(c));
            double d = ((Double)money.get(player)).doubleValue();
            newEcoIndicator(player.getOpenInventory().getTopInventory(), b, player);
            newEcoIndicator(b.getOpenInventory().getTopInventory(), player, b);
            player.sendMessage(ChatPaginator.wordWrap(getReplacedMessage("ECONOMY_YOU_ADDED", player, new Double(eco2op), new Double(d)), 21));
            b.sendMessage(ChatPaginator.wordWrap(getReplacedMessage("ECONOMY_OTHER_ADDED", player, new Double(eco2op), new Double(d)), 21));
            return;
          }
          player.sendMessage(ChatColor.translateAlternateColorCodes('&', (String)messageData.get("ECONOMY_NOT_ENOUGH_MONEY_ADD")));
        }
      }
      else {
        player.sendMessage(ChatColor.translateAlternateColorCodes('&', (String)messageData.get("ECONOMY_NOT_ENOUGH_MONEY_ADD")));
      }
    }
    else if (event.isRightClick())
    {
      if (money.get(player) == null)
      {
        player.sendMessage(ChatColor.translateAlternateColorCodes('&', (String)messageData.get("ECONOMY_NOT_ENOUGH_MONEY_WITHDRAW")));
        return;
      }
      double a = ((Double)money.get(player)).doubleValue();
      double g = a - eco2op;
      if (g < 0.0D)
      {
        player.sendMessage(ChatColor.translateAlternateColorCodes('&', (String)messageData.get("ECONOMY_NOT_ENOUGH_MONEY_WITHDRAW")));
      }
      else
      {
        money.put(player, Double.valueOf(g));
        newEcoIndicator(player.getOpenInventory().getTopInventory(), b, player);
        newEcoIndicator(b.getOpenInventory().getTopInventory(), player, b);
        player.sendMessage(ChatPaginator.wordWrap(getReplacedMessage("ECONOMY_YOU_WITHDREW", player, new Double(eco2op), new Double(g)), 21));
        b.sendMessage(ChatPaginator.wordWrap(getReplacedMessage("ECONOMY_OTHER_WITHDREW", player, new Double(eco2op), new Double(g)), 21));
      }
    }
  }
  
  public void onEco3Click(InventoryClickEvent event, Player player, Player b)
  {
    event.setCancelled(true);
    event.setResult(Result.DENY);
    if (this.accepted.contains(player))
    {
      this.accepted.remove(player);
      player.sendMessage(ChatPaginator.wordWrap(ChatColor.translateAlternateColorCodes('&', (String)messageData.get("YOU_CHANGED_TRADE")), 21));
    }
    if (this.accepted.contains(b))
    {
      this.accepted.remove(b);
      b.sendMessage(ChatPaginator.wordWrap(getReplacedMessage("OTHER_PLAYER_CHANGED_TRADE", player, null, null), 21));
    }
    double eco3op = this.plugin.plugin.quicktrade.getEco3();
    if (event.isLeftClick())
    {
      double f = this.plugin.plugin.quicktrade.economy.getBalance(player.getName());
      if (f >= eco3op)
      {
        if (money.get(player) == null)
        {
          money.put(player, Double.valueOf(eco3op));
          newEcoIndicator(player.getOpenInventory().getTopInventory(), b, player);
          newEcoIndicator(b.getOpenInventory().getTopInventory(), player, b);
          player.sendMessage(ChatPaginator.wordWrap(getReplacedMessage("ECONOMY_YOU_ADDED", player, new Double(eco3op), new Double(eco3op)), 21));
          b.sendMessage(ChatPaginator.wordWrap(getReplacedMessage("ECONOMY_OTHER_ADDED", player, new Double(eco3op), new Double(eco3op)), 21));
        }
        else
        {
          double a = ((Double)money.get(player)).doubleValue();
          double g = f - a;
          if (g >= eco3op)
          {
            double c = a + eco3op;
            money.put(player, Double.valueOf(c));
            double d = ((Double)money.get(player)).doubleValue();
            newEcoIndicator(player.getOpenInventory().getTopInventory(), b, player);
            newEcoIndicator(b.getOpenInventory().getTopInventory(), player, b);
            player.sendMessage(ChatPaginator.wordWrap(getReplacedMessage("ECONOMY_YOU_ADDED", player, new Double(eco3op), new Double(d)), 21));
            b.sendMessage(ChatPaginator.wordWrap(getReplacedMessage("ECONOMY_OTHER_ADDED", player, new Double(eco3op), new Double(d)), 21));
            return;
          }
          player.sendMessage(ChatColor.translateAlternateColorCodes('&', (String)messageData.get("ECONOMY_NOT_ENOUGH_MONEY_ADD")));
        }
      }
      else {
        player.sendMessage(ChatColor.translateAlternateColorCodes('&', (String)messageData.get("ECONOMY_NOT_ENOUGH_MONEY_ADD")));
      }
    }
    else if (event.isRightClick())
    {
      if (money.get(player) == null)
      {
        player.sendMessage(ChatColor.translateAlternateColorCodes('&', (String)messageData.get("ECONOMY_NOT_ENOUGH_MONEY_WITHDRAW")));
        return;
      }
      double a = ((Double)money.get(player)).doubleValue();
      double g = a - eco3op;
      if (g < 0.0D)
      {
        player.sendMessage(ChatColor.translateAlternateColorCodes('&', (String)messageData.get("ECONOMY_NOT_ENOUGH_MONEY_WITHDRAW")));
      }
      else
      {
        money.put(player, Double.valueOf(g));
        newEcoIndicator(player.getOpenInventory().getTopInventory(), b, player);
        newEcoIndicator(b.getOpenInventory().getTopInventory(), player, b);
        player.sendMessage(ChatPaginator.wordWrap(getReplacedMessage("ECONOMY_YOU_WITHDREW", player, new Double(eco3op), new Double(g)), 21));
        b.sendMessage(ChatPaginator.wordWrap(getReplacedMessage("ECONOMY_OTHER_WITHDREW", player, new Double(eco3op), new Double(g)), 21));
      }
    }
  }
  
  private String getReplacedMessage(String string, Player player, Double amount, Double totalamount)
  {
    Integer distance = Integer.valueOf(this.plugin.plugin.quicktrade.getDistance());
    String a = (String)messageData.get(string);
    if (a.contains("%player%")) {
      a = a.replaceAll("%player%", player.getName());
    }
    if (a.contains("%distance%")) {
      a = a.replaceAll("%distance%", distance.toString());
    }
    if ((amount != null) && (totalamount != null))
    {
      if (a.contains("%amount%")) {
        a = a.replaceAll("%amount%", amount.toString());
      }
      if (a.contains("%totalamount%")) {
        a = a.replaceAll("%totalamount%", totalamount.toString());
      }
    }
    return ChatColor.translateAlternateColorCodes('&', a);
  }
  
  private ItemStack newEcoIndicator(Inventory inventory, Player player, Player you)
  {
    if (money.get(player) == null) {
      money.put(player, Double.valueOf(0.0D));
    }
    if (money.get(you) == null) {
      money.put(you, Double.valueOf(0.0D));
    }
    ItemStack ecoIndicator = new ItemStack(this.plugin.plugin.quicktrade.getEconomyIndicator());
    ItemMeta ecoMeta = ecoIndicator.getItemMeta();
    String name = "Money in Trade";
    ecoMeta.setDisplayName(ChatColor.WHITE + name);
    ecoMeta.setLore(Arrays.asList(new String[] { ChatColor.GOLD + player.getName() + " is offering: " + ChatColor.YELLOW + "$" + money.get(player), ChatColor.GOLD + "You are offering: " + ChatColor.YELLOW + "$" + money.get(you) }));
    ecoIndicator.setItemMeta(ecoMeta);
    inventory.setItem(4, ecoIndicator);
    return ecoIndicator;
  }
}