package com.aeonblox.rpg.economy;

import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class TradeItemStack implements Listener
{
  QuickTrade plugin;
  
  public TradeItemStack(QuickTrade quickTrade)
  {
    this.plugin = quickTrade;
  }
  
  public ItemStack getSeparator()
  {
    ItemStack separator = this.plugin.getSeparatorItem();
    String name = "Instance Seperator";
    ItemMeta separatormeta = separator.getItemMeta();
    separatormeta.setDisplayName(ChatColor.WHITE + name);
    separator.setItemMeta(separatormeta);
    return separator;
  }
  
  public ItemStack getEconomyIndicator(Player player)
  {
    ItemStack ecoIndicator = this.plugin.getEconomyIndicator();
    String name = "Money in Trade";
    ItemMeta ecoIndicatorMeta = ecoIndicator.getItemMeta();
    ecoIndicatorMeta.setDisplayName(ChatColor.WHITE + name);
    ecoIndicatorMeta.setLore(Arrays.asList(new String[] { ChatColor.GOLD + player.getName() + " is offering: " + ChatColor.YELLOW + "0 Blox", ChatColor.GOLD + "You are offering: " + ChatColor.YELLOW + "0 Blox" }));
    ecoIndicator.setItemMeta(ecoIndicatorMeta);
    return ecoIndicator;
  }
  
  public ItemStack getAccept()
  {
    ItemStack accept = this.plugin.getAcceptItem();
    String name = "Accept";
    ItemMeta acceptmeta = accept.getItemMeta();
    acceptmeta.setDisplayName(ChatColor.DARK_GREEN + name);
    accept.setItemMeta(acceptmeta);
    return accept;
  }
  
  public ItemStack getAccepted()
  {
    ItemStack accepted = this.plugin.getAcceptedItem();
    String name = "Accepted";
    ItemMeta acceptedmeta = accepted.getItemMeta();
    acceptedmeta.setDisplayName(ChatColor.AQUA + name);
    acceptedmeta.setLore(Arrays.asList(new String[] { ChatColor.WHITE + "Other player has accepted the trade" }));
    accepted.setItemMeta(acceptedmeta);
    return accepted;
  }
  
  public ItemStack getDecline()
  {
    ItemStack decline = this.plugin.getDeclineItem();
    String name = "Decline";
    ItemMeta declinemeta = decline.getItemMeta();
    declinemeta.setDisplayName(ChatColor.DARK_RED + name);
    decline.setItemMeta(declinemeta);
    return decline;
  }
  
  public ItemStack getEcoSeparator1()
  {
    ItemStack eco1 = new ItemStack(Material.COAL);
    int eco1op = this.plugin.getEco1();
    ItemMeta eco1meta = eco1.getItemMeta();
    eco1meta.setDisplayName(ChatColor.GOLD + "$" + eco1op);
    eco1meta.setLore(Arrays.asList(new String[] { ChatColor.DARK_RED + "Left" + ChatColor.WHITE + " click to add money to the trade!", ChatColor.DARK_RED + "Right" + ChatColor.WHITE + " click to remove money from the trade!" }));
    eco1.setItemMeta(eco1meta);
    return eco1;
  }
  
  public ItemStack getEcoSeparator2()
  {
    ItemStack eco2 = new ItemStack(Material.IRON_INGOT);
    int eco2op = this.plugin.getEco2();
    ItemMeta eco2meta = eco2.getItemMeta();
    eco2meta.setDisplayName(ChatColor.GOLD + "$" + eco2op);
    eco2meta.setLore(Arrays.asList(new String[] { ChatColor.DARK_RED + "Left" + ChatColor.WHITE + " click to add money to the trade!", ChatColor.DARK_RED + "Right" + ChatColor.WHITE + " click to remove money from the trade!" }));
    eco2.setItemMeta(eco2meta);
    return eco2;
  }
  
  public ItemStack getEcoSeparator3()
  {
    ItemStack eco3 = new ItemStack(Material.DIAMOND);
    int eco3op = this.plugin.getEco3();
    ItemMeta eco3meta = eco3.getItemMeta();
    eco3meta.setDisplayName(ChatColor.GOLD + "$" + eco3op);
    eco3meta.setLore(Arrays.asList(new String[] { ChatColor.DARK_RED + "Left" + ChatColor.WHITE + " click to add money to the trade!", ChatColor.DARK_RED + "Right" + ChatColor.WHITE + " click to remove money from the trade!" }));
    eco3.setItemMeta(eco3meta);
    return eco3;
  }
  
  public ItemStack getEXP1()
  {
    ItemStack exp1 = new ItemStack(Material.EXP_BOTTLE);
    int expOption = this.plugin.expOption();
    ItemMeta exp1meta = exp1.getItemMeta();
    exp1meta.setDisplayName(ChatColor.DARK_AQUA + "" + expOption);
    exp1meta.setLore(Arrays.asList(new String[] { ChatColor.GREEN + "Left click: ADD EXP", ChatColor.RED + "Right click: REMOVE EXP!" }));
    exp1.setItemMeta(exp1meta);
    return exp1;
  }
}