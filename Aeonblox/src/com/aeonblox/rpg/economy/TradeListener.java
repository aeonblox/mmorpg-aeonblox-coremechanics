package com.aeonblox.rpg.economy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.commons.lang.WordUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.ChatPaginator;

public class TradeListener implements Listener{
	private QuickTrade plugin;
	private TradeCommand tcmd;
	private TradeInventory tinv;
	public ArrayList<Integer> rightside = TradeInventory.rightside;
	public ArrayList<Integer> leftside = TradeInventory.leftside;
	public static ArrayList<Player> accepted = new ArrayList<Player>();
	public Map<String, Player> trades = TradeCommand.trades;
	public static Map<Player, Integer> exp = EXPListener.exp;
	public ArrayList<Player> rct = RightClickTrade.rct;
	public ArrayList<Player> requestedlist = TradeCommand.requestedlist;
	public ArrayList<Player> requesterlist = TradeCommand.requesterlist;
	public static Map<String, String> requests = TradeCommand.requests;
	public static Map<Player, Double> money = EconomyListener.money;
	public Map<String, Player> rightclickrequests = RightClickTrade.rightclickrequests;
	public static HashMap<String, String> messageData = QuickTrade.messageData;
	public static ArrayList<Player> timeoutcheck = TradeCommand.timeoutcheck;
	public static ArrayList<Player> playersintrades = TradeCommand.playersintrades;
	public static ArrayList<String> drops = new ArrayList<String>();
	public static Map<String, ItemStack[]> startInventory = TradeCommand.startInventory;
	private static final Logger log = Logger.getLogger("Minecraft");
	public static Map<String, Inventory> invlist = TradeCommand.invlist;

	public TradeListener(QuickTrade plugin) {
		this.plugin = plugin;
		this.tcmd = plugin.getTradeCommand();
		this.tinv = plugin.getTradeInventory();
		
		plugin.plugin.getServer().getPluginManager().registerEvents(this, plugin.plugin);
	}


	@EventHandler
	public void onInventoryClick(final InventoryClickEvent event){
		TradeInventory tinv = this.plugin.getTradeInventory();
		EconomyListener ecolistener = this.plugin.getEconomyListener();
		EXPListener explistener = this.plugin.getEXPListener();
		List<Integer> blacklist = plugin.getBlacklist();
		final Inventory inventory = event.getInventory();
		String inventoryname = this.plugin.getInventoryName();
		final Player player = (Player) event.getWhoClicked();
		if(inventory.getTitle().equals(inventoryname)){
			if (event.getRawSlot() < inventory.getSize()) {
				if(event.getCurrentItem() == null){
					return;
				}

				if(blacklist.contains(event.getCursor().getTypeId()) && (event.getSlot() != 5 || event.getSlot() != 3)){
					player.sendMessage(ChatPaginator.wordWrap(ChatColor.translateAlternateColorCodes('&', messageData.get("BLACKLISTED_ITEM")), 21));
					event.setCancelled(true);
					return;
				}
				
				ItemStack ecoIndicator = this.plugin.getEconomyIndicator();
				if(event.getCurrentItem().getType() == ecoIndicator.getType() && event.getSlot() == 4){
					event.setCancelled(true);
					event.setResult(Result.DENY);
					return;
				}
				
				if(event.getAction() == InventoryAction.PICKUP_ALL){
				}

				if(event.getCurrentItem().getType() == Material.COAL || event.getCurrentItem().getType() == Material.IRON_INGOT || event.getCurrentItem().getType() == Material.DIAMOND){
					ecolistener.onEconomyClick(event);
				}
				
				if(event.getCurrentItem().getType() == Material.ENDER_PEARL){
					explistener.onEXPClick(event);
				}
				
				// Check if they tried to move separator
				ItemStack separatoritem = this.plugin.tradeitemstack.getSeparator();
				if(event.getCurrentItem().equals(separatoritem)){
					event.setCancelled(true);
					return;
				}
				
				
				if(event.isShiftClick()){
					ItemStack air = new ItemStack(Material.AIR);
					if(!rct.contains(player)){
						if(requestedlist.contains(player)){
							if(rightside.contains(event.getSlot())){
								event.setCancelled(true);
								event.setResult(Result.DENY);
							}
						} else {
							if(requesterlist.contains(player)){
								if(leftside.contains(event.getSlot())){
									event.setCancelled(true);
									event.setResult(Result.DENY);
								}
							}
						}
					} else {
						if(requestedlist.contains(player)){
							if(leftside.contains(event.getSlot())){
								event.setCancelled(true);
								event.setResult(Result.DENY);
							}
						} else {
							if(requesterlist.contains(player)){
								if(rightside.contains(event.getSlot())){
									event.setCancelled(true);
									event.setResult(Result.DENY);
								}
							}
						}
					}
				}
				
				if(event.getCurrentItem() != null && event.getCurrentItem().getType() != Material.AIR && event.getSlot() != 5 && event.getSlot() != 3){
					Player otherplayer = trades.get(player.getName());
					if(!event.isCancelled()){
						if(otherplayer == null){
							// if b = null, that means player is <Player>(requested) and b is <String>(requester)
							// requested is inv 1, requester is inv 2
							Player b = getOtherPlayerInTrade(player);
							if(accepted.contains(player)){
								accepted.remove(player);
								player.sendMessage(ChatPaginator.wordWrap(ChatColor.translateAlternateColorCodes('&', messageData.get("YOU_CHANGED_TRADE")), 21));
							}
							if(accepted.contains(b)){
								accepted.remove(b);
								b.sendMessage(ChatPaginator.wordWrap(getReplacedMessage("OTHER_PLAYER_CHANGED_TRADE", player), 21));
							}
						} else {
							// if b != null, that means player is <String>(requester) and b is <Player>(requested)
							// requested is inv 1, requester is inv 2
							if(accepted.contains(player)){
								accepted.remove(player);
								player.sendMessage(ChatPaginator.wordWrap(ChatColor.translateAlternateColorCodes('&', messageData.get("YOU_CHANGED_TRADE")), 21));
							}
							if(accepted.contains(otherplayer)){
								accepted.remove(otherplayer);
								otherplayer.sendMessage(ChatPaginator.wordWrap(getReplacedMessage("OTHER_PLAYER_CHANGED_TRADE", player), 21));
							}
						}
					}
				}

				// Check if they tried to move the accept button
				ItemStack acceptitem = this.plugin.tradeitemstack.getAccept();
				ItemStack accepteditem = this.plugin.tradeitemstack.getAccepted();
				if((event.getCurrentItem().equals(acceptitem) || event.getCurrentItem().equals(accepteditem)) && (event.getSlot() == 5 || event.getSlot() == 3)){
					event.setCancelled(true);

					Player b = trades.get(player.getName());
					// if b != null, that means player is <String>(requester) and b is <Player>(requested)
					if(b == null){
						getSecondPlayerAndAccept(player);
					} else {
						// if b != null, that means player is <String>(requester) and b is <Player>(requested)
						// requested is inv 1, requester is inv 2
						// requested uses LEFT SIDE, requester uses RIGHT SIDE
						checkForDupe(player);
						checkForDupe(b);
						if(player.getItemOnCursor() != null){
							ItemStack playerItem = player.getItemOnCursor();
							ItemStack air = new ItemStack(Material.AIR);
							player.setItemOnCursor(air);
							player.getInventory().addItem(playerItem);
						}
						
						if(b.getItemOnCursor() != null){
							ItemStack bItem = b.getItemOnCursor();
							ItemStack air = new ItemStack(Material.AIR);
							b.setItemOnCursor(air);
							b.getInventory().addItem(bItem);
						}
						
						if(accepted.contains(b)) {
							if(!rct.contains(player)){
								
								for (Integer a : rightside){
									int e = a.intValue();
									//gets items in requester's left side
									Inventory playerinv = invlist.get(player.getName());
									ItemStack f = playerinv.getItem(e);
									if(f != null){
										
										if(b.getInventory().firstEmpty() == -1){
											World world = b.getWorld();
											Location location = b.getLocation();
											world.dropItem(location, f);
											drops.add(b.getName());
										}
										if(f.getType() != Material.AIR){
											//requested
											b.getInventory().addItem(f);
										}
									}
								}
							} else {
								for (Integer a : leftside){
									int e = a.intValue();
									Inventory playerinv = invlist.get(player.getName());
									ItemStack f = playerinv.getItem(e);
									if(f != null){
										
										if(b.getInventory().firstEmpty() == -1){
											World world = b.getWorld();
											Location location = b.getLocation();
											world.dropItem(location, f);
											drops.add(b.getName());
										}
										if(f.getType() != Material.AIR){
											//requested
											b.getInventory().addItem(f);
										}
									}
								}
							}

							if(!rct.contains(player)){
								for (Integer a : leftside){
									int e = a.intValue();
									//gets items in requested's right side
									Inventory binv = invlist.get(b.getName());
									ItemStack f = binv.getItem(e);
									if(f != null){
										
										if(player.getInventory().firstEmpty() == -1){
											World world = player.getWorld();
											Location location = player.getLocation();
											world.dropItem(location, f);
											drops.add(player.getName());
										}
										if(f.getType() != Material.AIR){
											//requester
											player.getInventory().addItem(f);
										}
									}
								}
							} else {
								for (Integer a : rightside){
									int e = a.intValue();
									//gets items in requested's right side
									Inventory binv = invlist.get(b.getName());
									ItemStack f = binv.getItem(e);
									if(f != null){
										
										if(player.getInventory().firstEmpty() == -1){
											World world = player.getWorld();
											Location location = player.getLocation();
											world.dropItem(location, f);
											drops.add(player.getName());
										}
										if(f.getType() != Material.AIR){
											//requester
											player.getInventory().addItem(f);
										}
									}
								}
							}
							explistener.expAccept(player, b);
							ecolistener.economyAccept(player, b);
							acceptTrade(b, player);
							return;
						} else {
							if(!accepted.contains(player)){
								accepted.add(player);
								player.sendMessage(ChatPaginator.wordWrap(getReplacedMessage("ACCEPTED_TRADE", b), 21));
								b.sendMessage(ChatPaginator.wordWrap(getReplacedMessage("OTHER_PLAYER_ACCEPTED_TRADE", player), 21));
								// requested is inv 1, requester is inv 2
								if(!rct.contains(player)){
									if(requestedlist.contains(player)) {
										log.info("WORKED1");
										changeAcceptItem(tcmd.inventory2, player);
									}
									if(requesterlist.contains(player)) {
										log.info("WORKED2");
										changeAcceptItem(tcmd.inventory1, player);
									}
								} else {
									if(requestedlist.contains(player)) {
										log.info("WORKED3");
										changeAcceptItem(this.plugin.rightclicktrade.inv1, player);
									}
									if(requesterlist.contains(player)) {
										log.info("WORKED4");
										changeAcceptItem(this.plugin.rightclicktrade.inv2, player);
									}
								}
							} else {
								player.sendMessage(ChatPaginator.wordWrap(ChatColor.translateAlternateColorCodes('&', messageData.get("ALREADY_ACCEPTED_TRADE")), 21));
							}
						}
					}
					return;
				}

				// Check if they tried to move the decline button
				ItemStack declineitem = this.plugin.tradeitemstack.getDecline();
				if(event.getCurrentItem().equals(declineitem) && (event.getSlot() == 5 || event.getSlot() == 3)){

					event.setCancelled(true);
					// Find other player involved in trade and close their inventory (cancel trade)
					Player b = trades.get(player.getName());

					if(b == null){
						getSecondPlayerAndExit(player);

					} else {	
						checkForDupe(player);
						checkForDupe(b);
						if(player.getItemOnCursor() != null){
							ItemStack playerItem = player.getItemOnCursor();
							ItemStack air = new ItemStack(Material.AIR);
							player.setItemOnCursor(air);
							player.getInventory().addItem(playerItem);
						}
						
						if(b.getItemOnCursor() != null){
							ItemStack bItem = b.getItemOnCursor();
							ItemStack air = new ItemStack(Material.AIR);
							b.setItemOnCursor(air);
							b.getInventory().addItem(bItem);
						}
						
						// if b != null, that means player is <String>(requester) and b is <Player>(requested)
						// requested is inv 1, requester is inv 2
						// requested uses LEFT SIDE, requester uses RIGHT SIDE
						if(!rct.contains(player)){
							for (Integer a : leftside){
								int e = a.intValue();
								//gets items in requested's left side
								Inventory playerinv = invlist.get(player.getName());
								ItemStack f = playerinv.getItem(e);
								if(f != null){
									if(f.getType() != Material.AIR){
										//requested
										b.getInventory().addItem(f);
									}
								}
							}
						} else {
							for (Integer a : rightside){
								int e = a.intValue();
								Inventory playerinv = invlist.get(player.getName());
								ItemStack f = playerinv.getItem(e);
								if(f != null){
									if(f.getType() != Material.AIR){
										//requested
										b.getInventory().addItem(f);
									}
								}
							}
						}

						if(!rct.contains(player)){
							for (Integer a : rightside){
								int e = a.intValue();
								//gets items in requester's right side
								Inventory binv = invlist.get(b.getName());
								ItemStack f = binv.getItem(e);
								if(f != null){
									if(f.getType() != Material.AIR){
										//requester
										player.getInventory().addItem(f);
									}
								}
							}
						} else {
							for (Integer a : leftside){
								int e = a.intValue();
								Inventory binv = invlist.get(b.getName());
								ItemStack f = binv.getItem(e);
								if(f != null){
									if(f.getType() != Material.AIR){
										//requester
										player.getInventory().addItem(f);
									}
								}
							}
						}
						exitTrade(player, b);
						return;
					}
				}







				// Prevent using other side of inv & add items to opposite inv
				// If Player is requested
				// requested is inv 1, requester is inv 2
				// requested uses LEFT SIDE, requester uses RIGHT SIDE
				if(!rct.contains(player)){
					if(requestedlist.contains(player)) {

						if(rightside.contains(event.getRawSlot()) || rightside.contains(event.getSlot())){
							event.setCancelled(true);
							player.sendMessage(ChatPaginator.wordWrap(ChatColor.translateAlternateColorCodes('&', messageData.get("INVALID_SLOT")), 21));
							return;
						}
					}
					if(requesterlist.contains(player)) {

						if(leftside.contains(event.getRawSlot()) || leftside.contains(event.getSlot())){
							event.setCancelled(true);
							player.sendMessage(ChatPaginator.wordWrap(ChatColor.translateAlternateColorCodes('&', messageData.get("INVALID_SLOT")), 21));
							return;

						}
					}
				} else {
					if(requestedlist.contains(player)) {

						if(leftside.contains(event.getRawSlot()) || leftside.contains(event.getSlot())){
							event.setCancelled(true);
							player.sendMessage(ChatPaginator.wordWrap(ChatColor.translateAlternateColorCodes('&', messageData.get("INVALID_SLOT")), 21));
							return;
						}
					}
					if(requesterlist.contains(player)) {

						if(rightside.contains(event.getRawSlot()) || rightside.contains(event.getSlot())){
							event.setCancelled(true);
							player.sendMessage(ChatPaginator.wordWrap(ChatColor.translateAlternateColorCodes('&', messageData.get("INVALID_SLOT")), 21));
							return;

						}
					}
				}
			}

			if(event.getAction() == InventoryAction.COLLECT_TO_CURSOR || event.getClick() == ClickType.DOUBLE_CLICK){
				event.setCancelled(true);
				return;
			}
			if (event.getRawSlot() > inventory.getSize()) {
				if(event.isShiftClick()){
					ItemStack air = new ItemStack(Material.AIR);
					if(!rct.contains(player)){
						if(requestedlist.contains(player)){
							int intair = 0;
							for (Integer a : leftside){
								int e = a.intValue();
								//gets items in requested's right side
								Inventory playerinv = invlist.get(player.getName());
								ItemStack f = playerinv.getItem(e);
								if(f == null){
									intair = e;
									break;
								}
							}
							event.setCancelled(true);
							if(intair != 0){
								Inventory playerinv = invlist.get(player.getName());
								ItemStack currentitem = event.getCurrentItem();
								playerinv.setItem(intair, currentitem);
								event.setCurrentItem(air);
							}
						} else {
							if(requesterlist.contains(player)){
								int intair = 0;
								for (Integer a : rightside){
									int e = a.intValue();
									//gets items in requested's right side
									Inventory playerinv = invlist.get(player.getName());
									ItemStack f = playerinv.getItem(e);
									if(f == null){
										intair = e;
										break;
									}
								}
								event.setCancelled(true);
								if(intair != 0){
									Inventory playerinv = invlist.get(player.getName());
									ItemStack currentitem = event.getCurrentItem();
									playerinv.setItem(intair, currentitem);
									event.setCurrentItem(air);
								}
							}
						}
					} else {
						if(requestedlist.contains(player)){
							int intair = 0;
							for (Integer a : rightside){
								int e = a.intValue();
								//gets items in requested's right side
								Inventory playerinv = invlist.get(player.getName());
								ItemStack f = playerinv.getItem(e);
								if(f == null && e != 0){
									intair = e;
									break;
								}
							}
							event.setCancelled(true);
							if(intair != 0){
								Inventory playerinv = invlist.get(player.getName());
								ItemStack currentitem = event.getCurrentItem();
								playerinv.setItem(intair, currentitem);
								event.setCurrentItem(air);
							}
						} else {
							if(requesterlist.contains(player)){
								int intair = 0;
								for (Integer a : leftside){
									int e = a.intValue();
									//gets items in requested's right side
									Inventory playerinv = invlist.get(player.getName());
									ItemStack f = playerinv.getItem(e);
									if(f == null){
										intair = e;
										break;
									}
								}
								event.setCancelled(true);
								if(intair != 0){
									Inventory playerinv = invlist.get(player.getName());
									ItemStack currentitem = event.getCurrentItem();
									playerinv.setItem(intair, currentitem);
									event.setCurrentItem(air);
								}
							}
						}
					}
				}
			}

			if(requestedlist.contains(player)) {
				invlist.put(player.getName(), event.getView().getTopInventory());
				Bukkit.getServer().getScheduler().runTaskLater(plugin.plugin, new Runnable()
				{
					@Override
					public void run()
					{
						if(!rct.contains(player)){
							for (Integer a : leftside){
								int e = a.intValue();
								Inventory playerinv = invlist.get(player.getName());
								ItemStack f = playerinv.getItem(e);
								Player b = getOtherPlayerInTrade(player);
								Inventory binv = b.getOpenInventory().getTopInventory();
								if(f != null){
									binv.setItem(e, f);
									invlist.put(b.getName(), binv);
								} else {
									ItemStack air = new ItemStack(Material.AIR);
									binv.setItem(e, air);
									invlist.put(b.getName(), binv);
									ItemStack t = binv.getItem(e);
								}
							}
						} else {
							for (Integer a : rightside){
								int e = a.intValue();
								Inventory playerinv = invlist.get(player.getName());
								ItemStack f = playerinv.getItem(e);
								Player b = getOtherPlayerInTrade(player);
								Inventory binv = b.getOpenInventory().getTopInventory();
								if(f != null){
									binv.setItem(e, f);
									invlist.put(b.getName(), binv);
								} else {
									ItemStack air = new ItemStack(Material.AIR);
									binv.setItem(e, air);
									invlist.put(b.getName(), binv);
									ItemStack t = binv.getItem(e);
								}
							}
						}
					}
				}, 5); // Half a second delay on executing loop

			} else {

				if(requesterlist.contains(player)) {
					invlist.put(player.getName(), event.getView().getTopInventory());
					Bukkit.getServer().getScheduler().runTaskLater(plugin.plugin, new Runnable()
					{
						@Override
						public void run()
						{
							if(!rct.contains(player)){
								for (Integer a : rightside){
									int e = a.intValue();
									Inventory playerinv = invlist.get(player.getName());
									ItemStack f = playerinv.getItem(e);
									Player b = trades.get(player.getName());
									Inventory binv = b.getOpenInventory().getTopInventory();
									if(f != null){
										binv.setItem(e, f);
										invlist.put(b.getName(), binv);
									} else {
										ItemStack air = new ItemStack(Material.AIR);
										binv.setItem(e, air);
										invlist.put(b.getName(), binv);
										ItemStack t = binv.getItem(e);
									}
								}
							} else {
								for (Integer a : leftside){
									int e = a.intValue();
									Inventory playerinv = invlist.get(player.getName());
									ItemStack f = playerinv.getItem(e);
									Player b = trades.get(player.getName());
									Inventory binv = b.getOpenInventory().getTopInventory();
									if(f != null){
										binv.setItem(e, f);
										invlist.put(b.getName(), binv);
									} else {
										ItemStack air = new ItemStack(Material.AIR);
										binv.setItem(e, air);
										invlist.put(b.getName(), binv);
										ItemStack t = binv.getItem(e);
									}
								}

							}
						}
					}, 5); // Half a second delay on executing loop
				}
			}
		}
		
		if(event.isCancelled()){
			player.updateInventory();
			player.saveData();
		}
	}		




	@EventHandler
	public void onTradeInventoryDrag(final InventoryDragEvent event){
		List<Integer> blacklist = plugin.getBlacklist();
		Inventory inventory = event.getInventory();
		final Player player = (Player) event.getWhoClicked();
		String inventoryname = this.plugin.getInventoryName();
		if(inventory.getTitle().equals(inventoryname)){
			
			if(blacklist.contains(event.getOldCursor().getTypeId())){
				player.sendMessage(ChatPaginator.wordWrap(ChatColor.translateAlternateColorCodes('&', messageData.get("BLACKLISTED_ITEM")), 21));
				event.setCancelled(true);
				return;
			}
			
			
			if(requestedlist.contains(player)) {
				for(Integer a : event.getRawSlots()){
					int b = a.intValue();
					if(!rct.contains(player)){
						if(rightside.contains(b)){
							event.setCancelled(true);
						}
					} else {
						if(leftside.contains(b)){
							event.setCancelled(true);
						}
					}
				}
				// if b != null, that means player is <String>(requester) and b is <Player>(requested)
				// requested is inv 1, requester is inv 2
				// requested uses LEFT SIDE, requester uses RIGHT SIDE
				if(event.isCancelled()){
					player.sendMessage(ChatPaginator.wordWrap(ChatColor.translateAlternateColorCodes('&', messageData.get("INVALID_SLOT")), 21));
					return;
				}
				invlist.put(player.getName(), event.getView().getTopInventory());
				Bukkit.getServer().getScheduler().runTaskLater(plugin.plugin, new Runnable()
				{
					
					@Override
					public void run()
					{
						if(!rct.contains(player)){
							for (Integer a : leftside){
								int e = a.intValue();
								Inventory playerinv = invlist.get(player.getName());
								ItemStack f = playerinv.getItem(e);
								Player b = getOtherPlayerInTrade(player);
								Inventory binv = b.getOpenInventory().getTopInventory();
								if(f != null){
									binv.setItem(e, f);
									invlist.put(b.getName(), binv);
								} else {
									ItemStack air = new ItemStack(Material.AIR);
									binv.setItem(e, air);
									invlist.put(b.getName(), binv);
									ItemStack t = binv.getItem(e);
								}
							}
						} else {
							for (Integer a : rightside){
								int e = a.intValue();
								Inventory playerinv = invlist.get(player.getName());
								ItemStack f = playerinv.getItem(e);
								Player b = getOtherPlayerInTrade(player);
								Inventory binv = b.getOpenInventory().getTopInventory();
								if(f != null){
									binv.setItem(e, f);
									invlist.put(b.getName(), binv);
								} else {
									ItemStack air = new ItemStack(Material.AIR);
									binv.setItem(e, air);
									invlist.put(b.getName(), binv);
									ItemStack t = binv.getItem(e);
								}
							}
						}
					}
				}, 5);

			} else {

				// Prevent using other side of inv & add items to opposite inv
				// If Player is requester
				if(requesterlist.contains(player)) {
					for(Integer a : event.getRawSlots()){
						int b = a.intValue();
						if(!rct.contains(player)){
							if(leftside.contains(b)){
								event.setCancelled(true);
							}
						} else {
							if(rightside.contains(b)){
								event.setCancelled(true);
							}
						}
					}

					if(event.isCancelled()){
						player.sendMessage(ChatPaginator.wordWrap(ChatColor.translateAlternateColorCodes('&', messageData.get("INVALID_SLOT")), 21));
						return;
					}
					invlist.put(player.getName(), event.getView().getTopInventory());
					Bukkit.getServer().getScheduler().runTaskLater(plugin.plugin, new Runnable()
					{
						@Override
						public void run()
						{
							if(!rct.contains(player)){
								for (Integer a : rightside){
									int e = a.intValue();
									Inventory playerinv = invlist.get(player.getName());
									ItemStack f = playerinv.getItem(e);
									Player b = getOtherPlayerInTrade(player);
									Inventory binv = b.getOpenInventory().getTopInventory();
									if(f != null){
										binv.setItem(e, f);
										invlist.put(b.getName(), binv);
									} else {
										ItemStack air = new ItemStack(Material.AIR);
										binv.setItem(e, air);
										invlist.put(b.getName(), binv);
										ItemStack t = binv.getItem(e);
									}
								}
							} else {
								for (Integer a : leftside){
									int e = a.intValue();
									Inventory playerinv = invlist.get(player.getName());
									ItemStack f = playerinv.getItem(e);
									Player b = getOtherPlayerInTrade(player);
									Inventory binv = b.getOpenInventory().getTopInventory();
									if(f != null){
										binv.setItem(e, f);
										invlist.put(b.getName(), binv);
									} else {
										ItemStack air = new ItemStack(Material.AIR);
										binv.setItem(e, air);
										invlist.put(b.getName(), binv);
										ItemStack t = binv.getItem(e);
									}
								}
							}
						}
					}, 5);
				}
			}
		}
	}


	@EventHandler
	public void onTradeInventoryCloseEvent(InventoryCloseEvent event){
		Inventory inventory = event.getInventory();
		Player player = (Player) event.getPlayer();
		if(trades.containsValue(player) || trades.containsKey(player.getName())){
			String inventoryname = this.plugin.getInventoryName();
			if(inventory.getTitle() == inventoryname){
				Player b = trades.get(player.getName());
				// Find other player involved in trade and send decline message
				if(b == null){
					getSecondPlayerAndExit(player);

				} else {
					if(player.getItemOnCursor() != null){
						ItemStack playerItem = player.getItemOnCursor();
						ItemStack air = new ItemStack(Material.AIR);
						player.setItemOnCursor(air);
						player.getInventory().addItem(playerItem);
					}
					
					if(b.getItemOnCursor() != null){
						ItemStack bItem = b.getItemOnCursor();
						ItemStack air = new ItemStack(Material.AIR);
						b.setItemOnCursor(air);
						b.getInventory().addItem(bItem);
					}
					// if b != null, that means player is <String>(requester) and b is <Player>(requested)
					// requested is inv 1, requester is inv 2
					// requested uses LEFT SIDE, requester uses RIGHT SIDE
				/*	Inventory playerinv1 = invlist.get(player.getName());
					for(ItemStack i : playerinv1.getContents()){
						if(i == null){
							log.info("NULL");
						} else {
							if(i.getType() == Material.STONE_SWORD){
								log.info("ITEM SLOT:" + playerinv1.first(i));
							}
							log.info("794 " + i.getType());
						}
					}*/
					
					if(!rct.contains(player)){
						for (Integer a : leftside){
							int e = a.intValue();
							//gets items in requested's left side
							Inventory playerinv = invlist.get(player.getName());
							ItemStack f = playerinv.getItem(e);
							if(f != null){
								if(f.getType() != Material.AIR){
									//requested
									b.getInventory().addItem(f);
								}
							}
						}
					} else {
						for (Integer a : rightside){
							int e = a.intValue();
							Inventory playerinv = invlist.get(player.getName());
							ItemStack f = playerinv.getItem(e);
							if(f != null){
								if(f.getType() != Material.AIR){
									//requested
									b.getInventory().addItem(f);
								}
							}
						}
					}

					if(!rct.contains(player)){
						for (Integer a : rightside){
							int e = a.intValue();
							//gets items in requester's right side
							Inventory binv = invlist.get(b.getName());
							ItemStack f = binv.getItem(e);
							if(f != null){
								if(f.getType() != Material.AIR){
									//requester
									player.getInventory().addItem(f);
								}
							}
						}
					} else {
						for (Integer a : leftside){
							int e = a.intValue();
							Inventory binv = invlist.get(b.getName());
							ItemStack f = binv.getItem(e);
							if(f != null){
								if(f.getType() != Material.AIR){
									//requester
									player.getInventory().addItem(f);
								}
							}
						}
					}
					exitTrade(b, player);
				}
			}
		}
	}

	public void getSecondPlayerAndExit(Player player){
				Player c = getOtherPlayerInTrade(player);

				// if b = null, means player is <Player>(requested) and c is <String> (requester)
				// requested is inv 1, requester is inv 2
				// requested uses LEFT SIDE, requester uses RIGHT SIDE
				checkForDupe(player);
				checkForDupe(c);

				if(player.getItemOnCursor() != null){
					ItemStack playerItem = player.getItemOnCursor();
					ItemStack air = new ItemStack(Material.AIR);
					player.setItemOnCursor(air);
					player.getInventory().addItem(playerItem);
					player.updateInventory();
				}
				
				if(c.getItemOnCursor() != null){
					ItemStack bItem = c.getItemOnCursor();
					ItemStack air = new ItemStack(Material.AIR);
					c.setItemOnCursor(air);
					c.getInventory().addItem(bItem);
					player.updateInventory();
				}
				
				if(!rct.contains(player)){
					for (Integer a : rightside){
						int e = a.intValue();
						//gets items in requester's right side
						Inventory playerinv = invlist.get(player.getName());
						ItemStack f = playerinv.getItem(e);
						if(f != null){
							if(f.getType() != Material.AIR){
								//requester
								c.getInventory().addItem(f);
							}
						}
					}
				} else {
					for (Integer a : leftside){
						int e = a.intValue();
						Inventory playerinv = invlist.get(player.getName());
						ItemStack f = playerinv.getItem(e);
						if(f != null){
							if(f.getType() != Material.AIR){
								//requester
								c.getInventory().addItem(f);
							}
						}
					}
				}

				if(!rct.contains(player)){
					for (Integer a : leftside){
						int e = a.intValue();
						//gets items in requested's right side
						Inventory cinv = invlist.get(c.getName());
						ItemStack f = cinv.getItem(e);
						if(f != null){
							if(f.getType() != Material.AIR){
								//requested
								player.getInventory().addItem(f);
							}
						}
					}
				} else {
					for (Integer a : rightside){
						int e = a.intValue();
						//gets items in requested's right side
						Inventory cinv = invlist.get(c.getName());
						ItemStack f = cinv.getItem(e);
						if(f != null){
							if(f.getType() != Material.AIR){
								//requested
								player.getInventory().addItem(f);
							}
						}
					}
				}
				exitTrade(player, c);
			}

	
	
	
	public void getSecondPlayerAndAccept(Player player){
		EXPListener explistener = this.plugin.getEXPListener();
		EconomyListener ecolistener = this.plugin.getEconomyListener();
		Player c = getOtherPlayerInTrade(player);
		if(accepted.contains(c)) {
			checkForDupe(player);
			checkForDupe(c);
			
			if(player.getItemOnCursor() != null){
				ItemStack playerItem = player.getItemOnCursor();
				ItemStack air = new ItemStack(Material.AIR);
				player.setItemOnCursor(air);
				player.getInventory().addItem(playerItem);
			}
			
			if(c.getItemOnCursor() != null){
				ItemStack bItem = c.getItemOnCursor();
				ItemStack air = new ItemStack(Material.AIR);
				c.setItemOnCursor(air);
				c.getInventory().addItem(bItem);
			}
			// requested is inv 1, requester is inv 2
			// requested uses LEFT SIDE, requester uses RIGHT SIDE
			// if b = null, means player is <Player>(requested) and c is <String> (requester)
			if(!rct.contains(player)){
				for (Integer a : leftside){
					int e = a.intValue();
					//gets items in requester's left side
					Inventory playerinv = invlist.get(player.getName());
					ItemStack f = playerinv.getItem(e);
					if(f != null){
						
						if(c.getInventory().firstEmpty() == -1){
							World world = c.getWorld();
							Location location = c.getLocation();
							world.dropItem(location, f);
							drops.add(c.getName());
						}
						if(f.getType() != Material.AIR){
							//requester
							c.getInventory().addItem(f);
						}
					}
				}
			} else {
				for (Integer a : rightside){
					int e = a.intValue();
					Inventory playerinv = invlist.get(player.getName());
					ItemStack f = playerinv.getItem(e);
					if(f != null){
						
						if(c.getInventory().firstEmpty() == -1){
							World world = c.getWorld();
							Location location = c.getLocation();
							world.dropItem(location, f);
							drops.add(c.getName());
						}
						if(f.getType() != Material.AIR){
							//requester
							c.getInventory().addItem(f);
						}
					}
				}
			}
			
			if(!rct.contains(player)){
				for (Integer a : rightside){
					int e = a.intValue();
					//gets items in requested's right side
					Inventory cinv = invlist.get(c.getName());
					ItemStack f = cinv.getItem(e);
					if(f != null){
						
						if(player.getInventory().firstEmpty() == -1){
							World world = player.getWorld();
							Location location = player.getLocation();
							world.dropItem(location, f);
							drops.add(player.getName());
						}
						if(f.getType() != Material.AIR){
							//requested
							player.getInventory().addItem(f);
						}
					}
				}
			} else {
				for (Integer a : leftside){
					int e = a.intValue();
					//gets items in requested's right side
					Inventory cinv = invlist.get(c.getName());
					ItemStack f = cinv.getItem(e);
					if(f != null){
						
						if(player.getInventory().firstEmpty() == -1){
							World world = player.getWorld();
							Location location = player.getLocation();
							world.dropItem(location, f);
							drops.add(player.getName());
						}
						if(f.getType() != Material.AIR){
							//requested
							player.getInventory().addItem(f);
						}
					}
				}
			}
			explistener.expAccept(player, c);
			ecolistener.economyAccept(player, c);
			acceptTrade(player, c);
			return;
		} else {
			if(!accepted.contains(player)){
				accepted.add(player);
				player.sendMessage(ChatPaginator.wordWrap(getReplacedMessage("ACCEPTED_TRADE", c), 21));
				c.sendMessage(ChatPaginator.wordWrap(getReplacedMessage("OTHER_PLAYER_ACCEPTED_TRADE", player), 21));
				if(!rct.contains(player)){
					if(requestedlist.contains(player)) {
						changeAcceptItem(tcmd.inventory2, player);
					}
					if(requesterlist.contains(player)) {
						changeAcceptItem(tcmd.inventory1, player);
					}
				} else {
					if(requestedlist.contains(player)) {
						changeAcceptItem(this.plugin.rightclicktrade.inv1, player);
					}
					if(requesterlist.contains(player)) {
						changeAcceptItem(this.plugin.rightclicktrade.inv2, player);
					}
				}
			} else {
				player.sendMessage(ChatPaginator.wordWrap(ChatColor.translateAlternateColorCodes('&', messageData.get("ALREADY_ACCEPTED_TRADE")), 21));
			}
		}
	}
		
	public Player getOtherPlayerInTrade(Player player){
		String key= null;
		Player value= player;
		Player c = null;
		for(Map.Entry entry: trades.entrySet()){
			if(value.equals(entry.getValue())){
				key = (String) entry.getKey();
				c = Bukkit.getServer().getPlayerExact(key);
				break;
			}
		}
		return c;
	}
	
	public void exitTrade(Player player,  Player c){
		removeFromLists(player, c);
		log.info("[QuickTrade] Trade ended between " + WordUtils.capitalize(player.getName()) + " and " + WordUtils.capitalize(c.getName()) + ".");
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', messageData.get("YOU_DECLINED_TRADE")));
		c.sendMessage(getReplacedMessage("OTHER_PLAYER_DECLINED_TRADE", player));
		player.closeInventory();
		c.closeInventory();
		player.updateInventory();
		c.updateInventory();
	}
	
	public void removeFromLists(Player player, Player c){
		if(trades.containsKey(player.getName()) || trades.containsKey(c.getName())){
			trades.remove(player.getName());
			trades.remove(c.getName());	
		}
		requestedlist.remove(player);
		requesterlist.remove(c);
		requestedlist.remove(c);
		requesterlist.remove(player);
		if(rct.contains(player)){
			rct.remove(player);
			rct.remove(c);
		}
		if(accepted.contains(player) || accepted.contains(c)){
			accepted.remove(c);
			accepted.remove(player);
		}
		if(rightclickrequests.containsKey(player.getName()) || rightclickrequests.containsKey(c.getName())){
			rightclickrequests.remove(player);
			rightclickrequests.remove(c.getName());
		}
		if(money.containsKey(player) || money.containsKey(c)){
			money.remove(player);
			money.remove(c);
		}
		timeoutcheck.remove(c);
		timeoutcheck.remove(player);
		playersintrades.remove(player);
		playersintrades.remove(c);
		startInventory.remove(player);
		startInventory.remove(c);
		drops.remove(player);
		drops.remove(c);
		invlist.remove(player.getName());
		invlist.remove(c.getName());
		exp.remove(player);
		exp.remove(c);
		invlist.remove(player.getName());
		invlist.remove(c.getName());
	}
	
	
	public void acceptTrade(Player player, Player c){
		removeFromLists(player, c);
		log.info("[QuickTrade] Trade successful between " + WordUtils.capitalize(player.getName()) + " and " + WordUtils.capitalize(c.getName()) + ".");
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', messageData.get("SUCCESSFUL_TRADE")));
		c.sendMessage(ChatColor.translateAlternateColorCodes('&', messageData.get("SUCCESSFUL_TRADE")));
		if(drops.contains(player)){
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', messageData.get("INVENTORY_FULL")));
			drops.remove(player);
		}
		if(drops.contains(c)){
			c.sendMessage(ChatColor.translateAlternateColorCodes('&', messageData.get("INVENTORY_FULL")));
			drops.remove(c);
		}
		player.closeInventory();
		c.closeInventory();
		player.updateInventory();
		c.updateInventory();
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event){
		Player player = event.getPlayer();
		if(requesterlist.contains(player) || requestedlist.contains(player)){
			if(!trades.containsKey(player.getName()) || !trades.containsValue(player)){
				String b = requests.get(player.getName());
				if(b == null){
					//null = player is requester, c is requested
					String key= null;
					String value= player.getName();
					Player c = null;
					for(Map.Entry entry: requests.entrySet()){
						if(value.equals(entry.getValue())){
							key = (String) entry.getKey();
							c = Bukkit.getServer().getPlayerExact(key);
							requests.remove(c.getName());
							requests.remove(player.getName());
							player.sendMessage(ChatColor.translateAlternateColorCodes('&', messageData.get("YOU_TRADE_DECLINED")));
							c.sendMessage(getReplacedMessage("TRADE_DECLINED", player));
							requestedlist.remove(c);
							requestedlist.remove(player);
							requesterlist.remove(player);
							requesterlist.remove(c);
							timeoutcheck.remove(c);
							timeoutcheck.remove(player);
							startInventory.remove(player);
							startInventory.remove(c);
							drops.remove(player);
							drops.remove(c);
							exp.remove(player);
							exp.remove(c);
							if(rightclickrequests.containsKey(player.getName())){
								rightclickrequests.remove(player.getName());
							} else {
								if(rightclickrequests.containsKey(c.getName())){
									rightclickrequests.remove(c.getName());
								}
							}
							break;
						}
					}
				} else {
					//!null = player is requested, d is requester
					Player d = Bukkit.getServer().getPlayerExact(b);
					requests.remove(player.getName());
					requests.remove(d.getName());
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', messageData.get("YOU_TRADE_DECLINED")));
					d.sendMessage(getReplacedMessage("TRADE_DECLINED", player));
					requestedlist.remove(player);
					requesterlist.remove(d);
					requestedlist.remove(d);
					requestedlist.remove(player);
					timeoutcheck.remove(d);
					timeoutcheck.remove(player);
					startInventory.remove(player);
					startInventory.remove(d);
					drops.remove(player);
					drops.remove(d);
					exp.remove(player);
					exp.remove(d);
					if(rightclickrequests.containsKey(player.getName())){
						rightclickrequests.remove(player.getName());
					} else {
						if(rightclickrequests.containsKey(d.getName())){
							rightclickrequests.remove(d.getName());
						}
					}
				}
			}
		}
	}
	
	@EventHandler
	public void stopPlayerChat(AsyncPlayerChatEvent event){
		if(this.plugin.noChat() == true){
			if(playersintrades != null){
				event.getRecipients().removeAll(playersintrades);
			}
			if(rct != null){
				event.getRecipients().removeAll(rct);
			}
		}
	}
	
	
	public void changeAcceptItem(Inventory inventory, Player player){
		ItemStack acceptitem = this.plugin.tradeitemstack.getAccept();
		ItemStack accepteditem = this.plugin.tradeitemstack.getAccepted();
		int acceptslot = -1;
		if(requestedlist.contains(player)) {
			acceptslot = 3;
		}
		if(requesterlist.contains(player)) {
			acceptslot = 5;
		}
		inventory.setItem(acceptslot, accepteditem);
	}
	
	public void changeAcceptItemBack(Inventory inventory, Player player){
		ItemStack acceptitem = this.plugin.tradeitemstack.getAccept();
		ItemStack accepteditem = this.plugin.tradeitemstack.getAccepted();
		int acceptslot = -1;
		if(requestedlist.contains(player)) {
			acceptslot = 3;
		}
		if(requesterlist.contains(player)) {
			acceptslot = 5;
		}
		inventory.setItem(acceptslot, acceptitem);
	}
	
	@EventHandler
	public void onPlayerPickupItem(PlayerPickupItemEvent event){
		Player player = event.getPlayer();
		if(playersintrades.contains(player)){
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPlayerDropItem(PlayerDropItemEvent event){
		Player player = event.getPlayer();
		if(playersintrades.contains(player)){
			ItemStack item = event.getItemDrop().getItemStack();
			event.getItemDrop().remove();
			ItemStack air = new ItemStack(Material.AIR);
			player.setItemOnCursor(air);
			player.getInventory().addItem(item);
			player.updateInventory();
		}
	}
	
	public void checkForDupe(Player player){
		Inventory inv = player.getInventory();
		for(ItemStack a : inv.getContents()){
			if(a != null && a.getType() != Material.AIR){
				if(a.getItemMeta().hasLore()){
					List<String> lore = a.getItemMeta().getLore();
					ItemStack eco = this.plugin.tradeitemstack.getEcoSeparator1();
					List<String> ecoLore = eco.getItemMeta().getLore();
					if(a.getItemMeta().hasDisplayName()){
						if(a.getItemMeta().getDisplayName().equals("Money in Trade")
								|| a.getItemMeta().getDisplayName().equals("Accept") 
								|| a.getItemMeta().getDisplayName().equals("Decline") 
								|| a.getItemMeta().getDisplayName().equals("Accepted") 
								|| a.getItemMeta().getDisplayName().equals("Separator")){
							inv.remove(a);
						}
					}
					if(lore.get(0).equals(ecoLore.get(0))){
						inv.remove(a);
					}
				}
			}
		}
	}
	
	private String getReplacedMessage(String string, Player player){
		Integer distance = this.plugin.getDistance();
		String a = messageData.get(string);
		if(a.contains("%player%")){
			a = a.replaceAll("%player%", player.getName());
		}
		if(a.contains("%distance%")){
			a = a.replaceAll("%distance%", distance.toString());
		}
		return ChatColor.translateAlternateColorCodes('&', a);
	}
}