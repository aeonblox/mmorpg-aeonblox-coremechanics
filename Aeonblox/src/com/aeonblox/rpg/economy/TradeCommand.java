package com.aeonblox.rpg.economy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.commons.lang.WordUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

public class TradeCommand implements CommandExecutor, InventoryHolder, Listener {
	
	private QuickTrade plugin;
	public Inventory inventory1, inventory2 = null;
	public static ArrayList<Player> requestedlist = new ArrayList<Player>();
	public static ArrayList<Player> requesterlist = new ArrayList<Player>();
	public static ArrayList<Player> timeoutcheck = new ArrayList<Player>();
	public static Map<String, String> requests = new HashMap<String, String>();
	public static Map<String, Player> trades = new HashMap<String, Player>();
	public static ArrayList<Player> playersintrades = new ArrayList<Player>();
	public static Map<String, ItemStack[]> startInventory = new HashMap<String, ItemStack[]>();
	public static HashMap<String, String> messageData = QuickTrade.messageData;
	public static Map<String, Inventory> invlist = new HashMap<String, Inventory>();
	private TradeItemStack tis = new TradeItemStack(plugin);
	private static final Logger log = Logger.getLogger("Minecraft");

	 public TradeCommand(QuickTrade plugin) {
		 this.plugin = plugin;
		 tis = plugin.getTradeItemStack();
		 plugin.plugin.getCommand("trade").setExecutor(this);
		 plugin.plugin.getServer().getPluginManager().registerEvents(this, plugin.plugin);
	}
	
	
	public Inventory getInventory() {
		return inventory1;
	}
	public Inventory getInventory2() {
		return inventory2;
		
	}
	
	@SuppressWarnings("deprecation")
	public boolean onCommand(final CommandSender sender, Command cmd, String label, final String[] args){
		TradeInventory tinv = this.plugin.getTradeInventory();
		int timeout = plugin.getTimeout();
		int tradedistancelimit = plugin.getTradeDistanceLimit();
		
			if(sender instanceof Player){
				 if(!sender.hasPermission("aeonblox.trade.command")){
					 sender.sendMessage(ChatColor.translateAlternateColorCodes('&', messageData.get("NO_PERMISSION_COMMAND")));
					 return true;
				 }
				 Player player1 = (Player) sender;
				 if(player1.getGameMode() == GameMode.CREATIVE){
					 if(plugin.allowCreative() == false){
						 if(!player1.hasPermission("aeonblox.creative.bypass")){
							 player1.sendMessage(ChatColor.translateAlternateColorCodes('&', messageData.get("NO_CREATIVE_TRADE")));
							 return true;
						 } 
					 }
				 }
				String inventoryname = this.plugin.getInventoryName();
				inventory1 = Bukkit.createInventory(this, plugin.getSize(), inventoryname);
				inventory2 = Bukkit.createInventory(this, plugin.getSize(), inventoryname);
			
				if(args.length == 0){
					Player player = (Player) sender;
			        player.sendMessage(ChatColor.GRAY + "-----------------------------------------------------");
			        player.sendMessage(ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "                  Aeonblox Trade System");
			        player.sendMessage(ChatColor.GREEN + "   /trade <player>");
			        player.sendMessage(ChatColor.GOLD + "" + ChatColor.ITALIC + "Request to trade with <player>");
			        player.sendMessage(ChatColor.GREEN + "   /trade accept");
			        player.sendMessage(ChatColor.GOLD + "" + ChatColor.ITALIC + "Accept a trade request");
			        player.sendMessage(ChatColor.GREEN + "   /trade decline");
			        player.sendMessage(ChatColor.GOLD + "" + ChatColor.ITALIC + "Refuse a trade request");
			        player.sendMessage(ChatColor.GREEN + "   /trade toggle");
			        player.sendMessage(ChatColor.GOLD + "" + ChatColor.ITALIC + "Disable the sending of shift-right-click trading requests for you ");     
			        player.sendMessage(ChatColor.GRAY + "-----------------------------------------------------");
					return true;
				}
				
				if(args.length == 1){
					
					if(args[0].equalsIgnoreCase("eco")){
						Player player = (Player) sender;
						ItemStack eco = this.plugin.tradeitemstack.getEcoSeparator1();
						player.getInventory().addItem(eco);
						return true;
					}
					
					if(args[0].equalsIgnoreCase("toggle")){
						Player player = (Player) sender;
						if(!player.hasPermission("aeonblox.toggle")){
							 sender.sendMessage(ChatColor.translateAlternateColorCodes('&', messageData.get("NO_PERMISSION_COMMAND")));
							 return true;
						}
						if(!this.plugin.playersStorage.contains(player.getName())){
							player.sendMessage(ChatColor.translateAlternateColorCodes('&', messageData.get("DISABLE_SHIFT_CLICK")));
							this.plugin.playersStorage.add(player.getName());
							return true;
						} else {
							player.sendMessage(ChatColor.translateAlternateColorCodes('&', messageData.get("ENABLE_SHIFT_CLICK")));
							this.plugin.playersStorage.remove(player.getName());
							return true;
						}
					}
					
					if(args[0].equalsIgnoreCase("accept")){
						if(requests.get(sender.getName()) == null){
							sender.sendMessage(ChatColor.translateAlternateColorCodes('&', messageData.get("NOT_REQUESTED")));
							return true;
						} else {
							String requestername = requests.get(sender.getName());
							Player requested = (Player) sender;
							Player requester = plugin.plugin.getServer().getPlayerExact(requestername);
							requested.sendMessage(requester.getName());
							if(playersintrades.contains(requester)){
								requested.sendMessage(getReplacedMessage("ALREADY_IN_TRADE", requester));
								requests.remove(requested);
								requestedlist.remove(requested);
								requesterlist.remove(requester);
								timeoutcheck.remove(requested);
								timeoutcheck.remove(requester);
								requests.remove(requested.getName());
								return true;
							}
							log.info("[QuickTrade] Trade started between " + WordUtils.capitalize(requester.getName()) + " and " + WordUtils.capitalize(requested.getName()) + ".");
							requested.sendMessage(getReplacedMessage("TRADE_INITIATED", requester));
							requester.sendMessage(getReplacedMessage("TRADE_INITIATED", requested));
							trades.put(requestername, requested);
							timeoutcheck.remove(requested);
							timeoutcheck.remove(requester);
							requests.remove(requested.getName());
							playersintrades.add(requester);
							playersintrades.add(requested);
							startInventory.put(requester.getName(), requester.getInventory().getContents());
							startInventory.put(requested.getName(), requested.getInventory().getContents());
							requested.openInventory(inventory1);	
							tinv.fillInventory1(inventory1, requester);
							requested.updateInventory();
							requester.openInventory(inventory2);	
							tinv.fillInventory2(inventory2, requested);
							requested.updateInventory();
							invlist.put(requested.getName(), inventory1);
							invlist.put(requester.getName(), inventory2);
							return true;
						}
					}
					
					if(args[0].equalsIgnoreCase("decline")){
						if(requests.get(sender.getName()) == null){
							sender.sendMessage(ChatColor.translateAlternateColorCodes('&', messageData.get("NOT_REQUESTED")));
							return true;
						} else {
							Player requested = (Player) sender;
							String requestername = requests.get(sender.getName());
							Player requester = plugin.plugin.getServer().getPlayer(requestername);
							requested.sendMessage(ChatColor.translateAlternateColorCodes('&', messageData.get("YOU_TRADE_DECLINED")));
							requester.sendMessage(getReplacedMessage("TRADE_DECLINED", requested));
							requestedlist.remove(requested);
							requesterlist.remove(requester);
							timeoutcheck.remove(requested);
							timeoutcheck.remove(requester);
							requests.remove(requested.getName());
							return true;
						}
					}
					
					if (args[0].equalsIgnoreCase("help") ){
						Player player = (Player) sender;
				        player.sendMessage(ChatColor.GRAY + "-----------------------------------------------------");
				        player.sendMessage(ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "                  Aeonblox Trade System");
				        player.sendMessage(ChatColor.GREEN + "   /trade <player>");
				        player.sendMessage(ChatColor.GOLD + "" + ChatColor.ITALIC + "Request to trade with <player>");
				        player.sendMessage(ChatColor.GREEN + "   /trade accept");
				        player.sendMessage(ChatColor.GOLD + "" + ChatColor.ITALIC + "Accept a trade request");
				        player.sendMessage(ChatColor.GREEN + "   /trade decline");
				        player.sendMessage(ChatColor.GOLD + "" + ChatColor.ITALIC + "Refuse a trade request");
				        player.sendMessage(ChatColor.GREEN + "   /trade toggle");
				        player.sendMessage(ChatColor.GOLD + "" + ChatColor.ITALIC + "Disable the sending of shift-right-click trading requests for you ");     
				        player.sendMessage(ChatColor.GRAY + "-----------------------------------------------------");
						return true;
					}
					Player player = (Player) sender;
					if(args[0].equalsIgnoreCase("reload")){
						if(player.hasPermission("aeonblox.admin")){
							player.sendMessage(ChatColor.GREEN + "Configuration and messages successfully reloaded");
							log.info("[QuickTrade] Configuration reloaded by " + sender.getName());
							plugin.plugin.cfgHandler.reloadCustomConfig(plugin.plugin.tradeConfig);;
						    this.plugin.reloadMessages();
							return true;
						} else {
							player.sendMessage(ChatColor.translateAlternateColorCodes('&', messageData.get("NO_PERMISSION_COMMAND")));
							return true;
						}
					}
					
					Player requested = plugin.plugin.getServer().getPlayer(args[0]);
					
					if(requested == null){
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', (messageData.get("PLAYER_NOT_FOUND")).replace("%player%", args[0])));
					} else {
						if(requested.getName() == player.getName()){
							player.sendMessage(ChatColor.translateAlternateColorCodes('&', messageData.get("NO_SELF_TRADE")));
							return true;
						}
						if(!requested.hasPermission("aeonblox.trade.command")){
							player.sendMessage(getReplacedMessage("OTHER_PLAYER_NO_PERMISSION_COMMAND", requested));
							return true;
						}
						
						if(this.plugin.getTradeDistanceLimit() != 0 && player.getWorld() != requested.getWorld()){
							player.sendMessage(ChatColor.translateAlternateColorCodes('&', (messageData.get("NOT_SAME_WORLD"))));
							return true;
						}
						
						 if(requested.getGameMode() == GameMode.CREATIVE){
							 if(plugin.allowCreative() == false){
								 if(!requested.hasPermission("aeonblox.creative.bypass")){
									 player.sendMessage(getReplacedMessage("OTHER_PLAYER_NO_CREATIVE_TRADE", requested));
									 return true;
								 } 
							 }
						 }
						
						Location location1 = player.getLocation();
						Location location2 = requested.getLocation();
						if(tradedistancelimit != 0){
							if(location1.distance(location2) > tradedistancelimit || location2.distance(location1) > tradedistancelimit){
							player.sendMessage(getReplacedMessage("TOO_FAR", requested));
							return true;
							}
						}
						
						
						if(!requestedlist.contains(requested) || requestedlist == null){
							requesterlist.add(player);
							requestedlist.add(requested);
							requests.put(requested.getName(), player.getName());
							timeoutcheck.add(player);
							timeoutcheck.add(requested);
							player.sendMessage(getReplacedMessage("SENT_REQUEST", requested));
							requested.sendMessage(getReplacedMessage("RECEIVED_REQUEST", player));
							requested.sendMessage(getReplacedMessage("RECEIVED_REQUEST_P2", player));
						} else {
							
							if(trades.containsKey(player.getName())){
								player.sendMessage(getReplacedMessage("ALREADY_IN_TRADE", requested));
								return true;
							} else {
							player.sendMessage(getReplacedMessage("ALREADY_REQUESTED", requested));
							return true;
							}
						}
						Bukkit.getServer().getScheduler().runTaskLater(plugin.plugin, new Runnable() {
					      @Override
					      public void run()
					      {
							Player requester = (Player) sender;
							Player requested = plugin.plugin.getServer().getPlayer(args[0]);
					    	  if(timeoutcheck.contains(requester)){
					    		  timeoutcheck.remove(requester);
					    		  timeoutcheck.remove(requested);
					    		  requester.sendMessage(getReplacedMessage("NO_REPLY", requested));
					    		  requested.sendMessage(getReplacedMessage("YOU_NO_REPLY", requester));
					    		  requesterlist.remove(requester);
					    		  requestedlist.remove(requested);
					    		  requests.remove(requested.getName());
					    	  }
					      }
					    }, timeout);
					}
				}
			}
	return true;
}
	
	private String getReplacedMessage(String string, Player player){
		Integer distance = this.plugin.getDistance();
		String a = messageData.get(string);
		if(a.contains("%player%")){
			a = a.replaceAll("%player%", player.getName());
		}
		if(a.contains("%distance%")){
			a = a.replaceAll("%distance%", distance.toString());
		}
		return ChatColor.translateAlternateColorCodes('&', a);
	}

}
