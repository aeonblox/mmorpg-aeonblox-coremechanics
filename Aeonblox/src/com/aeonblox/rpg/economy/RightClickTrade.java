package com.aeonblox.rpg.economy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.commons.lang.WordUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

public class RightClickTrade implements Listener, InventoryHolder{
	
	private QuickTrade plugin;
	public Inventory inv1, inv2 = null;
	public static ArrayList<Player> requestedlist = TradeCommand.requestedlist;
	public static ArrayList<Player> requesterlist = TradeCommand.requesterlist;
	public static ArrayList<Player> timeoutcheck = TradeCommand.timeoutcheck;
	public static Map<String, Player> trades = TradeCommand.trades;
	private TradeItemStack tis = new TradeItemStack(plugin);
	public static ArrayList<Player> rct = new ArrayList<Player>();
	public static Map<String, Player> rightclickrequests = new HashMap<String, Player>();
	public static HashMap<String, String> messageData = QuickTrade.messageData;
	public static ArrayList<Player> playersintrades = TradeCommand.playersintrades;
	public static Map<String, ItemStack[]> startInventory = TradeCommand.startInventory;
	public static Map<String, String> requests = TradeCommand.requests;
	public static Map<String, Inventory> invlist = TradeCommand.invlist;
	private static final Logger log = Logger.getLogger("Minecraft");
	
	
	 public RightClickTrade(QuickTrade plugin) {
		 this.plugin = plugin;
		 plugin.plugin.getServer().getPluginManager().registerEvents(this, plugin.plugin);
	}
	
	
	
	@SuppressWarnings("deprecation")
	@EventHandler
	 public void onPlayerRightClick(PlayerInteractEntityEvent event){
		TradeInventory tinv = this.plugin.getTradeInventory();
		final Player requester = event.getPlayer();
		String inventoryname = this.plugin.getInventoryName();
		this.inv1 = Bukkit.createInventory(this, this.plugin.getSize(), inventoryname);
		this.inv2 = Bukkit.createInventory(this, this.plugin.getSize(), inventoryname);
		int timeout = this.plugin.getTimeout();
		 
		if(this.plugin.allowShiftRightClick() == false){
			return;
		}
		
		 if(requester.getGameMode() == GameMode.CREATIVE){
			 if(plugin.allowCreative() == false){
				 if(!requester.hasPermission("aeonblox.creative.bypass")){
					 requester.sendMessage(ChatColor.translateAlternateColorCodes('&', messageData.get("NO_CREATIVE_TRADE")));
					 return;
				 } 
			 }
		 }
		 
		if(this.plugin.playersStorage.contains(requester.getName())){
			return;
		}else{
			
		}
		 
		 if(event.getRightClicked() instanceof Player){
			 final Player requested = (Player) event.getRightClicked();
			 
			 if(!Arrays.asList(this.plugin.plugin.getServer().getOnlinePlayers()).contains(requested)){
				 return;
			 }
			 
			 if(!requester.hasPermission("aeonblox.trade.rightclick")){
				 requester.sendMessage(ChatColor.translateAlternateColorCodes('&', messageData.get("NO_PERMISSION_SHIFT_CLICK")));
				 return;
			 } else {
				 if(!requested.hasPermission("aeonblox.trade.rightclick")){
					 requester.sendMessage(getReplacedMessage("OTHER_PLAYER_NO_PERMISSION_SHIFT_CLICK", requested));
					 return;
				 }
			 }
			 
			 if(requested.getGameMode() == GameMode.CREATIVE){
				 if(plugin.allowCreative() == false){
					 if(!requested.hasPermission("aeonblox.creative.bypass")){
						 requester.sendMessage(getReplacedMessage("OTHER_PLAYER_NO_CREATIVE_TRADE", requested));
						 return ;
					 } 
				 }
			 }
			 
			 if(event.getPlayer().isSneaking()){
				 if(rightclickrequests != null || !rightclickrequests.containsKey(requester.getName()) || !rightclickrequests.containsValue(requester)){
					 if(!requesterlist.contains(requested)){
						 if(!requestedlist.contains(requested) || requestedlist == null){
							 requests.put(requested.getName(), requester.getName());
							 requesterlist.add(requester);
							 requestedlist.add(requested);
							 timeoutcheck.add(requester);
							 timeoutcheck.add(requested);
							 rightclickrequests.put(requester.getName(), requested);
								requester.sendMessage(getReplacedMessage("SENT_REQUEST", requested));
								requested.sendMessage(getReplacedMessage("RECEIVED_REQUEST", requester));
								requested.sendMessage(getReplacedMessage("SHIFT_CLICK_RECEIVED_REQUEST", requester));
						 } else {
									
							 if(trades.containsKey(requester.getName())){
								 requester.sendMessage(getReplacedMessage("ALREADY_IN_TRADE", requested));
									return;
							 } else {
								 requester.sendMessage(getReplacedMessage("ALREADY_REQUESTED", requested));
									return;
							 }
						 }
						 Bukkit.getServer().getScheduler().runTaskLater(plugin.plugin, new Runnable() {
							 @Override
							 public void run()
							 {
								 if(timeoutcheck.contains(requester)){
									 timeoutcheck.remove(requester);
									 timeoutcheck.remove(requested);
									 requester.sendMessage(getReplacedMessage("NO_REPLY", requested));
									 requested.sendMessage(getReplacedMessage("YOU_NO_REPLY", requester));
									 requesterlist.remove(requester);
									 requestedlist.remove(requested);
								 }
							 }
						 }, timeout);
						 
					 } else {
						 if(requesterlist.contains(requested)){
							 	requests.remove(requested.getName());
								startInventory.put(requester.getName(), requester.getInventory().getContents());
								startInventory.put(requested.getName(), requested.getInventory().getContents());
								String requestedname = requested.getName();
								log.info("[QuickTrade] Trade started between " + WordUtils.capitalize(requester.getName()) + " and " + WordUtils.capitalize(requested.getName()) + ".");
								requested.sendMessage(getReplacedMessage("TRADE_INITIATED", requester));
								requester.sendMessage(getReplacedMessage("TRADE_INITIATED", requested));
								trades.put(requestedname, requester);
								timeoutcheck.remove(requested);
								timeoutcheck.remove(requester);
								playersintrades.add(requester);
								playersintrades.add(requested);
								rct.add(requester);
								rct.add(requested);
								requested.openInventory(inv1);	
								tinv.fillInventory1(inv1, requester);
								requested.updateInventory();
								requester.openInventory(inv2);	
								tinv.fillInventory2(inv2, requested);
								requester.updateInventory();
								invlist.put(requested.getName(), inv1);
								invlist.put(requester.getName(), inv2);
						 }
				 }
			 }
		 }
	 }
}
	 
	 @SuppressWarnings("rawtypes")
	@EventHandler
	 public void onPlayerMoveEvent(PlayerMoveEvent event){
		 int distance = this.plugin.getDistance();
		 Player player = event.getPlayer();
		 Location location1 = player.getLocation();
		 
		 if(rightclickrequests.containsKey(player) || rightclickrequests.containsValue(player)){
			 Player player1 = rightclickrequests.get(player.getName());
			// if b = null, means player is requested and c is <String> (requester)
			 if(player1 == null){
				 String key= null;
				 Player value= player;
				 for(Map.Entry entry: rightclickrequests.entrySet()){
					 
					 if(value.equals(entry.getValue())){
						 key = (String) entry.getKey();
						 Player c = Bukkit.getServer().getPlayerExact(key);
						 
						 if(player.getWorld() != c.getWorld()){
							 player.sendMessage(ChatColor.translateAlternateColorCodes('&', messageData.get("TOO_FAR_SHIFT_CLICK")));
							 c.sendMessage(ChatColor.translateAlternateColorCodes('&', messageData.get("TOO_FAR_SHIFT_CLICK")));
							 requestedlist.remove(player);
							 requesterlist.remove(c);
							 timeoutcheck.remove(player);
							 timeoutcheck.remove(c);
							 rightclickrequests.remove(key);
							 rightclickrequests.remove(player);
							 return;
						 }
						 
						 Location location2 = c.getLocation();
						 
						 if(location1.distance(location2) > distance || location2.distance(location1) > distance){
							 player.sendMessage(ChatColor.translateAlternateColorCodes('&', messageData.get("TOO_FAR_SHIFT_CLICK")));
							 c.sendMessage(ChatColor.translateAlternateColorCodes('&', messageData.get("TOO_FAR_SHIFT_CLICK")));
							 requestedlist.remove(player);
							 requesterlist.remove(c);
							 timeoutcheck.remove(player);
							 timeoutcheck.remove(c);
							 rightclickrequests.remove(key);
							 rightclickrequests.remove(player);
							 return;
						 }
						 break;
					 }
				 }
			 } else {
				 if(player.getWorld() != player1.getWorld()){
					 player.sendMessage(ChatColor.translateAlternateColorCodes('&', messageData.get("TOO_FAR_SHIFT_CLICK")));
					 player1.sendMessage(ChatColor.translateAlternateColorCodes('&', messageData.get("TOO_FAR_SHIFT_CLICK")));
					 requestedlist.remove(player1);
					 requesterlist.remove(player);
					 timeoutcheck.remove(player);
					 timeoutcheck.remove(player1);
					 rightclickrequests.remove(player.getName());
					 rightclickrequests.remove(player1);
					 return;
				 }
				 
					// if b != null, that means player is <String>(requester) and player1 is <Player>(requested)
				 Location location2 = player1.getLocation();
				 if(location2.distance(location1) > distance || location1.distance(location2) > distance){
					 player.sendMessage(ChatColor.translateAlternateColorCodes('&', messageData.get("TOO_FAR_SHIFT_CLICK")));
					 player1.sendMessage(ChatColor.translateAlternateColorCodes('&', messageData.get("TOO_FAR_SHIFT_CLICK")));
					 requestedlist.remove(player1);
					 requesterlist.remove(player);
					 timeoutcheck.remove(player);
					 timeoutcheck.remove(player1);
					 rightclickrequests.remove(player.getName());
					 rightclickrequests.remove(player1);
					 return;
				 }
			 }
		 }
	 }
	 
		private String getReplacedMessage(String string, Player player){
			Integer distance = this.plugin.getDistance();
			String a = messageData.get(string);
			if(a.contains("%player%")){
				a = a.replaceAll("%player%", player.getName());
			}
			if(a.contains("%distance%")){
				a = a.replaceAll("%distance%", distance.toString());
			}
			return ChatColor.translateAlternateColorCodes('&', a);
		}

	public Inventory getInventory() {
		return inv1;
	}
	
	public Inventory getInventory2() {
		return inv2;
	}
}
