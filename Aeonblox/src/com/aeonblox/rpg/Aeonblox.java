/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aeonblox.rpg;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitScheduler;

import com.RPGMakerDev.RPGMaker.Commands.CommandGuild;
import com.RPGMakerDev.RPGMaker.Commands.CommandRPGMaker;
import com.RPGMakerDev.RPGMaker.Commands.help;
import com.RPGMakerDev.RPGMaker.Commands.item;
import com.RPGMakerDev.RPGMaker.Commands.socialManager;
import com.RPGMakerDev.RPGMaker.EntityData.EntityDatas;
import com.RPGMakerDev.RPGMaker.EntityData.RPGEntity;
import com.RPGMakerDev.RPGMaker.Events.RPGPlayerJoinServer;
import com.RPGMakerDev.RPGMaker.Social.Channel;
import com.RPGMakerDev.RPGMaker.Social.SocialManager;
import com.RPGMakerDev.RPGMaker.Social.SocialPlayer;
import com.RPGMakerDev.RPGMaker.StoredData.Database;
import com.aeonblox.RaceMechanics.Main;
import com.aeonblox.rpg.combat.Enchanting;
import com.aeonblox.rpg.combat.HealPotions;
import com.aeonblox.rpg.commands.AeonEXPCommands;
import com.aeonblox.rpg.commands.Bank;
import com.aeonblox.rpg.commands.InventoryArray;
import com.aeonblox.rpg.commands.RandomTPMechanics;
import com.aeonblox.rpg.commands.Reboot;
import com.aeonblox.rpg.commands.Roll;
import com.aeonblox.rpg.commands.Suicide;
import com.aeonblox.rpg.commands.Sync;
import com.aeonblox.rpg.commands.ToggleTrails;
import com.aeonblox.rpg.economy.EXPListener;
import com.aeonblox.rpg.economy.EconomyListener;
import com.aeonblox.rpg.economy.PlayerUtil;
import com.aeonblox.rpg.economy.QuickTrade;
import com.aeonblox.rpg.economy.RightClickTrade;
import com.aeonblox.rpg.economy.TradeCommand;
import com.aeonblox.rpg.economy.TradeInventory;
import com.aeonblox.rpg.economy.TradeItemStack;
import com.aeonblox.rpg.economy.TradeListener;
import com.aeonblox.rpg.entitydata.InteractionListener;
import com.aeonblox.rpg.entitydata.MobDrops;
import com.aeonblox.rpg.entitydata.Mobs;
import com.aeonblox.rpg.entitydata.NPC;
import com.aeonblox.rpg.events.AeonEXP;
import com.aeonblox.rpg.events.Blocker;
import com.aeonblox.rpg.events.BookClicks;
import com.aeonblox.rpg.events.BookDrop;
import com.aeonblox.rpg.events.BookEvents;
import com.aeonblox.rpg.events.BookJoin;
import com.aeonblox.rpg.events.BookMechanics;
import com.aeonblox.rpg.events.BowPush;
import com.aeonblox.rpg.events.Fishing;
import com.aeonblox.rpg.events.Listeners;
import com.aeonblox.rpg.events.Mining;
import com.aeonblox.rpg.events.NoEntityGrief;
import com.aeonblox.rpg.events.Orbs;
import com.aeonblox.rpg.events.PlayerJoinMechanics;
import com.aeonblox.rpg.events.Respawn;
import com.aeonblox.rpg.events.Runner;
import com.aeonblox.rpg.events.TPListener;
import com.aeonblox.rpg.events.TrailClickEvent;
import com.aeonblox.rpg.events.Untradeable;
import com.aeonblox.rpg.events.WhitelistMessage;
import com.aeonblox.rpg.events.WhitelistPlayerListener;
import com.aeonblox.rpg.social.ChatAlert;
import com.aeonblox.rpg.social.ChatListener;
import com.aeonblox.rpg.social.PartyManager;
import com.aeonblox.rpg.social.PartySystem;
import com.aeonblox.rpg.spellsystem.Magic;
import com.aeonblox.rpg.spellsystem.StaffMechanics;
import com.aeonblox.rpg.utils.Util_WorldGuard;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;

/**
 *
 * @author Zach S.
 * @author Jona D.
 * @author Adam Canfield
 * @author Ryan
 * @docsince 1.9.0-SNAPSHOT
 * 
 */
@SuppressWarnings("static-access")
public class Aeonblox extends JavaPlugin implements Listener {

	private static boolean debugMode = true;
	public static String serverName;
	public static String serverDescription;
	public Aeonblox plugin;

	// MySQL Database Information
	public static String hostname;
	public static String username;
	public static String password;
	public static String port;
	public static String schema;
	@SuppressWarnings("unused")
	private static boolean mysqlEnabled = false;

	// Configuration Information
	public ConfigHandler cfgHandler;
	public Config sprintConfig, chatConfig, randomConfig, expConfig, trailConfig, whitelistConfig, entitygriefConfig, tradeConfig;
	public Integer foodsubtractfortick;
	public Integer foodsubtractickvalue;
	public Integer foodaddfortick;
	public Integer foodaddtickvalue;
	public Integer foodmaxtickvalue;

	// Representation of Classes
	public PartyManager partyManager;
	public ChatAlert chatalert;
	public Reboot reboot;
	public Runner hpbar;
	public RandomTPMechanics randomtp;
	public PlayerJoinMechanics playerjoinmech;
	public StaffMechanics staffmech;
	public Util_WorldGuard util_WorldGuard;
	public Sync sync;
	public Bank bankrep;
	public ToggleTrails trail;
	public Suicide suic;
	public Roll rollc;
	public Respawn respawn;
	public Magic magic;
	public BookMechanics bookmech;
	public AeonEXP aeonexp;
	public BookClicks bookclick;
	public AeonEXPCommands expcommands;
	public TrailClickEvent trailclickevent;
	public InventoryArray togglebook;
	public Blocker block;
	public BowPush bowpush;
	public Mining mining;
	public Untradeable untradeable;
	public Fishing fishing;
	public Orbs orbs;
	public Enchanting enchanting;
	public Listeners listeners;
	public Mobs mobs;
	public MobDrops mobdrops;
	public NPC npcs;
	public WhitelistMessage whitelistmsg;
	public NoEntityGrief noentitygrief;
	public TradeCommand tradecommand;
	public TradeListener tradelistener;
	public TradeInventory tradeinventory;
	public TradeItemStack tradeitemstack;
	public PlayerUtil playersStorage;
	public RightClickTrade rightclicktrade;
	public EconomyListener economylistener;
	public EXPListener explistener;
	public QuickTrade quicktrade;
	public Channel channel;
	public SocialManager socialmanager;

	public static Permission permission = null;
	public static Economy econ = null;
	public static Chat chat = null;

	// Arraylists
	public static List<String> suicide = new ArrayList<String>();
	public static List<String> toggletrail = new ArrayList<String>();
	public static List<String> tagged = new ArrayList<String>();
	public static List<String> bank = new ArrayList<String>();
	public static List<String> character = new ArrayList<String>();

	public static List<String> teleporting = new ArrayList<String>();
	public static Inventory Itrail = Bukkit.createInventory(null, 18, "Trails");

	public int timesRan = 4;

	public ArrayList<Fireball> fballs = new ArrayList<Fireball>();
	public ArrayList<Snowball> balls = new ArrayList<Snowball>();
	public ArrayList<Arrow> arrow = new ArrayList<Arrow>();
	public ArrayList<EnderPearl> enderpearl = new ArrayList<EnderPearl>();

	public ArrayList<String> cooldown = new ArrayList<String>();
	public static List<String> invarray = new ArrayList<String>();

	// other plugins main classes
	public static Main RMMain;

	// REMOVE SCOREBOARD player.setScoreboard(manager.getNewScoreboard());

	@Override
	public void onDisable() {
		Bukkit.broadcastMessage(ChatColor.DARK_GRAY + ">>" + ChatColor.AQUA + "Aeonblox" + ChatColor.DARK_GRAY + ">> The system is reloading, please relog upon completion.");
		// CustomEntity.unregisterEntities();
		cfgHandler.saveCustomConfig(whitelistConfig);
		this.playersStorage.save();
	}

	@Override
	public void onEnable() {
		/**
		 * 
		 * @Aeonblox
		 * 
		 */
		new BukkitRunnable() {
			public void run() {
				Aeonblox.this.reboot();

			}
		}.runTaskLater(this, 144000L);
		File configFile = new File(getDataFolder() + "/config.yml");
		if (!configFile.exists()) {
			saveDefaultConfig();
		}

		getLogger().info(ChatColor.RED + " >> >> Scheduled Reload Activated Successfully]");
		getLogger().info(ChatColor.RED + " >> >> ObjectiveMechanics Activated Successfully]");
		getLogger().info(ChatColor.RED + " >> >> PlayerJoinMechanics Activated Successfully]");
		getLogger().info(ChatColor.RED + " >> >> SprintMechanics Activated Successfully]");
		getLogger().info(ChatColor.RED + " >> >> HealthMechanics Activated Successfully]");
		getLogger().info(ChatColor.RED + " >> >> PartyMechanics Activated Successfully]");
		getLogger().info(ChatColor.RED + " >> >> ChatPingMechanics Activated Successfully]");
		getLogger().info(ChatColor.RED + " >> >> ChatMechanics Activated Successfully]");
		getLogger().info(ChatColor.RED + " >> >> SocialMechanics Activated Successfully]");
		getLogger().info(ChatColor.RED + " >> >> MagicMechanics Activated Successfully]");
		getLogger().info(ChatColor.RED + " >> >> StaffMechanics Activated Successfully]");
		getLogger().info(ChatColor.RED + " >> >> AeonEXPMechanics Activated Successfully]");
		getLogger().info(ChatColor.RED + " >> >> BookMechanics Activated Successfully]");
		getLogger().info(ChatColor.RED + " >> >> TrailMechanics Activated Successfully]");
		getLogger().info(ChatColor.RED + " >> >> DatabaseMechanics Activated Successfully]");
		getLogger().info(ChatColor.RED + " >> >> TeleportationMechanics Activated Successfully]");
		getLogger().info(ChatColor.RED + " >> >> RandomTPMechanics Activated Successfully]");
		getLogger().info(ChatColor.RED + " >> >> TradeMechanics Activated Successfully]");
		getLogger().info(ChatColor.RED + " >> >> CombatMechanics Activated Successfully]");
		getLogger().info(ChatColor.RED + " >> >> HealMechanics Activated Successfully]");
		getLogger().info(ChatColor.RED + " >> >> BlockerMechanics Activated Successfully]");
		getLogger().info(ChatColor.RED + " >> >> HorseMechanics Activated Successfully]");
		getLogger().info(ChatColor.RED + " >> >> FishingMechanics Activated Successfully]");
		getLogger().info(ChatColor.RED + " >> >> BowPushMechanics Activated Successfully]");
		getLogger().info(ChatColor.RED + " >> >> OrbMechanics Activated Successfully]");
		getLogger().info(ChatColor.RED + " >> >> RespawnMechanics Activated Successfully]");
		getLogger().info(ChatColor.RED + " >> >> TeleportMechanics Activated Successfully]");
		getLogger().info(ChatColor.RED + " >> >> WhitelistMechanics Activated Successfully]");
		getLogger().info(ChatColor.RED + " >> >> NoEntityGrief Activated Successfully]");
		getLogger().info(ChatColor.RED + " >> >> NPCMechanics Activated Successfully]");
		getLogger().info(ChatColor.RED + " >> >> MountMechanics Activated Successfully]");
		getLogger().info(ChatColor.RED + " >> >> EconomyMechanics Activated Successfully]");
		getLogger().info(ChatColor.RED + " >> >> TradeMechanics Activated Successfully]");
		getLogger().info(ChatColor.RED + " >> >> ExpMechanics Activated Successfully]");
		getLogger().info(ChatColor.RED + " >> >> EnchantingMechanics Activated Successfully]");

		Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
			public void run() {
				for (Player p : Bukkit.getServer().getOnlinePlayers()) {
					if ((!Aeonblox.tagged.contains(p.getName())) && (!p.isDead())) {
						PlayerInventory i = p.getInventory();
						double amt = 5.0D;
						if ((i.getHelmet() != null) && (i.getHelmet().getItemMeta().hasLore())) {
							double added = Aeonblox.getHpgenFromLore(i.getHelmet(), "HP REGEN");
							amt += added;
						}
						if ((i.getChestplate() != null) && (i.getChestplate().getItemMeta().hasLore())) {
							double added = Aeonblox.getHpgenFromLore(i.getChestplate(), "HP REGEN");
							amt += added;
						}
						if ((i.getLeggings() != null) && (i.getLeggings().getItemMeta().hasLore())) {
							double added = Aeonblox.getHpgenFromLore(i.getLeggings(), "HP REGEN");
							amt += added;
						}
						if ((i.getBoots() != null) && (i.getBoots().getItemMeta().hasLore())) {
							double added = Aeonblox.getHpgenFromLore(i.getBoots(), "HP REGEN");
							amt += added;
						}
						Damageable d = (Player) p;
						if (d.getHealth() + amt > d.getMaxHealth()) {
							p.setHealth(d.getMaxHealth());
						} else {
							p.setHealth(d.getHealth() + amt);
						}
					}
				}
			}
		}, 1L, 20L);

		/** **/
		plugin = this;
		/** **/

		/**
		 * 
		 * 
		 * @RPGMaker
		 * 
		 * 
		 */
		// CustomEntity.registerEntities();
		Bukkit.broadcastMessage(ChatColor.DARK_GRAY + ">>" + ChatColor.AQUA + "Aeonblox" + ChatColor.DARK_GRAY + ">> Reload completed.  To avoid any complications, please relog.");
		this.getConfig();
		serverName = this.getConfig().getString("Name");
		serverDescription = this.getConfig().getString("Description");
		hostname = this.getConfig().getString("Host");
		username = this.getConfig().getString("User");
		password = this.getConfig().getString("Password");
		port = this.getConfig().getString("Port");
		schema = this.getConfig().getString("Schema");
		mysqlEnabled = this.getConfig().getBoolean("mysql-enabled");
		debugMode = this.getConfig().getBoolean("debug-enabled");
		this.saveConfig();
		this.getServer().getPluginManager().registerEvents(new SocialManager(), this);
		this.getServer().getPluginManager().registerEvents(new RPGPlayerJoinServer(), this);
		this.getCommand("rpgmaker").setExecutor(new CommandRPGMaker());
		this.getCommand("socialmanager").setExecutor(new socialManager());
		this.getCommand("guild").setExecutor(new CommandGuild());
		this.getCommand("help").setExecutor(new help());
		this.getCommand("item").setExecutor(new item());

		try {
			Database getEntityData = new Database();
			String Query = "SELECT * FROM `creatures`";
			getEntityData.getConnection();
			getEntityData.Query = getEntityData.connection.prepareStatement(Query);
			getEntityData.Results = getEntityData.Query.executeQuery();
			while (getEntityData.Results.next()) {
				EntityDatas datas = new EntityDatas();
				datas.id = getEntityData.Results.getInt("ID");
				datas.name = getEntityData.Results.getString("NAME");
				datas.damage = getEntityData.Results.getInt("DAMAGE");
				datas.moveSpeed = getEntityData.Results.getInt("MOVESPEED");
				datas.entityType = getEntityData.Results.getString("ENTITYTYPE");
				EntityDatas.entityData.put(getEntityData.Results.getInt("ID"), datas);
			}
		} catch (SQLException exception) {
			exception.printStackTrace();
		} finally {

			for (Player p : Bukkit.getOnlinePlayers()) {
				SocialPlayer New = new SocialPlayer(p.getUniqueId());
				SocialManager.Global.joinChannel(New);
				SocialPlayer.socialPlayers.put(p.getUniqueId(), New);

				RPGEntity.players.put(p.getUniqueId(), new RPGEntity(p.getUniqueId()));
			}

			BukkitScheduler Scheduler = Bukkit.getScheduler();
			Scheduler.scheduleSyncRepeatingTask(this, new Runnable() {
				@Override
				public void run() {
					for (RPGEntity entity : RPGEntity.players.values()) {
						if (entity.state.requestDuration > 0) {
							entity.state.requestDuration = entity.state.requestDuration - 1;
						}
						if (entity.state.teleportCooldown > 0) {
							entity.state.teleportCooldown = entity.state.teleportCooldown - 1;
							if (entity.state.teleportCooldown == 0) {
							}
						}
					}
				}
			}, 0L, 20L);
		}
		this.cfgHandler = new ConfigHandler(this);
		this.chatConfig = new Config("ChatPingMechanics");
		this.sprintConfig = new Config("SprintMechanics");
		this.randomConfig = new Config("RandomTPMechanics");
		this.expConfig = new Config("AeonEXP");
		this.trailConfig = new Config("Trails");
		this.whitelistConfig = new Config("WhitelistMechanics");
		this.entitygriefConfig = new Config("NoEntityGrief");
		this.tradeConfig = new Config("TradeMechanics");
		getWorldGuard();

		if (sprintConfig.file == null || sprintConfig.fileConfig == null) {
			cfgHandler.saveDefaultCustomConfig(sprintConfig);
		}
		cfgHandler.getCustomConfig(sprintConfig).options().copyDefaults(true);
		cfgHandler.saveDefaultCustomConfig(sprintConfig);

		if (chatConfig.file == null || chatConfig.fileConfig == null) {
			cfgHandler.saveDefaultCustomConfig(chatConfig);
		}
		cfgHandler.getCustomConfig(chatConfig).options().copyDefaults(true);
		cfgHandler.saveDefaultCustomConfig(chatConfig);

		if (randomConfig.file == null || randomConfig.fileConfig == null) {
			cfgHandler.saveDefaultCustomConfig(randomConfig);
		}
		cfgHandler.getCustomConfig(randomConfig).options().copyDefaults(true);
		cfgHandler.saveDefaultCustomConfig(randomConfig);

		if (expConfig.file == null || expConfig.fileConfig == null) {
			cfgHandler.saveDefaultCustomConfig(expConfig);
		}
		cfgHandler.getCustomConfig(expConfig).options().copyDefaults(true);
		cfgHandler.saveDefaultCustomConfig(expConfig);

		if (trailConfig.file == null || trailConfig.fileConfig == null) {
			cfgHandler.saveDefaultCustomConfig(trailConfig);
		}
		cfgHandler.getCustomConfig(trailConfig).options().copyDefaults(true);
		cfgHandler.saveDefaultCustomConfig(trailConfig);

		if (whitelistConfig.file == null || whitelistConfig.fileConfig == null) {
			cfgHandler.saveDefaultCustomConfig(whitelistConfig);
		}
		cfgHandler.getCustomConfig(whitelistConfig).options().copyDefaults(true);
		cfgHandler.saveDefaultCustomConfig(whitelistConfig);

		if (entitygriefConfig.file == null || entitygriefConfig.fileConfig == null) {
			cfgHandler.saveDefaultCustomConfig(entitygriefConfig);
		}
		cfgHandler.getCustomConfig(entitygriefConfig).options().copyDefaults(true);
		cfgHandler.saveDefaultCustomConfig(entitygriefConfig);

		if (tradeConfig.file == null || tradeConfig.fileConfig == null) {
			cfgHandler.saveDefaultCustomConfig(tradeConfig);
		}
		cfgHandler.getCustomConfig(tradeConfig).options().copyDefaults(true);
		cfgHandler.saveDefaultCustomConfig(tradeConfig);

		this.hpbar = new Runner();
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, this.hpbar, 5L, 5L);
		this.getServer().getPluginManager().registerEvents(this, this);
		this.chatalert = new ChatAlert(this);
		this.reboot = new Reboot(this);
		this.playerjoinmech = new PlayerJoinMechanics(this);
		this.randomtp = new RandomTPMechanics(this);
		this.staffmech = new StaffMechanics(this);
		this.bankrep = new Bank(this);
		this.sync = new Sync(this);
		this.trail = new ToggleTrails(this);
		this.suic = new Suicide(this);
		this.rollc = new Roll(this);
		this.respawn = new Respawn(this);
		this.magic = new Magic(this);
		this.bookmech = new BookMechanics(this);
		this.aeonexp = new AeonEXP(this);
		this.bookclick = new BookClicks(this);
		this.expcommands = new AeonEXPCommands(this);
		this.trailclickevent = new TrailClickEvent(this);
		this.togglebook = new InventoryArray(this);
		this.block = new Blocker(this);
		this.bowpush = new BowPush(this);
		this.mining = new Mining(this);
		this.untradeable = new Untradeable(this);
		this.fishing = new Fishing(this);
		this.orbs = new Orbs(this);
		this.enchanting = new Enchanting(this);
		this.listeners = new Listeners(this);
		this.mobs = new Mobs(this);
		this.mobdrops = new MobDrops(this);
		this.npcs = new NPC(this);
		this.whitelistmsg = new WhitelistMessage(this);
		this.noentitygrief = new NoEntityGrief(this);
		// this.tradeitemstack = new TradeItemStack(this);
		// this.tradeinventory = new TradeInventory(this);
		this.quicktrade = new QuickTrade(this);

		this.randomtp.getBlocksBlackList();
		this.randomtp.getMaxTryCount();
		this.randomtp.getRadius();
		this.randomtp.getPortal();

		Bukkit.getScheduler().cancelTask(Magic.SCHEDULE_ID);

		if (cfgHandler.getCustomConfig(entitygriefConfig).options().header() == null) {
			cfgHandler.getCustomConfig(entitygriefConfig).options().copyHeader();
			cfgHandler.getCustomConfig(entitygriefConfig).options().copyDefaults(true);
			cfgHandler.saveCustomConfig(entitygriefConfig);
		}
		this.noentitygrief.EWs[0] = cfgHandler.getCustomConfig(entitygriefConfig).getStringList("Endermen");
		this.noentitygrief.EWs[1] = cfgHandler.getCustomConfig(entitygriefConfig).getStringList("Sheep");
		this.noentitygrief.EWs[2] = cfgHandler.getCustomConfig(entitygriefConfig).getStringList("MobExplosion");
		this.noentitygrief.EWs[3] = cfgHandler.getCustomConfig(entitygriefConfig).getStringList("TNT");
		this.noentitygrief.EWs[4] = cfgHandler.getCustomConfig(entitygriefConfig).getStringList("Snowman");
		this.noentitygrief.EWs[5] = cfgHandler.getCustomConfig(entitygriefConfig).getStringList("Silverfish");

		// this.getServer().getPluginManager().registerEvents(new Runner(),
		// this);
		this.getServer().getPluginManager().registerEvents(this.playerjoinmech, this);
		this.getServer().getPluginManager().registerEvents(this.untradeable, this);
		this.getServer().getPluginManager().registerEvents(this.quicktrade, this);
		// this.getServer().getPluginManager().registerEvents(this.tradeinventory,
		// this);
		this.getServer().getPluginManager().registerEvents(this.noentitygrief, this);
		this.getServer().getPluginManager().registerEvents(this.fishing, this);
		this.getServer().getPluginManager().registerEvents(this.orbs, this);
		this.getServer().getPluginManager().registerEvents(this.enchanting, this);
		this.getServer().getPluginManager().registerEvents(this.listeners, this);
		this.getServer().getPluginManager().registerEvents(this.mobs, this);
		this.getServer().getPluginManager().registerEvents(this.mobdrops, this);
		this.getServer().getPluginManager().registerEvents(this.npcs, this);
		this.getServer().getPluginManager().registerEvents(new InteractionListener(), this);
		this.getServer().getPluginManager().registerEvents(new WhitelistPlayerListener(), this);
		this.getServer().getPluginManager().registerEvents(new HealPotions(), this);
		this.getServer().getPluginManager().registerEvents(this.bowpush, this);
		this.getServer().getPluginManager().registerEvents(this.mining, this);
		this.getServer().getPluginManager().registerEvents(this.respawn, this);
		this.getServer().getPluginManager().registerEvents(this.staffmech, this);
		this.getServer().getPluginManager().registerEvents(this.randomtp, this);
		this.getServer().getPluginManager().registerEvents(this.chatalert, this);
		this.getServer().getPluginManager().registerEvents(this.block, this);
		this.getServer().getPluginManager().registerEvents(new ChatListener(this.chatalert, this), this);
		// this.getServer().getPluginManager().registerEvents(new
		// SprintMechanics(this), this);
		this.getServer().getPluginManager().registerEvents(this.magic, this);
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new BookMechanics(this), 0L, 40L);
		this.getServer().getPluginManager().registerEvents(new BookEvents(this), this);
		this.getServer().getPluginManager().registerEvents(new BookJoin(), this);
		this.getServer().getPluginManager().registerEvents(new BookDrop(), this);
		this.getServer().getPluginManager().registerEvents(new BookClicks(this), this);
		this.getServer().getPluginManager().registerEvents(new AeonEXP(this), this);
		this.getServer().getPluginManager().registerEvents(new TPListener(this), this);
		this.getServer().getPluginManager().registerEvents(this.trailclickevent, this);

		if (!setupEconomy()) {
			getLogger().severe(String.format("[%s] - Disabled due to no Vault dependency found!", new Object[] { getDescription().getName() }));
			getServer().getPluginManager().disablePlugin(this);
			return;
		}
		if (!setupPermissions()) {
			getLogger().severe(String.format("[%s] - Disabled due to no Vault dependency found!", new Object[] { getDescription().getName() }));
			getServer().getPluginManager().disablePlugin(this);
			return;
		}
		if (!setupChat()) {
			getLogger().severe(String.format("[%s] - Disabled due to no Vault dependency found!", new Object[] { getDescription().getName() }));
			getServer().getPluginManager().disablePlugin(this);
			return;
		}

		String PluginFolder = getDataFolder().getAbsolutePath();

		new File(PluginFolder).mkdirs();
		this.playersStorage = new PlayerUtil(new File(PluginFolder + File.separator + "ToggledPlayers.txt"));
		this.playersStorage.load();

		quicktrade.f = new File(getDataFolder() + File.separator + "messages.yml");
		if (!quicktrade.f.exists()) {
			try {
				quicktrade.f.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		quicktrade.addMessages();

		FileConfiguration config = YamlConfiguration.loadConfiguration(quicktrade.f);
		for (String message : config.getConfigurationSection("").getKeys(false)) {
			QuickTrade.messageData.put(message, config.getString(message));
		}

		this.getCommand("cpm").setExecutor(this.chatalert);
		this.getCommand("tpr").setExecutor(this.randomtp);
		this.getCommand("reboot").setExecutor(this.reboot);
		this.getCommand("sync").setExecutor(this.sync);
		this.getCommand("bank").setExecutor(this.bankrep);
		this.getCommand("toggletrail").setExecutor(this.trail);
		this.getCommand("suicide").setExecutor(this.suic);
		this.getCommand("roll").setExecutor(this.rollc);
		this.getCommand("aeonexp").setExecutor(this.expcommands);
		this.getCommand("toggleblock").setExecutor(this.togglebook);
		this.getCommand("whitelistmessage").setExecutor(this.whitelistmsg);
		// this.getCommand("trade").setExecutor(this.tradecommand);

		cfgHandler.reloadCustomConfig(whitelistConfig);

		this.partyManager = new PartyManager(this);
		PartySystem p = new PartySystem(partyManager);
		this.getServer().getPluginCommand("party").setExecutor(p);
		getServer().getPluginManager().registerEvents(p, this);
		foodsubtractfortick = cfgHandler.getCustomConfig(this.sprintConfig).getInt("Food-subtracted-for-tick");
		foodsubtractickvalue = cfgHandler.getCustomConfig(this.sprintConfig).getInt("Food-subtracted-tick-value");
		foodaddfortick = cfgHandler.getCustomConfig(this.sprintConfig).getInt("Food-added-for-tick");
		foodaddtickvalue = cfgHandler.getCustomConfig(this.sprintConfig).getInt("Food-add-tick-value");
		foodmaxtickvalue = cfgHandler.getCustomConfig(this.sprintConfig).getInt("Food-max-tick-value");
		this.RMMain = ((com.aeonblox.RaceMechanics.Main) Bukkit.getPluginManager().getPlugin("RaceMechanics"));

		ItemStack i1 = new ItemStack(Material.RED_ROSE, 1);
		ItemMeta id1 = i1.getItemMeta();
		id1.setDisplayName(ChatColor.RED + "Hearts");
		i1.setItemMeta(id1);

		ItemStack i2 = new ItemStack(Material.PAPER, 1);
		ItemMeta id2 = i2.getItemMeta();
		id2.setDisplayName(ChatColor.WHITE + "None");
		i2.setItemMeta(id2);

		ItemStack i3 = new ItemStack(Material.BLAZE_POWDER, 1);
		ItemMeta id3 = i3.getItemMeta();
		id3.setDisplayName(ChatColor.GOLD + "Angry");
		i3.setItemMeta(id3);

		ItemStack i4 = new ItemStack(Material.BOOK, 1);
		ItemMeta id4 = i4.getItemMeta();
		id4.setDisplayName(ChatColor.DARK_AQUA + "Magic");
		i4.setItemMeta(id4);

		ItemStack i5 = new ItemStack(Material.REDSTONE, 1);
		ItemMeta id5 = i5.getItemMeta();
		id5.setDisplayName(ChatColor.YELLOW + "Colors");
		i5.setItemMeta(id5);

		ItemStack i6 = new ItemStack(Material.WEB, 1);
		ItemMeta id6 = i6.getItemMeta();
		id6.setDisplayName(ChatColor.WHITE + "Cloud");
		i6.setItemMeta(id6);

		ItemStack i7 = new ItemStack(Material.SKULL_ITEM, 1);
		ItemMeta id7 = i7.getItemMeta();
		id7.setDisplayName(ChatColor.AQUA + "Witch Magic");
		i7.setItemMeta(id7);

		ItemStack i8 = new ItemStack(Material.ENDER_PEARL, 1);
		ItemMeta id8 = i8.getItemMeta();
		id8.setDisplayName(ChatColor.GRAY + "Ender");
		i8.setItemMeta(id8);

		ItemStack i9 = new ItemStack(Material.EMERALD, 1);
		ItemMeta id9 = i9.getItemMeta();
		id9.setDisplayName(ChatColor.GREEN + "Green");
		i9.setItemMeta(id9);

		ItemStack i10 = new ItemStack(Material.FLINT, 1);
		ItemMeta id10 = i10.getItemMeta();
		id10.setDisplayName(ChatColor.WHITE + "Spark");
		i10.setItemMeta(id10);

		ItemStack i11 = new ItemStack(Material.FLINT_AND_STEEL, 1);
		ItemMeta id11 = i11.getItemMeta();
		id11.setDisplayName(ChatColor.GOLD + "Flame");
		i11.setItemMeta(id11);

		ItemStack i12 = new ItemStack(Material.SPIDER_EYE, 1);
		ItemMeta id12 = i12.getItemMeta();
		id12.setDisplayName(ChatColor.WHITE + "White Magic");
		i12.setItemMeta(id12);

		ItemStack i13 = new ItemStack(Material.RECORD_3, 1);
		ItemMeta id13 = i13.getItemMeta();
		id13.setDisplayName(ChatColor.RED + "Music");
		i13.setItemMeta(id13);

		ItemStack i14 = new ItemStack(Material.SNOW_BALL, 1);
		ItemMeta id14 = i14.getItemMeta();
		id14.setDisplayName(ChatColor.WHITE + "Snow");
		i14.setItemMeta(id14);

		ItemStack i15 = new ItemStack(Material.WATER_BUCKET, 1);
		ItemMeta id15 = i15.getItemMeta();
		id15.setDisplayName(ChatColor.DARK_AQUA + "Water Line");
		i15.setItemMeta(id15);

		ItemStack i16 = new ItemStack(Material.LAVA_BUCKET, 1);
		ItemMeta id16 = i16.getItemMeta();
		id16.setDisplayName(ChatColor.RED + "Lava Line");
		i16.setItemMeta(id16);

		ItemStack i17 = new ItemStack(Material.IRON_SWORD, 1);
		ItemMeta id17 = i17.getItemMeta();
		id17.setDisplayName(ChatColor.GRAY + "Crit");
		i17.setItemMeta(id17);

		ItemStack i18 = new ItemStack(Material.OBSIDIAN, 1);
		ItemMeta id18 = i18.getItemMeta();
		id18.setDisplayName(ChatColor.GRAY + "Smoke");
		i18.setItemMeta(id18);

		Itrail.setItem(1, i1);
		Itrail.setItem(0, i2);
		Itrail.setItem(2, i3);
		Itrail.setItem(3, i4);
		Itrail.setItem(4, i5);
		Itrail.setItem(5, i6);
		Itrail.setItem(6, i7);
		Itrail.setItem(7, i8);
		Itrail.setItem(8, i9);
		Itrail.setItem(9, i10);
		Itrail.setItem(10, i11);
		Itrail.setItem(11, i12);
		Itrail.setItem(12, i13);
		Itrail.setItem(13, i14);
		Itrail.setItem(14, i15);
		Itrail.setItem(15, i16);
		Itrail.setItem(16, i17);
		Itrail.setItem(17, i18);

		BukkitRunnable b = new BukkitRunnable() {
			public void run() {
				for (Player b1 : Bukkit.getOnlinePlayers()) {
					if (plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Players." + b1.toString()) == 1) {
						Aeonblox.ParticleEffect.sendParticleToAll(Aeonblox.ParticleEffect.HEART, b1.getLocation().add(0.0D, 0.5D, 0.0D), 0.0F, 0.0F, 0.0F, 0.0F, plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Heart.Amount"));
					}
					if (plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Players." + b1.toString()) == 2) {
						Aeonblox.ParticleEffect.sendParticleToAll(Aeonblox.ParticleEffect.ANGRY_VILLAGER, b1.getLocation().add(0.0D, 2.0D, 0.0D), 0.0F, 0.0F, 0.0F, 0.0F, plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Angry.Amount"));
					}
					if (plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Players." + b1.toString()) == 3) {
						Aeonblox.ParticleEffect.sendParticleToAll(Aeonblox.ParticleEffect.MAGIC_CRIT, b1.getLocation().add(1.0D, 1.0D, 0.0D), 0.0F, 0.0F, 0.0F, 0.0F, plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Magic.Amount"));
						Aeonblox.ParticleEffect.sendParticleToAll(Aeonblox.ParticleEffect.MAGIC_CRIT, b1.getLocation().add(-1.0D, 1.0D, 0.0D), 0.0F, 0.0F, 0.0F, 0.0F, plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Magic.Amount"));
						Aeonblox.ParticleEffect.sendParticleToAll(Aeonblox.ParticleEffect.MAGIC_CRIT, b1.getLocation().add(0.0D, 1.0D, 1.0D), 0.0F, 0.0F, 0.0F, 0.0F, plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Magic.Amount"));

						Aeonblox.ParticleEffect.sendParticleToAll(Aeonblox.ParticleEffect.MAGIC_CRIT, b1.getLocation().add(0.0D, 1.0D, -1.0D), 0.0F, 0.0F, 0.0F, 0.0F, plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Magic.Amount"));
					}
					if (plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Players." + b1.toString()) == 4) {
						Aeonblox.ParticleEffect.sendParticleToAll(Aeonblox.ParticleEffect.RED_DUST, b1.getLocation().add(0.0D, 0.5D, 0.0D), 0.0F, 0.0F, 0.0F, 3.0F, plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Fun.Amount"));
					}
					if (plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Players." + b1.toString()) == 5) {
						Aeonblox.ParticleEffect.sendParticleToAll(Aeonblox.ParticleEffect.CLOUD, b1.getLocation().add(0.0D, 1.0D, 0.0D), 0.0F, 0.0F, 0.0F, (float) plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getDouble("Cloud.Speed"), plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Cloud.Amount"));
					}
					if (plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Players." + b1.toString()) == 6) {
						Aeonblox.ParticleEffect.sendParticleToAll(Aeonblox.ParticleEffect.WITCH_MAGIC, b1.getLocation().add(0.0D, 0.5D, 0.0D), 0.0F, 0.0F, 0.0F, (float) plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getDouble("Witch.Speed"), plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Witch.Amount"));
					}
					if (plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Players." + b1.toString()) == 7) {
						Aeonblox.ParticleEffect.sendParticleToAll(Aeonblox.ParticleEffect.PORTAL, b1.getLocation().add(0.0D, 0.5D, 0.0D), 0.0F, 0.0F, 0.0F, (float) plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getDouble("Ender.Speed"), plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Ender.Amount"));
					}
					if (plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Players." + b1.toString()) == 8) {
						Aeonblox.ParticleEffect.sendParticleToAll(Aeonblox.ParticleEffect.HAPPY_VILLAGER, b1.getLocation().add(0.0D, 0.5D, 0.0D), 0.0F, 0.0F, 0.0F, 0.1F, plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Green.Amount"));
					}
					if (plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Players." + b1.toString()) == 9) {
						Aeonblox.ParticleEffect.sendParticleToAll(Aeonblox.ParticleEffect.FIREWORKS_SPARK, b1.getLocation().add(0.0D, 0.5D, 0.0D), 0.0F, 0.0F, 0.0F, (float) plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getDouble("Spark.Speed"), plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Spark.Amount"));
					}
					if (plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Players." + b1.toString()) == 10) {
						Aeonblox.ParticleEffect.sendParticleToAll(Aeonblox.ParticleEffect.FLAME, b1.getLocation().add(0.0D, 0.5D, 0.0D), 0.0F, 0.0F, 0.0F, (float) plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getDouble("Flame.Speed"), plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Flame.Amount"));
					}
					if (plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Players." + b1.toString()) == 11) {
						Aeonblox.ParticleEffect.sendParticleToAll(Aeonblox.ParticleEffect.INSTANT_SPELL, b1.getLocation().add(0.0D, 0.5D, 0.0D), 0.0F, 0.0F, 0.0F, (float) plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getDouble("WhiteMagic.Speed"), plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("WhiteMagic.Amount"));
					}
					if (plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Players." + b1.toString()) == 12) {
						Aeonblox.ParticleEffect.sendParticleToAll(Aeonblox.ParticleEffect.NOTE, b1.getLocation().add(0.0D, 0.5D, 0.0D), 0.0F, 0.0F, 0.0F, 5.0F, plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Note.Amount"));
					}
					if (plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Players." + b1.toString()) == 13) {
						Aeonblox.ParticleEffect.sendParticleToAll(Aeonblox.ParticleEffect.SNOW_SHOVEL, b1.getLocation().add(0.0D, 0.5D, 0.0D), 0.0F, 0.0F, 0.0F, 0.0F, plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Snow.Amount"));
					}
					if (plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Players." + b1.toString()) == 14) {
						Aeonblox.ParticleEffect.sendParticleToAll(Aeonblox.ParticleEffect.DRIP_WATER, b1.getLocation().add(0.0D, 0.5D, 0.0D), 0.0F, 0.0F, 0.0F, 1.0F, plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Water.Amount"));
					}
					if (plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Players." + b1.toString()) == 15) {
						Aeonblox.ParticleEffect.sendParticleToAll(Aeonblox.ParticleEffect.DRIP_LAVA, b1.getLocation().add(0.0D, 0.5D, 0.0D), 0.0F, 0.0F, 0.0F, 1.0F, plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Lava.Amount"));
					}
					if (plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Players." + b1.toString()) == 16) {
						Aeonblox.ParticleEffect.sendParticleToAll(Aeonblox.ParticleEffect.CRIT, b1.getLocation().add(0.0D, 0.5D, 0.0D), 0.0F, 0.0F, 0.0F, (float) plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getDouble("Crit.Speed"), plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Crit.Amount"));
					}
					if (plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Players." + b1.toString()) == 17) {
						Aeonblox.ParticleEffect.sendParticleToAll(Aeonblox.ParticleEffect.LARGE_SMOKE, b1.getLocation().add(0.0D, 0.5D, 0.0D), 0.0F, 0.0F, 0.0F, (float) plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getDouble("Smoke.Speed"), plugin.cfgHandler.getCustomConfig(plugin.trailConfig).getInt("Smoke.Amount"));
					}
				}
			}
		};
		b.runTaskTimer(this, 1L, 2L);

	}

	/**
	 * 
	 * Public Methods To be used by other classes, or in on enable.
	 * 
	 */

	/**
	 * Get values of HP Regen
	 * 
	 * @param item
	 *            - The item to get the HPGen from.
	 * @param value
	 *            - Value to search for in lore.
	 * @return - The hpgen
	 * 
	 */
	public static int getHpgenFromLore(ItemStack item, String value) {

		int returnVal = 0;
		ItemMeta meta = item.getItemMeta();
		try {

			List<String> lore = meta.getLore();
			if ((lore != null) && (((String) lore.get(2)).contains(value))) {

				String vals = ((String) lore.get(2)).split(": +")[1];
				vals = ChatColor.stripColor(vals);
				vals = vals.replace(" HP/s", "").trim().toString();
				returnVal = Integer.parseInt(vals.trim());
			}
		} catch (Exception localException) {
		}
		return returnVal;
	}

	/**
	 * 
	 * Get the instance of the Vault plugin.
	 * 
	 */
	private boolean setupEconomy() {

		if (getServer().getPluginManager().getPlugin("Vault") == null) {

			return false;
		}

		RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
		if (rsp == null) {

			return false;
		}
		this.econ = ((Economy) rsp.getProvider());
		return this.econ != null;
	}

	private boolean setupPermissions() {
		RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class);
		if (permissionProvider != null) {
			permission = permissionProvider.getProvider();
		}
		return (permission != null);
	}

	private boolean setupChat() {
		RegisteredServiceProvider<Chat> chatProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.chat.Chat.class);
		if (chatProvider != null) {
			chat = chatProvider.getProvider();
		}

		return (chat != null);
	}

	/**
	 * 
	 * Get the instance of the WorldGuard plugin
	 * @return an instance of the WorldGuardPlugin.
	 * 
	 */

	public WorldGuardPlugin getWorldGuard() {
		Plugin WorldGuard = Bukkit.getServer().getPluginManager().getPlugin("WorldGuard");
		if ((WorldGuard == null) || (!(WorldGuard instanceof WorldGuardPlugin))) {
			return null;
		}
		this.util_WorldGuard = new Util_WorldGuard(plugin);
		return (WorldGuardPlugin) WorldGuard;
	}

	public WorldGuardPlugin wg = (WorldGuardPlugin) Bukkit.getServer().getPluginManager().getPlugin("WorldGuard");

	/**
	 * 
	 * 
	 * AeonEXP and BookMechanics Methods
	 * @return the value of the custom damage.
	 * 
	 */
	public Double getCustomDamage() {
		return Double.valueOf(this.cfgHandler.getCustomConfig(plugin.expConfig).getDouble("extra damage"));
	}

	/**
	 * Method, to remove unnesscecary enchantments from non-donators.
	 */
	public void removeEnchantments(ItemStack item) {
		for (Enchantment e : item.getEnchantments().keySet()) {
			ItemMeta i = item.getItemMeta(); // event.getItemOnCursor
			i.removeEnchant(e);
		}
	}

	/**
	 * Checks if the plugin is in Debug Mode. If the plugin is in debug mode,
	 * messages that would normally not be sent to players will be. This is
	 * mainly for testing purposes.
	 * 
	 * @return True or False.
	 */
	public static boolean debugMode() {
		return debugMode;
	}

	/**
	 * Debug message chat formatting
	 * 
	 * @param Message
	 *            The message being sent to the player.
	 * @return Returns the message. If debugMode is not enabled, returns null;
	 */
	public static String debugMessage(String Message) {
		if (Aeonblox.debugMode()) {
			return ChatColor.RED + "" + ChatColor.BOLD + "[Debug]" + ChatColor.DARK_GRAY + Message;
		}
		return null;
	}

	/**
	 * Automated Server Reboot
	 * 
	 */
	public void reboot() {
		Bukkit.getServer().broadcastMessage(ChatColor.RED + "" + ChatColor.BOLD + ">>" + ChatColor.RED + " The server will be " + ChatColor.UNDERLINE + "REBOOTING" + ChatColor.RED + " in " + ChatColor.BOLD + "60s...");
		new BukkitRunnable() {
			public void run() {
				Bukkit.getServer().broadcastMessage(ChatColor.RED + "" + ChatColor.BOLD + ">>" + ChatColor.RED + " The server will be " + ChatColor.UNDERLINE + "REBOOTING" + ChatColor.RED + " in " + ChatColor.BOLD + "30s...");
			}
		}.runTaskLater(this, 600L);
		new BukkitRunnable() {
			public void run() {
				Bukkit.getServer().broadcastMessage(ChatColor.RED + "" + ChatColor.BOLD + ">>" + ChatColor.RED + " The server will be " + ChatColor.UNDERLINE + "REBOOTING" + ChatColor.RED + " in " + ChatColor.BOLD + "10s...");
			}
		}.runTaskLater(this, 1000L);
		new BukkitRunnable() {
			public void run() {
				Bukkit.getServer().broadcastMessage(ChatColor.RED + "" + ChatColor.BOLD + ">>" + ChatColor.RED + " The server will be " + ChatColor.UNDERLINE + "REBOOTING" + ChatColor.RED + " in " + ChatColor.BOLD + "5s...");
			}
		}.runTaskLater(this, 1100L);
		new BukkitRunnable() {
			public void run() {
				Bukkit.getServer().broadcastMessage(ChatColor.RED + "" + ChatColor.BOLD + ">>" + ChatColor.RED + " The server will be " + ChatColor.UNDERLINE + "REBOOTING" + ChatColor.RED + " in " + ChatColor.BOLD + "4s...");
			}
		}.runTaskLater(this, 1120L);
		new BukkitRunnable() {
			public void run() {
				Bukkit.getServer().broadcastMessage(ChatColor.RED + "" + ChatColor.BOLD + ">>" + ChatColor.RED + " The server will be " + ChatColor.UNDERLINE + "REBOOTING" + ChatColor.RED + " in " + ChatColor.BOLD + "3s...");
			}
		}.runTaskLater(this, 1140L);
		new BukkitRunnable() {
			public void run() {
				Bukkit.getServer().broadcastMessage(ChatColor.RED + "" + ChatColor.BOLD + ">>" + ChatColor.RED + " The server will be " + ChatColor.UNDERLINE + "REBOOTING" + ChatColor.RED + " in " + ChatColor.BOLD + "2s...");
			}
		}.runTaskLater(this, 1160L);
		new BukkitRunnable() {
			public void run() {
				Bukkit.getServer().broadcastMessage(ChatColor.RED + "" + ChatColor.BOLD + ">>" + ChatColor.RED + " The server will be " + ChatColor.UNDERLINE + "REBOOTING" + ChatColor.RED + " in " + ChatColor.BOLD + "1s...");
			}
		}.runTaskLater(this, 1180L);
		new BukkitRunnable() {
			public void run() {
				Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), "stop");
			}
		}.runTaskLater(this, 1200L);
	}

	/** **/
	/**
	 * Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, new
	 * Runnable() {
	 * 
	 * @SuppressWarnings("deprecation") public void run() { Damageable pl;
	 *                                  ScoreboardManager manager =
	 *                                  Bukkit.getScoreboardManager();
	 *                                  Scoreboard board =
	 *                                  manager.getNewScoreboard(); Objective o
	 *                                  = board.registerNewObjective("test",
	 *                                  "dummy");
	 *                                  o.setDisplaySlot(DisplaySlot.SIDEBAR);
	 *                                  o.setDisplayName(ChatColor.LIGHT_PURPLE
	 *                                  + "" + ChatColor.BOLD + "Party"); for
	 *                                  (Player player :
	 *                                  Bukkit.getOnlinePlayers()) { if
	 *                                  (Party.getParty(player) != null) { Score
	 *                                  score; for(Player p :
	 *                                  Party.getParty(player).getPlayers()) {
	 *                                  score = o.getScore(getServer().
	 *                                  getOfflinePlayer(p.getName())); pl =
	 *                                  (Damageable) p; score.setScore((int)
	 *                                  pl.getHealth()); }
	 *                                  player.setScoreboard(board); } } } },
	 *                                  20, 20);
	 */
	public static enum ParticleEffect {
		HUGE_EXPLOSION("hugeexplosion"), LARGE_EXPLODE("largeexplode"), FIREWORKS_SPARK("fireworksSpark"), BUBBLE("bubble"), SUSPEND("suspend"), DEPTH_SUSPEND("depthSuspend"), TOWN_AURA("townaura"), CRIT("crit"), MAGIC_CRIT("magicCrit"), MOB_SPELL("mobSpell"), MOB_SPELL_AMBIENT("mobSpellAmbient"), SPELL("spell"), INSTANT_SPELL("instantSpell"), WITCH_MAGIC("witchMagic"), NOTE("note"), PORTAL("portal"), ENCHANTMENT_TABLE("enchantmenttable"), EXPLODE("explode"), FLAME("flame"), LAVA("lava"), FOOTSTEP("footstep"), SPLASH("splash"), LARGE_SMOKE("largesmoke"), CLOUD("cloud"), RED_DUST("reddust"), SNOWBALL_POOF("snowballpoof"), DRIP_WATER("dripWater"), DRIP_LAVA("dripLava"), SNOW_SHOVEL("snowshovel"), SLIME("slime"), HEART("heart"), ANGRY_VILLAGER("angryVillager"), HAPPY_VILLAGER("happyVillager"), ICONCRACK("iconcrack_"), TILECRACK("tilecrack_"), BLOCKDUST("blockdust_");

		private String particleName;
		private static Random random = new Random();

		private ParticleEffect(String particleName) {
			this.particleName = particleName;
		}

		public String getName() {
			return this.particleName;
		}

		private static Object getEntityPlayer(Player p) throws Exception {
			return p.getClass().getMethod("getHandle", new Class[0]).invoke(p, new Object[0]);
		}

		private static String getPackageName() {
			return "net.minecraft.server." + Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3];
		}

		private static Object getParticlePacket() throws Exception {
			Class<?> packet = Class.forName(getPackageName() + ".PacketPlayOutWorldParticles");
			return packet.getConstructors()[0].newInstance(new Object[0]);
		}

		private static Class<?> getPacketClass() throws Exception {
			return Class.forName(getPackageName() + ".Packet");
		}

		public static Object getPacket(String particleName, Location location, float offsetX, float offsetY, float offsetZ, float speed, int amount) {
			try {
				Object packet = getParticlePacket();
				setField(packet, "a", particleName);
				setField(packet, "b", Float.valueOf((float) location.getX()));
				setField(packet, "c", Float.valueOf((float) location.getY()));
				setField(packet, "d", Float.valueOf((float) location.getZ()));
				setField(packet, "e", Float.valueOf(offsetX));
				setField(packet, "f", Float.valueOf(offsetY));
				setField(packet, "g", Float.valueOf(offsetZ));
				setField(packet, "h", Float.valueOf(speed));
				setField(packet, "i", Integer.valueOf(amount));
				return packet;
			} catch (Exception e) {
			}
			return null;
		}

		public static void sendParticle(Player player, String particleName, Location location, float offsetX, float offsetY, float offsetZ, float speed, int amount) {
			try {
				Object packet = getPacket(particleName, location, offsetX, offsetY, offsetZ, speed, amount);

				Object eplayer = getEntityPlayer(player);
				Field playerConnectionField = eplayer.getClass().getField("playerConnection");
				Object playerConnection = playerConnectionField.get(eplayer);
				playerConnection.getClass().getMethod("sendPacket", new Class[0]).invoke(playerConnection, new Object[] { packet });
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		public static void sendParticle(Player player, ParticleEffect particle, Location location, float offsetX, float offsetY, float offsetZ, float speed, int amount) {
			sendParticle(player, particle.particleName, location, offsetX, offsetY, offsetZ, speed, amount);
		}

		public static void sendIconCrack(Player player, String iconName, Location location, float offsetX, float offsetY, float offsetZ, float speed, int amount) {
			sendParticle(player, "iconcrack_" + iconName, location, offsetX, offsetY, offsetZ, speed, amount);
		}

		public static void sendTileCrack(Player player, String tileName, Location location, float offsetX, float offsetY, float offsetZ, float speed, int amount) {
			sendParticle(player, "tilecrack_" + tileName, location, offsetX, offsetY, offsetZ, speed, amount);
		}

		public static void sendParticleToAll(ParticleEffect particle, Location location, float offsetX, float offsetY, float offsetZ, float speed, int amount) {
			sendParticleToAll(particle.getName(), location, offsetX, offsetY, offsetZ, speed, amount);
		}

		public static void sendParticleNearby(String particleName, Location location, float offsetX, float offsetY, float offsetZ, float speed, int amount) {
			double f = (random.nextDouble() - 0.5D) * 2.0D;
			double d3 = f > 1.0D ? 16.0D * f : 16.0D;
			try {
				Object ws = location.getWorld().getClass().getMethod("getHandle", new Class[0]).invoke(location.getWorld(), new Object[0]);
				Object mcs = ws.getClass().getMethod("getMinecraftServer", new Class[0]).invoke(ws, new Object[0]);
				Object plist = mcs.getClass().getMethod("getPlayerList", new Class[0]).invoke(mcs, new Object[0]);
				int dimension = ((Integer) getField(ws, "dimension")).intValue();

				Method m = plist.getClass().getMethod("sendPacketNearby", new Class[] { getPacketClass() });
				m.invoke(plist, new Object[] { Double.valueOf(location.getX()), Double.valueOf(location.getY()), Double.valueOf(location.getZ()), Double.valueOf(d3), Integer.valueOf(dimension), getPacket(particleName, location, offsetX, offsetY, offsetZ, speed, amount) });
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		public static void sendParticleToAll(String particleName, Location location, float offsetX, float offsetY, float offsetZ, float speed, int amount) {
			try {
				Object ws = location.getWorld().getClass().getMethod("getHandle", new Class[0]).invoke(location.getWorld(), new Object[0]);
				Object mcs = ws.getClass().getMethod("getMinecraftServer", new Class[0]).invoke(ws, new Object[0]);
				Object plist = mcs.getClass().getMethod("getPlayerList", new Class[0]).invoke(mcs, new Object[0]);

				Method m = plist.getClass().getMethod("sendAll", new Class[] { getPacketClass() });

				m.invoke(plist, new Object[] { getPacket(particleName, location, offsetX, offsetY, offsetZ, speed, amount) });
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		public static void sendIconCrackToAll(int id, Integer data, Location location, float offsetX, float offsetY, float offsetZ, float speed, int amount) {
			String s = "iconcrack_" + id + "_" + data;
			sendParticleToAll(s, location, offsetX, offsetY, offsetZ, speed, amount);
		}

		public static void sendTileCrackToAll(int id, Integer data, Location location, float offsetX, float offsetY, float offsetZ, float speed, int amount) {
			String s = "tilecrack_" + id + "_" + data;
			sendParticleToAll(s, location, offsetX, offsetY, offsetZ, speed, amount);
		}

		public static void sendBlockDustToAll(int id, Integer data, Location location, float offsetX, float offsetY, float offsetZ, float speed, int amount) {
			String s = "blockdust_" + id + "_" + data;
			sendParticleToAll(s, location, offsetX, offsetY, offsetZ, speed, amount);
		}

		public static ParticleEffect get(String name) {
			for (ParticleEffect e : values()) {
				if (e.getName().equalsIgnoreCase(name)) {
					return e;
				}
			}
			return null;
		}

		public static void setField(Object instance, String fieldName, Object value) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
			Field field = instance.getClass().getDeclaredField(fieldName);
			field.setAccessible(true);
			field.set(instance, value);
		}

		public static Object getField(Object instance, String fieldName) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
			Field field = instance.getClass().getDeclaredField(fieldName);
			field.setAccessible(true);
			return field.get(instance);
		}
	}
}
