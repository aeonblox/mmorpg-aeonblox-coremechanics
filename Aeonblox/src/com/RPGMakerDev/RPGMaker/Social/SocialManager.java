/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.RPGMakerDev.RPGMaker.Social;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.aeonblox.rpg.Aeonblox;

@SuppressWarnings("deprecation")
public class SocialManager implements Listener {

    public static Channel Global;
    public static List<String> chatLog = new ArrayList<String>();

    public static enum ChatFormat {

        ERROR,
        DEBUG,
        CHATCOLOR,
        ENEMYCOLOR,
        ALLYCOLOR
    }

    /**
     * Starts the SocialManager plugin. This plugin will cause other chat
     * plugins to not work.
     */
    public SocialManager() {
        Global = new Channel(1, "Global");
    }

    @EventHandler
    public void smChatEvent(PlayerChatEvent e) {
        e.setCancelled(true);
        
        if (SocialPlayer.getStoredSocialPlayer(e.getPlayer().getUniqueId()) == null) {
            SocialPlayer newPlayer = new SocialPlayer(e.getPlayer().getUniqueId());
            SocialPlayer.socialPlayers.put(e.getPlayer().getUniqueId(), newPlayer);
            SocialPlayer.storeSocialPlayer(e.getPlayer().getUniqueId());
            Global.joinChannel(newPlayer);
        }else if (SocialPlayer.getStoredSocialPlayer(e.getPlayer().getUniqueId()) != null) {
        	Global.sendMessage(SocialPlayer.getStoredSocialPlayer(e.getPlayer().getUniqueId()), e.getMessage());
            System.out.println("[" + Aeonblox.RMMain.utils.getPlayerRace(e.getPlayer().getUniqueId()) + "] " + (e.getPlayer().getName() + " : " + e.getMessage()));
        }
    }

    @EventHandler
    public void smJoinEvent(PlayerJoinEvent e) {
        e.getPlayer().sendMessage("");
        e.getPlayer().sendMessage(ChatColor.GRAY + "-----------------------------------------------------");
        e.getPlayer().sendMessage(ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "                  Welcome to Aeonblox!");
        e.getPlayer().sendMessage(ChatColor.BLUE + "                            www.aeonblox.com");
        e.getPlayer().sendMessage(ChatColor.GREEN + "             There are currently " + ChatColor.BOLD + Bukkit.getOnlinePlayers().length + " / " + Bukkit.getMaxPlayers() + " players" + ChatColor.GREEN + " online.");
        e.getPlayer().sendMessage("");
        e.getPlayer().sendMessage("");
        e.getPlayer().sendMessage(ChatColor.GRAY + "                         Patch Version 1.0.0");
        e.getPlayer().sendMessage(ChatColor.GRAY + "-----------------------------------------------------");
        e.getPlayer().sendMessage("");
        e.getPlayer().sendMessage(ChatColor.GRAY + "This server has a lot of " + ChatColor.AQUA + "" + ChatColor.BOLD + "unique" + ChatColor.GRAY + " features, so feel free to ask around for help, or use the built-in " + ChatColor.YELLOW + "" + ChatColor.ITALIC + "/guide" + ChatColor.GRAY + " command to learn about this server, a completely new experience which Minecraft has never seen!");
        e.getPlayer().setHealthScaled(true);
        e.getPlayer().setHealthScale(20);
        if (SocialPlayer.getStoredSocialPlayer(e.getPlayer().getUniqueId()) == null) {
            SocialPlayer newPlayer = new SocialPlayer(e.getPlayer().getUniqueId());
            SocialPlayer.socialPlayers.put(e.getPlayer().getUniqueId(), newPlayer);
            Global.joinChannel(newPlayer);
        }
    }
    @EventHandler
    public void smLeaveEvent(PlayerQuitEvent e){
    	if (SocialPlayer.getStoredSocialPlayer(e.getPlayer().getUniqueId()) != null){
    		SocialPlayer.socialPlayers.remove(e.getPlayer().getUniqueId());
    	}
    }
}
